/**
 * Created by arif on 10/16/17.
 */
/**
 * Created by arif on 8/10/17.
 */

/*global moment*/

angular.module('wealthQuotientApp')
    .controller('tasksSearchCtrl', ['$scope', '$timeout', '$state', '$stateParams', '$window', '$document', '$rootScope', 'searchService', 'registerService', 'loginService','tasksService','mySharedService','serachType','Store','$q', function ($scope, $timeout, $state, $stateParams, $window, $document, $rootScope, searchService, registerService, loginService,tasksService,mySharedService,serachType,Store,$q) {
        $scope.curPage = 0;
        $scope.pageSize = 5;
        $scope.tasksDataBySearch = [];

        //console.log('tasksSearchCtrl callingh 00000000000000000000');
        
        $scope.Store = Store;
        $scope.dateClickFlag = false;
        if($scope.Store.customerview === null && $scope.Store.assingeeview === null && $scope.Store.dateview === null){
            console.log('coming here======');
            $state.go('tasks');
        }
        //console.log('$scope.Store===================',JSON.stringify($scope.Store));
        $scope.taskdata = {};
        $scope.fromMyClientsCheck = false;
        $scope.hideDropDownAssingeeAfterSelect = false;
        $scope.hideDropDownAfterSelect = false;
        $scope.clientObj_myClients = {};




        $scope.chooseAssignee = function (assignee) {
           // console.log('assignee================',JSON.stringify(assignee));
           // console.log('index in cust.name',assignee.assigned_to);
             $scope.taskdata.assigneename = assignee.assigned_to;
            var queryForSearch = {
                query : assignee.assigned_to
            }
            $scope.queryForSearch = {
                query : assignee.assigned_to
            }

            tasksService.getCrmAllTasksByAssignee(queryForSearch).then(function (clientsResult) {
                //console.log('getAllCrmTasks================>',JSON.stringify(clientsResult.data.response));
                var crmAllTasksByAssignee = clientsResult.data.response;
                if(crmAllTasksByAssignee !== 0){
                    crmAllTasksByAssignee = crmAllTasksByAssignee.filter(function (ele) {
                        var tempArr = Object.keys(ele);
                        //console.log('the keys are ', tempArr);
                        tempArr.map(function (arrEle, i) {
                            // console.log('the arrEle are ', arrEle);
                            if (ele[arrEle] === null) {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                            if (ele[arrEle] === "Invalid date") {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                            if (ele[arrEle] === "") {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                        });
                        //console.log(ele);
                        return ele;
                    });
                }
                //console.log('crmAllTasksByAssignee-------66666666666666-----------afterrrrrrr', JSON.stringify(crmAllTasksByAssignee));

                function arrayFromObject(obj) {
                    var arr = [];
                    for (var i in obj) {
                        arr.push(obj[i]);
                    }
                    return arr;
                }

                function groupBy(list, fn) {
                    var groups = {};
                    for (var i = 0; i < list.length; i++) {
                        var group = JSON.stringify(fn(list[i]));
                        if (group in groups) {
                            groups[group].push(list[i]);
                        } else {
                            groups[group] = [list[i]];
                        }
                    }
                    return arrayFromObject(groups);
                }
                $scope.crmTaskAssingee = groupBy(crmAllTasksByAssignee, function(item) {
                    //console.log('8888888888888888888',JSON.stringify(item));

                    return [item.task_title, item.customer_name,item.isSubtask];
                });
                var assingeeSearch =[];
                for(var K = 0 ; K < $scope.crmTaskAssingee.length ; K++){

                    var task_title = '';
                    var customername = '';
                    var isSubtask = '';
                    for(var j = 0 ; j < $scope.crmTaskAssingee[K].length ; j++){
                        task_title = $scope.crmTaskAssingee[K][j].task_title;
                        customername = $scope.crmTaskAssingee[K][j].customer_name;
                        isSubtask = $scope.crmTaskAssingee[K][j].isSubtask;

                    }
                    //console.log('filter subtaks777777777777',JSON.stringify($scope.crmTaskAssingee))
                    assingeeSearch.push({task_title : task_title,customername:customername,isSubtask:isSubtask,data : $scope.crmTaskAssingee[K]});
                }
                //console.log('tasksDataBySearch 11111111111',JSON.stringify($scope.tasksDataBySearch));
                $scope.tasksDataBySearch = _.each(assingeeSearch,function (data1) {
                    var filterdata2 = _.filter(data1.data,function (data2) {
                        return data2.subtask_completed === 'N';
                    })
                    var filterDates =[];
                    _.each(filterdata2,function (sb) {
                        filterDates.push(sb.dueDate);
                    })
                    var dates = _.map(filterDates,function(date){
                        return moment(date);
                    });
                    var start = _.min(dates);
                    var findObject = _.find(filterdata2, function(item) {
                        //console.log('item print hora',JSON.stringify(item.dueDate));
                        if(moment(item.dueDate).isSame(start)){
                            //alert('success');
                            return item;
                        }

                    });

                    var todaysDateCheckUp = moment(new Date()).startOf("day").add(1,'days').subtract({hours:18,minutes:30});
                   // console.log('todaysDateCheckUp>>>>>>>>>>>>>.',JSON.stringify(todaysDateCheckUp));
                   // console.log('findObject>>>>>>>>>>>>>.',JSON.stringify(moment(findObject.dueDate).startOf("day").add(1,'days').subtract({hours:18,minutes:30})));
                    data1.next = findObject;

                    if(moment(findObject.dueDate).startOf("day").add(1,'days').subtract({hours:18,minutes:30}).isAfter(todaysDateCheckUp)){
                        data1.next.taskStatus = "Upcoming Task";
                        return data1;
                    }

                    if(moment(findObject.dueDate).startOf("day").add(1,'days').subtract({hours:18,minutes:30}).isSameOrBefore(todaysDateCheckUp)){
                        data1.next.taskStatus = "Due Task";
                        return data1;
                    }

                });


                $scope.tasksDataBySearch.sort(function(a,b){
                    return new Date(a.next.dueDate) - new Date(b.next.dueDate);
                });

                for(var i = 0 ; i < $scope.tasksDataBySearch.length ; i++){
                    //console.log('$scope.tasksDataBySearch222222222',JSON.stringify($scope.tasksDataBySearch[i]));
                    $scope.tasksDataBySearch[i].data = _.sortBy($scope.tasksDataBySearch[i].data, 'sort_id');
                }

                $scope.TotalRecord = $scope.tasksDataBySearch.length;
               // console.log('tasksDataBySearch 11111111111',JSON.stringify($scope.tasksDataBySearch));
                //console.log('tasksDataBySearch 11111111111',JSON.stringify($scope.tasksDataBySearch.length));



            }).then(function () {
                $scope.numberOfPagesSearch = function() {
                    return Math.ceil($scope.tasksDataBySearch.length / $scope.pageSize);
                };
            }).catch(function (error) {
                console.log('error',error);
            });
            $scope.hideDropDownAssingeeAfterSelect = false;
        };

        $scope.getAssigneeNames = function (data) {
            $scope.hideDropDownAssingeeAfterSelect = true;
            //console.log('data for cutomers',JSON.stringify(data))
            $scope.showDropDown = data
            //console.log('data for $scope.showDropDown',JSON.stringify($scope.showDropDown.length))
            var searchQuery = {
                query:data
            };
            tasksService.getAssigneeNames(searchQuery).then(function (result) {
                $scope.allAssigneeNames  = result.data.value
                //console.log('$scope.allAssigneeNames',JSON.stringify($scope.allAssigneeNames));
                //console.log('$scope.allAssigneeNames',JSON.stringify($scope.allAssigneeNames.length));
            });

        };




        $scope.assignCustomersForTemp = function (data) {
            //console.log('data for cutomers',JSON.stringify(data))
            $scope.hideDropDownAfterSelect = true
            $scope.showDropDown = data
            //console.log('data for $scope.showDropDown',JSON.stringify($scope.showDropDown.length))
            var searchQuery = {
                search:data
            };
            tasksService.getAllClientsDetailById(searchQuery).then(function (result) {
                var customerData = result.data.value
                $scope.allCustomersDataCust = customerData.filter(function (data) {
                    if(data.customer_type === null){
                        data.customer_type = "Leads";
                    }
                    return data;
                });
                //console.log('$scope.allCustomersData',JSON.stringify($scope.allCustomersDataCust));
                //console.log('$scope.allCustomersData',JSON.stringify($scope.allCustomersDataCust.length));
            });

        };



        $scope.chosenCustomerForInd = function (cust) {
            //console.log('chosenCustomerForInd',cust.name);
            $scope.customer_id = cust.user_id;
            $scope.customername = cust.name;
            $scope.c.tempcustomername = cust.name;
            //console.log('tempcustomername',$scope.c.tempcustomername);

            $scope.hideDropDownAfterSelect = false;
        };

        $scope.chosenCustomerForTemp = function (cust) {
            $scope.spinner = true;
            //console.log('cust================',JSON.stringify(cust));
           // console.log('index in cust.name',cust.name);
            $scope.taskdata.customername = cust.name;
            var queryForSearch = {
                query : cust.user_id
            }

            tasksService.getCrmAllTasksBySearch(queryForSearch).then(function (clientsResult) {
                //console.log('getAllCrmTasks================>',JSON.stringify(clientsResult.data.response));
                var crmAllTasks = clientsResult.data.response;

                //console.log('crmAllTasks-------bbbbbbbbb-----------afterrrrrrr', crmAllTasks);
                // crmAllTasks = crmAllTasks.filter(function (obj) {
                //     var todaysDate = new Date();
                //     //console.log('todays date', JSON.stringify(todaysDate));
                //     if(moment(todaysDate).isAfter(obj.duedate)){
                //         //console.log('objects being filtered are ',JSON.stringify(obj));
                //     }else{
                //         return obj;
                //     }
                // });

                //console.log('the final objects are ', JSON.stringify(crmAllTasks));

                if(crmAllTasks !== 0){
                    crmAllTasks = crmAllTasks.filter(function (ele) {
                        var tempArr = Object.keys(ele);
                        //console.log('the keys are ', tempArr);
                        tempArr.map(function (arrEle, i) {
                            // console.log('the arrEle are ', arrEle);
                            if (ele[arrEle] === null) {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                            if (ele[arrEle] === "Invalid date") {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                            if (ele[arrEle] === "") {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                        });
                        //console.log(ele);
                        return ele;
                    });
                }
                //console.log('crmAllTasks-------66666666666666-----------afterrrrrrr', JSON.stringify(crmAllTasks));

                function arrayFromObject(obj) {
                    var arr = [];
                    for (var i in obj) {
                        arr.push(obj[i]);
                    }
                    return arr;
                }

                function groupBy(list, fn) {
                    var groups = {};
                    for (var i = 0; i < list.length; i++) {
                        var group = JSON.stringify(fn(list[i]));
                        if (group in groups) {
                            groups[group].push(list[i]);
                        } else {
                            groups[group] = [list[i]];
                        }
                    }
                    return arrayFromObject(groups);
                }
                $scope.crmAllTasksfinal = groupBy(crmAllTasks, function(item) {
                    //console.log('8888888888888888888',JSON.stringify(item));

                    return [item.task_title, item.customer_name,item.isSubtask];
                });
                var DataBySearch =[];
                for(var K = 0 ; K < $scope.crmAllTasksfinal.length ; K++){

                    var task_title = '';
                    var customername = '';
                    var isSubtask = '';
                    for(var j = 0 ; j < $scope.crmAllTasksfinal[K].length ; j++){
                        task_title = $scope.crmAllTasksfinal[K][j].task_title;
                        customername = $scope.crmAllTasksfinal[K][j].customer_name;
                        isSubtask = $scope.crmAllTasksfinal[K][j].isSubtask;

                    }
                    //console.log('filter subtaks777777777777',JSON.stringify($scope.crmAllTasksfinal))
                    DataBySearch.push({task_title : task_title,customername:customername,isSubtask:isSubtask,data : $scope.crmAllTasksfinal[K]});
                }

                $scope.tasksDataBySearch = _.each(DataBySearch,function (data1) {
                    var filterdata2 = _.filter(data1.data,function (data2) {
                        return data2.isSubtask === 'Y';
                    })
                    var filterDates =[];
                    _.each(filterdata2,function (sb) {
                        filterDates.push(sb.dueDate);
                    })
                    var dates = _.map(filterDates,function(date){
                        return moment(date);
                    });
                    var start = _.min(dates);
                    var findObject = _.find(filterdata2, function(item) {
                        //console.log('item print hora',JSON.stringify(item.dueDate));
                        if(moment(item.dueDate).isSame(start)){
                            //alert('success');
                            return item;
                        }

                    });
                    data1.next = findObject;
                    //console.log('findObject================', JSON.stringify(findObject));
                    var todaysDateCheckUp = moment(new Date()).startOf("day").add(1,'days').subtract({hours:18,minutes:30});
                    //console.log('todaysDateCheckUp>>>>>>>>>>>>>.',JSON.stringify(todaysDateCheckUp));
                   // console.log('findObject>>>>>>>>>>>>>.',JSON.stringify(moment(findObject.dueDate).startOf("day").add(1,'days').subtract({hours:18,minutes:30})));


                    if(moment(findObject.dueDate).startOf("day").add(1,'days').subtract({hours:18,minutes:30}).isAfter(todaysDateCheckUp)){
                        data1.next.taskStatus = "Upcoming Task";
                        return data1;
                    }

                    if(moment(findObject.dueDate).startOf("day").add(1,'days').subtract({hours:18,minutes:30}).isSameOrBefore(todaysDateCheckUp)){
                        data1.next.taskStatus = "Due Task";
                        return data1;
                    }


                });


                $scope.tasksDataBySearch.sort(function(a,b){
                    return new Date(a.next.dueDate) - new Date(b.next.dueDate);
                });

                for(var i = 0 ; i < $scope.tasksDataBySearch.length ; i++){
                    //console.log('$scope.tasksDataBySearch222222222',JSON.stringify($scope.tasksDataBySearch[i]));
                    $scope.tasksDataBySearch[i].data = _.sortBy($scope.tasksDataBySearch[i].data, 'sort_id');
                }
                $scope.TotalRecord = $scope.tasksDataBySearch.length
                //console.log('tasksDataBySearch 11111111111',JSON.stringify($scope.tasksDataBySearch));
                //console.log('tasksDataBySearch 11111111111',JSON.stringify($scope.tasksDataBySearch.length));
                $scope.spinner = false;

            }).then(function () {
                $scope.numberOfPagesSearch = function() {
                    return Math.ceil($scope.tasksDataBySearch.length / $scope.pageSize);
                };
            }).catch(function (error) {
                console.log('error',error);
            });
            $scope.hideDropDownAfterSelect = false;
        };

        // setTimeout(function(){
        //
        // }, 2000);



        var getClientTasks = function (cust) {
            //console.log('cust================',JSON.stringify(cust));
            // console.log('index in cust.name',cust.name);
            $scope.taskdata.customername = cust.name;
            var queryForSearch = {
                query : cust.user_id
            }

            tasksService.getCrmAllTasksBySearch(queryForSearch).then(function (clientsResult) {
                //console.log('getAllCrmTasks================>',JSON.stringify(clientsResult.data.response));
                var crmAllTasks = clientsResult.data.response;

                //console.log('crmAllTasks-------bbbbbbbbb-----------afterrrrrrr', crmAllTasks);
                // crmAllTasks = crmAllTasks.filter(function (obj) {
                //     var todaysDate = new Date();
                //     //console.log('todays date', JSON.stringify(todaysDate));
                //     if(moment(todaysDate).isAfter(obj.duedate)){
                //         //console.log('objects being filtered are ',JSON.stringify(obj));
                //     }else{
                //         return obj;
                //     }
                // });

                //console.log('the final objects are ', JSON.stringify(crmAllTasks));

                if(crmAllTasks !== 0){
                    crmAllTasks = crmAllTasks.filter(function (ele) {
                        var tempArr = Object.keys(ele);
                        //console.log('the keys are ', tempArr);
                        tempArr.map(function (arrEle, i) {
                            // console.log('the arrEle are ', arrEle);
                            if (ele[arrEle] === null) {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                            if (ele[arrEle] === "Invalid date") {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                            if (ele[arrEle] === "") {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                        });
                        //console.log(ele);
                        return ele;
                    });
                }
                //console.log('crmAllTasks-------66666666666666-----------afterrrrrrr', JSON.stringify(crmAllTasks));

                function arrayFromObject(obj) {
                    var arr = [];
                    for (var i in obj) {
                        arr.push(obj[i]);
                    }
                    return arr;
                }

                function groupBy(list, fn) {
                    var groups = {};
                    for (var i = 0; i < list.length; i++) {
                        var group = JSON.stringify(fn(list[i]));
                        if (group in groups) {
                            groups[group].push(list[i]);
                        } else {
                            groups[group] = [list[i]];
                        }
                    }
                    return arrayFromObject(groups);
                }
                $scope.crmAllTasksfinal = groupBy(crmAllTasks, function(item) {
                    //console.log('8888888888888888888',JSON.stringify(item));

                    return [item.task_title, item.customer_name,item.isSubtask];
                });
                $scope.tasksDataBySearch =[];
                for(var K = 0 ; K < $scope.crmAllTasksfinal.length ; K++){

                    var task_title = '';
                    var customername = '';
                    var isSubtask = '';
                    for(var j = 0 ; j < $scope.crmAllTasksfinal[K].length ; j++){
                        task_title = $scope.crmAllTasksfinal[K][j].task_title;
                        customername = $scope.crmAllTasksfinal[K][j].customer_name;
                        isSubtask = $scope.crmAllTasksfinal[K][j].isSubtask;

                    }
                    //console.log('filter subtaks777777777777',JSON.stringify($scope.crmAllTasksfinal))
                    $scope.tasksDataBySearch.push({task_title : task_title,customername:customername,isSubtask:isSubtask,data : $scope.crmAllTasksfinal[K]});
                }

                $scope.tasksDataBySearch = _.each($scope.tasksDataBySearch,function (data1) {
                    var filterdata2 = _.filter(data1.data,function (data2) {
                        return data2.isSubtask === 'Y';
                    })
                    var filterDates =[];
                    _.each(filterdata2,function (sb) {
                        filterDates.push(sb.dueDate);
                    })
                    var dates = _.map(filterDates,function(date){
                        return moment(date);
                    });
                    var start = _.min(dates);
                    var findObject = _.find(filterdata2, function(item) {
                        //console.log('item print hora',JSON.stringify(item.dueDate));
                        if(moment(item.dueDate).isSame(start)){
                            //alert('success');
                            return item;
                        }

                    });
                    data1.next = findObject;
                   // console.log('findObject================', JSON.stringify(findObject));
                    var todaysDateCheckUp = moment(new Date()).startOf("day").add(1,'days').subtract({hours:18,minutes:30});
                    //console.log('todaysDateCheckUp>>>>>>>>>>>>>.',JSON.stringify(todaysDateCheckUp));
                    // console.log('findObject>>>>>>>>>>>>>.',JSON.stringify(moment(findObject.dueDate).startOf("day").add(1,'days').subtract({hours:18,minutes:30})));


                    if(moment(findObject.dueDate).startOf("day").add(1,'days').subtract({hours:18,minutes:30}).isAfter(todaysDateCheckUp)){
                        data1.next.taskStatus = "Upcoming Task";
                        return data1;
                    }

                    if(moment(findObject.dueDate).startOf("day").add(1,'days').subtract({hours:18,minutes:30}).isSameOrBefore(todaysDateCheckUp)){
                        data1.next.taskStatus = "Due Task";
                        return data1;
                    }


                });


                $scope.tasksDataBySearch.sort(function(a,b){
                    return new Date(a.next.dueDate) - new Date(b.next.dueDate);
                });

                for(var i = 0 ; i < $scope.tasksDataBySearch.length ; i++){
                    //console.log('$scope.tasksDataBySearch222222222',JSON.stringify($scope.tasksDataBySearch[i]));
                    $scope.tasksDataBySearch[i].data = _.sortBy($scope.tasksDataBySearch[i].data, 'sort_id');
                }
                $scope.TotalRecord = $scope.tasksDataBySearch.length
                //console.log('tasksDataBySearch 11111111111',JSON.stringify($scope.tasksDataBySearch));
                //console.log('tasksDataBySearch 11111111111',JSON.stringify($scope.tasksDataBySearch.length));

            }).catch(function (error) {
                console.log('error',error);
            });
            $scope.hideDropDownAfterSelect = false;
        };

        // $rootScope.$on("searchTasksForClient", function (event, args) {
        //     console.log('from searchTask ctrl >>>', args);
        //     $scope.fromMyClientsCheck = true;
        //     var custFromMC = args;
        //     custFromMC.name = custFromMC.first_name + " " + custFromMC.last_name;
        //     console.log('Pass this cust Obj to func', custFromMC);
        //     Store.customerview = true;
        //     Store.assingeeview = false;
        //     Store.dateview = false;
        //     $scope.chosenCustomerForTemp(custFromMC);
        //     // Store.customerview = true;
        // });

        //for MYCLIENTS
        if($stateParams.myClientsObj != null){
            // alert($stateParams.clientID);
            $scope.clientObj_myClients.user_id = $stateParams.myClientsObj.user_id;
            $scope.clientObj_myClients.name = $stateParams.myClientsObj.name;
            $scope.fromMyClientsCheck = true;
           // console.log('DATAAAAAA', Store.customerview, Store.assingeeview, Store.dateview, $scope.clientObj_myClients);
        }

        if(Store.isEmptyOrUndefinedOrNull($scope.clientObj_myClients.user_id)){

        }
        else{
           // console.log('CALLING FROM HERE');
            $scope.taskdata.customername = $scope.clientObj_myClients.name;
            $scope.chosenCustomerForTemp($scope.clientObj_myClients);
        }

        $scope.search = {};
        ///////search by date////////////
        
        $scope.searchByDate = function () {
            if(!$scope.search.startDate){
               $scope.errorMessage1 = 'please select start date';
               return;
            }else{
                $scope.errorMessage1 ='';
            }
            if(!$scope.search.endDate){
                $scope.errorMessage2 = 'please select end date';
                return;
            }else{
                $scope.errorMessage2 ='';
            }
            //console.log('search by date',$scope.search);
            var startDate1 = new Date($scope.search.startDate);
            var endtDate1 = new Date($scope.search.endDate);
            //console.log('startDate1',JSON.stringify(startDate1));
            //console.log('endDate1',JSON.stringify(endtDate1));

            var datequery = {
                start : startDate1,
                end:endtDate1
            }
            $scope.queryDate = {
                start : startDate1,
                end:endtDate1
            }

            tasksService.getCrmAllTasksByDate(datequery).then(function (clientsResult) {
                var crmAllTasksByDate = clientsResult.data.response;
                //console.log('crmAllTasksByDate================>',JSON.stringify(crmAllTasksByDate));
                if(crmAllTasksByDate !== 0){
                    crmAllTasksByDate = crmAllTasksByDate.filter(function (ele) {
                        var tempArr = Object.keys(ele);
                        //console.log('the keys are ', tempArr);
                        tempArr.map(function (arrEle, i) {
                            // console.log('the arrEle are ', arrEle);
                            if (ele[arrEle] === null) {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                            if (ele[arrEle] === "Invalid date") {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                            if (ele[arrEle] === "") {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                        });
                        //console.log(ele);
                        return ele;
                    });
                }
                //console.log('crmAllTasksByDate-------66666666666666-----------afterrrrrrr', JSON.stringify(crmAllTasksByDate));

                function arrayFromObject(obj) {
                    var arr = [];
                    for (var i in obj) {
                        arr.push(obj[i]);
                    }
                    return arr;
                }

                function groupBy(list, fn) {
                    var groups = {};
                    for (var i = 0; i < list.length; i++) {
                        var group = JSON.stringify(fn(list[i]));
                        if (group in groups) {
                            groups[group].push(list[i]);
                        } else {
                            groups[group] = [list[i]];
                        }
                    }
                    return arrayFromObject(groups);
                }
                $scope.crmAllTasksfinalDate = groupBy(crmAllTasksByDate, function(item) {
                    //console.log('8888888888888888888',JSON.stringify(item));

                    return [item.task_title, item.customer_name,item.isSubtask];
                });
                var searchByDate =[];
                for(var K = 0 ; K < $scope.crmAllTasksfinalDate.length ; K++){

                    var task_title = '';
                    var customername = '';
                    var isSubtask = '';
                    for(var j = 0 ; j < $scope.crmAllTasksfinalDate[K].length ; j++){
                        task_title = $scope.crmAllTasksfinalDate[K][j].task_title;
                        customername = $scope.crmAllTasksfinalDate[K][j].customer_name;
                        isSubtask = $scope.crmAllTasksfinalDate[K][j].isSubtask;

                    }
                    //console.log('filter subtaks777777777777',JSON.stringify($scope.crmAllTasksfinalDate))
                    searchByDate.push({task_title : task_title,customername:customername,isSubtask:isSubtask,data : $scope.crmAllTasksfinalDate[K]});
                }

                $scope.tasksDataBySearch = _.each(searchByDate,function (data1) {
                    var filterdata2 = _.filter(data1.data,function (data2) {
                        return data2.isSubtask === 'Y';
                    })
                    var filterDates =[];
                    _.each(filterdata2,function (sb) {
                        filterDates.push(sb.dueDate);
                    })
                    var dates = _.map(filterDates,function(date){
                        return moment(date);
                    });
                    var start = _.min(dates);
                    var findObject = _.find(filterdata2, function(item) {
                        //console.log('item print hora',JSON.stringify(item.dueDate));
                        if(moment(item.dueDate).isSame(start)){
                            //alert('success');
                            return item;
                        }

                    });
                    var todaysDateCheckUp = moment(new Date()).add({hours:18,minutes:59});
                    //console.log('todaysDateCheckUp',JSON.stringify(todaysDateCheckUp));
                   // console.log('findObject>>>>>>>>>>>>>.',JSON.stringify(findObject));
                    data1.next = findObject;
                    if(moment(findObject.dueDate).isAfter(todaysDateCheckUp)){
                        data1.next.taskStatus = "Upcoming Task";
                        return data1;
                    }

                    if(moment(findObject.dueDate).isSameOrBefore(todaysDateCheckUp)){
                        data1.next.taskStatus = "Due Task";
                        return data1;
                    }


                });


                $scope.tasksDataBySearch.sort(function(a,b){
                    return new Date(a.next.dueDate) - new Date(b.next.dueDate);
                });

                for(var i = 0 ; i < $scope.tasksDataBySearch.length ; i++){
                    //console.log('$scope.tasksDataBySearch222222222',JSON.stringify($scope.tasksDataBySearch[i]));
                    $scope.tasksDataBySearch[i].data = _.sortBy($scope.tasksDataBySearch[i].data, 'sort_id');
                }
                $scope.TotalRecord = $scope.tasksDataBySearch.length
                //console.log('tasksDataBySearch date 11111111111',JSON.stringify($scope.tasksDataBySearch));
                //console.log('tasksDataBySearch date 11111111111',JSON.stringify($scope.tasksDataBySearch.length));

            }).then(function () {
                $scope.numberOfPagesSearch = function() {
                    return Math.ceil($scope.tasksDataBySearch.length / $scope.pageSize);
                };
            }).catch(function (error) {
                console.log('error',error);
            });


            };


        


        /////////edit temp task////////
        $scope.c = {};
        $scope.TempSubTasksData = [];
        $scope.openEditTempTaskModal = function (task,event) {
           // console.log('task 111111111111',JSON.stringify(task))
            $scope.c.temptaskname = task.task_title;
            $scope.c.tempcustomername = task.customername;
            for(var  i = 0 ; i < task.data.length ; i++){
                var editTempObj= {};
                editTempObj.subtask_name = task.data[i].subtask_name;
                editTempObj.currentTime = moment(task.data[i].dueDate).format('ll')
                editTempObj.assignto = task.data[i].assigned_to;
                editTempObj.sort_id = task.data[i].sort_id;
                editTempObj.task_id = task.data[i].task_id;
                editTempObj.subtask_completed = task.data[i].subtask_completed;
                editTempObj.customer_id = task.data[i].customer_id;
                $scope.TempSubTasksData.push(editTempObj);
            }
            _.each($scope.TempSubTasksData, function (item, index) {
                //console.log('item sort',JSON.stringify(item));
                //console.log('index sort',index);
                item.sort_id = index + 1;
                item.index = index;
                //console.log('ye dekh pehle',JSON.stringify($scope.TempSubTasksData));
            });
            var list123 = document.getElementById("sortThisTempTask");
            // console.log('list123==========',list123);
            var sortable = new Sortable(list123, {
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent

                    $scope.TempSubTasksData = move($scope.TempSubTasksData, evt.oldIndex, evt.newIndex)
                    _.each($scope.TempSubTasksData, function (item, index) {
                        item.sort_id = index + 1;
                        item.index = index;
                        //console.log('other sort 999999999999999999',JSON.stringify($scope.TempSubTasksData));
                    });
                },
                onUpdate: function (/**Event*/evt) {
                    // same properties as onEnd
                    $timeout(function(){
                        //console.log('after update calling this', $scope.TempSubTasksData);
                        _.each($scope.TempSubTasksData, function (item, index) {
                            item.sort_id = index + 1;
                            item.index = index;
                        });
                    });
                }
            });

            //console.log('$scope.otherSubTasksData edit',JSON.stringify($scope.TempSubTasksData))
            event.stopPropagation();
            $('#Updatetasktemplate').modal('open');
        };

        function move(arr, old_index, new_index) {
            while (old_index < 0) {
                old_index += arr.length;
            }
            while (new_index < 0) {
                new_index += arr.length;
            }
            if (new_index >= arr.length) {
                var k = new_index - arr.length;
                while ((k--) + 1) {
                    arr.push(undefined);
                }
            }
            arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
            return arr;
        }

        $scope.spinner = false;
        $scope.tempUpdateButtonSearch = false;
        $scope.UpdateTemplateTasks = function (other) {
            $scope.tempUpdateButtonSearch = true;
            var validUpdate = true;
            $scope.spinner = true;
           // console.log(' other1111111',JSON.stringify(other));

            var filterTaskNotHavingIds = _.filter($scope.TempSubTasksData,function (noTaskIds) {
                if(!noTaskIds.task_id){
                    return noTaskIds;
                }
            });

            if($scope.dateClickFlag === true){
                console.log('calling true');
                _.map(filterTaskNotHavingIds,function (data) {
                    data.currentTime = moment(new Date(data.currentTime)).add(1,'days').subtract({hours:2,minutes:30})
                    return data;
                });
            }else{
                //console.log('calling false');
                _.map(filterTaskNotHavingIds,function (data) {
                    data.currentTime = new Date(data.currentTime)
                    return data;
                });
            }

            //console.log('filterTaskNotHavingIds==========',JSON.stringify(filterTaskNotHavingIds));

            var templateDataTasks = [];
            for(var j = 0 ; j < $scope.TempSubTasksData.length; j++){
                //console.log('other22222222',JSON.stringify($scope.TempSubTasksData[j]));
                if(!($scope.TempSubTasksData[j].subtask_name && $scope.TempSubTasksData[j].currentTime)){
                    validUpdate = false;
                    $scope.spinner = false;
                    $scope.tempUpdateButtonSearch = false;
                }
                var TempSubTaskObj = {};
                TempSubTaskObj.subtask_name = $scope.TempSubTasksData[j].subtask_name;
                TempSubTaskObj.dueDate = new Date($scope.TempSubTasksData[j].currentTime);
                TempSubTaskObj.assigned_to = $scope.TempSubTasksData[j].assignto;
                TempSubTaskObj.sort_id = $scope.TempSubTasksData[j].sort_id;
                TempSubTaskObj.task_id = $scope.TempSubTasksData[j].task_id;
                TempSubTaskObj.customer_id = $scope.TempSubTasksData[j].customer_id;
                templateDataTasks.push(TempSubTaskObj);
            }
           // console.log('>>>otherDataTasks otherrrrrrrrrrr>>>>>>.',JSON.stringify(templateDataTasks[0].customer_id));
            var updateTempquery = {
                task_title:$scope.c.temptaskname,
                customer_name: $scope.c.tempcustomername,
                customer_id: $scope.customer_id ? $scope.customer_id : templateDataTasks[0].customer_id,
                TempTaskData:templateDataTasks
            };
            var tempquery = {
                "name":$scope.c.tempcustomername,
                "user_id":$scope.customer_id ? $scope.customer_id : templateDataTasks[0].customer_id
            }
            var assignQuery = {
                'assigned_to' :templateDataTasks[0].assigned_to
            }
           // console.log('assignQuery=========',JSON.stringify(assignQuery));
            var datequery = $scope.queryDate;
           // console.log('dateQuery1=========',JSON.stringify(datequery));

            var subtaskTempqueryadd = {
                task_title:$scope.c.temptaskname,
                customer_name:$scope.c.tempcustomername,
                customer_id:$scope.customer_id ? $scope.customer_id : templateDataTasks[0].customer_id,
                othertTask:filterTaskNotHavingIds
            };

            if(validUpdate){
                $q.all([tasksService.crmUpdateTasksTemplate(updateTempquery),tasksService.AddMoreTemplateTasks(subtaskTempqueryadd)])
                    .then(function(result) {
                        console.log('result---------------------',result[0].data.response);
                        if(result[0].data.response === 'true'){
                            if(Store.customerview === true){
                                console.log('calling customerview ');
                                $scope.chosenCustomerForTemp(tempquery);
                                $scope.taskdata.assigneename = '';
                            }else if(Store.assingeeview === true){
                                console.log('calling assignrview ');
                                console.log('calling assignQuery ');
                                $scope.chooseAssignee(assignQuery);
                            }else{
                                $scope.searchByDate(datequery)
                                console.log('calling date ');
                            }
                            $scope.spinner = false;
                        }else{
                            console.log('field');
                        }
                        $scope.other ={};
                        $scope.taskname='';
                        $scope.customername='';
                        $scope.customer_id='';
                        $scope.temptaskname = '';
                        $scope.tempUpdateButtonSearch = false;
                        $scope.c = {};
                        $scope.otherSubTasksData =[];
                        $scope.TempSubTasksData = [];
                        $('#Updatetasktemplate').modal('close');
                        $('#UpdatetasktemplateDue').modal('close');

                    }).catch(function (error) {
                    console.log('error',error);
                });

            }else{
                $scope.validationError = 'Please fill the missing fields';
                console.log('else igot you while updating');
                $scope.spinner = false;
                $scope.tempUpdateButtonSearch = false;
            }
         
           // console.log('updateTempquery>>>>>>>>>>>>>>>',JSON.stringify(updateTempquery));
           //  tasksService.crmUpdateTasksTemplate(updateTempquery).then(function (crmresult) {
           //      // console.log('CRM OTHER RESULT================>',JSON.stringify(crmresult));
           //      if(crmresult.data.response === 'true'){
           //          if(Store.customerview === true){
           //              console.log('calling customerview ');
           //              $scope.chosenCustomerForTemp(tempquery);
           //              $scope.taskdata.assigneename = '';
           //          }else if(Store.assingeeview === true){
           //              console.log('calling assignrview ');
           //              console.log('calling assignQuery ');
           //              $scope.chooseAssignee(assignQuery);
           //          }else{
           //              $scope.searchByDate(datequery)
           //              console.log('calling date ');
           //          }
           //          $scope.spinner = false;
           //      }else{
           //          console.log('field');
           //      }
           //
           //  }).catch(function (error) {
           //      console.log('error',error);
           //  });



            //console.log('subtaskTempqueryadd',JSON.stringify(subtaskTempqueryadd));
            // tasksService.AddMoreTemplateTasks(subtaskTempqueryadd).then(function (result) {
            //     //console.log('result================>',JSON.stringify(result));
            //     if(result.data.response === 'true'){
            //         if(Store.customerview === true){
            //             console.log('calling customerview ');
            //             $scope.chosenCustomerForTemp(tempquery);
            //             $scope.taskdata.assigneename = '';
            //         }else if(Store.assingeeview === true){
            //             console.log('calling assignQuery ');
            //             $scope.chooseAssignee(assignQuery);
            //         }else{
            //             console.log('calling date ');
            //             $scope.searchByDate(datequery);
            //         }
            //     }else{
            //         console.log('field');
            //     }
            // }).catch(function (error) {
            //     console.log('error',error);
            // });



        };

        $scope.closeDatePickerForIndTask = function (date,index) {

            $scope.dateClickFlag = true;
            console.log('calling date===========',date);
            if(moment(date).format("DD MMM YYYY")){
                $('.picker__close').click();
            }

            $scope.otherSubTasksData[index].currentTime = moment(date).format('ll');
        }

        $scope.searchSubUser = function (data,index,value) {
            //$scope.hideDropDownSubuser = true;
            if(data.length === 0){
                value.active = false;
            }else {
                value.active = true;
            }
            console.log('data-------------------------',data);
            console.log('index-------------------------',index);
            console.log('length-------------------------',data.length);
            console.log('active-------------------------',value.active);

            $scope.subuserText =data;
            var querySubuser = {
                query:data
            };
            tasksService.getsubuserName(querySubuser).then(function (result) {
                // console.log('calling download template');
                // console.log('result================>',JSON.stringify(result));
                if(result.data.status === true){
                    console.log('calling this 111111111111111111');
                    $scope.allSubuserNames = result.data.value
                    console.log('$scope.allSubuserNames-----------',JSON.stringify($scope.allSubuserNames));
                    if($scope.allSubuserNames.length === 0){
                        value.active = false;
                    }

                }else{
                    console.log('error while searching template');
                }
            }).catch(function (error) {
                console.log('error',error);
            });
        }

        $scope.choseSubUserTempUpcoming = function (user,index,value) {
            console.log('user====================',user);
            $scope.TempSubTasksData[index].assignto = user.username;
            console.log('$scope.TempSubTasksData[index]',$scope.TempSubTasksData[index].assignto,index);
            // $scope.hideDropDownSubuser = false;
            value.active = false;
        }

        $scope.choseSubUserSrach = function (user,index,value) {
            console.log('user====================',user);
            $scope.otherSubTasksData[index].assignto = user.username;
            console.log('$scope.otherSubTasksData[index]',$scope.otherSubTasksData[index].assignto,index);
            // $scope.hideDropDownSubuser = false;
            value.active = false;
        }


        $scope.changeDateForPreviousSearchTask = function (allsubTaskfinal,date1,index) {
            $scope.dateClickFlag = true;
            $scope.allsubTaskfinal = allsubTaskfinal;
            var d = new Date(date1)
            $scope.date = d.toISOString();

            $scope.indexClick = index
            // console.log('allsubTaskfinal',allsubTaskfinal);
            // console.log('allsubTaskfinal',allsubTaskfinal.length);
            // console.log('date1',date1);
            // console.log('index',index);
            // console.log('calling date change');
            if(moment(date1).format("DD MMM YYYY")){
                $('.picker__close').click();
            }
            if(index+1 !== allsubTaskfinal.length){
                $('#tempDateCnfrmModalsearch').modal('open');
            }else{
                console.log('dont open pop');
                $scope.TempSubTasksData[index].currentTime = moment($scope.date).format('ll')
            }

        };
        $scope.onOpen = function (data) {
            console.log('onOpen calling',data);
            $scope.firstDate = data;
        };

        $scope.changeSubtaskTempDateSearch = function () {
            // console.log('calling yes function');
            // console.log('$scope.firstDate ',$scope.firstDate);
            var firstDate = new Date($scope.firstDate);
            var allsubTask = $scope.allsubTaskfinal
            var date1 = new Date($scope.date)
            var index = $scope.indexClick
            // console.log('firstDate ',firstDate);
            // console.log('allsubTask',allsubTask);
            // console.log('date1',date1);
            // console.log('index ',index);
            var diffDate = moment(date1).startOf('day').diff(moment(firstDate).startOf('day'),'days')
            //console.log('diffDate ',diffDate);

            $scope.TempSubTasksData = allsubTask;
           // console.log('$scope.TempSubTasksData==============',$scope.TempSubTasksData);
            var dateTemp = new Date(date1);
            for(var i = index ; i< $scope.TempSubTasksData.length ; i++){
                var lastDate2;
                var lastDate1;
                if($scope.TempSubTasksData[i].subtask_completed === 'N'){
                    var dateTemp1 = new Date(date1)
                    if(i === index){
                        lastDate1 = dateTemp1.setDate(dateTemp1.getDate())
                        //console.log('inside if',new Date(lastDate1));
                        //$scope.TempSubTasksData[i].currentTime = new Date(lastDate1);
                        $scope.TempSubTasksData[i].currentTime =moment(new Date(lastDate1)).format('ll');
                    }else{
                        //console.log('inside else');
                        //console.log('$scope.TempSubTasksData[i].currentTime',new Date($scope.TempSubTasksData[i].currentTime));
                        lastDate2 = new Date($scope.TempSubTasksData[i].currentTime).setDate(new Date($scope.TempSubTasksData[i].currentTime).getDate() + diffDate)
                        //$scope.TempSubTasksData[i].currentTime = new Date(lastDate2);
                        $scope.TempSubTasksData[i].currentTime = moment(new Date(lastDate2)).format('ll');

                    }
                   // console.log('diffDate 1111111111',diffDate);

                }else{
                    console.log('do nuthing');
                }
                //var diffDate =''
            }
           // console.log('out of looooooooooooooooop');
            diffDate = 0
           // console.log('diffDate 2222222',diffDate);


        }

        $scope.dateChangeOnlySelectedSearch = function () {
            var index = $scope.indexClick
            console.log('index--------------',index);
            var dateSele = new Date($scope.date);
            $scope.TempSubTasksData = $scope.allsubTaskfinal;
            $scope.TempSubTasksData[index].currentTime =moment(new Date(dateSele)).format('ll');
            $('#tempDateCnfrmModal').modal('close')
        }

        $scope.getTheSelectedDate = function (data) {
            var dummyDate = new Date(data);
            $scope.firstDate = dummyDate.toISOString()
            console.log('$scope.firstDate calling',$scope.firstDate);
        };


        $scope.addTemplateSubTasks = function() {

            $scope.testStatusOther1 = {};
            //console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.otherSubTasksData));
            // console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.TempSubTasksData));
            // console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.TempSubTasksData.length));

            //console.log('object>..',JSON.stringify($scope.testStatusOther1));
            $scope.TempSubTasksData.push($scope.testStatusOther1);
            //console.log('$scope.subTasksData>>>>>before>>>>>>>..',JSON.stringify($scope.TempSubTasksData));
            for(var i = 0 ; i < $scope.TempSubTasksData.length; i++){
                //console.log('$scope.TempSubTasksData.current',JSON.stringify($scope.TempSubTasksData[i].currentTime));
                if($scope.TempSubTasksData[i].currentTime === undefined){
                    $scope.TempSubTasksData[i].currentTime = new Date();
                }
            }
            //console.log('$scope.subTasksData>>>>>after>>>>>>>..',JSON.stringify($scope.TempSubTasksData));
            if($scope.TempSubTasksData.length >= 100){
                $rootScope.errorToast('can not add more that 20 Subtasks');
            }

            _.each($scope.TempSubTasksData, function (item, index) {
                // console.log('item sort',JSON.stringify(item));
                // console.log('index sort',index);
                item.sort_id = index + 1;
                //console.log('ye dekh pehle',JSON.stringify($scope.TempSubTasksData));
            });
            var list123 = document.getElementById("sortThisTempTask");
            //console.log('list123==========',list123);
            var sortable = new Sortable(list123, {
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent

                    $scope.TempSubTasksData = move($scope.TempSubTasksData, evt.oldIndex, evt.newIndex)
                    _.each($scope.TempSubTasksData, function (item, index) {
                        item.sort_id = index + 1;
                        //console.log('other sort 999999999999999999',JSON.stringify($scope.TempSubTasksData));
                    });
                }
            });
        };

        $scope.addTempTasksBetween = function(index) {

            // console.log('calling inbetween');
            // console.log('index',index);

            if($scope.taskname){
                $scope.errorMessage = '';
            }
            if($scope.customername){
                $scope.errorMessage1 = '';
            }
            $scope.testStatusOther = {};
            //console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.TempSubTasksData));
            // console.log('$scope.TempSubTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.TempSubTasksData.currentTime));
            // console.log('$scope.TempSubTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.TempSubTasksData.length));

            // console.log('object>..',JSON.stringify($scope.testStatusOther));
            //$scope.otherSubTasksData.push($scope.testStatusOther);
            $scope.TempSubTasksData.splice(index+1,0,$scope.testStatusOther);
            //console.log('$scope.subTasksData>>>>>before>>>>>>>..',JSON.stringify($scope.TempSubTasksData));
            for(var i = 0 ; i < $scope.TempSubTasksData.length; i++){
                //console.log('$scope.TempSubTasksData.current',JSON.stringify($scope.TempSubTasksData[i].currentTime));
                if($scope.TempSubTasksData[i].currentTime === undefined){
                    $scope.TempSubTasksData[i].currentTime = new Date();
                }
            }
            //console.log('$scope.subTasksData>>>>>after>>>>>>>..',JSON.stringify($scope.TempSubTasksData));
            if($scope.TempSubTasksData.length >= 100){
                $rootScope.errorToast('can not add more that 20 Subtasks');
            }
            _.each($scope.TempSubTasksData, function (item, index) {
                // console.log('item sort',JSON.stringify(item));
                // console.log('index sort',index);
                item.sort_id = index + 1;
                // console.log('ye dekh pehle',JSON.stringify($scope.TempSubTasksData));
            });
            var list123 = document.getElementById("sortThisTempTask");
            var sortable = new Sortable(list123, {
                draggable: ".item",
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent

                    $scope.TempSubTasksData = move($scope.TempSubTasksData, evt.oldIndex, evt.newIndex)
                    _.each($scope.TempSubTasksData, function (item, index) {
                        item.sort_id = index + 1;
                        //console.log('other sort 999999999999999999',JSON.stringify($scope.TempSubTasksData));
                    });
                }
            });

        };


        ////////////edit fresh task////////////
        $scope.otherSubTasksData = [];
        $scope.openEditSratchTempModal = function (task,event,editstatus) {
            $scope.statusfresh = editstatus;
           // console.log('editstatus',JSON.stringify($scope.statusfresh))
            // console.log('task',JSON.stringify(task))
            $scope.taskname = task.task_title;
            $scope.customername = task.customername;
            for(var  i = 0 ; i < task.data.length ; i++){
                var editOtherObj= {};
                editOtherObj.content = task.data[i].task_content;
                editOtherObj.currentTime = moment(task.data[i].dueDate).format('ll');
                editOtherObj.assignto = task.data[i].assigned_to;
                editOtherObj.sort_id = task.data[i].sort_id;
                editOtherObj.task_id = task.data[i].task_id;
                editOtherObj.subtask_completed = task.data[i].subtask_completed;
                editOtherObj.customer_id = task.data[i].customer_id;
                $scope.otherSubTasksData.push(editOtherObj);
            }
            //$scope.subTasksData.shift();
            _.each($scope.otherSubTasksData, function (item, index) {
                // console.log('item sort',JSON.stringify(item));
                // console.log('index sort',index);
                item.sort_id = index + 1;
                item.index = index;
                //console.log('ye dekh pehle',JSON.stringify($scope.otherSubTasksData));
            });
            var list123 = document.getElementById("sortThisFreshTask1");
            // console.log('list123==========',list123);
            var sortable = new Sortable(list123, {
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent

                    $scope.otherSubTasksData = move($scope.otherSubTasksData, evt.oldIndex, evt.newIndex)
                    _.each($scope.otherSubTasksData, function (item, index) {
                        item.sort_id = index + 1;
                        item.index = index;
                        //console.log('other sort 999999999999999999',JSON.stringify($scope.otherSubTasksData));
                    });
                },
                onUpdate: function (/**Event*/evt) {
                    // same properties as onEnd
                    $timeout(function(){
                        console.log('after update calling this', $scope.otherSubTasksData);
                        _.each($scope.otherSubTasksData, function (item, index) {
                            item.sort_id = index + 1;
                            item.index = index;
                        });
                    });
                }
            });
            //console.log('$scope.otherSubTasksData edit',JSON.stringify($scope.otherSubTasksData))
            event.stopPropagation();
            $('#individualtasks').modal('open');
        };

        $scope.UpdateOtherTask = function (other) {
            $scope.spinner = true;
            //console.log(' other1111111', other);
            if(!$scope.taskname){
                $scope.errorMessage = 'Please enter task title';
                // console.log('calling',JSON.stringify($scope.errorMessage));
                return;
            }else {
                $scope.errorMessage='';
            }
            if(!$scope.customername){
                $scope.errorMessage1 = 'Please select customer name';
                //console.log('calling',JSON.stringify($scope.errorMessage1));
                return;
            }else {
                $scope.errorMessage1='';
            }
            var filterTaskNotIds1 = _.filter($scope.otherSubTasksData,function (noTaskIdTasks) {
                if(!noTaskIdTasks.task_id){
                    return noTaskIdTasks;
                }
            });
            if($scope.dateClickFlag === true){
                console.log('calling true');
                _.map(filterTaskNotIds1,function (data) {
                    data.currentTime = moment(new Date(data.currentTime)).add(1,'days').subtract({hours:2,minutes:30})
                    return data;
                });
            }else{
                console.log('calling false');
                _.map(filterTaskNotIds1,function (data) {
                    data.currentTime = new Date(data.currentTime)
                    return data;
                });
            }
            // console.log('filterTaskNotIds1==========',JSON.stringify(filterTaskNotIds1));

            var otherDataTasks = [];
            for(var j = 0 ; j < $scope.otherSubTasksData.length; j++){
                //console.log(' other22222222',JSON.stringify($scope.otherSubTasksData[j]));
                var OtherSubTaskObj = {};
                OtherSubTaskObj.task_title = $scope.taskname;
                OtherSubTaskObj.customer_name = $scope.customername;
                OtherSubTaskObj.customer_id = $scope.customer_id ? $scope.customer_id : $scope.otherSubTasksData[j].customer_id;
                OtherSubTaskObj.task_content = $scope.otherSubTasksData[j].content;
                OtherSubTaskObj.dueDate = new Date($scope.otherSubTasksData[j].currentTime);
                OtherSubTaskObj.assigned_to = $scope.otherSubTasksData[j].assignto;
                OtherSubTaskObj.sort_id = $scope.otherSubTasksData[j].sort_id;
                OtherSubTaskObj.task_id = $scope.otherSubTasksData[j].task_id;
                otherDataTasks.push(OtherSubTaskObj);
            }
            //console.log('>>>otherDataTasks otherrrrrrrrrrr>>>>>>.',JSON.stringify(otherDataTasks));
            var Othersubtaskquery = {
                othertTask:otherDataTasks
            };
            var tempquery = {
                "name":$scope.customername,
                "user_id":Othersubtaskquery.othertTask[0].customer_id
            }
            var assignQuery = {
                'assigned_to' :Othersubtaskquery.othertTask[0].assigned_to
            }
            var datequery = $scope.queryDate;
            //console.log('dateQuery1=========',JSON.stringify(datequery));


           // console.log('createTaskQuery>>>>>>>>>>>>>>>',JSON.stringify(Othersubtaskquery));
            tasksService.crmUpdateFreshTasks(Othersubtaskquery).then(function (crmresult) {
                //console.log('CRM OTHER RESULT================>',JSON.stringify(crmresult));
                if(crmresult.data.response === 'true'){
                    if(Store.customerview === true){
                        console.log('calling customerview ');
                        $scope.chosenCustomerForTemp(tempquery);
                        $scope.taskdata.assigneename = '';
                    }else if(Store.assingeeview === true){
                        console.log('calling assignQuery ');
                        $scope.chooseAssignee(assignQuery);
                    }else{
                        console.log('calling date ');
                        $scope.searchByDate(datequery)
                    }
                }else{
                    console.log('field');
                }
                $scope.spinner = false;
            }).catch(function (error) {
                console.log('error',error);
            });

            var subtaskqueryadd = {
                task_title:$scope.taskname,
                customer_name:$scope.customername,
                customer_id:Othersubtaskquery.othertTask[0].customer_id,
                othertTask:filterTaskNotIds1
            };

            //console.log('subtaskqueryadd',JSON.stringify(subtaskqueryadd));
            tasksService.crmAddTempTaskUpdate(subtaskqueryadd).then(function (result) {
                //console.log('result================>',JSON.stringify(result));

                if(result.data.response === 'true'){
                    if(Store.customerview === true){
                       // console.log('calling customerview ');
                        $scope.chosenCustomerForTemp(tempquery);
                        $scope.taskdata.assigneename = '';
                    }else if(Store.assingeeview === true){
                       // console.log('calling assignQuery ');
                        $scope.chooseAssignee(assignQuery);
                    }else{
                       // console.log('calling date ');
                        $scope.searchByDate(datequery)
                    }
                }else{
                    console.log('field');
                }
            }).catch(function (error) {
                console.log('error',error);
            });

            $scope.other ={};
            $scope.taskname='';
            $scope.customername='';
            $scope.otherSubTasksData =[];
            $('#individualtasks').modal('close');

        };

        $scope.addOtherSubTasks = function() {
            if($scope.taskname){
                $scope.errorMessage = '';
            }
            if($scope.customername){
                $scope.errorMessage1 = '';
            }
            $scope.testStatusOther = {};
            //console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.otherSubTasksData));
            // console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.otherSubTasksData.currentTime));
            // console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.otherSubTasksData.length));
            //
            // console.log('object>..',JSON.stringify($scope.testStatusOther));
            $scope.otherSubTasksData.push($scope.testStatusOther);
            //console.log('$scope.subTasksData>>>>>before>>>>>>>..',JSON.stringify($scope.otherSubTasksData));
            for(var i = 0 ; i < $scope.otherSubTasksData.length; i++){
                //console.log('$scope.otherSubTasksData.current',JSON.stringify($scope.otherSubTasksData[i].currentTime));
                if($scope.otherSubTasksData[i].currentTime === undefined){
                    $scope.otherSubTasksData[i].currentTime = new Date();
                }
            }
            //console.log('$scope.subTasksData>>>>>after>>>>>>>..',JSON.stringify($scope.otherSubTasksData));
            if($scope.otherSubTasksData.length >= 100){
                $rootScope.errorToast('can not add more that 20 Subtasks');
            }

            _.each($scope.otherSubTasksData, function (item, index) {
                // console.log('item sort',JSON.stringify(item));
                // console.log('index sort',index);
                item.sort_id = index + 1;
                //console.log('ye dekh pehle',JSON.stringify($scope.otherSubTasksData));
            });
            var list123 = document.getElementById("sortThisFreshTask1");
            //console.log('list123==========',list123);
            var sortable = new Sortable(list123, {
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent

                    $scope.otherSubTasksData = move($scope.otherSubTasksData, evt.oldIndex, evt.newIndex)
                    _.each($scope.otherSubTasksData, function (item, index) {
                        item.sort_id = index + 1;
                        //console.log('other sort 999999999999999999',JSON.stringify($scope.otherSubTasksData));
                    });
                }
            });
        };

        $scope.addFreshTasksBetween = function(index) {
            // console.log('$scope.subTasksData',JSON.stringify($scope.subTasksData));
            // console.log('calling inbetween');
            // console.log('index',index);

            if($scope.taskname){
                $scope.errorMessage = '';
            }
            if($scope.customername){
                $scope.errorMessage1 = '';
            }
            $scope.testStatusOther = {};
            //console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.otherSubTasksData));
            // console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.otherSubTasksData.currentTime));
            // console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.otherSubTasksData.length));

            // console.log('object>..',JSON.stringify($scope.testStatusOther));
            //$scope.otherSubTasksData.push($scope.testStatusOther);
            $scope.otherSubTasksData.splice(index+1,0,$scope.testStatusOther);
            //console.log('$scope.subTasksData>>>>>before>>>>>>>..',JSON.stringify($scope.otherSubTasksData));
            for(var i = 0 ; i < $scope.otherSubTasksData.length; i++){
                // console.log('$scope.otherSubTasksData.current',JSON.stringify($scope.otherSubTasksData[i].currentTime));
                if($scope.otherSubTasksData[i].currentTime === undefined){
                    $scope.otherSubTasksData[i].currentTime = new Date();
                }
            }
            //console.log('$scope.subTasksData>>>>>after>>>>>>>..',JSON.stringify($scope.otherSubTasksData));
            if($scope.otherSubTasksData.length >= 100){
                $rootScope.errorToast('can not add more that 20 Subtasks');
            }
            _.each($scope.otherSubTasksData, function (item, index) {
                // console.log('item sort',JSON.stringify(item));
                // console.log('index sort',index);
                item.sort_id = index + 1;
                //console.log('ye dekh pehle',JSON.stringify($scope.otherSubTasksData));
            });
            var list123 = document.getElementById("sortThisFreshTask1");
            var sortable = new Sortable(list123, {
                draggable: ".item",
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent

                    $scope.otherSubTasksData = move($scope.otherSubTasksData, evt.oldIndex, evt.newIndex)
                    _.each($scope.otherSubTasksData, function (item, index) {
                        item.sort_id = index + 1;
                        //console.log('other sort 999999999999999999',JSON.stringify($scope.otherSubTasksData));
                    });
                }
            });

        };

        $scope.snoozeArray = [];
        $scope.openSnoozedTaskModal = function (task,event) {
           // console.log('snooz 111111111111',JSON.stringify(task))
            $scope.tempquery1 = {
                "name":task.customername,
                "user_id":task.data[0].customer_id
            }

            if(Store.assingeeview){
                $scope.assignQuery = {
                    'assigned_to' :$scope.queryForSearch.query
                }
                //console.log('$scope.assignQuery=========',JSON.stringify($scope.assignQuery));
            }


            $scope.datequery = $scope.queryDate;
            //console.log('$scope.datequery=========',JSON.stringify($scope.datequery));

            var filterSnooze = _.filter(task.data,function (task) {
                //console.log('ids  111',JSON.stringify(task))
                $scope.snoozeArray.push(task.task_id);
                return task.task_id;
            });
            //console.log('$scope.snoozeArray 111111111111',JSON.stringify($scope.snoozeArray))

            event.stopPropagation();
            $('#openSnoozedPopUpModal').modal('open');
        };

        $scope.submitSnoozeTask = function () {
            $scope.spinner = true;
            var crmSnoozeIds={
                allSubTasksIds: $scope.snoozeArray
            };
            //console.log('crmSnoozeIds query',JSON.stringify(crmSnoozeIds));
            var serviceQuery = {
                number:'one'
            };
            mySharedService.prepForPublish(serviceQuery);
            tasksService.crmSnoozeTask(crmSnoozeIds).then(function (crmresult) {
                //console.log('CRM TEMPLATE RESULT FOR ALL FIRST================>',JSON.stringify(crmresult));
                if(crmresult.data.response === 'true'){
                    if(Store.customerview === true){
                        console.log('calling customerview ');
                        $scope.chosenCustomerForTemp($scope.tempquery1);
                    }else if(Store.assingeeview === true){
                        console.log('calling assignQuery ');
                        $scope.chooseAssignee($scope.assignQuery);
                    }else{
                        console.log('calling date ');
                        $scope.searchByDate($scope.datequery)
                    }
                }else{
                    console.log('field');
                }
                $scope.spinner = false;
            }).catch(function (error) {
                console.log('error',error);
            });
        };

        $scope.unsnoozeArray = [];
        $scope.openUnSnoozedTaskModal = function (task,event) {
            //console.log('snooz 111111111111',JSON.stringify(task))
            //console.log('snooz 111111111111',JSON.stringify(task))
            $scope.tempquery2 = {
                "name":task.customername,
                "user_id":task.data[0].customer_id
            }

            if(Store.assingeeview){
                $scope.assignQuery = {
                    'assigned_to' :$scope.queryForSearch.query
                }
                //console.log('$scope.assignQuery=========',JSON.stringify($scope.assignQuery));
            }
            $scope.datequery = $scope.queryDate;
           // console.log('$scope.datequery=========',JSON.stringify($scope.datequery));
            var filterSnooze = _.filter(task.data,function (task) {
                console.log('ids  111',JSON.stringify(task))
                $scope.unsnoozeArray.push(task.task_id);
                return task.task_id;
            });
            //console.log('$scope.snoozeArray 111111111111',JSON.stringify($scope.snoozeArray))

            event.stopPropagation();
            $('#openUnSnoozedPopUpModal').modal('open');
        }

        $scope.submitUnSnoozeTask = function () {
            $scope.spinner = true;
            var crmSnoozeIds={
                allSubTasksIds: $scope.unsnoozeArray,
                status:'N'
            };
            console.log('crmSnoozeIds query',JSON.stringify(crmSnoozeIds));
            var serviceQuery = {
                number:'one'
            };
            mySharedService.prepForPublish(serviceQuery);
            tasksService.crmSnoozeTask(crmSnoozeIds).then(function (crmresult) {
                //console.log('CRM TEMPLATE RESULT FOR ALL FIRST================>',JSON.stringify(crmresult));
                if(crmresult.data.response === 'true'){
                    if(Store.customerview === true){
                        //console.log('calling customerview ');
                        $scope.chosenCustomerForTemp($scope.tempquery2);
                    }else if(Store.assingeeview === true){
                        //console.log('calling assignQuery ');
                        $scope.chooseAssignee($scope.assignQuery);
                    }else{
                        //console.log('calling date ');
                        $scope.searchByDate($scope.datequery)
                    }
                }else{
                    console.log('field');
                }
                $scope.spinner = false;
            }).catch(function (error) {
                console.log('error',error);
            });
        };


        $scope.openModalTempTasks = function (one,alltasks,index) {

            if(one.subtask_completed === 'Y'){
                console.log('do nuthing');
            }else{
                $scope.openModalFunction(one, alltasks, index);
            }

        };

        $scope.taskIdNot =[];
        $scope.taskIds = [];
        $scope.markAllTaskCompArray =[];
        $scope.markAllTaskCompArray1 =[];

        ////open modal for upcoming FIRST///////
        $scope.openModalFunction = function(one, alltasks, index){
            console.log('calling on change',JSON.stringify(one));
            $scope.assingeeName = {
                'assigned_to' :one.assigned_to
            }
            $scope.getCustdata = {
                "name":one.customer_name,
                "user_id":one.customer_id
            }
            $scope.datequery = $scope.queryDate;
           // console.log('$scope.datequery=========',JSON.stringify($scope.datequery));

            $scope.taskIdNot.push(one.task_id);
            $scope.taskIds.push(one.task_id);
            //console.log('$scope.taskIds',JSON.stringify($scope.taskIds));
            //console.log('alltasks.data[i] length',JSON.stringify(alltasks.data.length));
            $scope.taskLength = alltasks.data.length;
            $scope.allTaskPrivious = alltasks.data.slice();
            $scope.indexLength = index+1;
            // console.log('$scope.taskLength',JSON.stringify($scope.taskLength));
            // console.log('$scope.indexLength',JSON.stringify($scope.indexLength));
            //console.log('alltasks-----------------------',JSON.stringify(alltasks));
            //console.log('index',JSON.stringify(index));
            var count = 0;
            var previousCheck=false;
            var checkForPrivious;
            var checkIndex;

            //////
            function checkForPreviousTask() {
                var filterNotCompleted1 = _.filter($scope.allTaskPrivious,function (oneTask2) {
                    $scope.markAllTaskCompArray.push(oneTask2.task_id);
                    return oneTask2.task_id !==  one.task_id;

                });
                var filterPriviousIds2 = _.filter(filterNotCompleted1,function (one1) {
                    if(one1.subtask_completed === 'Y'){

                    }else{
                        return one1.task_id;
                    }
                });
                // console.log('filterPriviousIds <<<<11<<<<<<<,',JSON.stringify(filterPriviousIds2));
                // console.log('filterPriviousIds <<<<<11<<<<<<,',JSON.stringify(filterPriviousIds2.length));
                if(filterPriviousIds2.length === 0){
                    previousCheck = true;
                }else{
                    previousCheck = false;
                }

                return previousCheck;
            }

            function checkForPreviousIndex(index) {
                for(var i = 0 ; i < index ; i++){
                    if(alltasks.data[i].subtask_completed === 'N'){
                        checkIndex=false;
                    }else{
                        checkIndex=true;
                    }

                }
                return checkIndex;
            }
            //////

            ///////check for last task//////
            if(alltasks.data.length === index+1){
                //console.log('calling last one check===========');
                if(checkForPreviousTask()){
                    // console.log('last task n abobe all completed 111111111');
                    $('#openModalForTempTasksCnf').modal('open');
                }else{
                    //console.log('last task n abobe all not completed 2222222------------');
                    for(var k = 0 ; k < index ; k++ ){
                        if(alltasks.data[k].subtask_completed === 'N'){
                            $scope.taskIds.push(alltasks.data[k].task_id);
                        }
                    }
                    $('#openModalForPreviuosTasks').modal('open');
                }
            }else{
                // console.log('3333333333333333333------------');
                var filterNotCompleted = _.filter(alltasks.data,function (oneTask) {
                    // $scope.markAllTaskCompArray.push(oneTask.task_id);
                    return oneTask.task_id !==  one.task_id;

                });
                // console.log('filterNotCompleted-----------------------',JSON.stringify(filterNotCompleted));
                var lastTask = false;
                for(var k = 0 ; k < filterNotCompleted.length;k++){
                    if(filterNotCompleted[k].subtask_completed === 'Y'){
                        lastTask = true;
                    }else{
                        lastTask = false;
                        break;
                    }
                }
                if(lastTask){
                    //console.log('coming here')
                    var filterNotCompletedOne = _.filter(alltasks.data,function (fil) {
                        $scope.markAllTaskCompArray.push(fil.task_id);
                        return fil.task_id !==  one.task_id;

                    });
                    $scope.indexLength = alltasks.data.length;
                    $('#openModalForTempTasksCnf').modal('open');
                }else{
                    if(index === 0){
                        checkIndex=true;
                    }
                    if(checkForPreviousIndex(index)){
                        //console.log('calling 1111111')
                        $('#openModalForTempTasksCnf').modal('open');
                    }else{
                        //console.log('calling 222222222 in else');
                        for(var j = 0 ; j < index ; j++ ){
                            if(alltasks.data[j].subtask_completed === 'N'){
                                $scope.taskIds.push(alltasks.data[j].task_id);
                            }
                        }
                        $('#openModalForPreviuosTasks').modal('open');
                    }
                    var spliceIndexArray =  $scope.allTaskPrivious.splice(index+1, alltasks.data.length);
                    //console.log('spliceIndexArray<<<<<<<<<<<,',JSON.stringify(spliceIndexArray));
                    var filterPriviousIds = _.filter(spliceIndexArray,function (onePrivious) {
                        if(onePrivious.subtask_completed === 'Y'){

                        }else{
                            return onePrivious.task_id;
                        }
                    });
                    // console.log('filterPriviousIds 22222222<<<<<<<<<<<,',JSON.stringify(filterPriviousIds));
                    // console.log('filterPriviousIds 22222222222<<<<<<<<<<<,',JSON.stringify(filterPriviousIds.length));
                    if(filterPriviousIds.length === 0){
                        checkForPrivious = true;
                    }else{
                        checkForPrivious = false;
                    }

                    if(checkForPrivious){
                        var filterIdForPriviousLast = _.filter(alltasks.data,function (oneTask1) {
                            $scope.markAllTaskCompArray.push(oneTask1.task_id);
                            return oneTask1;

                        });
                        //  console.log('checkForPrivious calling');
                        //  console.log('checkForPrivious ids',JSON.stringify($scope.markAllTaskCompArray));
                        $scope.indexLength = alltasks.data.length;
                        $('#openModalForPreviuosTasks').modal('open');
                        $('#openModalForTempTasksCnf').modal('close');
                    }
                }
            }


            //console.log('111111 before $scope.markAllTaskCompArray>>>>>>>>>.',JSON.stringify($scope.markAllTaskCompArray));
            //console.log('111111 before $scope.markAllTaskCompArray>>>>>>>>>.',JSON.stringify($scope.markAllTaskCompArray.length));
            if($scope.markAllTaskCompArray.length > 0){
                $scope.markAllTaskCompArray = _.union($scope.markAllTaskCompArray1, $scope.markAllTaskCompArray);
            }
            // console.log('222222 after $scope.finalTestArray>>>>>>>>>.',JSON.stringify($scope.markAllTaskCompArray));
            // console.log('final taskIds>>>>>>>>>.',JSON.stringify($scope.taskIds));
            // console.log('final taskIds>>>>>>>>>.',JSON.stringify($scope.taskIds.length));
            // $('#openModalForTempTasksCnf').modal('open');
        };

        $scope.taskData ={};
        ////////////First///////////////
        $scope.taskStatusOfSubTask = function () {
            var queryUpdateComment
            //console.log('print 11111111111',$scope.taskData);
            //console.log('$scope.taskLength',JSON.stringify($scope.taskLength));
            //console.log('$scope.indexLength',JSON.stringify($scope.indexLength));
            if($scope.taskLength === $scope.indexLength){
                //console.log('calling to update comment fiels',JSON.stringify($scope.markAllTaskCompArray));
                queryUpdateComment = {
                    allIdsForComment:$scope.markAllTaskCompArray,
                    comment:$scope.taskData.taskNote1
                };
            }
            //console.log('queryUpdateComment3333333333333',JSON.stringify(queryUpdateComment));
            tasksService.crmUpdateComments(queryUpdateComment).then(function (crmresult) {
                // console.log('CRM COMMENT RESULT FIRST================>',JSON.stringify(crmresult));
                $scope.taskIds = [];
                $scope.markAllTaskCompArray=[];
                $scope.taskData={};
                if(crmresult.data.response === 'true'){
                    if(Store.customerview === true){
                        //console.log('calling customerview ');
                        $scope.chosenCustomerForTemp($scope.getCustdata);
                    }else if(Store.assingeeview === true){
                        //console.log('calling assignQuery ');
                        $scope.chooseAssignee($scope.assingeeName);
                    }else{
                        //console.log('calling date ');
                        $scope.searchByDate($scope.datequery)
                    }
                }else{
                    console.log('field');
                }
            }).catch(function (error) {
                console.log('error',error);
            });
            var crmUpdate={
                subtask_name: $scope.taskIds
            };
            // console.log('crmUpdate',JSON.stringify(crmUpdate));
            tasksService.crmUpdateTemplateTasks(crmUpdate).then(function (crmresult) {
                //console.log('CRM TEMPLATE FIRST================>',JSON.stringify(crmresult));
                if(crmresult.data.response === 'true'){
                    if(Store.customerview === true){
                        console.log('calling customerview ');
                        $scope.chosenCustomerForTemp($scope.getCustdata);
                    }else if(Store.assingeeview === true){
                        console.log('calling assignQuery ');
                        $scope.chooseAssignee($scope.assingeeName);
                    }else{
                        console.log('calling date ');
                        $scope.searchByDate($scope.datequery)
                    }
                }else{
                    console.log('field');
                }
                $scope.taskIds = [];
                $scope.markAllTaskCompArray=[];
            }).catch(function (error) {
                console.log('error',error);
            });
        };

        $scope.taskStatusOfSubTaskForAllCompleted = function () {
            //console.log('calling taskStatusOfSubTaskForAllCompleted');
            var crmUpdatetest={
                allSubTasksIds: $scope.markAllTaskCompArray
            };
            var serviceQuery = {
                number:'one'
            };
            mySharedService.prepForPublish(serviceQuery);
            //console.log('taskStatusOfSubTaskForAllCompleted query',JSON.stringify(crmUpdatetest));
            tasksService.crmUpdateTemplateTasksForAllIds(crmUpdatetest).then(function (crmresult) {
                //console.log('CRM TEMPLATE RESULT FOR ALL FIRST================>',JSON.stringify(crmresult));
                $scope.taskIds = [];
                $scope.markAllTaskCompArray=[];
                if(crmresult.data.response === 'true'){
                    if(Store.customerview === true){
                        console.log('calling customerview ');
                        $scope.chosenCustomerForTemp($scope.getCustdata);
                    }else if(Store.assingeeview === true){
                        console.log('calling assignQuery ');
                        $scope.chooseAssignee($scope.assingeeName);
                    }else{
                        console.log('calling date ');
                        $scope.searchByDate($scope.datequery)
                    }
                }else{
                    console.log('field');
                }
            }).catch(function (error) {
                console.log('error',error);
            });
        };

        $scope.note = {};

        $scope.AddNoteForTask = function (data,index) {
            //console.log('index',index);
            $scope.noteQuerydata = {
                "name":data.customer_name,
                "user_id":data.customer_id
            }
            $scope.noteAssignee = {
                'assigned_to' :data.assigned_to
            }
            $scope.TaskIndex = index;
            console.log('note data',JSON.stringify(data));
            $scope.note.taskNote1 = data.note;
            //console.log('$scope.note',$scope.note);
            $scope.noteTaskId = data.task_id;
            $('#openNotePopUpModal').modal('open');
        };

        $scope.submitNoteForTask = function (data,index) {
            var taskId = $scope.noteTaskId
            // console.log('taskId111111',taskId);
            // console.log('$scope.TaskIndex',$scope.TaskIndex);
            console.log('task notr',JSON.stringify(data));
            var noteQuery = {
                task_id:taskId,
                note:data.taskNote1
            }
            //console.log('noteQuery333333333333: ',JSON.stringify(noteQuery));
            tasksService.crmUpdateNoteForTemp(noteQuery).then(function (crmresult) {
                //console.log('CRM TEMPLATE RESULT FOR ALL FIRST================>',JSON.stringify(crmresult));
                if(crmresult.data.response === 'true'){
                    if(Store.customerview === true){
                        //console.log('calling customerview ');
                        $scope.chosenCustomerForTemp($scope.noteQuerydata);
                        var anchorElement = $('#tooltip' + $scope.TaskIndex);
                        anchorElement.attr('data-tooltip', data.taskNote1);
                        anchorElement.tooltip();
                        $scope.note = {};
                    }else if(Store.assingeeview === true){
                        //console.log('calling assignQuery ');
                        $scope.chooseAssignee($scope.noteAssignee);
                        var anchorElement = $('#tooltip' + $scope.TaskIndex);
                        anchorElement.attr('data-tooltip', data.taskNote1);
                        anchorElement.tooltip();
                        $scope.note = {};
                    }else{
                        //console.log('calling date ');
                        $scope.searchByDate($scope.datequery)
                        var anchorElement = $('#tooltip' + $scope.TaskIndex);
                        anchorElement.attr('data-tooltip', data.taskNote1);
                        anchorElement.tooltip();
                        $scope.note = {};
                    }
                }else{
                    console.log('field');
                }
            }).catch(function (error) {
                console.log('error',error);
            });
        };

        $scope.clearDataOfTaskIdsPrivious = function () {
            //console.log('calling clear');
            var crmUpdateNot={
                subtask_name: $scope.taskIdNot
            };
            //console.log('crmUpdateNot',JSON.stringify(crmUpdateNot));
            tasksService.crmUpdateTemplateTasks(crmUpdateNot).then(function (crmresult) {
                //console.log('CRM TEMPLATE FIRST================>',JSON.stringify(crmresult));
                $scope.taskIds = [];
                $scope.markAllTaskCompArray=[];
                $scope.taskIdNot =[];
                if(crmresult.data.response === 'true'){
                    if(Store.customerview === true){
                        console.log('calling customerview ');
                        $scope.chosenCustomerForTemp($scope.getCustdata);
                    }else if(Store.assingeeview === true){
                        console.log('calling assignQuery ');
                        $scope.chooseAssignee($scope.assingeeName);
                    }else{
                        console.log('calling date ');
                        $scope.searchByDate($scope.datequery)
                    }
                }else{
                    console.log('field');
                }
            }).catch(function (error) {
                console.log('error',error);
            });
            $scope.taskIds = [];
            $scope.markAllTaskCompArray=[];
            $scope.markAllTaskCompArray1 =[];
            $scope.taskData = {};

        };

        $scope.clearDataOfTaskIds = function () {
            $scope.taskIds = [];
            $scope.markAllTaskCompArray=[];
            $scope.markAllTaskCompArray1 =[];
            $scope.taskData = {};

        };

        $scope.closeindividualSRModal = function () {
            $scope.other ={};
            $scope.otherSubTasksData =[];
            $scope.errorMessage1='';
            $scope.errorMessage = '';
            $scope.taskname ='';
            $scope.customername ='';
            $scope.statusfresh = ''
            $scope.TempSubTasksData = [];
        };

        $scope.tasksIdsToDelete = [];
        $scope.openDeleteTaskModal = function (task,event) {
            console.log('delet  111111111111',JSON.stringify(task))
            $scope.cName = task.next.customer_name;
            $scope.cid = task.next.customer_id;

            var filterSnooze = _.filter(task.data,function (task) {
                console.log('ids  111',JSON.stringify(task))
                $scope.tasksIdsToDelete.push(task.task_id);
                return task.task_id;
            });
            console.log('$scope.tasksIdsToDelete 111111111111',JSON.stringify($scope.tasksIdsToDelete))

            event.stopPropagation();
            $('#openTaskRemovePopUpModalSearch').modal('open');
        }

        $scope.submitTaskRemove = function () {
            $scope.spinner = true;
            var crmSnoozeIds={
                allSubTasksIds: $scope.tasksIdsToDelete
            };
            var tempquery = {
                "name":$scope.cName,
                "user_id":$scope.cid
            }
            console.log('tempquery',JSON.stringify(tempquery));
            tasksService.crmRemoveTask(crmSnoozeIds).then(function (crmresult) {
                //console.log('CRM TEMPLATE RESULT FOR ALL FIRST================>',JSON.stringify(crmresult));
                setTimeout(function(){
                    $scope.tasksIdsToDelete = [];
                    $scope.cName ='';
                    $scope.cid='';
                    $scope.chosenCustomerForTemp(tempquery);
                    $scope.spinner = false;
                }, 1000);
            }).catch(function (error) {
                console.log('error',error);
            });
        }

        $scope.removetempData = function (data,index) {
            $scope.spinner = true;
            console.log('data',JSON.stringify(data));
            var tempquery = {
                "name":$scope.taskdata.customername,
                "user_id":data.customer_id
            };
            $scope.assingeeName = {
                'assigned_to' :$scope.taskdata.assigneename
            }
            //console.log('$scope.queryDate',JSON.stringify($scope.queryDate));
            //$scope.spinner = true;
            //console.log('TempSubTasksData', JSON.stringify($scope.TempSubTasksData));
            //console.log('tempquery',JSON.stringify(tempquery));
            var removedtask = $scope.TempSubTasksData.splice(index, 1);
            var removeId = removedtask[0].task_id;
            $scope.TempSubTasksData.splice(index, 1);
            //console.log('removed', JSON.stringify($scope.TempSubTasksData));
            // console.log('removeId',JSON.stringify(removeId));
            var removeIdTempTask = {
                task_id:removeId
            }
            tasksService.crmUpdateRemoveTempTask(removeIdTempTask).then(function (crmresult) {
               // console.log('CRM OTHER RESULT================>',JSON.stringify(crmresult));
                if(crmresult.data.response === 'true'){
                    if(Store.customerview === true){
                       // console.log('calling customerview ');
                        $scope.chosenCustomerForTemp(tempquery);
                    }else if(Store.assingeeview === true){
                        //console.log('calling assignQuery ');
                        $scope.chooseAssignee($scope.assingeeName);
                    }else{
                       // console.log('calling date ');
                        $scope.searchByDate($scope.queryDate)
                    }
                }else{
                    console.log('field');
                }
                $scope.spinner = false;
            }).catch(function (error) {
                console.log('error',error);
            });

        };

        $scope.removeOthersubtasks = function (data,index) {
            console.log('data',data);
            $scope.spinner = true;
            var tempquery = {
                "name":$scope.taskdata.customername,
                "user_id":data.customer_id
            };
            $scope.assingeeName = {
                'assigned_to' :$scope.taskdata.assigneename
            }
            var removedtask = $scope.otherSubTasksData.splice(index, 1);
            //console.log('removedtask',JSON.stringify(removedtask));
            var removeId = removedtask[0].task_id;
            $scope.otherSubTasksData.splice(index, 1);
            //console.log('removed', JSON.stringify($scope.otherSubTasksData));
            //console.log('removeId',JSON.stringify(removeId));
            var removeIdTempTask = {
                task_id:removeId
            }
            tasksService.crmUpdateRemoveTempTask(removeIdTempTask).then(function (crmresult) {
                //console.log('CRM OTHER RESULT================>',JSON.stringify(crmresult));
                if(crmresult.data.response === 'true'){
                    if(Store.customerview === true){
                        console.log('calling customerview ');
                        $scope.chosenCustomerForTemp(tempquery);
                    }else if(Store.assingeeview === true){
                        console.log('calling assignQuery ');
                        $scope.chooseAssignee($scope.assingeeName);
                    }else{
                        console.log('calling date ');
                        $scope.searchByDate($scope.queryDate)
                    }
                }else{
                    console.log('field');
                }
                $scope.spinner = false;
            }).catch(function (error) {
                console.log('error',error);
            });

        };

        $scope.clickedColSearch = function (index) {
            $scope.selected1 = index;
           // console.log('calling collapsible',index);
            $scope.backgroundImageInfosearch='#C2DFFF';
        };


    }]);
