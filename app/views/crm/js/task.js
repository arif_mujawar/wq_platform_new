/**
 * Created by arif on 7/18/17.
 */
/*global moment*/
'use strict';

angular.module('wealthQuotientApp')
    .controller('taskCtrl', ['$scope', '$timeout', '$state', '$window', '$document', '$rootScope', 'searchService', 'registerService', 'loginService','tasksService','mySharedService','serachType','colorClick','colorClicksecond','colorClickThird','colorClickFive','$q','$log','fileUpload', function ($scope, $timeout, $state, $window, $document, $rootScope, searchService, registerService, loginService,tasksService,mySharedService,serachType,colorClick,colorClicksecond,colorClickThird,colorClickFive,$q,$log,fileUpload) {
        //$scope.localStorage = $localStorage;
        localStorage.getItem('activeTab');
        //console.log('localStorage-------------11111111',localStorage);

        $scope.getId1 = function (id) {
            //console.log('id----------------',id);
            localStorage.setItem('activeTab',id);
            //console.log('localStorage--------1111111--------',localStorage);
        };

        $scope.getId2 = function (id) {
            //console.log('id----------------',id);
            localStorage.setItem('activeTab', id);
            //console.log('localStorage--------222222--------',localStorage);
        }
        $scope.getId3 = function (id) {
           // console.log('id----------------',id);
            localStorage.setItem('activeTab', id);
           // console.log('localStorage--------3333333--------',localStorage);
        }
        $scope.getId4 = function (id) {
            //console.log('id----------------',id);
            localStorage.setItem('activeTab', id);
            //console.log('localStorage--------444444--------',localStorage);
        }
        $scope.getId5 = function (id) {
           // console.log('id----------------',id);
            localStorage.setItem('activeTab', id);
           // console.log('localStorage--------555555--------',localStorage);
        }
        var activeTab = localStorage.getItem('activeTab');
        //console.log('activeTab--------555555555555--------',activeTab);
        $timeout(function(){
            if(activeTab !== '#alltasks'){
                $('a[data-target="'+activeTab+'"]').trigger('click');
            }
        });

        //console.log('$scope.localStorage--------------',localStorage);
        $scope.user = {
            name: 'awesome user'
        };


        const newOne = () => {
            console.log("Hello World..!");
        }

        newOne()

        $scope.disablaStatusButton = false;
        $scope.otherTaskInputForm = false;
        $scope.subTasksData = [];
        $scope.task = {};
        $scope.createsubtask = '';
        $scope.showSubtasksTable = false;
        $scope.dateClickFlag = false;
        $scope.daysselect = ["0","1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30","31"];
        $scope.searchByType = function (type) {
            //console.log('type---------------------',type);
            var serviceType = {
                type:type
            };
            serachType.prepForPublish(serviceType);
        }

        // $rootScope.$on("viewTasksEvent", function (event, args) {
        //     console.log('from searchTask ctrl >>>', args);
        //     // $scope.fromMyClientsCheck = true;
        //     var custFromMC = args;
        //     custFromMC.name = custFromMC.first_name + " " + custFromMC.last_name;
        //     console.log('Pass this cust Obj to func', custFromMC);
        //     $rootScope.$broadcast("searchTasksForClient", custFromMC);
        //
        //     $scope.searchByType('customer'); //always type = 'customer', from My-Clients
        //     // $scope.chosenCustomerForTemp(custFromMC);
        //     // Store.customerview = true;
        // });


        // $scope.currentPage = 1;
        // $scope.pageSize = 5;
        // $scope.total = 0
        //$scope.$digest()

        $scope.totalItems = 0;
        $scope.viewby = 5;

        $scope.currentPage = 1;
        $scope.itemsPerPage = $scope.viewby;
        $scope.maxSize = 5; //Number of pager buttons to show

        /////////CRM TASKS///////

        $scope.cartsDataUpComing = [];
        $scope.dueTaskToShow = [];
        $rootScope.spinner = false;
        $scope.spinner = false;
        getCrmAllTasksDisplay();
        function getCrmAllTasksDisplay(query) {
            $scope.spinner = true;
            $rootScope.spinner = true;
            //console.log('calling getCrmAllTasksDisplay 11111111111111111111111');
            tasksService.getAllCrmTasks({query:query}).then(function (clientsResult) {
                //console.log('getAllCrmTasks================>',JSON.stringify(clientsResult.data.response));
                var crmAllTasks = clientsResult.data.response;

                if(crmAllTasks !== 0){
                    crmAllTasks = crmAllTasks.filter(function (ele) {
                        var tempArr = Object.keys(ele);
                        //console.log('the keys are ', tempArr);
                        tempArr.map(function (arrEle, i) {
                            // console.log('the arrEle are ', arrEle);
                            if (ele[arrEle] === null) {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                            if (ele[arrEle] === "Invalid date") {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                            if (ele[arrEle] === "") {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                        });
                        //console.log(ele);
                        return ele;
                    });
                }
                //console.log('crmAllTasks-------66666666666666-----------afterrrrrrr', JSON.stringify(crmAllTasks));

                function arrayFromObject(obj) {
                    var arr = [];
                    for (var i in obj) {
                        arr.push(obj[i]);
                    }
                    return arr;
                }

                function groupBy(list, fn) {
                    var groups = {};
                    for (var i = 0; i < list.length; i++) {
                        var group = JSON.stringify(fn(list[i]));
                        if (group in groups) {
                            groups[group].push(list[i]);
                        } else {
                            groups[group] = [list[i]];
                        }
                    }
                    return arrayFromObject(groups);
                }
                var crmAllTasksfinal = groupBy(crmAllTasks, function(item) {
                    //console.log('8888888888888888888',JSON.stringify(item));

                    return [item.task_title, item.customer_name,item.isSubtask];
                });
                var cartsData =[];
                for(var K = 0 ; K < crmAllTasksfinal.length ; K++){

                    var task_title = '';
                    var customername = '';
                    var isSubtask = '';
                    for(var j = 0 ; j < crmAllTasksfinal[K].length ; j++){
                        task_title = crmAllTasksfinal[K][j].task_title;
                        customername = crmAllTasksfinal[K][j].customer_name;
                        isSubtask = crmAllTasksfinal[K][j].isSubtask;

                    }
                    //console.log('filter subtaks777777777777',JSON.stringify(crmAllTasksfinal))
                    cartsData.push({task_title : task_title,customername:customername,isSubtask:isSubtask,data : crmAllTasksfinal[K]});
                }

                cartsData = _.each(cartsData,function (data1) {
                    var filterdata2 = _.filter(data1.data,function (data2) {
                         return data2.subtask_completed === 'N';
                    })
                    var filterDates =[];
                    _.each(filterdata2,function (sb) {
                        filterDates.push(sb.dueDate);
                    })
                    var dates = _.map(filterDates,function(date){
                        return moment(date);
                    });
                    var start = _.min(dates);
                    var findObject = _.find(filterdata2, function(item) {
                        //console.log('item print hora',JSON.stringify(item.dueDate));
                        if(moment(item.dueDate).isSame(start)){
                            //alert('success');
                            return item;
                        }

                    });
                    data1.next = findObject;

                });
                var cartsDataUpComing1;
                 cartsDataUpComing1 = _.map(cartsData,function (due1) {
                    var dueDateCompareUp;
                    var isoDate = "";
                    var todaysDateCheckUp = moment(new Date()).startOf("day").add(1,'days').subtract({hours:18,minutes:30});
                    //console.log('todaysDateCheckUp>>>>>>coming>>>>>.',JSON.stringify(todaysDateCheckUp));
                    if(due1.isSubtask === 'N'){
                        //moment(due1.data[0].dueDate
                        isoDate = moment(due1.data[0].dueDate, 'MMMM/Do/YYYY').format();
                        dueDateCompareUp =moment(isoDate).toISOString();
                        //console.log('due1 no subtask',JSON.stringify(dueDateCompareUp));

                    }else{
                        dueDateCompareUp = moment(due1.next.dueDate).startOf("day").add(1,'days').subtract({hours:18,minutes:30});
                      // console.log('due1>>>>>coming>>>>>>.',JSON.stringify(due1.next));

                    }
                    if(moment(dueDateCompareUp).isAfter(todaysDateCheckUp)){
                        return due1;
                    }

                });

                 var upcomingDAtaProcess;
                upcomingDAtaProcess = cartsDataUpComing1.filter(function(t2){
                    //console.log('$scope.t2<<<<<<<<<<<<<',JSON.stringify(t2));
                    return t2 !== undefined;
                });

                upcomingDAtaProcess.sort(function(a,b){
                    return new Date(b.next.dueDate) - new Date(a.next.dueDate);
                });

                for(var i = 0 ; i < upcomingDAtaProcess.length ; i++){
                    //console.log('$scope.cartsDataUpComing222222222',JSON.stringify($scope.cartsDataUpComing[i]));
                    upcomingDAtaProcess[i].data = _.sortBy(upcomingDAtaProcess[i].data, 'sort_id');
                }

                //console.log('upcomingDAtaProcess<<<<<<<99999999999999<<<<<<',JSON.stringify(upcomingDAtaProcess.length));
                $scope.getAllTaskTitles =[];
                for(var k = 0 ; k <upcomingDAtaProcess.length ; k++ ){
                    var tObject = {}
                    //console.log('upcomingDAtaProcess namessssssss',upcomingDAtaProcess[k].task_title)
                    tObject.task_title = upcomingDAtaProcess[k].task_title;
                    $scope.getAllTaskTitles.push(tObject);
                }
                //console.log('$scope.getAllTaskTitles------------',$scope.getAllTaskTitles);
                $scope.upComingCount = upcomingDAtaProcess.length;
                $scope.cartsDataUpComing = upcomingDAtaProcess;
                //console.log('$scope.cartsDataUpComing<<<<<<<99999999999999<<<<<<',JSON.stringify($scope.cartsDataUpComing));



                // localStorage.setItem('comingCount',$scope.cartsDataUpComing.length);
                // var getCountUp = localStorage.getItem('comingCount');
                // console.log('getCountUp<<<<<<<<<<<<<',JSON.stringify(getCountUp));

                  ////////////// overdue///////////
                    var showToast
                    var dueTask;
                    dueTask = _.map(cartsData,function (due1) {
                    var dueDateCompare;
                    var isoDate = "";

                    var todaysDateCheck = moment(new Date()).startOf("day").add(1,'days').subtract({hours:18,minutes:30});
                    //console.log('todaysDateCheck>>>>>due>>>>>>.',JSON.stringify(todaysDateCheck));
                    if(due1.isSubtask === 'N'){
                        //moment(due1.data[0].dueDate
                        isoDate = moment(due1.data[0].dueDate, 'MMMM/Do/YYYY').format();
                        dueDateCompare =moment(isoDate).toISOString();
                        //console.log('due1 no subtask',JSON.stringify(dueDateCompare));

                    }else{
                        dueDateCompare = moment(due1.next.dueDate).startOf("day").add(1,'days').subtract({hours:18,minutes:30});
                        //console.log('due1>>>>>>>due>>>>.',JSON.stringify(due1.next));

                    }
                    if(moment(dueDateCompare).isSameOrBefore(todaysDateCheck)){
                        return due1;
                    }

                });

               

                var dueDAtaProcess;
                dueDAtaProcess = dueTask.filter(function(t2){
                    //console.log('$scope.t2<<<<<<<<<<<<<',JSON.stringify(t2));
                    return t2 !== undefined;
                });
                dueDAtaProcess.sort(function(a,b){
                    return new Date(b.next.dueDate) - new Date(a.next.dueDate);
                });
                //console.log('$scope.dueTaskToShow',JSON.stringify($scope.dueTaskToShow));
                for(var k = 0 ; k < dueDAtaProcess.length ; k++){
                    dueDAtaProcess[k].data = _.sortBy(dueDAtaProcess[k].data, 'sort_id');
                }

                $scope.getAllTaskTitlesDue =[];
                for(var v = 0 ; v <dueDAtaProcess.length ; v++ ){
                    var tObjectDue = {}
                    //console.log('upcomingDAtaProcess namessssssss',upcomingDAtaProcess[k].task_title)
                    tObjectDue.task_title = dueDAtaProcess[v].task_title;
                    $scope.getAllTaskTitlesDue.push(tObjectDue);
                }
                $scope.dueTaskToShow = dueDAtaProcess;


                //console.log('$scope.getAllTaskTitlesDue<<<<<<<<<<<<<',JSON.stringify($scope.getAllTaskTitlesDue));


                var getToastCheck = localStorage.getItem('toastCheck');
                //console.log('getToastCheck=================',JSON.stringify(getToastCheck));


                if($scope.toastFromSearch){
                    console.log('from serach do nuthing',$scope.toastFromSearch);
                }else{
                    if(getToastCheck === 'not'){
                        console.log('do nuthing 11111');
                        localStorage.setItem('prev_dueCount',$scope.dueTaskToShow.length - 1);
                    }else if(getToastCheck === ''){
                        console.log('going in elae 222222222');
                        if(!localStorage.getItem('prev_dueCount')){
                            localStorage.setItem('prev_dueCount',$scope.dueTaskToShow.length);
                        }
                        localStorage.setItem('current_dueCount', $scope.dueTaskToShow.length)

                        if(parseInt(localStorage.getItem('prev_dueCount')) > parseInt(localStorage.getItem('current_dueCount'))){
                            //console.log('prev------------',localStorage.getItem('prev_dueCount'));
                            //console.log('current------------',localStorage.getItem('current_dueCount'));
                            $timeout(function(){
                                $rootScope.successToast("Task moved to Upcoming tasks");
                            },2000)
                            localStorage.setItem('prev_dueCount',$scope.dueTaskToShow.length);
                            localStorage.setItem('toastCheck','');
                        }else{
                            localStorage.setItem('prev_dueCount',$scope.dueTaskToShow.length);
                            localStorage.setItem('toastCheck','');
                        }
                        localStorage.setItem('toastCheck','');
                    }else{
                        console.log('going in last 333333333333');
                        localStorage.setItem('toastCheck','');
                    }

                }



            }).then(function () {
                console.log('getCrmAllTasksDisplay api then calling');
                localStorage.setItem('toastCheck','');
                $scope.numberOfPagesUp = function() {
                    return Math.ceil($scope.cartsDataUpComing.length / $scope.pageSize);
                };
                $scope.numberOfPagesDue = function() {
                    return Math.ceil($scope.dueTaskToShow.length / $scope.pageSize);
                };

                $scope.spinner = false;
                $rootScope.spinner = false;

            }).catch(function (error) {
                $scope.spinner = false;
                $rootScope.spinner = false;
                console.log('error',error);
            });

        }


        $scope.$on('handlePublish', function() {
            console.log('calling      publish11111111111');
            $scope.sharedmessage  = mySharedService.sharedmessage;
            if($scope.sharedmessage.number === 'one'){
                setTimeout(function(){
                    $scope.cartsDataUpComing = [];
                    $scope.dueTaskToShow = [];
                    getCrmAllTasksDisplay();
                }, 2000);

            }
            //console.log('$scope.sharedmessage11111111111111111111',JSON.stringify($scope.sharedmessage));
            //console.log('$scope.sharedmessage11111111111111111111',JSON.stringify($scope.sharedmessage.number));
        });



        $scope.contact ='';
        $scope.addSubTasks = function(index) {
            $scope.validationError =''
            //console.log('calling index',index);
            if($scope.templateName){
                $scope.errorMessage1 ='';
            }
            var ind = $scope.subTasksData.length;
            $timeout(function(){
                document.getElementById('myinput'+(ind-1)).blur();
                document.getElementById('myinput'+ind).focus();
            });

            $scope.testStatus = {};
            $scope.subTasksData[0].option = "first_task";


            //console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.subTasksData));

            for(var i = 0 ; i < $scope.subTasksData.length; i++){
                if($scope.subTasksData[i].days === undefined){
                       $scope.subTasksData[i].days = 0;
                 }
                if($scope.subTasksData[i].option === null){
                    $scope.subTasksData[i].option = "first_task";
                }
                $scope.subTasksData[i].sort_id = index;
            }

            $scope.subTasksData.push($scope.testStatus);
           // console.log('$scope.subTasksData>>>>>after>>>>>>>..',JSON.stringify($scope.subTasksData));
           //  if($scope.subTasksData.length >= 100){
           //      $rootScope.errorToast('can not add more that 100 Subtasks');
           //      $scope.disablaStatusButton = true;
           //  }

            _.each($scope.subTasksData, function (item, index) {
               // console.log('item sort',JSON.stringify(item));
               // console.log('index sort',index);
                item.sort_id = index + 1;
                item.index = index;
                //console.log('ye dekh pehle',JSON.stringify($scope.subTasksData));
            });
            var list123 = document.getElementById("sortThisTask");
            var sortable = new Sortable(list123, {
                draggable: ".item",
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent
                    //console.log('ent index11111111 oldIndex',evt.oldIndex);
                    //console.log('ent index11111111 newIndex',evt.newIndex);

                    $scope.subTasksData = move($scope.subTasksData, evt.oldIndex+1, evt.newIndex+1);
                },
                onUpdate: function (/**Event*/evt) {
                    // same properties as onEnd
                    $timeout(function(){
                        console.log('after update calling this', $scope.subTasksData);
                        _.each($scope.subTasksData, function (item, index) {
                            item.sort_id = index + 1;
                            item.index = index;
                        });
                    });
                },
            });
        };

        $scope.addSubTasksBetween = function(index) {
            // console.log('$scope.subTasksData',JSON.stringify($scope.subTasksData));
            // console.log('calling inbetween');
            //console.log('index',index);
            if($scope.templateName){
                $scope.errorMessage1 ='';
            }
            //var ind = $scope.subTasksData.length;
            var ind = index+1;
            console.log('ind-----------',ind);
            $timeout(function(){
                document.getElementById('myinput'+(ind-1)).blur();
                document.getElementById('myinput'+ind).focus();
            });

            $scope.testStatus = {};
            //$scope.subTasksData[index+1].option = "first_task";
            //console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.subTasksData));

            for(var i = 0 ; i < $scope.subTasksData.length; i++){
                if($scope.subTasksData[i].days === undefined){
                    $scope.subTasksData[i].days = 0;
                }
                if($scope.subTasksData[i].option === undefined){
                    $scope.subTasksData[i].option = "first_task";
                }
            }

            $scope.subTasksData.splice(index+1,0,$scope.testStatus);
            //console.log('$scope.subTasksData>>>>>after>>>>>>>..',JSON.stringify($scope.subTasksData));
            // if($scope.subTasksData.length >= 100){
            //     $rootScope.errorToast('can not add more that 100 Subtasks');
            //     $scope.disablaStatusButton = true;
            // }

            _.each($scope.subTasksData, function (item, index) {
                //console.log('item sort',JSON.stringify(item));
               // console.log('index sort',index);
                item.sort_id = index + 1;
                item.index = index;
               // console.log('ye dekh pehle',JSON.stringify($scope.subTasksData));
            });
            var list123 = document.getElementById("sortThisTask");
            //console.log('sortThisTask===========> ',list123);
            var sortable = new Sortable(list123, {
                draggable: ".item",
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent
                    $scope.subTasksData = move($scope.subTasksData, evt.oldIndex+1, evt.newIndex+1)
                    _.each($scope.subTasksData, function (item, index) {
                        item.sort_id = index + 1;
                        item.index = index;
                        //console.log('ye dekh pehle 999999999999999999',JSON.stringify($scope.subTasksData));
                    });
                },
                onUpdate: function (/**Event*/evt) {
                    // same properties as onEnd
                    $timeout(function(){
                        //console.log('after update calling this', $scope.subTasksData);
                        _.each($scope.subTasksData, function (item, index) {
                            item.sort_id = index + 1;
                            item.index = index;
                        });
                    });
                },
            });


        };

        function move(arr, old_index, new_index) {
            while (old_index < 0) {
                old_index += arr.length;
            }
            while (new_index < 0) {
                new_index += arr.length;
            }
            if (new_index >= arr.length) {
                var k = new_index - arr.length;
                while ((k--) + 1) {
                    arr.push(undefined);
                }
            }
            arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
            return arr;
        }


        // $scope.removeStatus = function (index) {
        //     console.log('calling remove');
        //     $scope.subTasksData.splice(index, 1);
        //     //console.log('subTasksData removed', $scope.subTasksData);
        // }

        $scope.removeStatus = function (index) {
            $scope.spinner = true;
            //console.log('callling reome',index);
            //console.log('$scope.subTasksData',JSON.stringify($scope.subTasksData));
            var removedtask = $scope.subTasksData.splice(index, 1);
            var removeId = removedtask[0].subtask_id;
            //console.log('removed', JSON.stringify($scope.subTasksData));
            //console.log('removeId',JSON.stringify(removeId));
            var removeIdTempTask = {
                task_id:removeId
            };
            if(removeId){
               // console.log('------removeIdTempTask----->',removeIdTempTask);
                tasksService.crmremoveTempSubtask(removeIdTempTask).then(function (crmresult) {
                    //console.log('CRM OTHER RESULT================>',JSON.stringify(crmresult));
                    //console.log('----------->',crmresult.data.subtask);
                    if(crmresult.data.subtask === true){
                        getAllTasksTemplates();
                        $scope.spinner = false;
                    }else{
                        console.log('api failed to remove task');
                    }
                }).catch(function (error) {
                    console.log('error',error);
                });
            }else{
                console.log('remove id not there');
                $scope.spinner = false;
            }
        };
        $rootScope.spinner1 = false;
        $scope.cartsDataOfTemp =[];
        getAllTasksTemplates();
        function getAllTasksTemplates(query) {
            $scope.spinner = true;
            $rootScope.spinner = true;
            $rootScope.spinner1 = true;
            //console.log('$rootScope.spinner1------------',$rootScope.spinner1);
            tasksService.getTasksTemplate({query:query}).then(function (data) {
                //console.log('----------------------data',data);
                if(data.data.response !== 0){
                    // console.log('----------------------data',data);
                    // if(data.data.response === 0){
                    //     $scope.cartsDataOfTemp =[];
                    //  }
                    $scope.allTaskTemplates = data.data.response;
                    //console.log('$scope.allTaskTemplates response',JSON.stringify($scope.allTaskTemplates));
                    for (var i = 0; i < data.data.response.length; i++) {
                        // console.log('getAllTasksTemplates------------------asdasdas',$scope.allTaskTemplates);
                        $scope.allTaskTemplates[i].row_created = new Date($scope.allTaskTemplates[i].row_created);
                    }
                    //console.log('getAllTasksTemplates------------------', JSON.stringify($scope.allTaskTemplates));

                    var allTaskTemplatesBefore = $scope.allTaskTemplates.filter(function (ele) {
                        var tempArr = Object.keys(ele);
                        //console.log('the keys are ', tempArr);
                        tempArr.map(function (arrEle, i) {
                            if (ele[arrEle] === 'null') {
                                delete ele[arrEle];
                            }
                        });
                        return ele;
                    });

                    function arrayFromObject(obj) {
                        var arr = [];
                        for (var i in obj) {
                            arr.push(obj[i]);
                        }
                        return arr;
                    }

                    function groupBy(list, fn) {
                        var groups = {};
                        for (var i = 0; i < list.length; i++) {
                            var group = JSON.stringify(fn(list[i]));
                            if (group in groups) {
                                groups[group].push(list[i]);
                            } else {
                                groups[group] = [list[i]];
                            }
                        }
                        return arrayFromObject(groups);
                    }
                    $scope.allTaskTemplates = groupBy(allTaskTemplatesBefore, function(item) {
                        //console.log('8888888888888888888',JSON.stringify(item));

                        return [item.template_name,item.template_id];
                    });
                    for(var K = 0 ; K < $scope.allTaskTemplates.length ; K++){
                        var template_name = '';
                        var row_created = '';
                        var template_id = '';
                        for(var j = 0 ; j < $scope.allTaskTemplates[K].length ; j++){
                            if($scope.allTaskTemplates[K][j].subtask_option === 'first_task'){
                                $scope.allTaskTemplates[K][j].subtask_option = 'Start with first task';
                            }else{
                                $scope.allTaskTemplates[K][j].subtask_option = 'Start after previous task';
                            }
                            template_name = $scope.allTaskTemplates[K][j].template_name;
                            template_id = $scope.allTaskTemplates[K][j].template_id;
                            row_created = $scope.allTaskTemplates[K][j].row_created;

                        }
                        $scope.cartsDataOfTemp.push({template_name : template_name,template_id:template_id,row_created:row_created,data : $scope.allTaskTemplates[K]});

                    }

                    $scope.cartsDataOfTemp.sort(function(a,b){
                        return new Date(b.row_created) - new Date(a.row_created);
                    });
                    //console.log('before sort-----ccccccccc', JSON.stringify($scope.cartsDataOfTemp));
                    for(var s = 0 ; s < $scope.cartsDataOfTemp.length ; s++){
                        //console.log('$scope.cartsDataUpComing222222222',JSON.stringify($scope.cartsDataUpComing[i]));
                        $scope.cartsDataOfTemp[s].data = _.sortBy($scope.cartsDataOfTemp[s].data, 'sort_id');
                    }


                   // console.log('before sort-----ccccccccc', JSON.stringify($scope.cartsDataOfTemp));

                    //console.log('before sort-----ccccccccc', JSON.stringify($scope.cartsDataOfTemp.length));

                    //console.log('======before==catch');
                }else{
                    $scope.cartsDataOfTemp =[];
                    console.log('no data found');
                }

            }).then(function () {
                //console.log('========than',$scope.cartsDataOfTemp.length);
                $scope.totalItems = $scope.cartsDataOfTemp.length;
                //$scope.total = $scope.cartsDataOfTemp.length;
               // console.log('========$scope.total',$scope.total);
                $scope.spinner = false;
               $rootScope.spinner = false;
                $rootScope.spinner1 = false;
                //console.log('$rootScope.spinner1------------',$rootScope.spinner1);

            }).catch(function (error) {
               // console.log('========catch');
                //error while sending otp
                console.log('error ', error);
                //toast message
            });
        }



        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function() {
            console.log('Page changed to: ' + $scope.currentPage);
        };

        $scope.setItemsPerPage = function(num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1; //reset to first page
        }

        $scope.DoCtrlPagingAct = function(text, page, pageSize, total) {
            console.log('calling----------------------')
            $log.info({
                text: text,
                page: page,
                pageSize: pageSize,
                total: total
            });
        };





        $scope.closeImportLeadsModal = function () {
            $('#myModal4').val('');
            $scope.file = '';
            $scope.fileName = ''
            $('#myModal4').modal('hide');
        };

        $scope.addFile = function (file) {
            console.log('calling addFile============',file);
            if (file) {
                $scope.fileName = file.name;
            }
        }

        $scope.uploadFile = function (file1) {
            console.log('uploadFile---------',file1);
            $rootScope.spinner = true;
            var file = file1;
            var uploadUrl = `${baseUrl}tasks/templatesUploadFile`;
            fileUpload.uploadFileToUrl(file, uploadUrl,function (err,result) {
                console.log('result---------------------------',result)
                if(!result.status){
                    toastr.error(result.message);
                }else{
                    toastr.success('template upload successfully!!!!!');
                }
                $rootScope.spinner = false;
                $scope.file = '';
                $scope.fileName = ''
                $('#myModal4').modal('hide');
                $scope.cartsDataOfTemp = [];
                getAllTasksTemplates();

            });

           // templateUploadFile(file);
        }

        // function templateUploadFile(file) {
        //     console.log('----------------11111111111')
        //     tasksService
        //         .templateUploadFile(file)
        //         .then(success,
        //             error,
        //             progress);
        // }

        function success(resp) {
            console.log('-resp---------------------------------------',resp);
            if(!resp.data.status){
                $rootScope.errorToast('some fields are missing please fill it and upload again');
            }else{
                $rootScope.successToast('Uploaded Successfully');
            }
            $rootScope.spinner = false;
            $scope.file = '';
            $scope.fileName = ''
            $('#uploadTemplatesModal').modal('close');
            $scope.cartsDataOfTemp = [];
            getAllTasksTemplates();
        }

        /**
         * error callback
         * @param {error} resp
         */
        function error(resp) {
            console.log('Error status:------------------------- ' + resp.status);
            $rootScope.spinner = false;
            $scope.file = '';
           // $rootScope.errorToast('Upload Failed');
        }

        /**
         * upload progress status
         * @param {event} evt
         */
        function progress(evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        }


//////////////////////////////// task upload///////////////////////////

        $scope.closeImporttaskModal = function () {
            $('#uploadTemplatesModal').val('');
            $scope.file = '';
            $scope.fileName = ''
            $('#uploadUpTasksModal').modal('close');
            $('#uploadDueTasksModal').modal('close');
        };

        $scope.addFileTask = function (file) {
            console.log('calling addFile============',file);
            if (file) {
                $scope.fileName = file.name;
            }
        }

        $scope.uploadUpTaskFile = function (file) {
            console.log('uploadFile---------',file);
            $rootScope.spinner = true;
            TaskUploadFile(file);
        }

        function TaskUploadFile(file) {
            tasksService
                .TaskUploadFile(file)
                .then(successTask,
                    errorTask,
                    progress);
        }

        function successTask(resp) {
            console.log('-resp---------------------------------------',resp);
            if(!resp.data.status){
                $rootScope.errorToast(resp.data.message);
            }else{
                $rootScope.successToast(resp.data.message);
            }
            $rootScope.spinner = false;
            $scope.file = '';
            $scope.fileName = ''
            $('#uploadUpTasksModal').modal('close');
            $('#uploadDueTasksModal').modal('close');
            $scope.cartsDataUpComing = [];
            $scope.dueTaskToShow = [];
            getCrmAllTasksDisplay();
        }

        /**
         * error callback
         * @param {error} resp
         */
        function errorTask(resp) {
            console.log('Error status:------------------------- ' + resp.status);
            $rootScope.spinner = false;
            $scope.file = '';
            $rootScope.errorToast('Upload Failed');
        }





        var s2ab = function (s) {
            var buf = new ArrayBuffer(s.length);
            var view = new Uint8Array(buf);
            for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
            return buf;
        };

        $scope.searchSubUser = function (data,index,value) {
            //$scope.hideDropDownSubuser = true;
            if(data.length === 0){
                value.active = false;
            }else {
                value.active = true;
            }
            console.log('data-------------------------',data);
            console.log('index-------------------------',index);
            console.log('length-------------------------',data.length);
            console.log('active-------------------------',value.active);

            $scope.subuserText =data;
            var querySubuser = {
                query:data
            };
            tasksService.getsubuserName(querySubuser).then(function (result) {
                // console.log('calling download template');
                // console.log('result================>',JSON.stringify(result));
                if(result.data.status === true){
                    console.log('calling this 111111111111111111');
                    $scope.allSubuserNames = result.data.value
                    console.log('$scope.allSubuserNames-----------',JSON.stringify($scope.allSubuserNames));
                    if($scope.allSubuserNames.length === 0){
                        value.active = false;
                    }

                }else{
                    console.log('error while searching template');
                }
            }).catch(function (error) {
                console.log('error',error);
            });
        }

        $scope.choseSubUser = function (user,index,value) {
            console.log('user====================',user);
            $scope.createtask[index].assignto = user.username;
            console.log('$scope.createtask[index].assignto',$scope.createtask[index].assignto,index);
            // $scope.hideDropDownSubuser = false;
            value.active = false;
        }

        $scope.choseSubUserTempUpcoming = function (user,index,value) {
            console.log('user====================',user);
            $scope.TempSubTasksData[index].assignto = user.username;
            console.log('$scope.TempSubTasksData[index]',$scope.TempSubTasksData[index].assignto,index);
            // $scope.hideDropDownSubuser = false;
            value.active = false;
        }

        $scope.choseSubUserSrach = function (user,index,value) {
            console.log('user====================',user);
            $scope.otherSubTasksData[index].assignto = user.username;
            console.log('$scope.otherSubTasksData[index]',$scope.otherSubTasksData[index].assignto,index);
            // $scope.hideDropDownSubuser = false;
            value.active = false;
        }


        $scope.s = {};
        $scope.allTempNames1 = [];
        $scope.searchTemplat = function (data) {
            // $scope.allTempNames1 = [];
            $scope.hideDropDownSearchTemp = true;
            //console.log('data searcg--------------',data.length);
            var queryTempName = {
                query:data
            };
            tasksService.getTemplatesByName(queryTempName).then(function (result) {
               // console.log('calling download template');
               // console.log('result================>',JSON.stringify(result));
                if(result.data.status === true){
                    //console.log('calling this 111111111111111111');
                    for(var i =0 ; i < result.data.value.length; i++){
                        $scope.allTempNames1[i]=result.data.value[i].template_name;
                        // $scope.allTempNames1.push(result.data.value[i].template_name)
                    }
                    $scope.allTempNames1 = $scope.allTempNames1.slice(0,result.data.value.length+1);
                    //console.log('calling this 111111111111111111',$scope.allTempNames1);
                }else{
                    //console.log('error while searching template');
                }
            }).catch(function (error) {
                console.log('error',error);
            });
            if(data.length === 0){
                $scope.cartsDataOfTemp =[];
               // console.log('calling id text length is zero');
                getAllTasksTemplates();
            }

        };
        $scope.hideDropDownSearchTemp = false;
        $scope.choseTempName = function (data) {
            $rootScope.spinner = true;
            console.log('choseTempName-------',data);
            //$scope.s.searchTemplateone = data.template_name
            // console.log('$scope.s.searchTemplateone-------',$scope.s.searchTemplateone);
            $scope.hideDropDownSearchTemp = false;
            $scope.cartsDataOfTemp =[];
            getAllTasksTemplates(data);

        };

        //$scope.searchTemplate1 = 'arif';
        $scope.clearTempData = function (data) {
            $scope.s.searchTemplateone = '';
            $scope.cartsDataOfTemp =[];
            getAllTasksTemplates();
        }




        $scope.t = {};
        $scope.searchTaskName = function (data) {
            $scope.toastFromSearch = false;
            console.log('data searcg--------------',data);
            $scope.tNameSearch = data
            $scope.hideDropDownSearchTemp = true;
            console.log('data searcg--------------',data.length);
            $scope.allTasksNames = $scope.getAllTaskTitles.filter(function (item) {
                return item.task_title.includes(data);
            });
            if(data.length === 0){
                // console.log('calling id text length is zero');
                $scope.cartsDataUpComing = [];
                getCrmAllTasksDisplay();
            }

        };

        $scope.toastFromSearch = false;
        $scope.choseTaskName = function (data) {
            $rootScope.spinner = true;
            $scope.t.taskName = data.task_title;
            console.log('$scope.t.taskName  -------',$scope.t.taskName);
            // console.log('$scope.s.searchTemplateone-------',$scope.s.t.taskName);
            $scope.hideDropDownSearchTemp = false;
            $scope.cartsDataUpComing = [];
            getCrmAllTasksDisplay($scope.t.taskName);
            $scope.toastFromSearch = true;

        };


        $scope.clearTaskData = function (data) {
            $scope.t.taskName = ''
            $scope.cartsDataUpComing = [];
            $scope.toastFromSearch = false;
            getCrmAllTasksDisplay();
        }

        //console.log('$scope.cartsDataUpComing------up------',JSON.stringify($scope.cartsDataUpComing));

        $scope.downloadAllUpComingTask = function () {
           // console.log('calling this 111111111111111111',$scope.cartsDataUpComing);
            var allExelUpTasks = [];
            for(var a = 0 ; a < $scope.cartsDataUpComing.length ; a++){
               // console.log('$scope.cartsDataUpComing',$scope.cartsDataUpComing[a]);
                for(var b = 0 ; b < $scope.cartsDataUpComing[a].data.length ; b++){
                    $scope.cartsDataUpComing[a].data[b].dueDate = moment($scope.cartsDataUpComing[a].data[b].dueDate).format("MMM Do YY");
                    //console.log('$scope.cartsDataUpComing',$scope.cartsDataUpComing[a].data[b]);
                    //console.log('$scope.cartsDataUpComing[a].data[b]', $scope.cartsDataUpComing[a].data[b]);
                   var dataToPush =  _.omit($scope.cartsDataUpComing[a].data[b], ['customer_id', 'isSubtask','sort_id','subtask_completed','task_completed','task_id']);
                    allExelUpTasks.push(dataToPush);

                }

            }

            console.log('allExelUpTasks', allExelUpTasks);
            /* generate a worksheet */
            var ws = XLSX.utils.json_to_sheet(allExelUpTasks);

            /* add to workbook */
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, "allExelUpTasks");

            /* write workbook (use type 'binary') */
            var wbout = XLSX.write(wb, {bookType: 'xlsx', type: 'binary'});

            saveAs(new Blob([s2ab(wbout)], {type: "application/octet-stream"}), "upcomingTasks.xls");

        };

        $scope.downloadAllDueTask = function () {
            // console.log('calling this 111111111111111111',$scope.cartsDataUpComing);
            var allExelDueTasks = [];
            for(var a = 0 ; a < $scope.dueTaskToShow.length ; a++){
                // console.log('$scope.cartsDataUpComing',$scope.cartsDataUpComing[a]);
                for(var b = 0 ; b < $scope.dueTaskToShow[a].data.length ; b++){
                    //console.log('$scope.cartsDataUpComing',$scope.cartsDataUpComing[a].data[b]);
                    $scope.dueTaskToShow[a].data[b].dueDate = moment($scope.dueTaskToShow[a].data[b].dueDate).format("MMM Do YY");
                    //console.log('$scope.cartsDataUpComing[a].data[b]', $scope.cartsDataUpComing[a].data[b]);
                    var dataToPushDue =  _.omit($scope.dueTaskToShow[a].data[b], ['customer_id', 'isSubtask','sort_id','subtask_completed','task_completed','task_id']);
                    allExelDueTasks.push(dataToPushDue);

                }

            }

            console.log('allExelDueTasks', allExelDueTasks);
            /* generate a worksheet */
            var ws = XLSX.utils.json_to_sheet(allExelDueTasks);

            /* add to workbook */
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, "allExelDueTasks");

            /* write workbook (use type 'binary') */
            var wbout = XLSX.write(wb, {bookType: 'xlsx', type: 'binary'});

            saveAs(new Blob([s2ab(wbout)], {type: "application/octet-stream"}), "dueTasks.xls");

        };





        $scope.d = {};
        $scope.searchTaskNameDue = function (data) {
            $scope.toastFromSearch = false;
            console.log('data searcg--------------',data);
            $scope.tNameSearchDue = data
            $scope.hideDropDownSearchTemp = true;
            console.log('data searcg--------------',data.length);
            $scope.allTasksNamesDue = $scope.getAllTaskTitlesDue.filter(function (item) {
                return item.task_title.includes(data);
            });
            if(data.length === 0){
                // console.log('calling id text length is zero');
                $scope.dueTaskToShow = [];
                getCrmAllTasksDisplay();
            }

        };

        $scope.toastFromSearch = false;
        $scope.choseTaskNameDue = function (data) {
            $rootScope.spinner = true;
            $scope.d.taskName = data.task_title;
            console.log('$scope.t.taskName  -------',$scope.d.taskName);
            // console.log('$scope.s.searchTemplateone-------',$scope.d.taskName);
            $scope.hideDropDownSearchTemp = false;
            $scope.dueTaskToShow = [];
            getCrmAllTasksDisplay($scope.d.taskName);
            $scope.toastFromSearch = true;

        };


        $scope.clearTaskDataDue = function (data) {
            $scope.d.taskName = ''
            $scope.dueTaskToShow = [];
            $scope.toastFromSearch = false;
            getCrmAllTasksDisplay();
        }



        $scope.downloadAllTemplates = function () {
            tasksService.getCrmTemplatesForDownload({}).then(function (result) {
                console.log('calling download template');
                //console.log('result================>',JSON.stringify(result));
                if(result.data.status === true){
                    console.log('calling this 111111111111111111');
                    //console.log('All Leads', result.response);
                    var allTempTasks = result.data.response

                    /* generate a worksheet */
                    var ws = XLSX.utils.json_to_sheet(allTempTasks);

                    /* add to workbook */
                    var wb = XLSX.utils.book_new();
                    XLSX.utils.book_append_sheet(wb, ws, "allTempTasks");

                    /* write workbook (use type 'binary') */
                    var wbout = XLSX.write(wb, {bookType: 'xlsx', type: 'binary'});

                    saveAs(new Blob([s2ab(wbout)], {type: "application/octet-stream"}), "template.xls");

                }else{
                    console.log('error while craeting temp');
                }
            }).catch(function (error) {
                console.log('error',error);
            });

        };

        $scope.checkNumber = function(days,index) {
            if(!(days < 365)){
                $scope.subTasksData[index].days = 365;
            }
        }


        $scope.subTasksData = [{option: 'first_task',days:0}];
        $scope.submitdata = function () {
            $scope.checkTempNamButton = true;
            $rootScope.spinner = true;
            console.log('>>> $scope.subTasksData>>>>finallllllllll>>>>>>.',JSON.stringify($scope.subTasksData));
            console.log(' $scope.templateName', $scope.templateName);


            var dataTasks = [];
            var count = 1;
            var valid = true;
            for(var j = 0 ; j < $scope.subTasksData.length; j++){
                if(!($scope.subTasksData[j].name && ($scope.subTasksData[j].days || $scope.subTasksData[j].days === 0)  && $scope.subTasksData[j].option)){
                    valid = false;
                    $rootScope.spinner = false;
                    $scope.checkTempNamButton = false;
                }
                var subTaskObj = {};
                subTaskObj.subtask_name = $scope.subTasksData[j].name;
                subTaskObj.subtask_days = $scope.subTasksData[j].days;
                subTaskObj.subtask_option = $scope.subTasksData[j].option;
                subTaskObj.assignto = $scope.subTasksData[j].assignto;
                subTaskObj.	sort_id = count++
                dataTasks.push(subTaskObj);
            }
            // if($scope.subTasksData[i].option === null){
            //     $scope.subTasksData[i].option = "first_task";
            // }
            //console.log('>>>dataTasksl>>>>>>.',JSON.stringify(dataTasks.length));
            //console.log('>>>dataTasksl>>>>>>.',JSON.stringify(dataTasks[0].option));
            if(dataTasks.length ===1 && dataTasks[0].option === undefined){
                dataTasks[0].subtask_option = "first_task";
            }

            var subtaskquery = {
                templateName:$scope.templateName,
               Tasksdata : dataTasks
            };
            if(valid) {
                $scope.validationError = '';
                //console.log('subtaskquery',JSON.stringify(subtaskquery));
                tasksService.createTasksTemplate(subtaskquery).then(function (result) {
                    //console.log('result================>',JSON.stringify(result.data.response));
                    if(result.data.response === 'true'){
                        //console.log('calling this 111111111111111111');
                        $scope.subTasksData = [{option: 'first_task',days:0}];
                        $scope.templateName = '';
                        $scope.validationError = '';
                        $rootScope.spinner = false;
                        $('#modal1').modal('hide');
                        $scope.cartsDataOfTemp =[];
                        $scope.checkTempNamButton = false;
                        getAllTasksTemplates();

                    }else{
                        console.log('error while craeting temp');
                    }
                }).catch(function (error) {
                    console.log('error111111111111111111111111111111',error);
                    $rootScope.spinner = false;
                });
            }else {
                $scope.validationError = 'Please fill the missing fields';
                $scope.checkTempNamButton = false;
                console.log('else igot you');
            }

        };
        $scope.tempUpdateButton = false;
        $scope.UpdateTaskdata = function () {
            $scope.tempUpdateButton = true;
            $rootScope.spinner = true;
            var validUpdate = true;
            //console.log('calling pdate');
            //console.log('>>> $scope.subTasksData>>>>finallllllllll>>>>>>.',JSON.stringify($scope.subTasksData));
            var filterTaskNotIds = _.filter($scope.subTasksData,function (noTaskIds) {
                if(!noTaskIds.subtask_id){
                   return noTaskIds;
                }
            });
            //console.log('filterTaskNotIds==========',JSON.stringify(filterTaskNotIds));
            // if(!$scope.templateName){
            //     $scope.errorMessage1 = 'Please enter a template name';
            //     $scope.spinner = false;
            //     $scope.tempUpdateButton = false;
            //     //console.log('calling',JSON.stringify($scope.errorMessage1));
            //     return;
            // }else {
            //     $scope.errorMessage1='';
            //     //$scope.tempUpdateButton = false;
            // }
            var dataTasks = [];
            for(var j = 0 ; j < $scope.subTasksData.length; j++){
                if(!($scope.subTasksData[j].name && ($scope.subTasksData[j].days || $scope.subTasksData[j].days === 0)  && $scope.subTasksData[j].option)){
                    validUpdate = false;
                    $rootScope.spinner = false;
                    $scope.tempUpdateButton = false;
                }
                var subTaskObj = {};
                subTaskObj.subtask_name = $scope.subTasksData[j].name;
                subTaskObj.subtask_days = $scope.subTasksData[j].days;
                subTaskObj.subtask_option = $scope.subTasksData[j].option;
                subTaskObj.sort_id = $scope.subTasksData[j].sort_id;
                subTaskObj.subtask_id = $scope.subTasksData[j].subtask_id;
                subTaskObj.assigned_to = $scope.subTasksData[j].assignto;
                dataTasks.push(subTaskObj);
            }
            //console.log('>>>dataTasksl>>>>>>.',JSON.stringify(dataTasks));
            var subtaskquery = {
                template_id:$scope.subTasksData[0].template_id,
                templateName:$scope.templateName,
                Tasksdata : dataTasks
            };
            var subtaskquery1 = {
                template_id:$scope.subTasksData[0].template_id,
                templateName:$scope.templateName,
                Tasksdata : filterTaskNotIds
            };

            if(validUpdate) {
                $q.all([tasksService.crmUpdateTemplate(subtaskquery), tasksService.AddTempTaskUpdate(subtaskquery1)])
                    .then(function(result) {
                        $scope.subTasksData = [{option: 'first_task',days:0}];
                        $scope.templateName = '';
                        $scope.statusEdit = ''
                        $scope.validationError =''
                        $('#modal1').modal('hide');
                        $rootScope.spinner = false;
                        $scope.tempUpdateButton = false;
                        $scope.cartsDataOfTemp =[];
                        getAllTasksTemplates();
                    }).catch(function (error) {
                      console.log('error',error);
                });


                // tasksService.crmUpdateTemplate(subtaskquery).then(function (result) {
                //     //console.log('result================>',JSON.stringify(result.data.response));
                //     if(result.data.response === "true"){
                //         console.log('goini inside');
                //         $scope.subTasksData = [{}];
                //         $scope.templateName = '';
                //         $scope.statusEdit = ''
                //         $scope.validationError =''
                //         $('#modal1').modal('close');
                //         $scope.spinner = false;
                //         $scope.tempUpdateButton = false;
                //     }else{
                //         console.log('error in updating temp');
                //         $scope.spinner = false;
                //     }
                // }).then(function () {
                //     console.log('then1111111111111');
                //     console.log('subtaskquery111111111',JSON.stringify(subtaskquery1));
                //     tasksService.AddTempTaskUpdate(subtaskquery1).then(function (result) {
                //         if(result.data.response === "true"){
                //             console.log('goini inside for add new tasks');
                //             // console.log('goini inside  22222');
                //             $scope.subTasksData = [{}];
                //             $scope.templateName = '';
                //             $scope.validationError =''
                //             $('#modal1').modal('close');
                //             $scope.cartsDataOfTemp =[];
                //             getAllTasksTemplates();
                //         }else{
                //             console.log('new task not added');
                //         }
                //     }).catch(function (error) {
                //         console.log('error',error);
                //     });
                //
                // }).catch(function (error) {
                //     console.log('error',error);
                // });

            }else {
                $scope.validationError = 'Please fill the missing fields';
                console.log('else igot you while updating');
                $scope.tempUpdateButton = false;

            }
            //console.log('subtaskquery',JSON.stringify(subtaskquery));



        };

        $scope.checkTempNamButton = false;
        $scope.checkTempNameExists = function (data) {
            console.log('tempname--------------',JSON.stringify(data));
            $scope.errorMessage ='';
            $scope.checkTempNamButton = false;
            var queryTempName = {
                query:data
            };
            tasksService.getTemplatesByName(queryTempName).then(function (result) {
                console.log('result of templates', JSON.stringify(result));
                $scope.resultSuggestTemp = result.data.value;
                console.log('result temp name', JSON.stringify($scope.resultSuggestTemp));
                if(!(result.data.value)){
                    console.log('calling if 22222222');
                    $scope.checkTempNamButton = false;
                }else{
                    var resultTemp = $scope.resultSuggestTemp.map(function(a) {
                        if( a.template_name === data){
                            $scope.errorMessage = "Template name already exists";
                            $scope.checkTempNamButton = true;
                            console.log('calling if');
                            console.log('$scope.iffffffff', JSON.stringify($scope.checkTempNamButton));
                        }
                        return $scope.errorMessage;
                    });
                }



                //console.log('result resultTemp name', JSON.stringify(resultTemp[0]));
            });
        };



        $scope.openDeleteTemplateModal = function (template_id,event) {
            console.log('calling',JSON.stringify(event))
             event.stopPropagation();
             $scope.template_id = template_id;
            // $('#delTemplateCnfrmModal').modal('open');

            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this Task !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel !",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $scope.DeleteTemplate()
                    } else {
                        swal("Cancelled", "Your Task is safe :)", "error");
                    }
                });
        }

        $scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California',
            'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii',
            'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
            'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
            'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
            'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
            'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
            'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
            'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
        ]

        $scope.value = '';


        $scope.DeleteTemplate = function () {
            $scope.spinner = true;
             console.log('$scope.template_id',JSON.stringify($scope.template_id));
             var query = {
                 template_id:$scope.template_id
             };
             tasksService.crmDeleteTemplate(query).then(function (result) {
              //  console.log('result================>',JSON.stringify(result));
                 swal("Deleted!", "Your Task has been deleted.", "success");
                 $scope.cartsDataOfTemp =[];
                getAllTasksTemplates();
                 $scope.spinner = false;
             }).catch(function (error) {
                console.log('error',error);
             });

        };

        $scope.openEditTemplateModal = function (task,event,editstatus) {
           // console.log('editstatus',JSON.stringify(editstatus))
            $scope.statusEdit = editstatus;
            //console.log('task',JSON.stringify(task.data))
            $scope.templateName = task.template_name;
            for(var  i = 0 ; i < task.data.length ; i++){
               var editObj= {};
                if(task.data[i].subtask_option === 'Start with first task'){
                    task.data[i].subtask_option = 'first_task';
                }else if(task.data[i].subtask_option === 'Start after previous task'){
                    task.data[i].subtask_option = 'previous_task';
                }
                editObj.name = task.data[i].subtask_name;
                editObj.days = task.data[i].subtask_days;
                editObj.option = task.data[i].subtask_option;
                editObj.template_id = task.data[i].template_id;
                editObj.sort_id = task.data[i].sort_id;
                editObj.subtask_id = task.data[i].subtask_id;
                editObj.assignto = task.data[i].assigned_to;
                $scope.subTasksData.push(editObj);
            }
            $scope.subTasksData.shift();
            //console.log('$scope.subTasksData edit',JSON.stringify($scope.subTasksData))
            _.each($scope.subTasksData, function (item, index) {
               // console.log('item sort',JSON.stringify(item));
               // console.log('index sort',index);
                item.sort_id = index + 1;
                item.index = index;
                //console.log('ye dekh pehle',JSON.stringify($scope.subTasksData));
            });
            var list123 = document.getElementById("sortThisTask");
            var sortable = new Sortable(list123, {
                draggable: ".item",
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent
                    //console.log('ent index11111111 oldIndex in iedt',evt.oldIndex + 1);
                    //console.log('ent index11111111 newIndex in iedt',evt.newIndex + 1);

                    $scope.subTasksData = move($scope.subTasksData, evt.oldIndex+1, evt.newIndex+1);
                },
                // Changed sorting within list
                onUpdate: function (/**Event*/evt) {
                    // same properties as onEnd
                    $timeout(function(){
                        console.log('after update calling this', $scope.subTasksData);
                        _.each($scope.subTasksData, function (item, index) {
                            item.sort_id = index + 1;
                            item.index = index;
                        });
                    });
                },
            });
            event.stopPropagation();
            $('#modal1').modal('show');
        }

        $scope.openCopyTemplateModal = function (task,event) {
            //console.log('task',JSON.stringify(task.data))
            var count = -1;
            $scope.templateName = task.template_name + '_one';
            for(var  i = 0 ; i < task.data.length ; i++){
                count ++;
                var editObj= {};
                if(task.data[i].subtask_option === 'Start with first task'){
                    task.data[i].subtask_option = 'first_task';
                }else if(task.data[i].subtask_option === 'Start after previous task'){
                    task.data[i].subtask_option = 'previous_task';
                }
                editObj.name = task.data[i].subtask_name;
                editObj.days = task.data[i].subtask_days;
                editObj.option = task.data[i].subtask_option;
                editObj.template_id = task.data[i].template_id;
                editObj.sort_id = task.data[i].sort_id;
                editObj.subtask_id = task.data[i].subtask_id;
                editObj.index = count;
                $scope.subTasksData.push(editObj);
            }

            var list123 = document.getElementById("sortThisTask");
            //console.log('list123==========',list123);
            var sortable = new Sortable(list123, {
                draggable: ".item",
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent

                    $scope.subTasksData = move($scope.subTasksData, evt.oldIndex+1, evt.newIndex+1)
                    _.each($scope.otherSubTasksData, function (item, index) {
                        item.sort_id = index + 1;
                        item.index = index;
                        //console.log('other sort 999999999999999999',JSON.stringify($scope.subTasksData));
                    });
                },
                onUpdate: function (/**Event*/evt) {
                    // same properties as onEnd
                    $timeout(function(){
                        //console.log('after update calling this', $scope.subTasksData);
                        _.each($scope.subTasksData, function (item, index) {
                            item.sort_id = index + 1;
                            item.index = index;
                        });
                    });
                },
            });
           // console.log('$scope.otherSubTasksData edit',JSON.stringify($scope.otherSubTasksData))
            $scope.subTasksData.shift();
           // console.log('$scope.subTasksData edit',JSON.stringify($scope.subTasksData))
            event.stopPropagation();
            $('#modal1').modal('show');
        };



        $scope.createTaskData = function () {
           // console.log(' $scope.templateName', $scope.taskname);
           // console.log('craete task',JSON.stringify($scope.createtask));

        };


        $scope.taskdata ={};
        $scope.createtask = [];

        $scope.TaskbuttonDisable = false;
        $scope.createTaskData1 = function (content,createtask,taskdata) {
            $scope.TaskbuttonDisable = true;
            $rootScope.spinner = true;

            // console.log(' from funtion content',(content));
            // console.log(' from funtion createtask',JSON.stringify(createtask));
            // console.log(' from funtion taskdata',JSON.stringify(taskdata));
            // console.log(' from funtion value1.date',JSON.stringify($scope.value1));

            if(!taskdata.customername){
                $scope.errorMessage = 'Please select a customer';
                $rootScope.spinner = false;
                $scope.TaskbuttonDisable = false;
                //console.log('calling',JSON.stringify($scope.errorMessage));
                return;
            }else {
                $scope.errorMessage='';
            }
            if(!taskdata.tasktitle){
                $scope.errorMessage1 = 'Please enter a task title';
                $rootScope.spinner = false;
                $scope.TaskbuttonDisable = false;
                //console.log('calling',JSON.stringify($scope.errorMessage));
                return;
            }else {
                $scope.errorMessage1 ='';
            }

            if(!taskdata.customer_id){
                $scope.errorMessage = 'Please select a customer once again from drop down menu';
                $scope.spinner = false;
                $scope.TaskbuttonDisable = false;
                //console.log('calling',JSON.stringify($scope.errorMessage));
                return;
            }else {
                $scope.errorMessage='';
            }

            var arr = [];
            var count = 1;
            for(var i=0;i<content.length;i++){
                //console.log('banta?',JSON.stringify(Object.keys(content)[i]));
                // console.log('createtask.length?',createtask.length);
                // console.log('length?',i);
                var tempObj = {};
                tempObj['task_title'] = taskdata.tasktitle;
                tempObj['subtask_name'] = content[i].subtask_name;
                // console.log('11111',createtask[i] !== null , createtask[i] !== undefined);
                // console.log('22222',createtask.length >= i, createtask.length);
                tempObj['dueDate'] = new Date(content[i].date);
                tempObj['assigned_to'] = (createtask.length >= i)?( (createtask[i] !== undefined)?(createtask[i].assignto):('') ):('');
                tempObj['customer_name'] = taskdata.customername;
                tempObj['customer_id'] = taskdata.customer_id;
                tempObj['sort_id'] = count++;
                //console.log('tempObj',tempObj);
                arr.push(tempObj);
            }
            //console.log('final data of array',JSON.stringify(arr));
            $scope.errorMessage='';

             tasksService.craeteCrmTask(arr).then(function (crmresult) {
               //console.log('CRM RESULT================>',JSON.stringify(crmresult));
                $scope.taskdata = {};
                $scope.taskname ='';
                $scope.createtask={};
                $scope.allsubTaskfinal = [];
                $scope.showSubtasksTable = true;
                $scope.showSubtasksTable = false;
                $scope.TaskbuttonDisable = false;
                if(crmresult.data.response === 'true'){
                    $scope.cartsDataUpComing = [];
                    $scope.dueTaskToShow = [];
                    getCrmAllTasksDisplay();
                    $rootScope.spinner = false;
                    $('#tasktemplate').modal('close');
                }else{
                    console.log('field');
                    $rootScope.spinner = false;
                }

            }).catch(function (error) {
                console.log('error',error);
            });
        };
        

        $scope.c = {};
        $scope.TempSubTasksData = [];

        $scope.openEditTempTaskModal = function (task,event) {
           //console.log('task 111111111111',JSON.stringify(task))
            $scope.c.temptaskname = task.task_title;
            $scope.c.tempcustomername = task.customername;
            for(var  i = 0 ; i < task.data.length ; i++){
                var editTempObj= {};
                editTempObj.subtask_name = task.data[i].subtask_name;
                editTempObj.currentTime = moment(task.data[i].dueDate).format('ll')
                editTempObj.assignto = task.data[i].assigned_to;
                editTempObj.sort_id = task.data[i].sort_id;
                editTempObj.task_id = task.data[i].task_id;
                editTempObj.subtask_completed = task.data[i].subtask_completed;
                editTempObj.customer_id = task.data[i].customer_id;
                $scope.TempSubTasksData.push(editTempObj);
            }
            console.log('$scope.TempSubTasksData==================',$scope.TempSubTasksData);
            _.each($scope.TempSubTasksData, function (item, index) {
                //console.log('item sort',JSON.stringify(item));
                //console.log('index sort',index);
                item.sort_id = index + 1;
                item.index = index;
                //console.log('ye dekh pehle',JSON.stringify($scope.TempSubTasksData));
            });
            var list123 = document.getElementById("sortThisTempTask");
           // console.log('list123==========',list123);
            var sortable = new Sortable(list123, {
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent

                    $scope.TempSubTasksData = move($scope.TempSubTasksData, evt.oldIndex, evt.newIndex)
                    _.each($scope.TempSubTasksData, function (item, index) {
                        item.sort_id = index + 1;
                        item.index = index;
                        //console.log('other sort 999999999999999999',JSON.stringify($scope.TempSubTasksData));
                    });
                },
                onUpdate: function (/**Event*/evt) {
                    // same properties as onEnd
                    $timeout(function(){
                       // console.log('after update calling this', $scope.TempSubTasksData);
                        _.each($scope.TempSubTasksData, function (item, index) {
                            item.sort_id = index + 1;
                            item.index = index;
                        });
                    });
                },

            });

            //console.log('$scope.otherSubTasksData edit',JSON.stringify($scope.TempSubTasksData))
            event.stopPropagation();
            $('#Updatetasktemplate').modal('open');
        }

        $scope.openEditTempTaskModalDue = function (task,event) {
            //console.log('task',JSON.stringify(task))
            $scope.c.temptaskname = task.task_title;
            $scope.c.tempcustomername = task.customername;
            for(var  i = 0 ; i < task.data.length ; i++){
                var editTempObj= {};
                editTempObj.subtask_name = task.data[i].subtask_name;
                editTempObj.currentTime = moment(task.data[i].dueDate).format('ll')
                editTempObj.assignto = task.data[i].assigned_to;
                editTempObj.sort_id = task.data[i].sort_id;
                editTempObj.task_id = task.data[i].task_id;
                editTempObj.subtask_completed = task.data[i].subtask_completed;
                editTempObj.customer_id = task.data[i].customer_id;
                $scope.TempSubTasksData.push(editTempObj);
            }
            //$scope.subTasksData.shift();
            _.each($scope.TempSubTasksData, function (item, index) {
               // console.log('item sort',JSON.stringify(item));
               // console.log('index sort',index);
                item.sort_id = index + 1;
                item.index = index;
               // console.log('ye dekh pehle',JSON.stringify($scope.TempSubTasksData));
            });
            var list123 = document.getElementById("sortThisTempTaskDue");
           // console.log('list123==========',list123);
            var sortable = new Sortable(list123, {
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent

                    $scope.TempSubTasksData = move($scope.TempSubTasksData, evt.oldIndex, evt.newIndex)
                    _.each($scope.TempSubTasksData, function (item, index) {
                        item.sort_id = index + 1;
                        item.index = index;
                       // console.log('other sort 999999999999999999',JSON.stringify($scope.TempSubTasksData));
                    });
                },
                onUpdate: function (/**Event*/evt) {
                    // same properties as onEnd
                    $timeout(function(){
                        console.log('after update calling this', $scope.TempSubTasksData);
                        _.each($scope.TempSubTasksData, function (item, index) {
                            item.sort_id = index + 1;
                            item.index = index;
                        });
                    });
                },
            });
           // console.log('$scope.otherSubTasksData edit',JSON.stringify($scope.TempSubTasksData))
            event.stopPropagation();
            $('#UpdatetasktemplateDue').modal('open');
        }

        $scope.removetempData = function (index) {
            $scope.spinner = true;
            // console.log('callling reome',index);
            // console.log('$scope.TempSubTasksData',JSON.stringify($scope.TempSubTasksData));
            var removedtask = $scope.TempSubTasksData.splice(index, 1);
            var removeId = removedtask[0].task_id;
            $scope.TempSubTasksData.splice(index, 1);
            // console.log('removed', JSON.stringify($scope.TempSubTasksData));
            // console.log('removeId',JSON.stringify(removeId));
            var removeIdTempTask = {
                task_id:removeId
            }
            if(removeId){
                tasksService.crmUpdateRemoveTempTask(removeIdTempTask).then(function (crmresult) {
                    //console.log('CRM OTHER RESULT================>',JSON.stringify(crmresult));
                    setTimeout(function () {
                        getCrmAllTasksDisplay();
                        $scope.spinner = false;
                    },1000);
                }).catch(function (error) {
                    console.log('error',error);
                });
            }else{
                console.log('remove id not there');
                $scope.spinner = false;
            };


        };

        $scope.addTemplateSubTasks = function() {

            $scope.testStatusOther1 = {};
            //console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.otherSubTasksData));
            // console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.TempSubTasksData));
            // console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.TempSubTasksData.length));

            //console.log('object>..',JSON.stringify($scope.testStatusOther1));
            $scope.TempSubTasksData.push($scope.testStatusOther1);
            //console.log('$scope.subTasksData>>>>>before>>>>>>>..',JSON.stringify($scope.TempSubTasksData));
            for(var i = 0 ; i < $scope.TempSubTasksData.length; i++){
                //console.log('$scope.TempSubTasksData.current',JSON.stringify($scope.TempSubTasksData[i].currentTime));
                if($scope.TempSubTasksData[i].currentTime === undefined){
                    $scope.TempSubTasksData[i].currentTime = new Date();
                }
            }
            //console.log('$scope.subTasksData>>>>>after>>>>>>>..',JSON.stringify($scope.TempSubTasksData));
            if($scope.TempSubTasksData.length >= 100){
                $rootScope.errorToast('can not add more that 20 Subtasks');
            }

            _.each($scope.TempSubTasksData, function (item, index) {
                // console.log('item sort',JSON.stringify(item));
                // console.log('index sort',index);
                item.sort_id = index + 1;
                //console.log('ye dekh pehle',JSON.stringify($scope.TempSubTasksData));
            });
            var list123 = document.getElementById("sortThisTempTask");
            //console.log('list123==========',list123);
            var sortable = new Sortable(list123, {
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent

                    $scope.TempSubTasksData = move($scope.TempSubTasksData, evt.oldIndex, evt.newIndex)
                    _.each($scope.TempSubTasksData, function (item, index) {
                        item.sort_id = index + 1;
                        //console.log('other sort 999999999999999999',JSON.stringify($scope.TempSubTasksData));
                    });
                }
            });
        };

        $scope.addTempTasksBetween = function(index) {

            // console.log('calling inbetween');
            // console.log('index',index);

            if($scope.taskname){
                $scope.errorMessage = '';
            }
            if($scope.customername){
                $scope.errorMessage1 = '';
            }
            $scope.testStatusOther = {};
            //console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.TempSubTasksData));
            // console.log('$scope.TempSubTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.TempSubTasksData.currentTime));
            // console.log('$scope.TempSubTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.TempSubTasksData.length));

           // console.log('object>..',JSON.stringify($scope.testStatusOther));
            //$scope.otherSubTasksData.push($scope.testStatusOther);
            $scope.TempSubTasksData.splice(index+1,0,$scope.testStatusOther);
            //console.log('$scope.subTasksData>>>>>before>>>>>>>..',JSON.stringify($scope.TempSubTasksData));
            for(var i = 0 ; i < $scope.TempSubTasksData.length; i++){
                //console.log('$scope.TempSubTasksData.current',JSON.stringify($scope.TempSubTasksData[i].currentTime));
                if($scope.TempSubTasksData[i].currentTime === undefined){
                    $scope.TempSubTasksData[i].currentTime = new Date();
                }
            }
            //console.log('$scope.subTasksData>>>>>after>>>>>>>..',JSON.stringify($scope.TempSubTasksData));
            if($scope.TempSubTasksData.length >= 100){
                $rootScope.errorToast('can not add more that 20 Subtasks');
            }
            _.each($scope.TempSubTasksData, function (item, index) {
                // console.log('item sort',JSON.stringify(item));
                // console.log('index sort',index);
                item.sort_id = index + 1;
               // console.log('ye dekh pehle',JSON.stringify($scope.TempSubTasksData));
            });
            var list123 = document.getElementById("sortThisTempTask");
            var sortable = new Sortable(list123, {
                draggable: ".item",
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent

                    $scope.TempSubTasksData = move($scope.TempSubTasksData, evt.oldIndex, evt.newIndex)
                    _.each($scope.TempSubTasksData, function (item, index) {
                        item.sort_id = index + 1;
                        //console.log('other sort 999999999999999999',JSON.stringify($scope.TempSubTasksData));
                    });
                }
            });

        };
        $scope.tempUpdateTaskButton=false;
        $scope.UpdateTemplateTasks = function (other) {
            $scope.tempUpdateTaskButton=true;
            $scope.spinner = true;
            var validUpdateTask = true;
            //console.log(' other1111111', other);

            var filterTaskNotHavingIds = _.filter($scope.TempSubTasksData,function (noTaskIds) {
                if(!noTaskIds.task_id){
                    return noTaskIds;
                }
            });

            if($scope.dateClickFlag === true){
                console.log('calling true');
                _.map(filterTaskNotHavingIds,function (data) {
                    data.currentTime = moment(new Date(data.currentTime)).add(1,'days').subtract({hours:2,minutes:30})
                    return data;
                });
            }else{
               console.log('calling false');
                _.map(filterTaskNotHavingIds,function (data) {
                    data.currentTime = new Date(data.currentTime)
                    return data;
                });
            }

           // console.log('filterTaskNotHavingIds==========',JSON.stringify(filterTaskNotHavingIds));
            //console.log('$scope.TempSubTasksData==========',JSON.stringify($scope.TempSubTasksData));

            var templateDataTasks = [];
            for(var j = 0 ; j < $scope.TempSubTasksData.length; j++){
               // console.log(' other22222222',JSON.stringify($scope.TempSubTasksData[j]));
                if(!($scope.TempSubTasksData[j].subtask_name && $scope.TempSubTasksData[j].currentTime)){
                    validUpdateTask = false;
                    $scope.spinner = false;
                    $scope.tempUpdateTaskButton=false;
                }
                var TempSubTaskObj = {};
                TempSubTaskObj.subtask_name = $scope.TempSubTasksData[j].subtask_name;
                TempSubTaskObj.dueDate = new Date($scope.TempSubTasksData[j].currentTime);
                TempSubTaskObj.assigned_to = $scope.TempSubTasksData[j].assignto;
                TempSubTaskObj.sort_id = $scope.TempSubTasksData[j].sort_id;
                TempSubTaskObj.task_id = $scope.TempSubTasksData[j].task_id;
                templateDataTasks.push(TempSubTaskObj);
            }
            //console.log('>>>otherDataTasks otherrrrrrrrrrr>>>>>>.',JSON.stringify(templateDataTasks));
            var updateTempquery = {
                task_title:$scope.c.temptaskname,
                customer_name: $scope.c.tempcustomername,
                customer_id:$scope.customer_id ? $scope.customer_id : $scope.TempSubTasksData[0].customer_id ,
                TempTaskData:templateDataTasks
            };
            var subtaskTempqueryadd = {
                task_title:$scope.c.temptaskname,
                customer_name:$scope.c.tempcustomername,
                customer_id:$scope.customer_id ? $scope.customer_id : $scope.TempSubTasksData[0].customer_id,
                othertTask:filterTaskNotHavingIds
            };

            if(validUpdateTask){
                // tasksService.crmUpdateTasksTemplate(updateTempquery).then(function (crmresult) {
                //     // console.log('CRM OTHER RESULT================>',JSON.stringify(crmresult.data.response));
                //     if(crmresult.data.response === 'true'){
                //         getCrmAllTasksDisplay();
                //         $scope.spinner = false;
                //     }else{
                //         console.log('update temp field');
                //         $scope.spinner = false;
                //     }
                // }).catch(function (error) {
                //     console.log('error',error);
                // });
                //console.log('subtaskTempqueryadd================>',JSON.stringify(subtaskTempqueryadd));
                // tasksService.AddMoreTemplateTasks(subtaskTempqueryadd).then(function (result) {
                //     //console.log('result================>',JSON.stringify(result));
                //     if(result.data.response === 'true'){
                //         getCrmAllTasksDisplay();
                //         $scope.spinner = false;
                //     }else{
                //         console.log('update temp field');
                //         $scope.spinner = false;
                //     }
                //
                // }).catch(function (error) {
                //     console.log('error',error);
                // });

                $q.all([tasksService.crmUpdateTasksTemplate(updateTempquery), tasksService.AddMoreTemplateTasks(subtaskTempqueryadd)])
                    .then(function(result) {
                        $scope.other ={};
                        $scope.taskname='';
                        $scope.customername='';
                        $scope.customer_id='';
                        $scope.temptaskname = '';
                        $scope.validationError = ''
                        $scope.c = {};
                        $scope.otherSubTasksData =[];
                        $scope.TempSubTasksData = [];
                        $scope.tempUpdateTaskButton=false;
                        $('#Updatetasktemplate').modal('close');
                        $('#UpdatetasktemplateDue').modal('close');
                        getCrmAllTasksDisplay();
                        $scope.spinner = false;
                    }).catch(function (error) {
                    console.log('error',error);
                });

            }else{
                $scope.validationError = 'Please fill the missing fields';
                console.log('else igot you while updating');
                $scope.spinner = false;
                $scope.tempUpdateTaskButton=false;

            }

        };

        $scope.taskStatusChange = function (status) {
           // console.log('taskStatusChange',status);
        };

        $scope.suggestProduct = function (suggestText) {
            //console.log('suggestText',suggestText);
            $scope.names = [
                "one","onetwo","three","four","five","six","seven"
            ];
        };



        $scope.showTaskTableData = false;
        $scope.resultSuggest ='';
        $scope.suggestTemplate = function (data) {
           // console.log('suggestTemplate',JSON.stringify(data));
            var queryText = {
                query:data
            };
            //console.log('queryText>>>>>>>>>>',queryText);
            tasksService.getTemplatesByName(queryText).then(function (result) {
                //console.log('result of templates',JSON.stringify(result));
                $scope.resultSuggest = result.data.value;
                //console.log('result of $scope.resultSuggest',JSON.stringify($scope.resultSuggest));
                var result4 = $scope.resultSuggest.map(function(a) {
                    return a.template_name;
                });
                var autoCompleteTemplateNameObj = {};
                result4.map(function (ele,i) {
                    autoCompleteTemplateNameObj[ele] = null;
                });
                $('input.autocomplete').autocomplete({
                    data: autoCompleteTemplateNameObj,
                    limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                    onAutocomplete: function(val) {
                        $scope.showSubtasksTable = true;
                        //  console.log('val of selected',val);
                        var tempQuery = {
                            query : val
                        };
                        $scope.spinner = true;
                        tasksService.getAllTemplatesTasksByName(tempQuery).then(function (resulttasks) {
                            //console.log('resulttasks======',JSON.stringify(resulttasks.data));
                            if(resulttasks.data.status === true){
                                $scope.spinner = false;
                                $scope.createtask = [];
                                $scope.allsubTaskfinal = resulttasks.data.value[0];
                                
                                var firstDate, lastDate;
                                var startDate = new Date();

                                $scope.allsubTaskfinal.map(function(obj,i){
                                   // console.log('obj-----------',JSON.stringify(obj));
                                    if(obj.subtask_option === 'first_task'){
                                        if(firstDate === undefined){
                                            firstDate = startDate.setDate(startDate.getDate() + obj.subtask_days);
                                            obj["date"] = new Date(firstDate);
                                        }else{
                                            var tempDate = new Date(firstDate);
                                            obj["date"] = new Date(tempDate.setDate(tempDate.getDate() + obj.subtask_days));
                                        }
                                        obj["display_opt"] = 'Start with first task';
                                        if($scope.createtask[i]){
                                            $scope.createtask[i].assignto = obj.assigned_to
                                        }else{
                                            $scope.createtask.push({assignto:obj.assigned_to})
                                        }
                                    }else{
                                        if((lastDate === undefined && firstDate === undefined)){
                                            lastDate = startDate.setDate(startDate.getDate() + obj.subtask_days);
                                            //console.log("first coming here");
                                        }else if(lastDate === undefined && firstDate !== undefined){
                                            console.log("first coming here for today");
                                            var tempDate2 = new Date(firstDate);
                                            lastDate = tempDate2.setDate(tempDate2.getDate() + obj.subtask_days);
                                        }else{
                                            //console.log("second coming here",lastDate);
                                            var tempDate2 = new Date(lastDate);
                                            lastDate = tempDate2.setDate(tempDate2.getDate() + obj.subtask_days);
                                        }
                                        obj["date"] = new Date(lastDate);
                                        obj["display_opt"] = 'Start after previous task';
                                        if($scope.createtask[i]){
                                            $scope.createtask[i].assignto = obj.assigned_to
                                        }else{
                                            $scope.createtask.push({assignto:obj.assigned_to})
                                        }
                                    }

                                    // someDate.setDate(someDate.getDate() + numberOfDaysToAdd);

                                });
                                //console.log('resulttasks======',JSON.stringify($scope.allsubTaskfinal));
                            }else{
                                console.log('data not found');
                            }

                        });

                        // Callback function when value is autcompleted.
                    },
                    minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                });
            }).catch(function (error) {
                console.log('error',error);
            });
        };

      $scope.changeDateForPrevious = function (allsubTaskfinal,date1,index) {
          //console.log('calling date cahnge function');
          if(moment(date1).format("DD MMM YYYY")){
              $('.picker__close').click();
          }
            $scope.allsubTaskfinal = allsubTaskfinal
           // var dateTemp = date;
           //  console.log('callsubTaskfinal',JSON.stringify($scope.allsubTaskfinal));
           //  console.log('calling change',JSON.stringify(date1));
           //  console.log('index',JSON.stringify(index));
            var lastDate;
            var lastDate1;
            var dateTemp = new Date(date1);

            for(var i = index ; i< $scope.allsubTaskfinal.length ; i++){
                //console.log('objects',JSON.stringify($scope.allsubTaskfinal[i]));
                if($scope.allsubTaskfinal[i].subtask_option === 'first_task' && index === 0){
                    var dateTemp1 = new Date(date1)
                    console.log('inside if for first_task',i);
                   // console.log('dateTemp FIRST 11111111',JSON.stringify(dateTemp1));
                        if(i === index){
                            lastDate1 = dateTemp1.setDate(dateTemp1.getDate())
                            $scope.allsubTaskfinal[i].date = new Date(lastDate1);
                        }else{
                            lastDate1 = dateTemp1.setDate(dateTemp1.getDate() + $scope.allsubTaskfinal[i].subtask_days);
                            $scope.allsubTaskfinal[i].date = new Date(lastDate1);
                        }
                }else if($scope.allsubTaskfinal[i].subtask_option === 'previous_task'){
                   // console.log('inside if for previous_task ',i);
                    //console.log('inside if for previous_task dateTemp1=========',JSON.stringify(dateTemp));
                    if(i === index){
                        lastDate = dateTemp.setDate(dateTemp.getDate())
                        dateTemp = new Date(lastDate);
                        $scope.allsubTaskfinal[i].date = new Date(lastDate);
                    }else{
                        lastDate = dateTemp.setDate(dateTemp.getDate() + $scope.allsubTaskfinal[i].subtask_days);
                        dateTemp = new Date(lastDate);
                        $scope.allsubTaskfinal[i].date = new Date(lastDate);
                    }

                }
            }
            //console.log('$scope.allsubTaskfinal[i]',JSON.stringify($scope.allsubTaskfinal));

        };
      
        $scope.changeSubtaskTempDate = function () {
            //console.log('calling yes function');
            //console.log('$scope.firstDate ',$scope.firstDate);
            var firstDate = new Date($scope.firstDate);
             var allsubTask = $scope.allsubTaskfinal
             var date1 = new Date($scope.date)
             var index = $scope.indexClick
            //console.log('firstDate ',firstDate);
            //console.log('allsubTask',allsubTask);
            //console.log('date1',date1);
            //console.log('index ',index);
            var diffDate = moment(date1).startOf('day').diff(moment(firstDate).startOf('day'),'days')
            //console.log('diffDate ',diffDate);

            $scope.TempSubTasksData = allsubTask;
            //console.log('$scope.TempSubTasksData==============',$scope.TempSubTasksData);
            var dateTemp = new Date(date1);
            for(var i = index ; i< $scope.TempSubTasksData.length ; i++){
                var lastDate2;
                var lastDate1;
                if($scope.TempSubTasksData[i].subtask_completed === 'N'){
                    var dateTemp1 = new Date(date1)
                    if(i === index){
                        lastDate1 = dateTemp1.setDate(dateTemp1.getDate())
                        //console.log('inside if',new Date(lastDate1));
                        $scope.TempSubTasksData[i].currentTime =moment(new Date(lastDate1)).format('ll');

                        //moment($scope.TempSubTasksData[i].currentTime).format('ll')
                        //console.log('$scope.TempSubTasksData[i].currentTime------',$scope.TempSubTasksData[i].currentTime);

                    }else{
                        //console.log('inside else');
                        //console.log('$scope.TempSubTasksData[i].currentTime',new Date($scope.TempSubTasksData[i].currentTime));
                        lastDate2 = new Date($scope.TempSubTasksData[i].currentTime).setDate(new Date($scope.TempSubTasksData[i].currentTime).getDate() + diffDate)
                        $scope.TempSubTasksData[i].currentTime = moment(new Date(lastDate2)).format('ll');

                        //console.log('$scope.TempSubTasksData[i].currentTime------',$scope.TempSubTasksData[i].currentTime);
                    }

                }else{
                    console.log('do nuthing');
                }

            }
            //console.log('out of looooooooooooooooop');
            diffDate = 0
            //console.log('diffDate 2222222',diffDate);

        }
        
        $scope.dateChangeOnlySelected = function () {
            var index = $scope.indexClick
            console.log('index--------------',index);
            var dateSele = new Date($scope.date);
            $scope.TempSubTasksData = $scope.allsubTaskfinal;
            $scope.TempSubTasksData[index].currentTime =moment(new Date(dateSele)).format('ll');
            $('#tempDateCnfrmModal').modal('close')
        }
        
        

        $scope.dateClickFlag = false;
        $scope.changeDateForPreviousInEdit = function (allsubTaskfinal,date1,index) {
            console.log('calling date function============');
            $scope.dateClickFlag = true;
            $scope.allsubTaskfinal = allsubTaskfinal;
            var d = new Date(date1)
            $scope.date = d.toISOString();
            console.log('$scope.date------------',$scope.date);
            $scope.indexClick = index
            console.log('allsubTaskfinal',allsubTaskfinal);
            console.log('allsubTaskfinal',allsubTaskfinal.length);
            console.log('$scope.date',$scope.date);
            console.log('index',index);
            console.log('calling date change');
            if(moment(date1).format("DD MMM YYYY")){
                $('.picker__close').click();
            }
            if(index+1 !== allsubTaskfinal.length){
                $('#tempDateCnfrmModal').modal('open')
            }else{
                console.log('dont open pop');
                $scope.TempSubTasksData[index].currentTime = moment($scope.date).format('ll')
            }
        };

        // $scope.dateChangeOnlySelected = function (allsubTaskfinal,date1,index) {
        //     $scope.date = d.toISOString();
        //     $scope.TempSubTasksData[index].currentTime = moment($scope.date).format('ll')
        // }



        $scope.changeOptionsDate = function (data,allsubTaskfinal,index) {

            // console.log('changeOptionsDate11111111',JSON.stringify(data));
            // var firstDateOption = allsubTaskfinal[0].date;
            // console.log('firstDateOption2222222',JSON.stringify(firstDateOption));
            // console.log('$index333333',JSON.stringify(index));
            //
            // $scope.allsubTaskfinal = allsubTaskfinal;
            // var lastDateOption;
            // var previousDateForPrevious ;
            // var startDateOption = new Date();
            // for(var j = index  ; j< $scope.allsubTaskfinal.length ; j++){
            //     console.log('objects',JSON.stringify($scope.allsubTaskfinal[j]));
            //     if(data === 'first_task'){
            //         console.log('call first_task=================>');
            //         if($scope.allsubTaskfinal[j].subtask_option === 'previous_task'){
            //             console.log('value of j',j)
            //             console.log('calling 111111111')
            //             lastDateOption = firstDateOption.setDate(firstDateOption.getDate() + $scope.allsubTaskfinal[j].subtask_days);
            //             $scope.allsubTaskfinal[j].date = new Date(lastDateOption);
            //         }
            //     }
            //     if(data === 'previous_task'){
            //         $scope.allsubTaskfinal[index].subtask_option = "previous_task";
            //         $scope.allsubTaskfinal[index].display_opt = 'start after previous task';
            //         var firstDateOptionTemp;
            //         for(var k = index-1 ; k >= 0 ; k-- ){
            //             console.log('calling 222222222')
            //          if($scope.allsubTaskfinal[k].subtask_option === 'previous_task'){
            //              console.log('value of k',k)
            //              previousDateForPrevious = $scope.allsubTaskfinal[k].date;
            //              console.log('previousDateForPrevious',JSON.stringify(previousDateForPrevious));
            //
            //
            //              if($scope.allsubTaskfinal[j].subtask_option === 'previous_task'){
            //                  console.log('calling 333333')
            //                  console.log('value of j 333333333',j)
            //                  console.log('calling 333333333333')
            //                  firstDateOptionTemp = previousDateForPrevious.setDate(previousDateForPrevious.getDate() + $scope.allsubTaskfinal[j].subtask_days);
            //                  console.log('firstDateOptionTemp444444444',new Date(firstDateOptionTemp))
            //                  $scope.allsubTaskfinal[j].date = new Date(firstDateOptionTemp);
            //
            //              }
            //              break;
            //
            //          }
            //
            //         }
            //
            //     }
            // }
            // console.log('$scope.allsubTaskfinal[i] optionnnnnnnnnn',JSON.stringify($scope.allsubTaskfinal));

            //console.log('changeOptionsDate11111111',(data));
            var firstDateOption = new Date(allsubTaskfinal[0].date);
            //console.log('firstDateOption2222222',(firstDateOption));
            //console.log('$index===========',(index));

            $scope.allsubTaskfinal = allsubTaskfinal
            var lastDateOption;
            var previousDateForPrevious ;
            // var startDateOption = new Date();
            for(var j = index  ; j< $scope.allsubTaskfinal.length ; j++){
                if(j === 0){
                    //console.log("insidde loop for j");
                    continue;
                }
                //console.log('objects',($scope.allsubTaskfinal));
                if(data === 'first_task'){
                    //console.log('calling if----------------');
                    if($scope.allsubTaskfinal[j].subtask_option === 'previous_task'){
                        // console.log('value of j',j)
                        // console.log('calling 111111111')
                        lastDateOption = firstDateOption.setDate(firstDateOption.getDate() + $scope.allsubTaskfinal[j].subtask_days);
                        //console.log('lastDateOption',new Date(lastDateOption))
                        $scope.allsubTaskfinal[j].date = new Date(lastDateOption);
                        //console.log(' $scope.allsubTaskfinal[index].date',index, new Date($scope.allsubTaskfinal[index].date))
                        break;
                    }
                }else{
                    //console.log('calling else=================>');
                    $scope.allsubTaskfinal[index].subtask_option = "previous_task";
                    $scope.allsubTaskfinal[index].display_opt = 'start after previous task';
                    var firstDateOptionTemp;
                    for(var k = index-1 ; k > 0 ; k-- ){
                        //console.log('calling 222222222')
                        if(k === 0){
                            //console.log("insidde loop for k");
                            continue;
                        }
                        if($scope.allsubTaskfinal[k].subtask_option === 'previous_task'){
                           // console.log('value of k',k)
                            previousDateForPrevious = new Date($scope.allsubTaskfinal[k].date);
                            //console.log('previousDateForPrevious',(previousDateForPrevious));

                            if($scope.allsubTaskfinal[j].subtask_option === 'previous_task'){
                                // console.log('calling 333333')
                                // console.log('value of j 333333333',j)
                                // console.log('calling 333333333333')
                                firstDateOptionTemp = previousDateForPrevious.setDate(previousDateForPrevious.getDate() + $scope.allsubTaskfinal[j].subtask_days);
                                //console.log('firstDateOptionTemp444444444',new Date(firstDateOptionTemp))
                                $scope.allsubTaskfinal[j].date = new Date(firstDateOptionTemp);

                            }
                            //console.log("before breaking the index k is ", k);
                            break;

                        }

                    }
                }
            }
            //console.log('$scope.allsubTaskfinal[i] optionnnnnnnnnn',($scope.allsubTaskfinal));

        };

        $scope.toProper = function(){
            console.log('calling option');
        };

        $scope.hideDropDownAfterSelect = true;

        $scope.assignCustomersForTemp = function (data) {
            // var data = $scope.tempcustomername
            $scope.hideDropDownAfterSelect = true;
            //console.log('data for cutomers',JSON.stringify(data), $scope.customername);
            $scope.showDropDown = data
            //console.log('data for $scope.showDropDown',JSON.stringify($scope.showDropDown.length))
            var searchQuery = {
                search:data
            };
            tasksService.getAllClientsDetails(searchQuery).then(function (result) {
                var customerData = result.data.value
                $scope.allCustomersDataCust = customerData.filter(function (data) {
                    if(data.customer_type === null){
                        data.customer_type = "Leads";
                    }
                    return data;
                });
                // console.log('$scope.allCustomersData',JSON.stringify($scope.allCustomersDataCust));
                // console.log('$scope.allCustomersData',JSON.stringify($scope.allCustomersDataCust.length));
            });

        };
        $scope.hideDropDownAfterSelect = false;
        $scope.chosenCustomerForTemp = function (cust) {
           // console.log('cust detail=======',JSON.stringify(cust));
            $scope.taskdata.customer_id = cust.user_id;
            //console.log('index in cust.name',cust.name);
            $scope.taskdata.customername = cust.name;

            $scope.hideDropDownAfterSelect = false;
            //console.log('$scope.taskdata=========',$scope.taskdata);
        };

        $scope.chosenCustomerForInd = function (cust) {
            //console.log('chosenCustomerForInd',cust.name);
            $scope.customer_id = cust.user_id;
            $scope.c.tempcustomername = cust.name;
            //console.log('tempcustomername',$scope.c.tempcustomername);

            $scope.hideDropDownAfterSelect = false;
        };

        $scope.chosenCustomerForInd1 = function (cust) {
            //console.log('chosenCustomerForInd',cust.name);
            $scope.customer_id = cust.user_id;
            $scope.customername = cust.name;
            //console.log('tempcustomername',$scope.c.tempcustomername);

            $scope.hideDropDownAfterSelect = false;
        };

        $scope.removesubtasks = function (index) {
           // console.log('callling reome',index);
           // console.log('$scope.allsubTaskfinal',JSON.stringify($scope.allsubTaskfinal));
            $scope.allsubTaskfinal.splice(index, 1);
           // console.log('removed', JSON.stringify($scope.allsubTaskfinal));
        };

        $scope.createOtherTask = function () {
            $scope.otherTaskInputForm = true;
            $scope.showTaskTableData = false;

        };

        $scope.closePopModal = function () {
            $scope.allsubTaskfinal = []
            $scope.otherTaskInputForm = false;
            $scope.showTaskTableData = false;
            $scope.taskdata = {};
            $scope.taskname ='';
            $scope.createtask='';
            $scope.errorMessage1='';
            $scope.errorMessage = '';
            $scope.showSubtasksTable = true;
            $scope.showSubtasksTable = false;
        };
        $scope.closeTempPopModal = function () {
            $scope.templateName = '';
            $scope.task = '';
        };

        $scope.closeTemplatePopModal = function () {
            //console.log('callimng')
            $scope.subTasksData = [{option:"first_task",days:0}];
            $scope.templateName = '';
            $scope.errorMessage1='';
            $scope.errorMessage = '';
            $scope.statusEdit = '';
            $scope.validationError = '';
        };

        $scope.closeindividualSRModal = function () {
            $scope.other ={};
            $scope.otherSubTasksData =[];
            $scope.errorMessage1='';
            $scope.errorMessage = '';
            $scope.taskname ='';
            $scope.customername ='';
            $scope.statusfresh = ''
            $scope.TempSubTasksData = [];
        };

        $scope.otherSubTasksData = [];
        //console.log('first11111111 ini',JSON.stringify($scope.otherSubTasksData.length));

        $scope.addOtherSubTasks = function() {
            if($scope.taskname){
                $scope.errorMessage = '';
            }
            if($scope.customername){
                $scope.errorMessage1 = '';
            }
            $scope.testStatusOther = {};
            //console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.otherSubTasksData));
            // console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.otherSubTasksData.currentTime));
            // console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.otherSubTasksData.length));
            //
            // console.log('object>..',JSON.stringify($scope.testStatusOther));
            $scope.otherSubTasksData.push($scope.testStatusOther);
            //console.log('$scope.subTasksData>>>>>before>>>>>>>..',JSON.stringify($scope.otherSubTasksData));
            for(var i = 0 ; i < $scope.otherSubTasksData.length; i++){
                //console.log('$scope.otherSubTasksData.current',JSON.stringify($scope.otherSubTasksData[i].currentTime));
               if($scope.otherSubTasksData[i].currentTime === undefined){
                   $scope.otherSubTasksData[i].currentTime = moment(new Date()).format('ll');
               }
            }
            console.log('$scope.subTasksData>>>>>after>>>>>>>..',JSON.stringify($scope.otherSubTasksData));
            // if($scope.otherSubTasksData.length >= 100){
            //     $rootScope.errorToast('can not add more that 20 Subtasks');
            // }

            _.each($scope.otherSubTasksData, function (item, index) {
                // console.log('item sort',JSON.stringify(item));
                // console.log('index sort',index);
                item.sort_id = index + 1;
                item.index = index;
                //console.log('ye dekh pehle',JSON.stringify($scope.otherSubTasksData));
            });
            var list123 = document.getElementById("sortThisFreshTask1");
            //console.log('list123==========',list123);
            var sortable = new Sortable(list123, {
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent

                    $scope.otherSubTasksData = move($scope.otherSubTasksData, evt.oldIndex, evt.newIndex)
                    _.each($scope.otherSubTasksData, function (item, index) {
                        item.sort_id = index + 1;
                        item.index = index;
                        //console.log('other sort 999999999999999999',JSON.stringify($scope.otherSubTasksData));
                    });
                },
                onUpdate: function (/**Event*/evt) {
                    // same properties as onEnd
                    $timeout(function(){
                        //console.log('after update calling this', $scope.otherSubTasksData);
                        _.each($scope.otherSubTasksData, function (item, index) {
                            item.sort_id = index + 1;
                            item.index = index;
                        });
                    });
                },
            });
        };


        $scope.saveOtherTask = function (other) {
            $scope.spinner = true;
            //console.log('$scope.dateClickFlag',$scope.dateClickFlag);
            console.log(' other1111111', $scope.taskname);
            if(!$scope.taskname){
                $scope.errorMessage10 = 'Please enter task title';
                $scope.spinner = false;
                //console.log('calling',JSON.stringify($scope.errorMessage));
                return;
            }else {
                $scope.errorMessage10='';
            }
            if(!$scope.customername){
                $scope.errorMessage1 = 'Please select customer name';
                $scope.spinner = false;
                //console.log('calling',JSON.stringify($scope.errorMessage1));
                return;
            }else {
                $scope.errorMessage1='';
            }

            var otherDataTasks = [];
            for(var j = 0 ; j < $scope.otherSubTasksData.length; j++){
                //console.log(' other22222222',JSON.stringify($scope.otherSubTasksData[j]));
                var OtherSubTaskObj = {};
                OtherSubTaskObj.task_title = $scope.taskname;
                OtherSubTaskObj.customer_name = $scope.customername;
                OtherSubTaskObj.customer_id = $scope.customer_id;
                OtherSubTaskObj.task_content = $scope.otherSubTasksData[j].content;
                if($scope.dateClickFlag === true && j === 0){
                    //console.log('calling for first one with date change');
                    OtherSubTaskObj.dueDate = moment(new Date($scope.otherSubTasksData[j].currentTime)).add(1,'days').subtract({hours:2,minutes:30});
                }else if($scope.dateClickFlag === true && j != 0){
                    //console.log('calling true');
                    OtherSubTaskObj.dueDate = moment(new Date($scope.otherSubTasksData[j].currentTime)).add(1,'days').subtract({hours:2,minutes:30});
                }else{
                    //console.log('calling false');
                    OtherSubTaskObj.dueDate = new Date($scope.otherSubTasksData[j].currentTime);

                }
                //OtherSubTaskObj.dueDate = new Date($scope.otherSubTasksData[j].currentTime);
                OtherSubTaskObj.assigned_to = $scope.otherSubTasksData[j].assignto;
                OtherSubTaskObj.sort_id = $scope.otherSubTasksData[j].sort_id;
                otherDataTasks.push(OtherSubTaskObj);
            }
            //console.log('>>>otherDataTasks otherrrrrrrrrrr>>>>>>.',JSON.stringify(otherDataTasks));
            var Othersubtaskquery = {
                othertTask:otherDataTasks
            };

            //console.log('createTaskQuery>>>>>>>>>>>>>>>',JSON.stringify(Othersubtaskquery));
            tasksService.craeteCrmTaskIndividualTasks(Othersubtaskquery).then(function (crmresult) {
               // console.log('CRM OTHER RESULT================>',JSON.stringify(crmresult));
                if(crmresult.data.response === "true"){
                    $scope.other ={};
                    $scope.taskname='';
                    $scope.customername='';
                    $scope.customer_id = '';
                    $scope.otherSubTasksData =[];
                    $scope.spinner = false;
                    $scope.TaskbuttonDisable = false;
                    $('#individualtasks').modal('close');
                    getCrmAllTasksDisplay();
                }else{
                    console.log('error while creating');
                    $scope.spinner = false;
                    $rootScope.errorToast('Please Fill all the Fields');
                }
            }).catch(function (error) {
                $scope.spinner = false;
                console.log('error sssssssss',error);


            });
        };

        $scope.UpdateOtherTask = function (other) {
            $scope.spinner = true;
            //console.log(' other1111111', other);
            if(!$scope.taskname){
                $scope.errorMessage = 'Please enter task title';
               // console.log('calling',JSON.stringify($scope.errorMessage));
                return;
            }else {
                $scope.errorMessage='';
            }
            if(!$scope.customername){
                $scope.errorMessage1 = 'Please select customer name';
                //console.log('calling',JSON.stringify($scope.errorMessage1));
                return;
            }else {
                $scope.errorMessage1='';
            }
            var filterTaskNotIds1 = _.filter($scope.otherSubTasksData,function (noTaskIdTasks) {
                if(!noTaskIdTasks.task_id){
                    return noTaskIdTasks;
                }
            });

            if($scope.dateClickFlag === true){
                console.log('calling true');
                _.map(filterTaskNotIds1,function (data) {
                    data.currentTime = moment(new Date(data.currentTime)).add(1,'days').subtract({hours:2,minutes:30})
                    return data;
                });
            }else{
                console.log('calling false');
                _.map(filterTaskNotIds1,function (data) {
                    data.currentTime = new Date(data.currentTime)
                    return data;
                });
            }

           // console.log('filterTaskNotIds1==========',JSON.stringify(filterTaskNotIds1));

            var otherDataTasks = [];
            for(var j = 0 ; j < $scope.otherSubTasksData.length; j++){
                //console.log(' other22222222',JSON.stringify($scope.otherSubTasksData[j]));
                var OtherSubTaskObj = {};
                OtherSubTaskObj.task_title = $scope.taskname;
                OtherSubTaskObj.customer_name = $scope.customername;
                OtherSubTaskObj.customer_id = $scope.customer_id ? $scope.customer_id : $scope.otherSubTasksData[j].customer_id;
                OtherSubTaskObj.task_content = $scope.otherSubTasksData[j].content;
                OtherSubTaskObj.dueDate = new Date($scope.otherSubTasksData[j].currentTime);
                OtherSubTaskObj.assigned_to = $scope.otherSubTasksData[j].assignto;
                OtherSubTaskObj.sort_id = $scope.otherSubTasksData[j].sort_id;
                OtherSubTaskObj.task_id = $scope.otherSubTasksData[j].task_id;
                otherDataTasks.push(OtherSubTaskObj);
            }
            //console.log('>>>otherDataTasks otherrrrrrrrrrr>>>>>>.',JSON.stringify(otherDataTasks));
            var Othersubtaskquery = {
                othertTask:otherDataTasks
            };

           //console.log('createTaskQuery>>>>>>>>>>>>>>>',JSON.stringify(Othersubtaskquery));
            tasksService.crmUpdateFreshTasks(Othersubtaskquery).then(function (crmresult) {
                //console.log('CRM OTHER RESULT================>',JSON.stringify(crmresult.data.response));
                if(crmresult.data.response === 'true'){
                    getCrmAllTasksDisplay();
                    $scope.spinner = false;
                }else{
                    console.log('update ind task fiels');
                }
            }).catch(function (error) {
                console.log('error',error);
            });

            var subtaskqueryadd = {
                task_title:$scope.taskname,
                customer_name:$scope.customername,
                customer_id:$scope.customer_id ? $scope.customer_id : $scope.otherSubTasksData[0].customer_id,
                othertTask:filterTaskNotIds1
            };

           // console.log('subtaskqueryadd 222222222',JSON.stringify(subtaskqueryadd));
            tasksService.crmAddTempTaskUpdate(subtaskqueryadd).then(function (result) {
                //console.log('result================>',JSON.stringify(result));
                if(result.data.response === 'true'){
                    getCrmAllTasksDisplay();
                    $scope.spinner = false;
                }else{
                    console.log('update ind task fiels');
                }
            }).catch(function (error) {
                console.log('error',error);
            });

            $scope.other ={};
            $scope.taskname='';
            $scope.customername='';
            $scope.otherSubTasksData =[];
            $('#individualtasks').modal('close');
        };


        $scope.addFreshTasksBetween = function(index) {
             //console.log('$scope.subTasksData',JSON.stringify($scope.subTasksData));
            // console.log('calling inbetween');
            // console.log('index',index);

            if($scope.taskname){
                $scope.errorMessage = '';
            }
            if($scope.customername){
                $scope.errorMessage1 = '';
            }
            $scope.testStatusOther = {};
            //console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.otherSubTasksData));
            // console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.otherSubTasksData.currentTime));
            // console.log('$scope.subTasksData>>>>beforeeeeee>>>>>>>>..',JSON.stringify($scope.otherSubTasksData.length));

           // console.log('object>..',JSON.stringify($scope.testStatusOther));
            //$scope.otherSubTasksData.push($scope.testStatusOther);
            $scope.otherSubTasksData.splice(index+1,0,$scope.testStatusOther);
            //console.log('$scope.subTasksData>>>>>before>>>>>>>..',JSON.stringify($scope.otherSubTasksData));
            for(var i = 0 ; i < $scope.otherSubTasksData.length; i++){
               // console.log('$scope.otherSubTasksData.current',JSON.stringify($scope.otherSubTasksData[i].currentTime));
                if($scope.otherSubTasksData[i].currentTime === undefined){
                    $scope.otherSubTasksData[i].currentTime = moment(new Date()).format('ll');
                }
            }
            //console.log('$scope.subTasksData>>>>>after>>>>>>>..',JSON.stringify($scope.otherSubTasksData));
            _.each($scope.otherSubTasksData, function (item, index) {
                // console.log('item sort',JSON.stringify(item));
                // console.log('index sort',index);
                item.sort_id = index + 1;
                item.index = index;
                //console.log('ye dekh pehle',JSON.stringify($scope.otherSubTasksData));
            });
            var list123 = document.getElementById("sortThisFreshTask1");
            var sortable = new Sortable(list123, {
                draggable: ".item",
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent

                    $scope.otherSubTasksData = move($scope.otherSubTasksData, evt.oldIndex, evt.newIndex)
                    _.each($scope.otherSubTasksData, function (item, index) {
                        item.sort_id = index + 1;
                        item.index = index;
                        //console.log('other sort 999999999999999999',JSON.stringify($scope.otherSubTasksData));
                    });
                },
                onUpdate: function (/**Event*/evt) {
                    // same properties as onEnd
                    $timeout(function(){
                        console.log('after update calling this', $scope.otherSubTasksData);
                        _.each($scope.otherSubTasksData, function (item, index) {
                            item.sort_id = index + 1;
                            item.index = index;
                        });
                    });
                }
            });

        };

        $scope.openEditSratchTempModal = function (task,event,editstatus) {
            $scope.statusfresh = editstatus;
            // console.log('editstatus',JSON.stringify($scope.statusfresh))
            // console.log('task',JSON.stringify(task))
             $scope.taskname = task.task_title;
             $scope.customername = task.customername;
            for(var  i = 0 ; i < task.data.length ; i++){
                var editOtherObj= {};
                editOtherObj.content = task.data[i].task_content;
                editOtherObj.currentTime = moment(task.data[i].dueDate).format('ll');
                editOtherObj.assignto = task.data[i].assigned_to;
                editOtherObj.sort_id = task.data[i].sort_id;
                editOtherObj.task_id = task.data[i].task_id;
                editOtherObj.subtask_completed = task.data[i].subtask_completed;
                editOtherObj.customer_id = task.data[i].customer_id;
                $scope.otherSubTasksData.push(editOtherObj);
            }
            //$scope.subTasksData.shift();
            _.each($scope.otherSubTasksData, function (item, index) {
                // console.log('item sort',JSON.stringify(item));
                // console.log('index sort',index);
                item.sort_id = index + 1;
                item.index = index;
                //console.log('ye dekh pehle',JSON.stringify($scope.otherSubTasksData));
            });
            var list123 = document.getElementById("sortThisFreshTask1");
           // console.log('list123==========',list123);
            var sortable = new Sortable(list123, {
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent

                    $scope.otherSubTasksData = move($scope.otherSubTasksData, evt.oldIndex, evt.newIndex)
                    _.each($scope.otherSubTasksData, function (item, index) {
                        item.sort_id = index + 1;
                        item.index = index;
                        //console.log('other sort 999999999999999999',JSON.stringify($scope.otherSubTasksData));
                    });
                },
                onUpdate: function (/**Event*/evt) {
                    // same properties as onEnd
                    $timeout(function(){
                        console.log('after update calling this', $scope.otherSubTasksData);
                        _.each($scope.otherSubTasksData, function (item, index) {
                            item.sort_id = index + 1;
                            item.index = index;
                        });
                    });
                },
            });
            //console.log('$scope.otherSubTasksData edit',JSON.stringify($scope.otherSubTasksData))
            event.stopPropagation();
            $('#individualtasks').modal('open');
        }



        $scope.removeOthersubtasks = function (index) {
            $scope.spinner = true;
            // console.log('callling reome',index);
             console.log('$scope.allsubTaskfinal',JSON.stringify($scope.otherSubTasksData));
             var removedtask = $scope.otherSubTasksData.splice(index, 1);
            //console.log('removedtask================', JSON.stringify(removedtask));
             var removeId = removedtask[0].task_id;
            $scope.otherSubTasksData.splice(index, 1);
            console.log('removed', JSON.stringify($scope.otherSubTasksData));
            console.log('removeId',JSON.stringify(removeId));
            if(removeId){
                var removeIdTempTask = {
                    task_id:removeId
                }
                tasksService.crmUpdateRemoveTempTask(removeIdTempTask).then(function (crmresult) {
                    console.log('CRM OTHER RESULT================>',JSON.stringify(crmresult));
                    setTimeout(function () {
                        getCrmAllTasksDisplay();
                        $scope.spinner = false;
                    },1000);
                }).catch(function (error) {
                    console.log('error',error);
                });  
            }else{
                console.log('nuthing');
                $scope.spinner = false;
            }
            

        };





        $scope.openIndTasksCnfModal = function (other,event) {
            //console.log('calling',JSON.stringify(other))
            event.stopPropagation();
            $scope.taskid = other.data[0].task_id
            $('#indTasksCnfrmModal').modal('open');
        }


        $scope.taskStatusChangeOfMain = function () {
            //console.log('calling on change of inddddddddddddddddd',$scope.taskIdOfIndTask )
            var query = {
                taskid:$scope.taskIdOfIndTask
            };

            //console.log('query',JSON.stringify(query));
            var serviceQuery = {
                number:'one'
            };
            mySharedService.prepForPublish(serviceQuery);
            tasksService.crmUpdateOtherTasks(query).then(function (crmresult) {
                //console.log('CRM OTHER RESULT================>',JSON.stringify(crmresult));
                getCrmAllTasksDisplay();
            }).catch(function (error) {
                console.log('error',error);
            });
        };
        $scope.taskStatusChangeOfMainDueTab = function () {
            //console.log('calling on change of inddddddddddddddddd',$scope.taskIdOfIndTask )
            var query = {
                taskid:$scope.taskIdOfIndTask
            };

            //console.log('query',JSON.stringify(query));
            var serviceQuery = {
                number:'one'
            };
            mySharedService.prepForPublish(serviceQuery);
            tasksService.crmUpdateOtherTasks(query).then(function (crmresult) {
                //console.log('CRM OTHER RESULT================>',JSON.stringify(crmresult));
                getCrmAllTasksDisplay();
            }).catch(function (error) {
                console.log('error',error);
            });
        };
        $scope.taskIdNot =[];
        $scope.taskIds = [];
        $scope.markAllTaskCompArray =[];
        $scope.markAllTaskCompArray1 =[];

        ////open modal for upcoming FIRST///////
        $scope.openModalFunction = function(one, alltasks, index){
            //console.log('calling on change',JSON.stringify(one));
            $scope.taskIdNot.push(one.task_id);
            $scope.taskIds.push(one.task_id);
            //console.log('$scope.taskIds',JSON.stringify($scope.taskIds));
            //console.log('alltasks.data[i] length',JSON.stringify(alltasks.data.length));
            $scope.taskLength = alltasks.data.length;
            $scope.allTaskPrivious = alltasks.data.slice();
            $scope.indexLength = index+1;
            // console.log('$scope.taskLength',JSON.stringify($scope.taskLength));
            // console.log('$scope.indexLength',JSON.stringify($scope.indexLength));
            //console.log('alltasks-----------------------',JSON.stringify(alltasks));
            //console.log('index',JSON.stringify(index));
            var count = 0;
            var previousCheck=false;
            var checkForPrivious;
            var checkIndex;

            //////
            function checkForPreviousTask() {
                var filterNotCompleted1 = _.filter($scope.allTaskPrivious,function (oneTask2) {
                    $scope.markAllTaskCompArray.push(oneTask2.task_id);
                    return oneTask2.task_id !==  one.task_id;

                });
                var filterPriviousIds2 = _.filter(filterNotCompleted1,function (one1) {
                    if(one1.subtask_completed === 'Y'){

                    }else{
                        return one1.task_id;
                    }
                });
                // console.log('filterPriviousIds <<<<11<<<<<<<,',JSON.stringify(filterPriviousIds2));
                // console.log('filterPriviousIds <<<<<11<<<<<<,',JSON.stringify(filterPriviousIds2.length));
                if(filterPriviousIds2.length === 0){
                    previousCheck = true;
                }else{
                    previousCheck = false;
                }

                return previousCheck;
            }

            function checkForPreviousIndex(index) {
                for(var i = 0 ; i < index ; i++){
                    if(alltasks.data[i].subtask_completed === 'N'){
                        checkIndex=false;
                    }else{
                        checkIndex=true;
                    }

                }
                return checkIndex;
            }
            //////

            ///////check for last task//////
            if(alltasks.data.length === index+1){
                //console.log('calling last one check===========');
                if(checkForPreviousTask()){
                   // console.log('last task n abobe all completed 111111111');
                    $('#openModalForTempTasksCnf').modal('open');
                }else{
                    //console.log('last task n abobe all not completed 2222222------------');
                    for(var k = 0 ; k < index ; k++ ){
                        if(alltasks.data[k].subtask_completed === 'N'){
                            $scope.taskIds.push(alltasks.data[k].task_id);
                        }
                    }
                    $('#openModalForPreviuosTasks').modal('open');
                }
            }else{
               // console.log('3333333333333333333------------');
                var filterNotCompleted = _.filter(alltasks.data,function (oneTask) {
                    // $scope.markAllTaskCompArray.push(oneTask.task_id);
                    return oneTask.task_id !==  one.task_id;

                });
               // console.log('filterNotCompleted-----------------------',JSON.stringify(filterNotCompleted));
                var lastTask = false;
                for(var k = 0 ; k < filterNotCompleted.length;k++){
                    if(filterNotCompleted[k].subtask_completed === 'Y'){
                        lastTask = true;
                    }else{
                        lastTask = false;
                        break;
                    }
                }
                if(lastTask){
                    //console.log('coming here')
                    var filterNotCompletedOne = _.filter(alltasks.data,function (fil) {
                        $scope.markAllTaskCompArray.push(fil.task_id);
                        return fil.task_id !==  one.task_id;

                    });
                    $scope.indexLength = alltasks.data.length;
                    $('#openModalForTempTasksCnf').modal('open');
                }else{
                    if(index === 0){
                        checkIndex=true;
                    }
                    if(checkForPreviousIndex(index)){
                        //console.log('calling 1111111')
                        $('#openModalForTempTasksCnf').modal('open');
                    }else{
                        //console.log('calling 222222222 in else');
                        for(var j = 0 ; j < index ; j++ ){
                            if(alltasks.data[j].subtask_completed === 'N'){
                                $scope.taskIds.push(alltasks.data[j].task_id);
                            }
                        }
                        $('#openModalForPreviuosTasks').modal('open');
                    }
                    var spliceIndexArray =  $scope.allTaskPrivious.splice(index+1, alltasks.data.length);
                    //console.log('spliceIndexArray<<<<<<<<<<<,',JSON.stringify(spliceIndexArray));
                    var filterPriviousIds = _.filter(spliceIndexArray,function (onePrivious) {
                        if(onePrivious.subtask_completed === 'Y'){

                        }else{
                            return onePrivious.task_id;
                        }
                    });
                   // console.log('filterPriviousIds 22222222<<<<<<<<<<<,',JSON.stringify(filterPriviousIds));
                   // console.log('filterPriviousIds 22222222222<<<<<<<<<<<,',JSON.stringify(filterPriviousIds.length));
                    if(filterPriviousIds.length === 0){
                        checkForPrivious = true;
                    }else{
                        checkForPrivious = false;
                    }

                    if(checkForPrivious){
                        var filterIdForPriviousLast = _.filter(alltasks.data,function (oneTask1) {
                            $scope.markAllTaskCompArray.push(oneTask1.task_id);
                            return oneTask1;

                        });
                      //  console.log('checkForPrivious calling');
                      //  console.log('checkForPrivious ids',JSON.stringify($scope.markAllTaskCompArray));
                        $scope.indexLength = alltasks.data.length;
                        $('#openModalForPreviuosTasks').modal('open');
                        $('#openModalForTempTasksCnf').modal('close');
                    }
                }
            }


            //console.log('111111 before $scope.markAllTaskCompArray>>>>>>>>>.',JSON.stringify($scope.markAllTaskCompArray));
            //console.log('111111 before $scope.markAllTaskCompArray>>>>>>>>>.',JSON.stringify($scope.markAllTaskCompArray.length));
            if($scope.markAllTaskCompArray.length > 0){
                $scope.markAllTaskCompArray = _.union($scope.markAllTaskCompArray1, $scope.markAllTaskCompArray);
            }
            // console.log('222222 after $scope.finalTestArray>>>>>>>>>.',JSON.stringify($scope.markAllTaskCompArray));
            // console.log('final taskIds>>>>>>>>>.',JSON.stringify($scope.taskIds));
            // console.log('final taskIds>>>>>>>>>.',JSON.stringify($scope.taskIds.length));
            // $('#openModalForTempTasksCnf').modal('open');
        }
        $scope.openModalTempTasks = function (one,alltasks,index) {

            if(one.subtask_completed === 'Y'){
                console.log('do nuthing');
            }else{
               $scope.openModalFunction(one, alltasks, index);
            }

        };

        ////open modal for overdue second///////
        $scope.openModalFunctionDueSection = function(one,alltasks,index){
            if(one.isSubtask === 'N'){
                $scope.taskIdOfIndTask = one.task_id;
                $('#indTasksCnfrmModal').modal('open');
            }
            //console.log('calling on change',JSON.stringify(one));
            $scope.taskIdNot.push(one.task_id);
            $scope.taskIds.push(one.task_id);
            //console.log('$scope.taskIds',JSON.stringify($scope.taskIds));
            //console.log('alltasks.data[i] length',JSON.stringify(alltasks.data.length));
            $scope.taskLength1 = alltasks.data.length;
            $scope.allTaskPrivious = alltasks.data.slice();
            $scope.indexLength1 = index+1;
            // console.log('$scope.taskLength',JSON.stringify($scope.taskLength));
            // console.log('$scope.indexLength',JSON.stringify($scope.indexLength));
            //console.log('alltasks-----------------------',JSON.stringify(alltasks));
            //console.log('index',JSON.stringify(index));
            var count = 0;
            var previousCheck=false;
            var checkForPrivious;
            var checkIndex;

            //////
            function checkForPreviousTask() {
                var filterNotCompleted1 = _.filter($scope.allTaskPrivious,function (oneTask2) {
                    $scope.markAllTaskCompArray.push(oneTask2.task_id);
                    return oneTask2.task_id !==  one.task_id;

                });
                var filterPriviousIds2 = _.filter(filterNotCompleted1,function (one1) {
                    if(one1.subtask_completed === 'Y'){

                    }else{
                        return one1.task_id;
                    }
                });
                // console.log('filterPriviousIds <<<<11<<<<<<<,',JSON.stringify(filterPriviousIds2));
                // console.log('filterPriviousIds <<<<<11<<<<<<,',JSON.stringify(filterPriviousIds2.length));
                if(filterPriviousIds2.length === 0){
                    previousCheck = true;
                }else{
                    previousCheck = false;
                }

                return previousCheck;
            }

            function checkForPreviousIndex(index) {
                for(var i = 0 ; i < index ; i++){
                    if(alltasks.data[i].subtask_completed === 'N'){
                        checkIndex=false;
                    }else{
                        checkIndex=true;
                    }

                }
                return checkIndex;
            }
            //////

            ///////check for last task//////
            if(alltasks.data.length === index+1){
                //console.log('calling last one check DUE===========');
                if(checkForPreviousTask()){
                    //console.log('last task n abobe all completed 111111111 DUE');
                    $('#openModalForTempTasksCnf2').modal('open');
                }else{
                    //console.log('last task n abobe all not completed 2222222-DUE-----------');
                    for(var k = 0 ; k < index ; k++ ){
                        if(alltasks.data[k].subtask_completed === 'N'){
                            $scope.taskIds.push(alltasks.data[k].task_id);
                        }
                    }
                    $('#openModalForPreviuosTasks2').modal('open');
                }
            }else{
                //console.log('3333333333333333333----DUE--------');
                var filterNotCompleted = _.filter(alltasks.data,function (oneTask) {
                    // $scope.markAllTaskCompArray.push(oneTask.task_id);
                    return oneTask.task_id !==  one.task_id;

                });
                //console.log('filterNotCompleted---------DUE--------------',JSON.stringify(filterNotCompleted));
                var lastTask = false;
                for(var k = 0 ; k < filterNotCompleted.length;k++){
                    if(filterNotCompleted[k].subtask_completed === 'Y'){
                        lastTask = true;
                    }else{
                        lastTask = false;
                        break;
                    }
                }
                if(lastTask){
                   // console.log('coming here')
                    var filterNotCompletedOne = _.filter(alltasks.data,function (fil) {
                        $scope.markAllTaskCompArray.push(fil.task_id);
                        return fil.task_id !==  one.task_id;

                    });
                    $scope.indexLength1 = alltasks.data.length;
                    $('#openModalForTempTasksCnf2').modal('open');
                }else{
                    if(index === 0){
                        checkIndex=true;
                    }
                    if(checkForPreviousIndex(index)){
                        //console.log('calling 1111111 DUE')
                        $('#openModalForTempTasksCnf2').modal('open');
                    }else{
                        //console.log('calling 222222222 DUE');
                        for(var j = 0 ; j < index ; j++ ){
                            if(alltasks.data[j].subtask_completed === 'N'){
                                $scope.taskIds.push(alltasks.data[j].task_id);
                            }
                        }
                        $('#openModalForPreviuosTasks2').modal('open');
                    }
                    var spliceIndexArray =  $scope.allTaskPrivious.splice(index+1, alltasks.data.length);
                    //console.log('spliceIndexArray<<<<<<<<DUE <<<,',JSON.stringify(spliceIndexArray));
                    var filterPriviousIds = _.filter(spliceIndexArray,function (onePrivious) {
                        if(onePrivious.subtask_completed === 'Y'){

                        }else{
                            return onePrivious.task_id;
                        }
                    });
                    //console.log('filterPriviousIds 22222222<<<<<DUE<<<<<<,',JSON.stringify(filterPriviousIds));
                    //console.log('filterPriviousIds 22222222222<<<<<DUE<<<<<<,',JSON.stringify(filterPriviousIds.length));
                    if(filterPriviousIds.length === 0){
                        checkForPrivious = true;
                    }else{
                        checkForPrivious = false;
                    }

                    if(checkForPrivious){
                        var filterIdForPriviousLast = _.filter(alltasks.data,function (oneTask1) {
                            $scope.markAllTaskCompArray.push(oneTask1.task_id);
                            return oneTask1;

                        });
                        // console.log('checkForPrivious calling DUE');
                        // console.log('checkForPrivious ids DUE',JSON.stringify($scope.markAllTaskCompArray));
                        $scope.indexLength1 = alltasks.data.length;
                        $('#openModalForPreviuosTasks2').modal('open');
                        $('#openModalForTempTasksCnf2').modal('close');
                    }
                }
            }

            // console.log('111111 before $scope.markAllTaskCompArray>>>>DUE>>>>>.',JSON.stringify($scope.markAllTaskCompArray));
            // console.log('111111 before $scope.markAllTaskCompArray>>>DUE>>>>>>.',JSON.stringify($scope.markAllTaskCompArray.length));
            if($scope.markAllTaskCompArray.length > 0){
                $scope.markAllTaskCompArray = _.union($scope.markAllTaskCompArray1, $scope.markAllTaskCompArray);
            }
            // console.log('222222 after $scope.finalTestArray>>>DUE>>>>>>.',JSON.stringify($scope.markAllTaskCompArray));
            // console.log('final taskIds>>>>DUE>>>>>.',JSON.stringify($scope.taskIds));
            // console.log('final taskIds>>>>DUE>>>>>.',JSON.stringify($scope.taskIds.length));
            // $('#openModalForTempTasksCnf').modal('open');
        }
        $scope.openModalTempTasks2 = function (one,alltasks,index) {
          if(one.subtask_completed === 'Y'){

          }else{
             $scope.openModalFunctionDueSection(one,alltasks,index);
          }
        };

        $scope.clearDataOfTaskIds = function () {
            $scope.taskIds = [];
            $scope.markAllTaskCompArray=[];
            $scope.markAllTaskCompArray1 =[];
            $scope.taskData = {};

        };

        $scope.clearDataOfTaskIdsPrivious = function () {
            //console.log('calling clear');
            var crmUpdateNot={
                subtask_name: $scope.taskIdNot
            };
            //console.log('crmUpdateNot',JSON.stringify(crmUpdateNot));
            tasksService.crmUpdateTemplateTasks(crmUpdateNot).then(function (crmresult) {
                //console.log('CRM TEMPLATE FIRST================>',JSON.stringify(crmresult));
                $scope.taskIds = [];
                $scope.markAllTaskCompArray=[];
                $scope.taskIdNot =[];
                setTimeout(function(){
                    getCrmAllTasksDisplay();
                }, 1000);
            }).catch(function (error) {
                console.log('error',error);
            });
            $scope.taskIds = [];
            $scope.markAllTaskCompArray=[];
            $scope.markAllTaskCompArray1 =[];
            $scope.taskData = {};

        };

        $scope.clearDataOfTaskIdsPriviousDue = function () {
           // console.log('calling clear');
            var crmUpdateNot={
                subtask_name: $scope.taskIdNot
            };
            //console.log('crmUpdateNot',JSON.stringify(crmUpdateNot));
            tasksService.crmUpdateTemplateTasks(crmUpdateNot).then(function (crmresult) {
                //console.log('CRM TEMPLATE FIRST================>',JSON.stringify(crmresult));
                $scope.taskIds = [];
                $scope.markAllTaskCompArray=[];
                $scope.taskIdNot =[];
                setTimeout(function(){
                    getCrmAllTasksDisplay();
                }, 1000);
            }).catch(function (error) {
                console.log('error',error);
            });
            $scope.taskIds = [];
            $scope.markAllTaskCompArray=[];
            $scope.markAllTaskCompArray1 =[];
            $scope.taskData = {};

        };

        $scope.clearDataOfTaskIdsDue = function () {
            //console.log('clear call');
            $scope.taskIds = [];
            $scope.markAllTaskCompArray=[];
            $scope.markAllTaskCompArray1 =[];
            $scope.taskData = {};
        };
        //$scope.taskNote1 ='';
        $scope.taskData ={};
        ////////////First///////////////
        $scope.taskStatusOfSubTask = function () {
            $(".collapsible-header").removeClass(function(){
                return "active";
            });
            $(".collapsible").collapsible({accordion: true});
            $(".collapsible").collapsible({accordion: false});


            $rootScope.spinner = true;
            $scope.cartsDataUpComing = [];
            $scope.dueTaskToShow = [];
            var queryUpdateComment;
            var serviceQuery = {
                number:'one'
            };
            //console.log('print 11111111111',$scope.taskData);
            //console.log('$scope.taskLength',JSON.stringify($scope.taskLength));
            //console.log('$scope.indexLength',JSON.stringify($scope.indexLength));
            if($scope.taskLength === $scope.indexLength){
                mySharedService.prepForPublish(serviceQuery);
                //console.log('calling to update comment fiels',JSON.stringify($scope.markAllTaskCompArray));
                queryUpdateComment = {
                    allIdsForComment:$scope.markAllTaskCompArray,
                    comment:$scope.taskData.taskNote1
                };
            }
           console.log('queryUpdateComment3333333333333',JSON.stringify(queryUpdateComment));
           //  tasksService.crmUpdateComments(queryUpdateComment).then(function (crmresult) {
           //     // console.log('CRM COMMENT RESULT FIRST================>',JSON.stringify(crmresult));
           //      $scope.taskIds = [];
           //      $scope.markAllTaskCompArray=[];
           //      $scope.taskData={};
           //      setTimeout(function(){
           //          getCrmAllTasksDisplay();
           //          $rootScope.successToast('Task Completion Saved');
           //          }, 1000);
           //  }).catch(function (error) {
           //      console.log('error',error);
           //  });
            var crmUpdate={
                subtask_name: $scope.taskIds
            };
           console.log('crmUpdate',JSON.stringify(crmUpdate));
           //  tasksService.crmUpdateTemplateTasks(crmUpdate).then(function (crmresult) {
           //      //console.log('CRM TEMPLATE FIRST================>',JSON.stringify(crmresult));
           //      $scope.taskIds = [];
           //      $scope.markAllTaskCompArray=[];
           //      setTimeout(function(){
           //          getCrmAllTasksDisplay();
           //          }, 1000);
           //  }).catch(function (error) {
           //      console.log('error',error);
           //  });
            $q.all([tasksService.crmUpdateComments(queryUpdateComment),  tasksService.crmUpdateTemplateTasks(crmUpdate)])
                .then(function(result) {
                    $scope.taskIds = [];
                    $scope.markAllTaskCompArray=[];
                    $scope.taskData={};
                    getCrmAllTasksDisplay();
                    $rootScope.successToast('Task Completion Saved');
                }).catch(function (error) {
                console.log('error',error);
            });
        };

        ////////////second////////////////
        $scope.taskStatusOfSubTaskDue = function () {
            $(".collapsible-header").removeClass(function(){
                return "active";
            });
            $(".collapsible").collapsible({accordion: true});
            $(".collapsible").collapsible({accordion: false});
            $rootScope.spinner = true;
            $scope.dueTaskToShow = [];

            var queryUpdateComment1
            var serviceQuery = {
                number:'one'
            };
            // console.log('print 222222222',$scope.taskData);
            // console.log('$scope.taskLength',JSON.stringify($scope.taskLength1));
            // console.log('$scope.indexLength',JSON.stringify($scope.indexLength1));
            if($scope.taskLength1 === $scope.indexLength1){
                mySharedService.prepForPublish(serviceQuery);
                //console.log('calling to update comment fiels',JSON.stringify($scope.markAllTaskCompArray));
                queryUpdateComment1 = {
                    allIdsForComment:$scope.markAllTaskCompArray,
                    comment:$scope.taskData.taskNote2
                };
                var checkTost = 'not';
                $scope.checkToastTest(checkTost);
            }


            // //console.log('queryUpdateComment44444444',JSON.stringify(queryUpdateComment1));
            // tasksService.crmUpdateComments(queryUpdateComment1).then(function (crmresult) {
            //     //console.log('CRM COMMENT RESULT SECOND================>',JSON.stringify(crmresult));
            //     $scope.taskIds = [];
            //     $scope.markAllTaskCompArray=[];
            //     $scope.taskData={};
            //     setTimeout(function(){
            //         getCrmAllTasksDisplay();
            //         $rootScope.successToast('Task Completion Saved');
            //     }, 1000);
            // }).catch(function (error) {
            //     console.log('error',error);
            // });
            //
            // //console.log('calling on change');
            var crmUpdate={
                subtask_name: $scope.taskIds
            };
            // //console.log('crmUpdate',JSON.stringify(crmUpdate));
            // tasksService.crmUpdateTemplateTasks(crmUpdate).then(function (crmresult) {
            //     //console.log('CRM TEMPLATE RESULT SECOND================>',JSON.stringify(crmresult));
            //     $scope.taskIds = [];
            //     $scope.markAllTaskCompArray=[];
            //     setTimeout(function(){
            //         getCrmAllTasksDisplay();
            //     }, 1000);
            // }).catch(function (error) {
            //     console.log('error',error);
            // });

            $q.all([tasksService.crmUpdateComments(queryUpdateComment1),  tasksService.crmUpdateTemplateTasks(crmUpdate)])
                .then(function(result) {
                    $scope.taskIds = [];
                    $scope.markAllTaskCompArray=[];
                    $scope.taskData={};
                    getCrmAllTasksDisplay();
                    $rootScope.successToast('Task Completion Saved');
                }).catch(function (error) {
                console.log('error',error);
            });


        };

        $scope.taskStatusOfSubTaskForAllCompletedDue = function () {
            // console.log('calling taskStatusOfSubTaskForAllCompleted');
            var crmUpdatetest={
                allSubTasksIds: $scope.markAllTaskCompArray
            };
            var serviceQuery = {
                number:'one'
            };
            mySharedService.prepForPublish(serviceQuery);
            //console.log('taskStatusOfSubTaskForAllCompleted query',JSON.stringify(crmUpdatetest));
            tasksService.crmUpdateTemplateTasksForAllIds(crmUpdatetest).then(function (crmresult) {
                //  console.log('CRM TEMPLATE RESULT FOR ALL SECOND================>',JSON.stringify(crmresult));
                $scope.taskIds = [];
                $scope.markAllTaskCompArray=[];
                setTimeout(function(){
                    getCrmAllTasksDisplay();
                }, 1000);
            }).catch(function (error) {
                console.log('error',error);
            });
        };


        $scope.taskStatusOfSubTaskForAllCompleted = function () {
            //console.log('calling taskStatusOfSubTaskForAllCompleted');
            var crmUpdatetest={
                allSubTasksIds: $scope.markAllTaskCompArray
            };
            var serviceQuery = {
                number:'one'
            };
            mySharedService.prepForPublish(serviceQuery);
            //console.log('taskStatusOfSubTaskForAllCompleted query',JSON.stringify(crmUpdatetest));
            tasksService.crmUpdateTemplateTasksForAllIds(crmUpdatetest).then(function (crmresult) {
                //console.log('CRM TEMPLATE RESULT FOR ALL FIRST================>',JSON.stringify(crmresult));
                $scope.taskIds = [];
                $scope.markAllTaskCompArray=[];
                setTimeout(function(){
                    getCrmAllTasksDisplay();
                }, 1000);
            }).catch(function (error) {
                console.log('error',error);
            });
        };


        $scope.checkToastTest = function (data) {
            console.log('data checkToastTest============',JSON.stringify(data));
            //$scope.toastCheck = data;
            if(!localStorage.getItem('toastCheck')){
                localStorage.setItem('toastCheck',data);
            }else{
                localStorage.setItem('toastCheck',data);
            }
        }

        // $scope.changeSubTaskContent = function (self,data) {
        //     console.log('data ',self.$data);
        //     console.log('value>>>>>>>>>>>>. ',JSON.stringify(data));
        //     var queryUpdate = {
        //         data:self.$data,
        //         task_id : data.subtask_id
        //     };
        //     console.log('queryUpdate>>>>>>>>>>>>. ',JSON.stringify(queryUpdate));
        //     tasksService.crmUpdateTaskContent(queryUpdate).then(function (crmresult) {
        //         //console.log('CRM crmUpdateTaskContent================>',JSON.stringify(crmresult));
        //         getAllTasksTemplates();
        //     }).catch(function (error) {
        //         console.log('error',error);
        //     });
        //
        // };

        $scope.showIditTextField = function (index) {
            //console.log('index',index);
            $scope.EditTask = index;
           // console.log('changeSub ',JSON.stringify(value1));

        };

        $scope.closeDatePickerForIndTask = function (date,index) {

            $scope.dateClickFlag = true;
            console.log('calling date===========',date);
            if(moment(date).format("DD MMM YYYY")){
                $('.picker__close').click();
            }

            $scope.otherSubTasksData[index].currentTime = moment(date).format('ll');
        }

        $scope.changeSubtaskTempDateDue = function () {
            //console.log('calling yes function');
            //console.log('$scope.firstDate ',$scope.firstDate);
            var firstDate = new Date($scope.firstDate);
            var allsubTask = $scope.allsubTaskfinal
            var date1 = new Date($scope.date)
            var index = $scope.indexClick
            // console.log('firstDate ',firstDate);
            // console.log('allsubTask',allsubTask);
            // console.log('date1',date1);
            // console.log('index ',index);
            var diffDate = moment(date1).startOf('day').diff(moment(firstDate).startOf('day'),'days')
            //console.log('diffDate ',diffDate);

            $scope.TempSubTasksData = allsubTask;
            //console.log('$scope.TempSubTasksData==============',$scope.TempSubTasksData);
            var dateTemp = new Date(date1);
            for(var i = index ; i< $scope.TempSubTasksData.length ; i++){
                var lastDate2;
                var lastDate1;
                if($scope.TempSubTasksData[i].subtask_completed === 'N'){
                    var dateTemp1 = new Date(date1)
                    if(i === index){
                        lastDate1 = dateTemp1.setDate(dateTemp1.getDate())
                       // console.log('inside if',new Date(lastDate1));
                        $scope.TempSubTasksData[i].currentTime =moment(new Date(lastDate1)).format('ll');

                    }else{
                        //console.log('inside else');
                        //console.log('$scope.TempSubTasksData[i].currentTime',new Date($scope.TempSubTasksData[i].currentTime));
                        lastDate2 = new Date($scope.TempSubTasksData[i].currentTime).setDate(new Date($scope.TempSubTasksData[i].currentTime).getDate() + diffDate)
                        $scope.TempSubTasksData[i].currentTime = new Date(lastDate2);
                        $scope.TempSubTasksData[i].currentTime = moment(new Date(lastDate2)).format('ll');
                    }
                   // console.log('diffDate 1111111111',diffDate);

                }else{
                    console.log('do nuthing');
                }
                //var diffDate =''
            }
           // console.log('out of looooooooooooooooop');
            diffDate = 0
            //console.log('diffDate 2222222',diffDate);


        }

        // console.log('calling date function============');
        // $scope.dateClickFlag = true;
        // $scope.allsubTaskfinal = allsubTaskfinal;
        // var d = new Date(date1)
        // $scope.date = d.toISOString();
        // console.log('$scope.date------------',$scope.date);
        // $scope.indexClick = index
        // console.log('allsubTaskfinal',allsubTaskfinal);
        // console.log('allsubTaskfinal',allsubTaskfinal.length);
        // console.log('$scope.date',$scope.date);
        // console.log('index',index);
        // console.log('calling date change');
        // if(moment(date1).format("DD MMM YYYY")){
        //     $('.picker__close').click();
        // }
        // if(index+1 !== allsubTaskfinal.length){
        //     $('#tempDateCnfrmModal').modal('open')
        // }else{
        //     console.log('dont open pop');
        //     $scope.TempSubTasksData[index].currentTime = moment($scope.date).format('ll')
        // }


        $scope.changeDateForPreviousDueTask = function (allsubTaskfinal,date1,index) {
            $scope.dateClickFlag = true;
            $scope.allsubTaskfinal = allsubTaskfinal;
            var d = new Date(date1)
            $scope.date = d.toISOString();

            $scope.indexClick = index
            // console.log('allsubTaskfinal',allsubTaskfinal);
            // console.log('allsubTaskfinal',allsubTaskfinal.length);
            // console.log('date1',date1);
            // console.log('index',index);
            // console.log('calling date change');
            if(moment(date1).format("DD MMM YYYY")){
                $('.picker__close').click();
            }
            if(index+1 !== allsubTaskfinal.length){
                $('#tempDateCnfrmModalDue').modal('open')
            }else{
                console.log('dont open pop');
                $scope.TempSubTasksData[index].currentTime = moment($scope.date).format('ll')
            }

        };


        $scope.onOpen = function (data) {
            console.log('onOpen calling',data);
            $scope.firstDate = data
        };

        $scope.getTheSelectedDate = function (data) {
             var dummyDate = new Date(data);
            $scope.firstDate = dummyDate.toISOString()
            console.log('$scope.firstDate calling',$scope.firstDate);
        };




        $scope.CheckTaskNameExists = function (data) {
            console.log('task name sheck',data.tasktitle);
            $scope.errorMessage2 ='';
            $scope.TaskbuttonDisable = false;
            var querytaskName = {
                query:data.tasktitle
            };
            tasksService.checkCrmTasksByName(querytaskName).then(function (result) {
               //console.log('result of templates', JSON.stringify(result));
                $scope.resultSuggestTemp = result.data.value;
                if(!(result.data.value)){
                    console.log('calling if 22222222');
                    $scope.checkTempNamButton = false;
                }else{
                    var resultTemp = $scope.resultSuggestTemp.map(function(a) {
                        if( a.task_title === data.tasktitle){
                            $scope.errorMessage2 = "Task name already exists";
                            $scope.TaskbuttonDisable = true;
                        }
                        return $scope.errorMessage2;
                    });
                }

               // console.log('result temp name', JSON.stringify($scope.resultSuggestTemp));

                //console.log('result resultTemp name', JSON.stringify(resultTemp[0]));
            });
        }


        $scope.CheckTaskNameExistsForFresh = function (data) {
            console.log('task name sheck',data);
            $scope.errorMessage2 ='';
            $scope.TaskbuttonDisable = false;
            var querytaskName = {
                query:data
            };
            tasksService.checkCrmTasksByName(querytaskName).then(function (result) {
                //console.log('result of templates', JSON.stringify(result));
                $scope.resultSuggestTemp = result.data.value;
                if(!(result.data.value)){
                    console.log('calling if 22222222');
                    $scope.checkTempNamButton = false;
                }else{
                    var resultTemp = $scope.resultSuggestTemp.map(function(a) {
                        if( a.task_title === data){
                            $scope.errorMessage2 = "Task name already exists";
                            $scope.TaskbuttonDisable = true;
                        }
                        return $scope.errorMessage2;
                    });
                }

                // console.log('result temp name', JSON.stringify($scope.resultSuggestTemp));

                //console.log('result resultTemp name', JSON.stringify(resultTemp[0]));
            });
        }
        
        $scope.AddNoteForTask = function (data,index) {
           // console.log('index',index);
            $scope.TaskIndex = index;
            //console.log('note data',JSON.stringify(data));
            $scope.note.taskNote1 = data.note;
            //console.log('$scope.note',$scope.note);
            $scope.noteTaskId = data.task_id;
            $('#openNotePopUpModal').modal('open');
        }

        $scope.AddNoteForTaskDue = function (data,index) {
           // console.log('index',index);
            $scope.TaskIndex = index;
           // console.log('note data',JSON.stringify(data));
            $scope.note.taskNote1 = data.note;
           // console.log('$scope.note',$scope.note);
            $scope.noteTaskId = data.task_id;
            $('#openNotePopUpModalDue').modal('open');
        }


        $scope.note = {};

        $scope.submitNoteForTask = function (data,index) {
            $rootScope.spinner = true;
            var taskId = $scope.noteTaskId
            // console.log('taskId111111',taskId);
            // console.log('$scope.TaskIndex',$scope.TaskIndex);
            // console.log('task notr',JSON.stringify(data));
            var noteQuery = {
                task_id:taskId,
                note:data.taskNote1
            }
            //console.log('noteQuery333333333333: ',JSON.stringify(noteQuery));
            tasksService.crmUpdateNoteForTemp(noteQuery).then(function (crmresult) {
                //console.log('CRM TEMPLATE RESULT FOR ALL FIRST================>',JSON.stringify(crmresult));
                $timeout(function(){
                    getCrmAllTasksDisplay();
                    var anchorElement = $('#tooltip' + $scope.TaskIndex);
                        anchorElement.attr('data-tooltip', data.taskNote1);
                        anchorElement.tooltip();
                    $scope.note = {};
                    $rootScope.spinner = false;
                }, 1000);
            }).catch(function (error) {
                console.log('error',error);
                $rootScope.spinner = false;
            });
        }

        // $scope.UpdateNoteData = function (data) {
        //     $scope.note.taskNote1 = data.note;
        //     var taskId = $scope.noteTaskId
        //     console.log('taskId 22222222222',taskId);
        //     console.log('task update',JSON.stringify(data));
        //     var noteQuery1 = {
        //         task_id:taskId,
        //         note:data.taskNote1
        //     }
        //     console.log('noteQuery44444444: ',JSON.stringify(noteQuery1));
        //     tasksService.crmUpdateNoteForTemp(noteQuery1).then(function (crmresult) {
        //         console.log('CRM TEMPLATE RESULT FOR ALL FIRST================>',JSON.stringify(crmresult));
        //         setTimeout(function(){
        //             getCrmAllTasksDisplay();
        //             $scope.note = {};
        //         }, 1000);
        //     }).catch(function (error) {
        //         console.log('error',error);
        //     });
        // }
        $scope.snoozeArray = [];
        $scope.openSnoozedTaskModal = function (task,event) {
           // console.log('snooz 111111111111',JSON.stringify(task))
            var filterSnooze = _.filter(task.data,function (task) {
                console.log('ids  111',JSON.stringify(task))
                $scope.snoozeArray.push(task.task_id);
                return task.task_id;
            });
            //console.log('$scope.snoozeArray 111111111111',JSON.stringify($scope.snoozeArray))

            event.stopPropagation();
            $('#openSnoozedPopUpModal').modal('open');
        }

        $scope.openSnoozedTaskModalDue = function (task,event) {
            //console.log('snooz 111111111111',JSON.stringify(task))
            var filterSnooze = _.filter(task.data,function (task) {
                //console.log('ids  111',JSON.stringify(task))
                $scope.snoozeArray.push(task.task_id);
                return task.task_id;
            });
            //console.log('$scope.snoozeArray 111111111111',JSON.stringify($scope.snoozeArray))

            event.stopPropagation();
            $('#openSnoozedPopUpModalDue').modal('open');
        }

        $scope.submitSnoozeTask = function () {
            $scope.spinner = true;
            var crmSnoozeIds={
                allSubTasksIds: $scope.snoozeArray
            };
            //console.log('crmSnoozeIds query',JSON.stringify(crmSnoozeIds));
            var serviceQuery = {
                number:'one'
            };
            var checkTost = 'not';
            $scope.checkToastTest(checkTost);
            mySharedService.prepForPublish(serviceQuery);
            tasksService.crmSnoozeTask(crmSnoozeIds).then(function (crmresult) {
                //console.log('CRM TEMPLATE RESULT FOR ALL FIRST================>',JSON.stringify(crmresult));
                setTimeout(function(){
                    getCrmAllTasksDisplay();
                    $scope.spinner = false;
                }, 1000);
            }).catch(function (error) {
                console.log('error',error);
            });
        }

        $scope.tasksIdsToDelete = [];
        $scope.openDeleteTaskModal = function (task,event) {
            // console.log('snooz 111111111111',JSON.stringify(task))
            var filterSnooze = _.filter(task.data,function (task) {
                //console.log('ids  111',JSON.stringify(task))
                $scope.tasksIdsToDelete.push(task.task_id);
                return task.task_id;
            });
            //console.log('$scope.tasksIdsToDelete 111111111111',JSON.stringify($scope.tasksIdsToDelete))

            event.stopPropagation();
            $('#openTaskRemovePopUpModal').modal('open');
        }

        $scope.submitTaskRemove = function () {
            $scope.spinner = true;
            var crmSnoozeIds={
                allSubTasksIds: $scope.tasksIdsToDelete
            };
            //console.log('crmSnoozeIds query',JSON.stringify(crmSnoozeIds));
            var serviceQuery = {
                number:'one'
            };
            var checkTost = 'not';
            $scope.checkToastTest(checkTost);
            mySharedService.prepForPublish(serviceQuery);
            tasksService.crmRemoveTask(crmSnoozeIds).then(function (crmresult) {
                //console.log('CRM TEMPLATE RESULT FOR ALL FIRST================>',JSON.stringify(crmresult));
                setTimeout(function(){
                    $scope.tasksIdsToDelete = [];
                    getCrmAllTasksDisplay();
                    $scope.spinner = false;
                }, 1000);
            }).catch(function (error) {
                console.log('error',error);
            });
        }

        $scope.openDeleteTaskModalDue = function (task,event) {
            // console.log('snooz 111111111111',JSON.stringify(task))
            var filterSnooze = _.filter(task.data,function (task) {
                console.log('ids  111',JSON.stringify(task))
                $scope.tasksIdsToDelete.push(task.task_id);
                return task.task_id;
            });
            //console.log('$scope.tasksIdsToDelete 111111111111',JSON.stringify($scope.tasksIdsToDelete))

            event.stopPropagation();
            $('#openTaskRemovePopUpModalDue').modal('open');
        }

        var taskClickCount = 0;
        $scope.clickedCol = function (index) {
            taskClickCount ++;
            //console.log('taskClickCount',taskClickCount);
            $scope.selected1 = index;
            //console.log('calling collapsible',index);
            $scope.backgroundImageInfo='#C2DFFF';
            $scope.backgroundImageInfodue='';
            $scope.backgroundImageInfoComp = '';
            $scope.backgroundTemp='';
            var serviceQuery = {
                number:'one'
            };
            //colorClick.prepForPublish(serviceQuery);
            //colorClickThird.prepForPublish(serviceQuery);

        }

        $scope.clickedColdue = function (index) {
            $scope.selected1 = index;
            //console.log('calling clickedColdue',index);
            $scope.backgroundImageInfodue='#C2DFFF';
            $scope.backgroundImageInfo = '';
            $scope.backgroundImageInfoComp = '';
            $scope.backgroundTemp='';
            var serviceQuery = {
                number:'one'
            };
           // colorClick.prepForPublish(serviceQuery);
            //colorClickThird.prepForPublish(serviceQuery);
        }

        $scope.clickedTempView = function (index) {
            $scope.selected1 = index;
            //console.log('calling collapsible',index);
            $scope.backgroundTemp='#C2DFFF';
            $scope.backgroundImageInfodue='';
            $scope.backgroundImageInfo = '';
            var serviceQuery = {
                number:'one'
            };
            //colorClickFive.prepForPublish(serviceQuery);

        }

        $scope.$on('handlePublishsecond', function() {
            console.log('calling servive111111111111');
            $scope.sharedmessage  = colorClicksecond.sharedmessage;
            if($scope.sharedmessage.number === 'one'){
                $scope.backgroundImageInfo='';
                $scope.backgroundImageInfodue='';
                $scope.backgroundImageInfoComp = ''
                $scope.backgroundTemp='';

            }
        });


        $scope.coppyAssigneeName = function (index,alldata,data) {
            //console.log('index',index);
            //console.log('alldata',alldata);
            var count=index - 1
            //console.log('data',JSON.stringify(data));
            for(var i = index ; i <= alldata.length ; i++ ){
                 count++
                if($scope.createtask[count]){
                    $scope.createtask[count].assignto = data
                }else{
                    $scope.createtask.push({assignto:data})
                }
                //console.log('count=========',count);
            }
        }

        $scope.coppyAssigneeForTemp = function (index,alldata,data) {
            //console.log('index',index);
            //console.log('alldata',alldata);
            var count=index - 1
            //console.log('data',JSON.stringify(data));
            for(var i = index ; i <= alldata.length ; i++ ){
                count++
                if($scope.subTasksData[count]){
                    $scope.subTasksData[count].assignto = data
                }

                // if($scope.subTasksData[count]){
                //     $scope.subTasksData[count].assignto = data
                // }else{
                //     $scope.subTasksData.push({assignto:data})
                // }
                //console.log('count=========',count);
            }
        }


    }
    ])

    .directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function(){
                    scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }])


    .service('fileUpload', ['$http', function ($http) {
        this.uploadFileToUrl = function(file, uploadUrl,cb){
            var fd = new FormData();
            fd.append('file', file);

            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })

                .success(function(result){
                    cb(null,result)
                })

                .error(function(err){
                    cb(err)
                });
        }
    }])





