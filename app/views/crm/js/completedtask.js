/**
 * Created by arif on 8/10/17.
 */

/*global moment*/

angular.module('wealthQuotientApp')
    .controller('completedTaskCtrl', ['$scope', '$timeout', '$state', '$window', '$document', '$rootScope', 'searchService', 'registerService', 'loginService','tasksService','mySharedService','colorClick','colorClicksecond','colorClickThird','colorClickFourth','colorClickFive', function ($scope, $timeout, $state, $window, $document, $rootScope, searchService, registerService, loginService,tasksService,mySharedService,colorClick,colorClicksecond,colorClickThird,colorClickFourth,colorClickFive) {

        console.log('completedTaskCtrl callingh 1111111111');

        $scope.curPage = 0;
        $scope.pageSize = 5;
        $scope.cartsData =[];
        $rootScope.spinner = false;

        getAllCrmCompetedTasks();
        function getAllCrmCompetedTasks() {
            $rootScope.spinner = true;
            tasksService.getCrmCompetedTempTasks().then(function (allTempResult) {
               //console.log('getAllCrmTasks===COMP=============>',JSON.stringify(allTempResult.data.response));
                var crmAllTasks = allTempResult.data.response;


                //console.log('crmAllTasks-------bbbbbbbbb-----------afterrrrrrr', crmAllTasks);
                // crmAllTasks = crmAllTasks.filter(function (obj) {
                //     var todaysDate = new Date();
                //     //console.log('todays date', JSON.stringify(todaysDate));
                //     if(moment(todaysDate).isAfter(obj.duedate)){
                //         //console.log('objects being filtered are ',JSON.stringify(obj));
                //     }else{
                //         return obj;
                //     }
                // });

                //console.log('the final objects are ', JSON.stringify(crmAllTasks));

                if(crmAllTasks !== 0){
                    crmAllTasks = crmAllTasks.filter(function (ele) {
                        var tempArr = Object.keys(ele);
                        //console.log('the keys are ', tempArr);
                        tempArr.map(function (arrEle, i) {
                            // console.log('the arrEle are ', arrEle);
                            if (ele[arrEle] === null) {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                            if (ele[arrEle] === "Invalid date") {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                            if (ele[arrEle] === "") {
                                ele[arrEle] = "";
                                //delete ele[arrEle];
                            }
                        });
                        //console.log(ele);
                        return ele;
                    });
                }
                //console.log('crmAllTasks-------66666666666666-----------afterrrrrrr', JSON.stringify(crmAllTasks));

                function arrayFromObject(obj) {
                    var arr = [];
                    for (var i in obj) {
                        arr.push(obj[i]);
                    }
                    return arr;
                }

                function groupBy(list, fn) {
                    var groups = {};
                    for (var i = 0; i < list.length; i++) {
                        var group = JSON.stringify(fn(list[i]));
                        if (group in groups) {
                            groups[group].push(list[i]);
                        } else {
                            groups[group] = [list[i]];
                        }
                    }
                    return arrayFromObject(groups);
                }
                var crmAllTasksfinal;
                crmAllTasksfinal = groupBy(crmAllTasks, function(item) {
                    //console.log('8888888888888888888',JSON.stringify(item));

                    return [item.task_title, item.customer_name,item.isSubtask];
                });
                var cartsDataProcess = [];
                for(var K = 0 ; K < crmAllTasksfinal.length ; K++){
                    if(crmAllTasksfinal[K].subtask_option === 'first_task'){
                        crmAllTasksfinal[K].subtask_option = 'start with first task';
                    }
                    var task_title = '';
                    var customername = '';
                    var isSubtask = '';
                    for(var j = 0 ; j < crmAllTasksfinal[K].length ; j++){
                        task_title = crmAllTasksfinal[K][j].task_title;
                        customername = crmAllTasksfinal[K][j].customer_name;
                        isSubtask = crmAllTasksfinal[K][j].isSubtask;

                    }
                    cartsDataProcess.push({task_title : task_title,customername:customername,isSubtask:isSubtask,data : crmAllTasksfinal[K]});
                }
                for(var m = 0 ; m < cartsDataProcess.length ; m++){
                    // console.log('LOOP',JSON.stringify(cartsDataProcess[m].data));
                    cartsDataProcess[m].data.sort(function(a,b) {
                        return new Date(a.dueDate) - new Date(b.dueDate);
                    });
                }

                cartsDataProcess.sort(function(first,prev){
                    //console.log('first@@@@@@@@@',JSON.stringify(first));
                    return new Date(prev.data[0].dueDate) - new Date(first.data[0].dueDate);
                });

                $scope.cartsData = cartsDataProcess;

                $rootScope.spinner = false;

            }).then(function () {
                $scope.numberOfPagescomp = function() {
                    return Math.ceil($scope.cartsData.length / $scope.pageSize);
                };
            }).catch(function (error) {
                console.log('error',error);
            });

        }


        $scope.$on('handlePublish', function() {
            console.log('calling handlePublish');
            $scope.sharedmessage  = mySharedService.sharedmessage;
            if($scope.sharedmessage.number === 'one'){
                setTimeout(function(){
                    $scope.cartsData = [];
                    getAllCrmCompetedTasks();
                }, 2000);

            }
            console.log('$scope.sharedmessage11111111111111111111',JSON.stringify($scope.sharedmessage));
            console.log('$scope.sharedmessage11111111111111111111',JSON.stringify($scope.sharedmessage.number));
        });

        $scope.clickedColComp = function (index) {
            $scope.selected1 = index;
            console.log('calling clickedColComp',index);
            $scope.backgroundImageInfoComp='#C2DFFF';
            var serviceQuery = {
                number:'one'
            };
            //colorClick.prepForPublish(serviceQuery);
            //colorClicksecond.prepForPublish(serviceQuery);
        };
        //

        $scope.$on('handlePublishFourth', function() {
            //console.log('calling servive111111111111');
            $scope.sharedmessage  = colorClickFourth.sharedmessage;
            if($scope.sharedmessage.number === 'one'){
                $scope.backgroundImageInfoComp='';

            }
        });

        $scope.$on('handlePublishThird', function() {
            //console.log('calling servive111111111111');
           // console.log('clickColor 3333333333333',$rootScope.clickColor);
            $scope.sharedmessage  = colorClickThird.sharedmessage;
            if($scope.sharedmessage.number === 'one'){
                $scope.backgroundImageInfoComp='';

            }
        });

        $scope.$on('handlePublishFive', function() {
            //console.log('calling servive111111111111');
            $scope.sharedmessage  = colorClickFive.sharedmessage;
            if($scope.sharedmessage.number === 'one'){
                $scope.backgroundImageInfoComp='';
            }
        });



    }]);
