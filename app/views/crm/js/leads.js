'use strict';

/**
 * @ngdoc function
 * @name wealthQuotientApp.controller:leadsCtrl
 * @description
 * # leadsCtrl
 * leadsCtrl of the wealthQuotientApp
 */

angular.module('wealthQuotientApp')
    .controller('leadsCtrl', ['$scope', '$timeout', '$state', '$window', '$document', '$rootScope', 'searchService', 'registerService', 'loginService', 'leadsService', 'Store', '$location', '$anchorScroll', '$q', '$log', function ($scope, $timeout, $state, $window, $document, $rootScope, searchService, registerService, loginService, leadsService, Store, $location, $anchorScroll, $q, $log) {

        $scope.leadsService = leadsService;

        var $leadsChips = angular.element('.chips.leads-searchBar');
        var $cLChips = angular.element('.chips.cL-searchBar');
        $scope.regexFirstName = '^[a-zA-Z]+$';
        $scope.regexLastName = '^[a-zA-Z]*$';
        $scope.regexEmail = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
        $scope.regexPhone = /^[0-9]{10,10}$/;
        $scope.regexNumber = /^[0-9]*$/;
        $scope.regExpUsername = /^[a-z0-9A-Z]+$/;
        $scope.regExHyperLink = /^(http[s]?:\/\/){1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
        $scope.passMatchFailed = false;


        $scope.Store = Store;
        $scope.disableAddStatusBtn = false;
        $scope.leadFlowStatus = [];
        $scope.partnerLeadsFlow = [];
        $scope.leadsCheck = false;
        $scope.partnerLeads = [];
        $scope.leadObj = {};
        $scope.leadCFData = {};
        $scope.sales = {};
        $scope.updateLeadCheck = false;
        $scope.tempLeadID = '';
        $scope.convertLeadObj = {};
        $scope.displayConverted = false;
        $scope.cfCheck = false;
        $scope.allCFs = [];
        $scope.activeCFs = [];
        $scope.CFforLeadsTable = [];
        $scope.disableAddCFBtn = false;
        $scope.deletedCF = [];
        var deletedCF_names = [];
        $scope.deletedLFStatus = [];
        $scope.leadsSearchInput = '';
        $scope.cLSearchInputs = [];
        $scope.searchInputs = [];
        $scope.allFields = [];
        $scope.templateFields = [];
        var excelHeaderFields = [];
        $scope.blankSheet = [];
        $scope.forShowHideFields = [];
        $scope.addCFCheck = false;
        $scope.addCFObj = {};
        $scope.cfName = '';
        $scope.cfType = '';
        $scope.dropDownCF = {
            field_name: '',
            cf_type: '',
            list: []
        };
        $scope.editDropDownCFCheck = false;
        $scope.deleteItem = '';
        $scope.deleteItemCFName = '';
        $scope.editItem = '';
        $scope.listItemEditCheck = false;
        $scope.disableAddCFItem_btn = false;
        $scope.tableHeaders = [];
        $scope.reorderFieldsList = [];
        $scope.forTable = [];
        $scope.cLCheck = false;
        $scope.convertedHeads = [];
        $scope.convertedLeads = [];
        $scope.forConvertedTable = [];
        $scope.fileUploadValidation = false;

        $scope.validationSuccess = false;
        $scope.validationFail_headers = false;
        $scope.disableFileUpload = true;
        $scope.importLeadsFile = {};
        $scope.totalLeads = 0;

        $scope.forbiddenFieldNames = ['First Name', 'Last Name', 'Email', 'Mobile', 'Source', 'Status', 'Assigned To', 'Action Date', 'Notes'];
        const pageZero = 0;
        const pageSize = 10;
        $scope.pageCount = [];
        $scope.currentPage = 0;
        $scope.leadsOnDisplay = {
            startValue: 0,
            endValue: 0
        };

        $scope.totalConvertedLeads = 0;
        $scope.pageCountCL = [];
        $scope.currentPageCL = 0;
        $scope.convertedLeadsOnDisplay = {
            startValue: 0,
            endValue: 0
        };

        $scope.reorderListLoader = false;
        $scope.manageCFModalLoader = false;
        $scope.convertedLeadsBtnLoader = false;
        $scope.noLeadsCheck = false;

        /**
         * PAGINATION functions
         * Count pages, change page ....
         */
        var numberOfPages = function (totalRecords, pgSize) {
            return parseInt(Math.ceil(totalRecords / pgSize));
        };

        var numberOfSearchPages = function () {
            return parseInt(Math.ceil(search_totalRecords / 10));
        };

        var setLeadsOnDisplay = function (pageNum, currentArrLen) {
            $scope.leadsOnDisplay.startValue = parseInt((pageNum * 10) + 1);
            $scope.leadsOnDisplay.endValue = parseInt((pageNum * 10) + currentArrLen);
        };

        $scope.changePage = function (pageTo) {
            $scope.tableLoader = true;
            if ($scope.underLeadsSearch) {
                var searchObj = {
                    searchID: currentLeadsSearchID
                };
                leadsService.getSearchedLeadsData(searchObj, pageTo).then(function (result) {
                    $log.debug('Change searchedLeads page result', result);
                    $scope.tableLoader = false;
                    if (result.data.status) {
                        $scope.partnerLeads.length = 0;
                        $scope.partnerLeads = result.data.response;
                        $scope.currentPage = pageTo;
                        setLeadsOnDisplay(pageTo, $scope.partnerLeads.length);
                    }
                    else {
                        $log.error('Couldn\'t update new page', pageTo + 1);
                        $rootScope.errorToast('Error while fetching leads data');
                    }
                }).catch(function (error) {
                    $scope.tableLoader = false;
                    $log.error('While fetching searched leads data on changeSearchedLeadsPage()', error);
                    $rootScope.errorToast('Error while fetching searched leads data');
                });
            }
            else {
                getAllLeads(pageTo, false).then(function () {
                    $scope.tableLoader = false;
                }).catch(function (error) {
                    $scope.tableLoader = false;
                    $log.error('Error in changePage()', error);
                    $rootScope.errorToast('Error while fetching leads for partner');
                });
            }
        };

        $scope.changeCLPage = function (pageTo) {
            if ($scope.underCLSearch) {
                searchCL($scope.cLSearchInputs, pageTo);
            }
            else {
                fetchConvertedLeads(pageTo).then(function () {
                    $log.debug('fetchConvertedLeads() success in changeCLPage()');
                }).catch(function (error) {
                    $log.error('Error in fetchConvertedLeads() from changeCLPage()', error);
                    $rootScope.errorToast('Error while fetching converted leads for partner');
                });
            }

        };

        $scope.copyTextError = function (e) {
            //console.log('Error while copying field value', e);
            $rootScope.errorToast('Error while copying');
        };

        $scope.copyTextSuccess = function (e) {
            //console.log('Copied field value successfully', e);
            $rootScope.successToast('Copied');

            e.clearSelection();
        };


        /**
         * Initial data for CRM LEADS PAGE
         * Data for LeadsFlow, PartnerLeads and CustomFields
         */

        //-- pre-fill Update Leads Flow modal --//
        if (Store.leadsFlowCheck && Store.leadsFlowData.length > 0) {
            $scope.leadFlowStatus.length = 0;   //Initialising for new data
            $scope.partnerLeadsFlow.length = 0;
            Store.leadsFlowData = _.sortBy(Store.leadsFlowData, 'sort_id');
            for (var i = 0; i < Store.leadsFlowData.length; i++) {
                $scope.leadFlowStatus = Store.leadsFlowData;
                $scope.partnerLeadsFlow.push(Store.leadsFlowData[i].status_name);
            }
            $scope.leadFlowStatus = _.sortBy($scope.leadFlowStatus, 'sort_id');
        }
        else {
            $scope.leadFlowStatus = [{}]; //Initialize leadFlowStatus array for one Status-field enabled by default
            $scope.partnerLeadsFlow.push('Create Leads Flow to add status');
        }
        //console.log('leadsFlowData & selectStatusList', $scope.leadFlowStatus, $scope.partnerLeadsFlow);


        //-- allCFs from Store --//
        // if (Store.customFieldsCheck && Store.customFieldsData.length > 0) {
        //     $scope.cfCheck = Store.customFieldsCheck;
        //     $scope.allCFs = Store.customFieldsData;
        //     $scope.allCFs = _.sortBy($scope.allCFs, 'sort_id');
        //     _.each($scope.allCFs, function (cf) {
        //         //console.log('CF fetched', cf);
        //     })
        //     //console.log('ALL customFields data taken into ctrl', $scope.allCFs);
        //
        //     $scope.activeCFs = _.filter($scope.allCFs, function (cfObj) {
        //         return cfObj.is_deleted !== 'Y';
        //     })
        //     //console.log('ACTIVE CF data taken into ctrl', $scope.activeCFs);
        // }
        // else {
        //     $scope.cfCheck = Store.customFieldsCheck;
        //     //Initialize allCFs array by default
        //     $scope.allCFs = [];
        // }
        // $log.debug('ALL customFieldsData', $scope.allCFs, $scope.cfCheck);

        //-- populate Leads table --//
        // if (Store.partnerLeads !== null) {
        //     $scope.leadsCheck = true;
        //     $scope.partnerLeads = Store.partnerLeads;
        //     $scope.totalLeads = $scope.partnerLeads[0].TOTAL_LEADS;
        //
        //     //Initialize with pageCount with pageNumber = 0
        //     _.times(numberOfPages($scope.totalLeads, pageSize), function (index) {
        //         $scope.pageCount.push(index + 1);
        //     });
        //     //Initialize leadsOnDisplay with pageNumber = 0
        //     setLeadsOnDisplay(pageZero, $scope.partnerLeads.length);
        //     $log.debug('all leads>>>', $scope.partnerLeads, $scope.pageCount);
        // }
        // else {
        //     $scope.leadsCheck = false;
        //     $log.debug('No leads present', $scope.leadsCheck);
        // }
        // //console.log('prefilled leads data', $scope.partnerLeads);


        /**
         * Generates a JS-XLSX file download.
         */
        var s2ab = function (s) {
            var buf = new ArrayBuffer(s.length);
            var view = new Uint8Array(buf);
            for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
            return buf;
        };


        /**
         * Gets ALL Leads table Fields.
         * NormalField and CustomField defined by Partner.
         */
        var setTableHeaders = function () {
            var deferred = $q.defer();
            $scope.tableHeaders.length = 0;
            leadsService.getVisibleFields().then(function (result) {
                if(result.data.status){
                    $log.debug('getVisibleFields result>>>', result.data.response);
                    var filteredArr = _.filter(result.data.response, function (fieldObj) {
                        return (fieldObj.field_name !== 'first_name' && fieldObj.field_name !== 'last_name');
                    });
                    $log.debug('filteredArr result>>>', filteredArr);

                    var sortedArr = _.sortBy(filteredArr, 'sort_id');

                    $scope.tableHeaders = JSON.parse(JSON.stringify(sortedArr));

                    $scope.forTable = JSON.parse(JSON.stringify(sortedArr));

                    _.each($scope.tableHeaders, function (field) {
                        if (field.field_name === 'email') {
                            field.field_name = 'Email';
                        }
                        else if (field.field_name === 'mobile') {
                            field.field_name = 'Mobile';
                        }
                        else if (field.field_name === 'source') {
                            field.field_name = 'Source';
                        }
                        else if (field.field_name === 'status') {
                            field.field_name = 'Status';
                        }
                        else if (field.field_name === 'assignedto') {
                            field.field_name = 'Assigned To';
                        }
                        else if (field.field_name === 'action_date') {
                            field.field_name = 'Action Date';
                        }
                        else if (field.field_name === 'notes') {
                            field.field_name = 'Notes';
                        }
                    });

                    $log.debug('sorted result>>>', $scope.tableHeaders, $scope.forTable);
                    deferred.resolve();
                }
                else{
                    $log.debug('No Data for setTableHeaders()', result);
                   $scope.tableHeaders = [];
                   $scope.forTable = [];
                   deferred.resolve();
                }

            }).catch(function (error) {
                $log.error('Error in getVisibleFields()', error);
                deferred.reject(error);
            });
            return deferred.promise;
        };
        setTableHeaders();

        var setActiveCustomFields = function () {
            //fetch ONLY active CF, `is_deleted` == 'N'
            leadsService.getActiveCustomFields().then(function (result) {
                $log.debug('setActiveCustomFields() result>>>', result);
                Store.customFieldsCheck = result.data.status;
                $log.debug('cf check in Store variables', Store.customFieldsCheck);
                if (result.data.status) {
                    $scope.activeCFs = result.data.response;
                    $scope.activeCFs = _.sortBy($scope.activeCFs, 'field_name');
                    //console.log('Custom fields fetched', $scope.activeCFs);
                    // $scope.CFforLeadsTable.length = 0; //Re-Initialize CFforLeadsTable array
                    // _.each($scope.activeCFs, function (cf) {
                    //     //console.log('cf for Leads Table header', cf);
                    //     $scope.CFforLeadsTable.push(cf.field_name);
                    // })
                    // //console.log('header cfs updated from controller', $scope.CFforLeadsTable);
                }
                else {
                    $scope.activeCFs.length = 0;
                    $scope.activeCFs.push({});
                }
            }).catch(function (error) {
                //console.log(error);
            });
        };
        setActiveCustomFields();

        var setManageCFList = function () {
            var deferred = $q.defer();
            //fetch ALL CF
            leadsService.getAllCustomFields().then(function (result) {
                $log.debug('getAllCustomFields result for setManageCFList>>>', result);
                if (result.data.status) {
                    // var processArr = JSON.parse(JSON.stringify(result.data.response));
                    _.each(result.data.response, function (cf) {
                        if (cf.is_deleted === 'N') {
                            cf.active = true;
                        }
                        else {
                            cf.active = false;
                        }
                        if (cf.visibility === 'Y') {
                            cf.visible = true;
                        }
                        else {
                            cf.visible = false;
                        }
                    });
                    $log.debug('Copy this to All CFs array', result.data.response);
                    $scope.allCFs = result.data.response;
                    $scope.cfCheck = true;
                    //CF restriction(only on front-end), limit = 100
                    if ($scope.allCFs.length >= 100) {
                        $scope.disableAddCFBtn = true;
                        $scope.errorToast('Can not add more than 100 fields');
                    }
                    deferred.resolve();
                }
                else {
                    $log.debug('No CF found', result.data);
                    deferred.resolve();
                }
            }).catch(function (error) {
                $log.error('Error from getAllCustomFields() service', error);
                deferred.reject(error);
            });
            return deferred.promise;
        };

        // setManageCFList();


        var setReorderFields = function () {
            var deferred = $q.defer();
            //fetch ALL fields, remove first_name and last_name
            leadsService.getAllFields().then(function (result) {
                $log.debug('Get All fields for setReorderFields() result>>>', result);
                if(result.data.status){
                    var resultData = result.data.response;
                    resultData = _.filter(resultData, function (fieldObj) {
                        return (fieldObj.field_name !== 'first_name' && fieldObj.field_name !== 'last_name');
                    });
                    resultData = _.sortBy(resultData, 'sort_id');
                    _.each(resultData, function (field) {
                        if (field.field_name === 'email') {
                            field.field_name = 'Email';
                        }
                        else if (field.field_name === 'mobile') {
                            field.field_name = 'Mobile';
                        }
                        else if (field.field_name === 'source') {
                            field.field_name = 'Source';
                        }
                        else if (field.field_name === 'status') {
                            field.field_name = 'Status';
                        }
                        else if (field.field_name === 'assignedto') {
                            field.field_name = 'Assigned To';
                        }
                        else if (field.field_name === 'action_date') {
                            field.field_name = 'Action Date';
                        }
                        else if (field.field_name === 'notes') {
                            field.field_name = 'Notes';
                        }
                    });
                    $scope.reorderFieldsList = JSON.parse(JSON.stringify(resultData));
                    //console.log('prepared list for reordering>>>', $scope.reorderFieldsList);
                    deferred.resolve();
                }
                else{
                    $scope.reorderFieldsList = [];
                    $log.debug('No fields for setReorderFields(), check result>>>', result);
                    deferred.resolve();
                }
            }).catch(function (error) {
                $log.error('Error in getAllFields service', error);
                deferred.reject(error);
            });

            return deferred.promise;
        };


        // setReorderFields();


        // $scope.getAllFields = function () {
        //     $scope.allFields = [];
        //     $scope.reorderFieldsList = [];
        //     // $scope.forTable = [];
        //     // $scope.tableHeaders = [];
        //     // var tempFields = [];
        //     leadsService.getAllFields().then(function (success) {
        //         //console.log('success', success.data);
        //         if (success.data.status) {
        //             // $scope.allFields = success.data.response;
        //             // //console.log('ALL fields fetched on update', $scope.allFields);
        //             //
        //             // //--- SET UP ACTIVE CF ---//
        //             // $scope.activeCFs = _.map($scope.allFields, _.clone);
        //             // $scope.activeCFs = _.filter($scope.activeCFs, function (fieldObj) {
        //             //     return (fieldObj.field_type === 'cf' && fieldObj.is_deleted === 'N');
        //             // })
        //
        //             // //--- REMOVE FIRST_NAME AND LAST_NAME FROM REORDER LIST ---//
        //             // $scope.reorderFieldsList = _.map($scope.allFields, _.clone)
        //             // $scope.reorderFieldsList = _.filter($scope.reorderFieldsList, function (fieldObj) {
        //             //     return (fieldObj.field_name !== 'first_name' && fieldObj.field_name !== 'last_name');
        //             // });
        //             // $scope.reorderFieldsList = _.sortBy($scope.reorderFieldsList, 'sort_id');
        //             // _.each($scope.reorderFieldsList, function (field) {
        //             //     if (field.field_name === 'email') {
        //             //         field.field_name = 'Email';
        //             //     }
        //             //     else if (field.field_name === 'mobile') {
        //             //         field.field_name = 'Mobile';
        //             //     }
        //             //     else if (field.field_name === 'source') {
        //             //         field.field_name = 'Source';
        //             //     }
        //             //     else if (field.field_name === 'status') {
        //             //         field.field_name = 'Status';
        //             //     }
        //             //     else if (field.field_name === 'assignedto') {
        //             //         field.field_name = 'Assigned To';
        //             //     }
        //             //     else if (field.field_name === 'action_date') {
        //             //         field.field_name = 'Action Date';
        //             //     }
        //             //     else if (field.field_name === 'notes') {
        //             //         field.field_name = 'Notes';
        //             //     }
        //             // });
        //             // //console.log('prepared list for reordering', $scope.reorderFieldsList);
        //
        //             //--- SET TABLE HEADERS(Only active fields) ---//
        //             // $scope.tableHeaders = _.map($scope.reorderFieldsList, _.clone)
        //             // $scope.tableHeaders = _.filter($scope.tableHeaders, function (fieldObj) {
        //             //     return fieldObj.is_deleted !== 'Y' && fieldObj.visibility === 'Y';
        //             // });
        //             // $scope.tableHeaders = _.sortBy($scope.tableHeaders, 'sort_id');
        //             // $scope.tableHeaders = _.filter($scope.tableHeaders, function (fieldObj) {
        //             //     return (fieldObj.field_name !== 'first_name' && fieldObj.field_name !== 'last_name');
        //             // });
        //             // //console.log('tableHeaders --->', $scope.tableHeaders);
        //
        //             //--- SET UP LEADS TABLE DATA ---//
        //             // tempFields = _.map($scope.tableHeaders, _.clone);
        //             // tempFields = _.sortBy(tempFields, 'sort_id');
        //             // _.filter(tempFields, function (fieldObj) {
        //             //     $scope.forTable.push(fieldObj);
        //             // })
        //             // _.each($scope.forTable, function (fieldObj, index) {
        //             //     if (fieldObj.field_name === 'Email') {
        //             //         $scope.forTable[index].field_name = 'email';
        //             //     }
        //             //     else if (fieldObj.field_name === 'Mobile') {
        //             //         $scope.forTable[index].field_name = 'mobile';
        //             //     }
        //             //     else if (fieldObj.field_name === 'Source') {
        //             //         $scope.forTable[index].field_name = 'source';
        //             //     }
        //             //     else if (fieldObj.field_name === 'Status') {
        //             //         $scope.forTable[index].field_name = 'status';
        //             //     }
        //             //     else if (fieldObj.field_name === 'Assigned To') {
        //             //         $scope.forTable[index].field_name = 'assignedto';
        //             //     }
        //             //     else if (fieldObj.field_name === 'Action Date') {
        //             //         $scope.forTable[index].field_name = 'action_date';
        //             //     }
        //             //     else if (fieldObj.field_name === 'Notes') {
        //             //         $scope.forTable[index].field_name = 'notes';
        //             //     }
        //             // });
        //             // //console.log('for TABLE', $scope.forTable);
        //         }
        //     }).catch(function (error) {
        //         //console.log('error', error);
        //         $rootScope.errorToast('Error while fetching leads table fields');
        //     });
        // };
        // $scope.getAllFields();

        /**
         * Gets LeadsFlow for the partner-leads
         * Call this after Adding or updating leads flow
         */
        var getLeadsFlow = function () {
            leadsService.getLeadsFlow().then(function (result) {
                //console.log('update leads flow post creation', result.data);
                if (result.data.status) {
                    //console.log('leadsFlow fetched into ctrl', result.data);
                    Store.leadsFlowCheck = true;
                    $scope.leadFlowStatus.length = 0;//empty array for new data -- for leadsFlow Modal
                    $scope.leadFlowStatus = result.data.response;
                    $scope.leadFlowStatus = _.sortBy($scope.leadFlowStatus, 'sort_id');
                    $scope.partnerLeadsFlow.length = 0;//empty array for new data -- for addLead Modal
                    for (var i = 0; i < $scope.leadFlowStatus.length; i++) {
                        $scope.partnerLeadsFlow.push($scope.leadFlowStatus[i].status_name);
                    }
                } else {
                    Store.leadsFlowCheck = false;
                    // $rootScope.successToast('No Leads flow data for partner');
                }
                //console.log('Leads flow check and data post updation', Store.leadsFlowCheck, $scope.leadFlowStatus);
            }).catch(function (error) {
                // $rootScope.errorToast('Error while fetching Leads Flow status');
                //console.log(error);
            });
        };

        /**
         * Gets all Leads for partner
         * Call this after Add, Update and Delete Leads
         */
        var getAllLeads = function (pageNo, updatePagination) {
            $scope.tableLoader = true; //start Loader
            var deferred = $q.defer();
            $scope.partnerLeads.length = 0;
            leadsService.getAllLeads(pageNo).then(function (result) {
                //console.log('getAllLeads', result);
                $scope.tableLoader = false;
                if (result.data.status) {
                    // $scope.leadsCheck = true;
                    $scope.partnerLeads = result.data.response;
                    $log.info('Updated Leads array', $scope.partnerLeads);
                    /**
                     * Update Pagination on first-call and post
                     * addLead(), deleteLead(), convertLead(), uploadLeads()
                     */
                    if (updatePagination) {
                        $scope.totalLeads = $scope.partnerLeads[0].TOTAL_LEADS;
                        $scope.pageCount.length = 0; //empty pageCount array for new pageCount
                        _.times(numberOfPages($scope.totalLeads, pageSize), function (index) {
                            $scope.pageCount.push(index + 1);
                        });
                        //Update leadsOnDisplay with passed pageNumber and current partnerLeads array length
                        setLeadsOnDisplay(pageNo, $scope.partnerLeads.length);
                        //Update currentPage to passed page number
                        $scope.currentPage = pageNo;
                    }
                    else {
                        //Update currentPage to passed page number
                        $scope.currentPage = pageNo;
                        //Update leadsOnDisplay with passed pageNumber and current partnerLeads array length
                        setLeadsOnDisplay(pageNo, $scope.partnerLeads.length);
                    }
                    deferred.resolve();
                }
                else {
                    $scope.noLeadsCheck = true;
                    $log.warn('No leads for partner');
                    deferred.resolve();
                }
            }).catch(function (error) {
                $scope.tableLoader = false;
                $log.error('Error while fetching all leads', error);
                deferred.reject();
            });
            return deferred.promise;
        };
        getAllLeads(pageZero, true);

        /**
         * Gets all CONVERTED Leads for partner
         * Call this in the beginning and after converting Leads
         */
        var getConvertedLeadsCount = function () {
            $scope.convertedLeadsBtnLoader = true;
            leadsService.convertedLeadsCount().then(function (result) {
                $log.debug('Count Converted Leads', result);
                $scope.cLCheck = result.data.status;
                $scope.convertedLeadsBtnLoader = false;
            }).catch(function (error) {
                $log.error(error);
                $scope.convertedLeadsBtnLoader = false;
            });
        };
        getConvertedLeadsCount();

        var fetchConvertedLeads = function (pageNumber) {
            $scope.tableLoader = true;
            var deferred = $q.defer();
            leadsService.getConvertedLeads(pageNumber).then(function (result) {
                $scope.tableLoader = false;
                $log.debug('result for getAllConvertedLeads api', pageNumber, result);
                if (result.data.status) {
                    $scope.totalConvertedLeads = parseInt(result.data.response[0].TOTAL_CONVERTED_LEADS);
                    $scope.pageCountCL.length = 0; //Empty pageCountCL for new data
                    _.times(numberOfPages($scope.totalConvertedLeads, pageSize), function (index) {
                        $scope.pageCountCL.push(index + 1);
                    });
                    $scope.currentPageCL = pageNumber;
                    $scope.convertedLeads = result.data.response;
                    $scope.cLCheck = true;
                    setConvertedLeadsOnDisplay(pageNumber, $scope.convertedLeads.length);
                    $log.debug('converted leads check', $scope.cLCheck);
                    deferred.resolve();
                }
                else {
                    //No ACTIVE(is_deleted = 'N') Converted Leads
                    $scope.cLCheck = false;
                    deferred.resolve();
                }
            }).catch(function (error) {
                $scope.tableLoader = false;
                $log.error('Error while fetching converted leads', error);
                deferred.reject();
            });
            return deferred.promise;
        };

        /**
         * Gets ALL Custom Fields defined by Partner
         * Call this enabling/disabling Custom Fields
         */
        // var getAllCustomFields = function () {
        //     leadsService.getAllCustomFields().then(function (result) {
        //         if (result.data.status) {
        //             //console.log('all cfs', result);
        //             $scope.cfCheck = true;
        //             $scope.allCFs = result.data.response;
        //             _.each($scope.allCFs, function (cf) {
        //                 if (cf.is_deleted === 'N') {
        //                     cf.active = true;
        //                 }
        //                 else {
        //                     cf.active = false;
        //                 }
        //                 if (cf.visibility === 'Y') {
        //                     cf.visible = true;
        //                 }
        //                 else {
        //                     cf.visible = false;
        //                 }
        //             });
        //             //console.log('updated array for cfs', $scope.allCFs);
        //             if ($scope.allCFs.length >= 100) {
        //                 $scope.disableAddCFBtn = true;
        //                 $scope.errorToast('Can not add more than 100 fields');
        //             }
        //         }
        //         else {
        //             //console.log('No CF added', result.data);
        //         }
        //     }).catch(function (error) {
        //         //console.log(error);
        //     });
        // };

        /**
         * MOVE function to sort Lists
         * and assign sort_id accordingly
         */
        function move(arr, old_index, new_index) {
            while (old_index < 0) {
                old_index += arr.length;
            }
            while (new_index < 0) {
                new_index += arr.length;
            }
            if (new_index >= arr.length) {
                var k = new_index - arr.length;
                while ((k--) + 1) {
                    arr.push(undefined);
                }
            }
            arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
            return arr;
        }

        /**
         * Gets DropDown CustomField list data
         * Call this after updating CF DropDown List
         */
        var getDropDownCFList = function (cfObj) {
            leadsService.getCFDropDownList(cfObj).then(function (result) {
                if (result.data.status) {
                    //console.log('CF DropDown list data fetched', result);
                    $scope.dropDownCF.updateList = [];
                    _.each(result.data.response, function (listObj) {
                        $scope.dropDownCF.updateList.push(listObj.dropDown_item);
                    });
                }
            }).catch(function (error) {
                //console.log(error);
            });
        };


        /**
         * Leads Flow section
         * Add, Remove, Create and Update Leads Flow Status
         * functions here
         */
        $scope.addLeadsFlowStatus = function () {
            //push new object for next empty Status-field to be displayed
            $scope.testStatus = {};
            $scope.leadFlowStatus.push($scope.testStatus);
            //console.log('$scope.leadFlowStatus>>>>>>>>>>>>..', $scope.leadFlowStatus);
            //console.log('$scope.leadFlowStatus>>>>>>>>>>>>..', $scope.leadFlowStatus.length);
            if ($scope.leadFlowStatus.length >= 100) {
                $rootScope.errorToast('Can not add more than 100 status');
                $scope.disableAddStatusBtn = true;
            }
        };

        $scope.removeLeadsFlowStatus = function (index, sID) {
            if (sID === undefined) {
                //for NEW CF
                $scope.leadFlowStatus.splice(index, 1);
                //console.log('remove NEW>>>> Status', index);
                //console.log('removed new LF status', $scope.leadFlowStatus);
            }
            else {
                //for OLD CF
                $scope.leadFlowStatus.splice(index, 1);
                //console.log('remove this LF status', index, sID);
                //console.log('removed old lf status', $scope.leadFlowStatus);
                $scope.deletedLFStatus.push(sID);
                //console.log('to be deleted status', $scope.deletedLFStatus);
            }
        };

        $scope.openSortLFModal = function () {
            _.each($scope.leadFlowStatus, function (item, index) {
                item.sort_id = index + 1;
                //console.log('ye dekh pehle', $scope.leadFlowStatus);
            });
            //console.log('inital', $scope.leadFlowStatus);
            var list123 = document.getElementById("sortThis");
            var sortable = new Sortable(list123, {
                // Element dragging ended
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent

                    $scope.leadFlowStatus = move($scope.leadFlowStatus, evt.oldIndex, evt.newIndex)
                    _.each($scope.leadFlowStatus, function (item, index) {
                        item.sort_id = index + 1;
                        //console.log('ye dekh pehle', $scope.leadFlowStatus);
                    });
                }
            });

            $('#leadsFlowModal').modal('close');
            $('#sortLFModal').modal('open');
        }

        $scope.createLeadsFlow = function () {
            $scope.leadFlowStatus = _.sortBy($scope.leadFlowStatus, 'sort_id');
            leadsService.createLeadsFlow($scope.leadFlowStatus).then(function (result) {
                //console.log('leads flow updated/inserted', result);
                if (result.data.status) {
                    //console.log('leads flow created', result.data);
                    getLeadsFlow();
                }
                $('#sortLFModal').modal('close');
                $rootScope.successToast('Leads Flow created');
            }).catch(function (error) {
                //console.log('error while adding leadsFlow status', error);
                $rootScope.errorToast('Error while creating Leads Flow');
            });
        };

        $scope.updateLeadsFlow = function () {
            $scope.leadFlowStatus = _.sortBy($scope.leadFlowStatus, 'sort_id');
            //console.log('deleted lf status', $scope.deletedLFStatus);
            //console.log('update this', $scope.leadFlowStatus);

            function updateSuccess() {
                $scope.deletedLFStatus = [];
                getLeadsFlow(); //update Leads Flow called
                $('#sortLFModal').modal('close');
                $rootScope.successToast('Leads Flow updated successfully');
            }

            var updateLFPromise = function () {
                new Promise(function (resolve, reject) {
                    if ($scope.deletedLFStatus.length !== 0) {
                        leadsService.deleteLeadsFlow($scope.deletedLFStatus).then(function (result) {
                            //console.log('after status deletion', result);
                            resolve();
                        }).catch(function (error) {
                            //console.log(error);
                        });
                    } else {
                        resolve();
                    }
                }).then(function (resolved_data) {
                    leadsService.createLeadsFlow($scope.leadFlowStatus).then(function (result) {
                        //console.log('post addition of new status in leads flow', result);
                        updateSuccess();
                    }).catch(function (error) {
                        //console.log(error);
                    });
                }).catch(function (error) {
                    //console.log("error is ", error);
                });
            }
            updateLFPromise();

            $('#leadsFlowModal').modal('close');

        }


        /**
         * Normal Fields and Custom Fields section
         * Add, Remove, Create and Update Custom Fields
         * functions here
         */

        // var fieldsList = document.getElementById("reorderFields");
        // $log.debug('ID fetched', fieldsList);
        // var sortableFields = Sortable.create(document.getElementById("reorderFields"), {
        //     // Element dragging ended
        //     onEnd: function (/**Event*/evt) {
        //         evt.oldIndex;  // element's old index within parent
        //         evt.newIndex;  // element's new index within parent
        //
        //         (function () {
        //             return new Promise(function (resolve, reject) {
        //                 $scope.reorderListLoader = true;
        //                 $scope.reorderFieldsList = move($scope.reorderFieldsList, evt.oldIndex, evt.newIndex);
        //                 _.each($scope.reorderFieldsList, function (item, index) {
        //                     item.sort_id = index + 1;
        //                     //console.log('sorted fieldsList', $scope.reorderFieldsList);
        //                     if(index === $scope.reorderFieldsList.length - 1){
        //                         resolve();
        //                     }
        //                 });
        //             })
        //         })().then(function () {
        //             leadsService.reorderFields($scope.reorderFieldsList).then(function (res) {
        //                 //console.log('Sort updation result', res);
        //                 if (res.data.status) {
        //                     // $('#reorderFieldsModal').modal('close');
        //                     $rootScope.successToast('Field order updated successfully');
        //                     setTableHeaders();
        //                     $scope.reorderListLoader = false;
        //                 }
        //             }).catch(function (err) {
        //                 //console.log('Error while updation sort order of fields', err);
        //                 $rootScope.errorToast('Error while updating fields sort order');
        //             });
        //         })
        //     }
        // });

        $scope.openReorderModal = function () {
            var fieldsList = document.getElementById("reorderFields");
            $log.debug('ID fetched', fieldsList);
            var sortableFields = Sortable.create(document.getElementById("reorderFields"), {
                // Element dragging ended
                onEnd: function (/**Event*/evt) {
                    evt.oldIndex;  // element's old index within parent
                    evt.newIndex;  // element's new index within parent

                    (function () {
                        return new Promise(function (resolve, reject) {
                            $scope.reorderListLoader = true;
                            $scope.reorderFieldsList = move($scope.reorderFieldsList, evt.oldIndex, evt.newIndex);
                            _.each($scope.reorderFieldsList, function (item, index) {
                                item.sort_id = index + 1;
                                //console.log('sorted fieldsList', $scope.reorderFieldsList);
                                if(index === $scope.reorderFieldsList.length - 1){
                                    resolve();
                                }
                            });
                        })
                    })().then(function () {
                        leadsService.reorderFields($scope.reorderFieldsList).then(function (res) {
                            //console.log('Sort updation result', res);
                            if (res.data.status) {
                                // $('#reorderFieldsModal').modal('close');
                                $rootScope.successToast('Field order updated successfully');
                                setTableHeaders();
                                $scope.reorderListLoader = false;
                            }
                        }).catch(function (err) {
                            //console.log('Error while updation sort order of fields', err);
                            $rootScope.errorToast('Error while updating fields sort order');
                        });
                    })
                }
            });
            setReorderFields().then(function () {
                $('#reorderFieldsModal').modal('open');
            })
        };

        // $scope.reorderFields = function () {
        //     var fieldArr = _.map($scope.reorderFieldsList, _.clone);
        //     //console.log('ALl fields', $scope.reorderFieldsList);
        //
        //     //console.log('??????', fieldArr);
        //     leadsService.reorderFields(fieldArr).then(function (res) {
        //         //console.log('Sort updation result', res);
        //         if (res.data.status) {
        //             $('#reorderFieldsModal').modal('close');
        //             $rootScope.successToast('Field order updated successfully');
        //             setTableHeaders();
        //         }
        //     }).catch(function (err) {
        //         //console.log('Error while updation sort order of fields', err);
        //         $rootScope.errorToast('Error while updating fields sort order');
        //     });
        // };

        $scope.closeMCFModal = function () {
            setTableHeaders().then(function () {
                //Be on the same page
                getAllLeads($scope.currentPage, false).then(function () {
                    $log.debug('Success getAllLeads post closeMCFModal with currentPage', $scope.currentPage);
                    $('#manageCFModal').modal('close');
                });
            })
            // $timeout(function () {
            //     $scope.getAllFields();
            // });
        };

        $scope.goToListBottom = function () {
            $location.hash('newCF_id');
            $anchorScroll();
        };

        $scope.toggleAddCFCheck = function (toggleVal) {
            $log.debug('curr val>>>', toggleVal);
            $scope.addCFCheck = toggleVal;
            if(toggleVal == false){
                $scope.addCFObj = {};
            }
            $log.debug('updated val>>>', $scope.addCFCheck, $scope.addCFObj);
        };

        $scope.checkCFNameUnique = function (form) {
            // if ($scope.cfName && $scope.cfName.length >= 1) {
            //     if ($scope.forbiddenFieldNames.includes($scope.cfName)) {
            //         form.cfName.$error.validationError = true;
            //     }
            //     else {
            //         leadsService.checkCFName({'cf_name': $scope.cfName}).then(function (result) {
            //             //console.log('Result', result);
            //             if (result.data.status) {
            //                 form.cfName.$error.validationError = true;
            //             } else {
            //                 form.cfName.$error.validationError = false;
            //             }
            //         });
            //     }
            // }
            $log.debug('Unique CF Check fn called with form elem', form, $scope.addCFObj);
            if ($scope.addCFObj.name && $scope.addCFObj.name.length >= 1) {
                if($scope.forbiddenFieldNames.includes($scope.addCFObj.name)){
                    form.cfName.$error.validationError = true;
                    $log.debug('CF Name in forbiddenNames array', form, form.cfName.$error.validationError);
                }
                else{
                    $log.debug('Checking in db', form, form.cfName.$error.validationError, $scope.addCFObj.name);
                    leadsService.checkCFName({'cf_name': $scope.addCFObj.name}).then(function (result) {
                        ////// //console.log('Result', result);
                        if (result.data.status) {
                            form.cfName.$error.validationError = true;
                        } else {
                            form.cfName.$error.validationError = false;
                        }
                    });
                }
            }
            else{
                $log.debug('CF Name ngModel empty or less than 1 in length', $scope.addCFObj);
            }
        };

        $scope.addCF = function () {
            //console.log('cf>>>', $scope.cfName, $scope.cfType);

            // if ($scope.cfName === '') {
            //     $rootScope.errorToast('Custom field name can not be empty');
            // }
            // else if ($scope.cfType === '') {
            //     $rootScope.errorToast('Choose a Custom field type');
            // }
            if ($scope.addCFObj['type'] === 'DropDown') {
                $scope.dropDownCF.field_name = $scope.addCFObj['name'];
                $scope.dropDownCF.cf_type = $scope.addCFObj['type'];
                $scope.dropDownCF.list.push('');

                //console.log('dropDownCF object ready', $scope.dropDownCF);
                $('#manageCFModal').modal('close');
                $('#dropDownCF_modal').modal('open');
            }
            else {
                var customFieldsObj = {};
                $scope.manageCFModalLoader = true;


                customFieldsObj.field_name = $scope.addCFObj['name'];
                customFieldsObj.cf_type = $scope.addCFObj['type'];
                $log.debug('Object for adding cf', customFieldsObj);

                leadsService.createCustomFields(customFieldsObj).then(function (result) {
                    //console.log('custom field inserted', result);
                    if (result.data.status) {
                        //console.log('custom field created', result.data);
                        $rootScope.successToast('Custom Field added successfully');

                        $scope.addCFCheck = false;
                        $scope.addCFObj = {};
                        // $scope.cfName = '';
                        // $scope.cfType = '';
                        setActiveCustomFields();
                        setManageCFList().then(function () {
                            setTableHeaders().then(function () {
                                $scope.manageCFModalLoader = false;
                            });
                        })
                        // $timeout(function () {
                        //     setManageCFList();
                        // });
                    }
                }).catch(function (error) {
                    //console.log('error while adding CF', error);
                    $scope.addCFCheck = false;
                    $scope.addCFObj = {};
                    $scope.errorToast('Error while adding Custom Field');
                    setManageCFList().then(function () {
                        setTableHeaders().then(function () {
                            $scope.manageCFModalLoader = false;
                        });
                    })
                });
            }
        };

        $scope.viewAllCFs = function () {
            setManageCFList();
            // $('#allCFModal').modal('open');
        };



        $scope.changeCFStatus = function (cfData) {
            $scope.manageCFModalLoader = true;
            //console.log('cf data>>>', cfData);
            leadsService.changeCFStatus(cfData).then(function (result) {
                //console.log('Result post CF updation', result);
                if (result.data.status) {
                    //console.log('Updation of CF was successful', result.data);
                    //update Active CFs for Add lead and Update lead
                    setActiveCustomFields();

                    //refresh ALL CF list post status updation
                    setManageCFList().then(function () {
                        setTableHeaders().then(function () {
                            $scope.manageCFModalLoader = false;
                        })
                    })
                    //update leads table with active data and be on the same page
                    // getAllLeads($scope.currentPage, false).then(function () {
                    //     $log.debug('Success getAllLeads post changeCFStatus() with currentPage', $scope.currentPage);
                    // });
                    // $scope.getCustomFields(); //get active CF fields for Add lead form
                }
            }).catch(function (error) {
                //console.log(error);
            });
        };

        $scope.updateFieldVisibility = function (fieldObj) {
            $scope.manageCFModalLoader = true;
            //console.log('Field data>>', fieldObj);

            leadsService.showORhideField(fieldObj).then(function (res) {
                //console.log('Show/Hide res', res);
                if (res.data.status) {
                    //refresh ALL CF list post status updation
                    setManageCFList().then(function () {
                        setTableHeaders().then(function () {
                            $scope.manageCFModalLoader = false;
                        })
                    })
                    //update leads table with active data and be on the same page
                    // getAllLeads($scope.currentPage, false).then(function () {
                    //     $log.debug('Success getAllLeads post updateFieldVisibility() with currentPage', $scope.currentPage);
                    // });
                }
            }).catch(function (err) {
                //console.log('error', err);
            });

        }

        $scope.addDropdownCFItem = function () {
            $scope.dropDownCF.list.push('');
            //console.log('cf dropdown list', $scope.dropDownCF);
            if ($scope.dropDownCF.list.length >= 10) {
                $rootScope.errorToast('Can not add more than 10 list items');
                $scope.disableAddCFItem_btn = true;
            }
        };

        $scope.confirmDropDownItemDel = function (item) {
            //console.log('Removing this Drop dwon Item', item, $scope.dropDownCF.field_name);
            $scope.deleteItem = item;
            $scope.deleteItemCFName = $scope.dropDownCF.field_name;
            $('#deleteListItem_Modal').modal('open');
        };

        $scope.deleteDropDownItem = function () {
            //console.log('Delete this', $scope.deleteItem, $scope.deleteItemCFName);
            var itemObj = {
                'cfName': $scope.deleteItemCFName,
                'listItem': $scope.deleteItem
            };
            leadsService.delCFDropDownItem(itemObj).then(function (result) {
                //console.log('Deletion result for item', itemObj, result);
                if (result.data.status) {
                    $('#deleteListItem_Modal').modal('close');
                    $rootScope.successToast('DropDown List Item deleted successfully');
                    $scope.deleteItem = '';
                    $scope.deleteItemCFName = '';
                    itemObj.cfName = '';
                    itemObj.listItem = '';
                    var cfObj = {
                        'cfName': $scope.dropDownCF.field_name
                    }
                    getDropDownCFList(cfObj);
                }
            }).catch(function (error) {
                //console.log('Error while deleting list item', itemObj, error);
                $rootScope.errorToast('Error while deleting DropDown List Item');
            });
        };

        $scope.closeDeleteListItemModal = function () {
            $('#deleteListItem_Modal').modal('close');
        };

        $scope.closeDropdownCFModal = function () {
            $scope.dropDownCF = {
                field_name: '',
                cf_type: '',
                list: []
            };
            $scope.editDropDownCFCheck = false;
            $('#dropDownCF_modal').modal('close');
            setActiveCustomFields();
            setManageCFList().then(function () {
                setTableHeaders().then(function () {
                    $('#manageCFModal').modal('open');
                })
            });
            // $timeout(function () {
            //     setManageCFList();
            // }, 100);

        };

        $scope.createDropdownCF = function () {
            //console.log('Create this cf', $scope.dropDownCF);
            leadsService.createDropDownCF($scope.dropDownCF).then(function (result) {
                //console.log('result for creating dropDown cf', result);
                if (result.data.status === 'existing') {
                    $('#dropDownCF_modal').modal('close');
                    $('#manageCFModal').modal('open');
                    $rootScope.errorToast('Custom Field already existing, kindly try with a different name');
                }
                else if (result.data.status === true) {
                    $('#dropDownCF_modal').modal('close');
                    $rootScope.successToast('Drop-down Custom field added successfully');
                    $scope.addCFCheck = false;
                    $scope.addCFObj = {};
                    setActiveCustomFields();
                    setManageCFList().then(function () {
                        setTableHeaders().then(function () {
                            $('#manageCFModal').modal('open');
                        })
                    })
                }
                else {
                    $rootScope.errorToast('Error while adding dropDown custom field');
                }
            }).catch(function (error) {
                //console.log('error while creating dropDown CF', error);
                $rootScope.errorToast('Error while adding dropDown custom field');
            });
        };

        $scope.editDropDownCF = function (cfName, cfList) {
            //console.log('CF dropDown', cfName, cfList);
            $scope.dropDownCF.field_name = cfName;
            $scope.dropDownCF.cf_type = 'DropDown';
            $scope.dropDownCF.updateList = cfList;
            $scope.dropDownCF.list = [];
            $scope.editDropDownCFCheck = true;
            $('#dropDownCF_modal').modal('open');
            $timeout(function () {
                $('#manageCFModal').modal('close');
            }, 100);
        };

        $scope.updateDropdownCF = function () {
            //console.log('Update this', $scope.dropDownCF);
            leadsService.addDropDownCFItem($scope.dropDownCF).then(function (result) {
                //console.log('Updated result', result);
                if (result.data.status) {
                    $scope.dropDownCF.list = [];
                    var cfObj = {
                        'cfName': $scope.dropDownCF.field_name
                    }
                    //console.log('Fetch data for this CF', cfObj);
                    $timeout(function () {
                        getDropDownCFList(cfObj);
                    }, 100);
                    //console.log('Updated dropDownCF object', $scope.dropDownCF);
                    $rootScope.successToast('Custom Field drop-down updated successfully');
                }
            }).catch(function (error) {
                //console.log('Updation dropDown CF error', error);
            });
        };

        $scope.openItemForEdit = function (item) {
            //console.log('Item being edited', item);
            $scope.editItem = item;
            // $scope.listItemEditCheck = true;
        };

        // $scope.cancelItemEdit = function () {
        //     $scope.listItemEditCheck = false;
        // }

        $scope.updateDropDownCFListItem = function (data) {
            //console.log('This to this', $scope.editItem, data);
            var editItemObj = {
                'old': $scope.editItem,
                'new': data,
                'cfName': $scope.dropDownCF.field_name
            };
            //console.log('Object sent for update', editItemObj);
            leadsService.updateCFDropDownItem(editItemObj).then(function (result) {
                //console.log('Updation Result', result);
                if (result.data.status) {
                    var cfObj = {
                        'cfName': $scope.dropDownCF.field_name
                    }
                    getDropDownCFList(cfObj);
                    $rootScope.successToast('DropDown list updated successfully');
                }
            }).catch(function (error) {
                //console.log('Error while updating dropdown item', error);
                $rootScope.errorToast('Error while updating DropDown list');
            });

        };


        /**
         * Partner Leads section
         * Add, Remove, Create and Update Leads
         * functions here
         */
            // $scope.openAddLeadModal = function () {
            //     $('#addLeadModal').modal('open');
            //     $('#select_status').prop('selectedIndex', 0);
            // };
        var topTableBody = $(".topTable-scroll");
        //console.log('scroll ISSUES', topTableBody);
        topTableBody.scroll(function () {
            var bottomTableBody = $(".bottomTable-scroll");
            // //console.log($(this).scrollLeft());
            bottomTableBody.scrollLeft($(this).scrollLeft())
        });

        var bottomTableBody = $(".bottomTable-scroll");
        //console.log('scroll ISSUES', bottomTableBody);
        bottomTableBody.scroll(function () {
            var topTableBody = $(".topTable-scroll");
            // //console.log($(this).scrollLeft());
            topTableBody.scrollLeft($(this).scrollLeft())
        });

        // var subCatContainer = $(".scrollable");
        //
        // subCatContainer.scroll(function () {
        //     subCatContainer.scrollLeft($(this).scrollLeft());
        // });

        $scope.closeAddLeadModal = function () {
            // $scope.leadFlowStatus = [{}];
            if ($scope.updateLeadCheck) {
                $scope.leadObj = {};
                $scope.updateLeadCheck = false;
            }
            $('#addLeadModal').modal('close');
        };

        $scope.addLead = function () {
            //console.log('create lead', $scope.leadObj);


            //console.log('create lead', $scope.leadObj);
            var leadData = {
                first_name: $scope.leadObj.first_name,
                last_name: $scope.leadObj.last_name || "",
                email: $scope.leadObj.email || "",
                mobile: $scope.leadObj.mobile || 0,
                source: $scope.leadObj.source || "",
                status: $scope.leadObj.status || "",
                assignedto: $scope.leadObj.assignedTo || "",
                actionDate: $scope.leadObj.actionDate || "",
                notes: $scope.leadObj.notes || "",
                cfData: $scope.leadObj.cfData || {}
            };
            _.each(leadData.cfData, function (cfObj, key) {
                //console.log('Cfobject', cfObj);
                if (cfObj !== null) {
                    leadData.cfData[key] = cfObj.toString();
                }
            });
            //console.log('leadData>>>>>>>>>>>>>', leadData);

            if (!(_.isEmpty(leadData.cfData))) {
                //console.log('cf present for Lead!!!!', leadData.cfData);
            }
            else {
                //console.log('cf not present for Lead!!!');
            }
            //
            leadsService.addPartnerLead(leadData).then(function (result) {
                //console.log('data of lead added', result);
                if (result.data.status) {
                    $rootScope.successToast('Lead added successfully');
                    //Update pagination and go to pageZero post adding a Lead
                    getAllLeads(pageZero, true).then(function () {
                        $log.debug('Success getAllLeads post addLead() with currentPage = 0?', $scope.currentPage);
                    });
                    $scope.leadObj = {};
                    var $input = $('.actionDate').pickadate();
                    var datePicker = $input.pickadate('picker');
                    datePicker.clear();
                }
                $('#addLeadModal').modal('close');

            }).catch(function (error) {
                //error while sending otp
                //console.log('error while inserting  data', error);
                $rootScope.errorToast('Error while adding Lead');
            });
        };

        $scope.openUpdateLeadModal = function (leadData) {
            $scope.updateLeadCheck = true;
            //console.log('update this lead', leadData);
            $scope.leadObj = {
                'lead_id': leadData.lead_id,
                'first_name': leadData.first_name,
                'last_name': leadData.last_name,
                'email': leadData.email,
                'mobile': leadData.mobile,
                'source': leadData.source,
                'status': leadData.status,
                'assignedTo': leadData.assignedto,
                'actionDate': leadData.action_date,
                'notes': leadData.notes
            };
            //console.log('leadObj -->', $scope.leadObj);
            _.each($scope.activeCFs, function (activeCF) {
                //console.log('CF active>>', activeCF);
                var search = _.findKey(leadData, function (value, key) {
                    if (key === activeCF.field_name) {
                        $scope.leadObj[activeCF.field_name] = value;
                        return key;
                    }
                    else {
                        $scope.leadObj[activeCF.field_name] = '';
                    }
                });
                //console.log('search-->', search);
            });
            //console.log('leadObj -->', $scope.leadObj);

            document.getElementById('select_status').selected = leadData.status;
            document.getElementById('select_status').selected = leadData.status;
            //console.log('lead data ready', $scope.leadObj, $scope.tempLeadID);
            $('#addLeadModal').modal('open');
        };

        $scope.updateLead = function () {
            $scope.leadObj.cfData = [];
            _.each($scope.activeCFs, function (cf) {
                var cfObj = {
                    'cf_name': cf.field_name,
                    'cf_value': $scope.leadObj[cf.field_name]
                }

                $scope.leadObj.cfData.push(cfObj);
                $scope.leadObj = _.omit($scope.leadObj, cf.field_name);
            });
            _.each($scope.leadObj.cfData, function (cfObj, key) {
                //console.log('CF_Object', cfObj);
                if (cfObj.cf_value !== null) {
                    cfObj.cf_value = cfObj.cf_value.toString();
                }
            });
            //console.log('update go ahead>>>', $scope.leadObj);

            //console.log('$scope.lead.status', $scope.leadObj.status);
            leadsService.updateLead($scope.leadObj).then(function (result) {
                //console.log('lead update result', result);
                if (result.data.status) {
                    //be on same page - $scope.currentPage post updateLead()
                    getAllLeads($scope.currentPage, false).then(function () {
                        $log.debug('Success getAllLeads post updateLead() with currentPage', $scope.currentPage);
                    });
                    //console.log('lead Obj empty var post updation??', $scope.leadObj);
                    $rootScope.successToast('Lead updated successfully');
                }
                $scope.closeAddLeadModal();
            }).catch(function (error) {
                //console.log(error);
                $rootScope.errorToast('Error while updating Lead');
            });

        };

        $scope.openDeleteLeadModal = function (leadID) {
            $scope.tempLeadID = leadID;
            $('#delLeadCnfrmModal').modal('open');
        };

        $scope.deleteLead = function () {
            var goToPage = 0;
            $log.debug('leadID for deletion', $scope.tempLeadID);
            var delLeadObj = {
                leadID: $scope.tempLeadID
            };
            $log.debug('Current array length', $scope.partnerLeads.length);
            if ($scope.partnerLeads.length <= 1) {
                goToPage = $scope.currentPage - 1; //go to previous page, if item is last on current page
            }
            else {
                goToPage = $scope.currentPage; //be on same page, if NOT last item on current page
            }
            leadsService.deleteLeadData(delLeadObj).then(function (result) {
                //console.log('lead deleted data', result);
                if (result.data.status) {
                    $scope.tempLeadID = '';
                    //Update pagination post deleteLead()
                    getAllLeads(goToPage, true).then(function () {
                        $log.debug('Success getAllLeads post deleteLead() with currentPage = ', $scope.currentPage);
                    });
                    //console.log('lead id var post deletion', $scope.tempLeadID);
                    $rootScope.successToast('Lead deleted successfully');
                }
            }).catch(function (error) {
                //console.log(error);
                //toast message
            });
        };

        var currentLeadsSearchID = '';
        var currentSearchLeadsArr = [];
        var totalSearchedLeads = 0;
        $scope.underLeadsSearch = false;
        $scope.pageCountSearchedLeads = [];
        $scope.currentPageSearchedLeads = 0;
        // $scope.searchedLeadsOnDisplay = {
        //     startValue : 0,
        //     endValue : 0
        // };

        var clearSearchLeadsParams = function () {
            currentLeadsSearchID = '';
            totalSearchedLeads = 0;
            $scope.underLeadsSearch = false;
        }

        $leadsChips.on('chip.add', function (e, chip) {
            if (chip.tag.length > 2) {
                $scope.searchInputs.push(chip.tag);
                //console.log('search input added', chip.tag, $scope.searchInputs);
                searchLead($scope.searchInputs);
            }
            else {
                var newData = [];
                $rootScope.errorToast('Minimum 3 characters are required for search');
                $scope.searchInputs = _.without($scope.searchInputs, chip.tag);
                //console.log('Search input left', $scope.searchInputs);
                _.each($scope.searchInputs, function (input) {
                    var chipObj = {tag: input};
                    newData.push(chipObj);
                })

                $('.leads-searchBar').material_chip({
                    placeholder: 'Further refine your search',
                    secondaryPlaceholder: 'Search and press enter',
                    data: newData
                });
            }

            // $scope.searchInputs.push(chip.tag);
            // //console.log('search input added', chip.tag, $scope.searchInputs);
            // if ($scope.searchInputs.length === 1) {
            //     //console.log('search called for first time');
            //     searchLead($scope.searchInputs[0]);
            // }
            // else {
            //     //console.log('Filter called from ADD CHIP');
            //     filterSearch();
            // }
        });

        $leadsChips.on('chip.delete', function (e, chip) {
            $scope.searchInputs = _.without($scope.searchInputs, chip.tag);
            //console.log('search input removed', chip.tag, $scope.searchInputs);
            // if ($scope.searchInputs.length === 0) {
            //     //Update pagination and get leads on Page = 0, post search clear
            //     getAllLeads(pageZero, true).then(function () {
            //         $log.debug('Success getAllLeads post last search-chip delete with pageZero', $scope.currentPage);
            //     });
            // } else {
            //     searchLead($scope.searchInputs[0]);
            // }
            if ($scope.searchInputs.length === 0) {
                clearSearchLeadsParams();
                //Update pagination and get leads on Page = 0, post search clear
                getAllLeads(pageZero, true).then(function () {
                    $log.debug('Success getAllLeads post last search-chip delete with pageZero', $scope.currentPage);
                });
            }
            else {
                searchLead($scope.searchInputs);
            }
        });


        var searchLead = function (searchInputs) {
            // var searchLeadObj = {
            //     'searchData': searchText
            // }
            // leadsService.searchLead(searchLeadObj).then(function (result) {
            //     //console.log('LEAD search result--', result);
            //     if (result.data.status) {
            //         //console.log('find unique lead ids in this', result.data.response);
            //         var tempUniques = _.uniq(result.data.response, 'lead_id');
            //         //console.log('Unique lead ids fetched', tempUniques);
            //         var leadIDArr = [];
            //         _.each(tempUniques, function (dataObj) {
            //
            //             leadIDArr.push(dataObj.lead_id);
            //         });
            //         //console.log(leadIDArr);
            //         leadsService.getSearchedLeadsData(leadIDArr).then(function (result) {
            //             //console.log('fetched searched leads data>>>', result);
            //             if (result.data.status) {
            //                 $scope.partnerLeads.length = 0;
            //                 $scope.partnerLeads = result.data.response;
            //                 //console.log('array with search data', $scope.partnerLeads, Store.customFieldsCheck, $scope.activeCFs);
            //                 if (Store.customFieldsCheck) {
            //                     _.each($scope.partnerLeads, function (lead) {
            //                         //console.log('unsorted cf data in getAllLeads', lead.cfData);
            //                         _.each($scope.CFforLeadsTable, function (cf) {
            //                             var availableCF = _.find(lead.cfData, function (cfObj) {
            //                                 return cfObj.cf_name === cf;
            //                             });
            //                             if (availableCF === undefined) {
            //                                 lead.cfData.push({'cf_name': cf});
            //                             }
            //                         })
            //                         lead.cfData = _.sortBy(lead.cfData, 'cf_name');
            //                         //console.log('sorted cf data', lead.cfData);
            //                     });
            //                 }
            //                 //console.log('Search result>>>>>>', $scope.partnerLeads);
            //                 filterSearch(); //filter search post search//
            //             }
            //         }).catch(function (error) {
            //             //console.log(error);
            //         });
            //
            //     }
            //     else {
            //         $rootScope.errorToast('Not Found');
            //     }
            // }).catch(function (error) {
            //     //console.log(error);
            // });
            $scope.tableLoader = true;
            var searchLeadObj = {
                searchArr: searchInputs
            }
            leadsService.searchLead(searchLeadObj).then(function (result) {
                //// //console.log('CUSTOMER search result--', result);
                if (result.data.status) {
                    $log.debug('find unique search ID in this', result.data.response);
                    //Save searchID
                    //Use it when paginating search results, pass with query
                    currentLeadsSearchID = result.data.response['searchID_forLeads'];
                    // currentSearchLeadsArr.length = 0;
                    // _.each(result.data.response.searchedLeadIDArr, function (dataObj) {
                    //     currentSearchLeadsArr.push(dataObj.lead_id);
                    // });


                    var searchObj = {
                        searchID: currentLeadsSearchID
                    };
                    leadsService.getSearchedLeadsData(searchObj, pageZero).then(function (result) {
                        $scope.tableLoader = false;
                        //console.log('fetched searched leads data>>>', result);
                        if (result.data.status) {
                            $scope.partnerLeads.length = 0;
                            $scope.partnerLeads = result.data.response;
                            totalSearchedLeads = $scope.partnerLeads[0].TOTAL_SEARCHED_LEADS;
                            //Using Leads-Pagination variables
                            $scope.pageCount.length = 0;
                            _.times(numberOfPages(totalSearchedLeads, pageSize), function (index) {
                                $scope.pageCount.push(index + 1);
                            });
                            $scope.underLeadsSearch = true;
                            $scope.currentPage = 0;
                            $log.debug('Check pagination variables here', totalSearchedLeads, $scope.pageCount, $scope.underLeadsSearch, $scope.currentPage);
                            setLeadsOnDisplay(pageZero, $scope.partnerLeads.length);
                            $scope.totalLeads = totalSearchedLeads;
                            //filter search post search//
                            // filterSearch();
                        }
                    }).catch(function (error) {
                        $scope.tableLoader = false;
                        //console.log(error);
                    });
                    // leadsService.getSearchedLeadsData(searchObj, pageZero).then(function (result) {
                    //     //// //console.log('fetched searched customers data>>>', result);
                    //     if (result.data.status) {
                    //         $scope.allCustomers.length = 0;
                    //         $scope.allCustomers = result.data.response;
                    //         search_totalRecords = $scope.allCustomers[0].SEARCHED_CLIENTS;
                    //         _.each($scope.allCustomers, function (custObj) {
                    //             custObj.row_created = new Date(custObj.row_created);
                    //             custObj.user_last_logged_in = new Date(custObj.user_last_logged_in);
                    //             custObj.row_created = moment(custObj.row_created).format('lll');
                    //             custObj.user_last_logged_in = moment(custObj.user_last_logged_in).format('lll');
                    //         });
                    //         $scope.searchedClients = search_totalRecords;
                    //         $scope.search_pageCount = [];
                    //         //console.log('Breaking here search');
                    //
                    //         _.times(numberOfSearchPages(), function (index) {
                    //             $scope.search_pageCount.push(index + 1);
                    //         });
                    //         //console.log('pages', $scope.search_pageCount);
                    //         $scope.search_currentPage = 0;
                    //         $scope.underSearch = true;
                    //     }
                    // }).catch(function (error) {
                    //     //// //console.log(error);
                    // });

                }
                else {
                    $scope.tableLoader = false;
                    $rootScope.errorToast('Not Found');
                }
            }).catch(function (error) {
                $scope.tableLoader = false;
                //// //console.log(error);
            });
        };

        // var filterSearch = function () {
        //
        //     var data = _.clone($scope.partnerLeads);
        //     //console.log('Filter with these>>>>', $scope.searchInputs, $scope.searchInputs.length);
        //
        //     //filtering over search inputs//
        //     for (i = 1; i < $scope.searchInputs.length; i++) {
        //
        //         var str = $scope.searchInputs[i];
        //         str = isNaN(str) ? str.toUpperCase() : str;
        //         //console.log('searching this now', str);
        //
        //         data = _.filter(data, function (leadObj) {
        //
        //             // var inBasicData = false;
        //
        //             //>>>>Check in lead basic data first<<<<//
        //             for (var key in leadObj) {
        //                 var prop = leadObj[key];
        //                 //console.log('>>>>', key, prop);
        //                 if (prop != null && key != 'cfData' && key != 'row_created' && key != 'row_last_updated' && key != 'id' && key != 'partner_id' && key != 'lead_id' && key != 'converted' && key != 'is_deleted') {
        //                     if (isNaN(prop)) {
        //                         var leadData = prop.toUpperCase();
        //                         if (leadData.includes(str)) {
        //                             //console.log('FOUND in lead basic text data', key, prop, str);
        //                             // inBasicData = true;
        //                             //console.log('Matched in basic text data', str, leadObj.id, key, prop);
        //                             return leadObj;
        //                         } else {
        //                             //console.log('Not found in leads basic text data>>', key, prop, str);
        //                         }
        //                     }
        //                     else {
        //                         //console.log('lead data is a number', key, prop);
        //                         prop = prop.toString();
        //                         if (prop.includes(str)) {
        //                             //console.log('FOUND in lead basic number data', key, prop, str);
        //                             // inBasicData = true;
        //                             //console.log('Matched in basic number data', str, leadObj.id, key, prop);
        //                             return leadObj;
        //                         }
        //                         else {
        //                             //console.log('Not found in leads basic number data>>>', key, prop, str);
        //                         }
        //                     }
        //                 }
        //                 else {
        //                     //console.log('Lead data is empty', key, prop);
        //                 }
        //             }
        //
        //             //>>>>if inBasicData = FALSE, check in Lead CF Data<<<<//
        //             for (var j = 0; j < leadObj.cfData.length; j++) {
        //                 var cfObj = leadObj.cfData[j];
        //                 //console.log('CF object copied', cfObj);
        //                 if (cfObj.cf_value != null) {
        //                     if (isNaN(cfObj.cf_value)) {
        //                         //console.log('CF value is text', cfObj.cf_name, cfObj.cf_value);
        //                         var cfValue = cfObj.cf_value.toUpperCase();
        //                         if (cfValue.includes(str)) {
        //                             // alert('Match');
        //                             //console.log('CF text value matched to string', leadObj.id, cfObj.cf_name, cfObj.cf_value);
        //                             return leadObj;
        //                         } else {
        //                             //console.log('Not found in this>>', cfObj, str);
        //                         }
        //                     }
        //                     else {
        //                         //console.log('CF value is a number', cfObj.cf_name, cfObj.cf_value);
        //                         if (cfObj.cf_value == str) {
        //                             //console.log('CF number value matched to string', leadObj.id, cfObj.cf_name, cfObj.cf_value);
        //                             return leadObj;
        //                         }
        //                         else {
        //                             //console.log('Not found in Number CF data?', cfObj, str);
        //                         }
        //                     }
        //
        //                 }
        //                 else {
        //                     //console.log('CF Value empty', cfObj.cf_name, cfObj.cf_value);
        //                 }
        //             }
        //         });
        //         //console.log('running filter', i, str, data);
        //     }
        //     //console.log('filtered table', data);
        //     $scope.partnerLeads = [];
        //     $timeout(function () {
        //         _.each(data, function (lead) {
        //             $scope.partnerLeads.push(lead);
        //         });
        //         //console.log('Final filtered table', $scope.partnerLeads);
        //     })
        //
        // };

        $scope.closeLeadsSearch = function () {
            clearSearchLeadsParams();
            //Update pagination and get leads on Page = 0, post search clear
            getAllLeads(pageZero, true).then(function () {
                $log.debug('Success getAllLeads post last search-chip delete with pageZero', $scope.currentPage);
                //clear chip-search Input-Box
                var data = [];
                $('.leads-searchBar').material_chip(data);
                $('.leads-searchBar').material_chip({
                    placeholder: 'Further refine your search',
                    secondaryPlaceholder: 'Search and press enter',
                });
            });
        };

        $scope.underCLSearch = false;

        var setConvertedLeadsOnDisplay = function (pageNum, currentArrLen) {
            $scope.convertedLeadsOnDisplay.startValue = parseInt((pageNum * 10) + 1);
            $scope.convertedLeadsOnDisplay.endValue = parseInt((pageNum * 10) + currentArrLen);
        };

        $cLChips.on('chip.add', function (e, chip) {
            // $scope.cLSearchInputs.push(chip.tag);
            // //console.log('search input added', chip.tag, $scope.cLSearchInputs);
            // if ($scope.cLSearchInputs.length === 1) {
            //     //console.log('search called for first time');
            //     searchCL($scope.cLSearchInputs[0]);
            // }
            // else {
            //     //console.log('Filter called from ADD CHIP');
            //     filterCLSearch();
            // }
            if (chip.tag.length > 2) {
                $scope.cLSearchInputs.push(chip.tag);
                //console.log('search input added', chip.tag, $scope.cLSearchInputs);
                searchCL($scope.cLSearchInputs, pageZero);
                $scope.underCLSearch = true;
            }
            else {
                var newData = [];
                $rootScope.errorToast('Minimum 3 characters are required for search');
                $scope.cLSearchInputs = _.without($scope.cLSearchInputs, chip.tag);
                //console.log('Search input left', $scope.cLSearchInputs);
                _.each($scope.cLSearchInputs, function (input) {
                    var chipObj = {tag: input};
                    newData.push(chipObj);
                })

                $('.cL-searchBar').material_chip({
                    placeholder: 'Further refine your search',
                    secondaryPlaceholder: 'Search and press enter',
                    data: newData
                });
            }
        });

        $cLChips.on('chip.delete', function (e, chip) {
            $scope.cLSearchInputs = _.without($scope.cLSearchInputs, chip.tag);
            $log.log('CL search input removed', chip.tag, $scope.cLSearchInputs);
            // if ($scope.cLSearchInputs.length === 0) {
            //     fetchConvertedLeads();
            // } else {
            //     searchCL($scope.cLSearchInputs[0]);
            // }
            if ($scope.cLSearchInputs.length === 0) {
                $scope.underCLSearch = false;
                //Update pagination and get converted leads on Page = 0, post search clear
                fetchConvertedLeads(pageZero).then(function () {
                    $log.debug('Success fetch all converted leads post last CLsearch-chip delete with pageZero', $scope.currentPage);
                });
            }
            else {
                searchCL($scope.cLSearchInputs, pageZero);
            }
        });

        var searchCL = function (searchStrArr, pageNumber) {
            $scope.tableLoader = true;
            var searchCLObj = {
                'searchArr': searchStrArr
            };
            leadsService.searchConvertedLead(searchCLObj, pageNumber).then(function (result) {
                $log.debug('searchConvertedLead result with unique search ID', result);
                $scope.tableLoader = false;
                if (result.data.status) {
                    $scope.convertedLeads.length = 0;
                    $scope.convertedLeads = result.data.response;
                    //console.log('array with searched converted clients', $scope.convertedLeads);
                    // filterCLSearch();

                    $scope.totalConvertedLeads = $scope.convertedLeads[0].TOTAL_SEARCHED_CONVERTED_LEADS;
                    $scope.pageCountCL.length = 0;
                    _.times(numberOfPages($scope.totalConvertedLeads, pageSize), function (index) {
                        $scope.pageCountCL.push(index + 1);
                    });
                    $scope.currentPageCL = pageNumber;
                    setConvertedLeadsOnDisplay(pageNumber, $scope.convertedLeads.length);
                }
                else {
                    $rootScope.errorToast('Not Found');
                }
            }).catch(function (error) {
                $scope.tableLoader = false;
                //console.log(error);
            });

        };

        // var filterCLSearch = function () {
        //
        //     var data = _.clone($scope.convertedLeads);
        //     //console.log('Filter with these>>>>', $scope.cLSearchInputs, $scope.cLSearchInputs.length);
        //
        //     //filtering over search inputs//
        //     for (i = 1; i < $scope.cLSearchInputs.length; i++) {
        //
        //         var str = $scope.cLSearchInputs[i];
        //         str = isNaN(str) ? str.toUpperCase() : str;
        //         //console.log('searching this now', str);
        //
        //         data = _.filter(data, function (leadObj) {
        //
        //             // var inBasicData = false;
        //
        //             //>>>>Check ONLY lead basic data <<<<//
        //             for (var key in leadObj) {
        //                 var prop = leadObj[key];
        //                 //console.log('>>>>', key, prop);
        //                 if (prop != null && key != 'cfData' && key != 'row_created' && key != 'row_last_updated' && key != 'id' && key != 'partner_id' && key != 'lead_id' && key != 'converted' && key != 'is_deleted') {
        //                     if (isNaN(prop)) {
        //                         var leadData = prop.toUpperCase();
        //                         if (leadData.includes(str)) {
        //                             //console.log('FOUND in lead basic text data', key, prop, str);
        //                             // inBasicData = true;
        //                             //console.log('Matched in basic text data', str, leadObj.id, key, prop);
        //                             return leadObj;
        //                         } else {
        //                             //console.log('Not found in leads basic text data>>', key, prop, str);
        //                         }
        //                     }
        //                     else {
        //                         //console.log('lead data is a number', key, prop);
        //                         prop = prop.toString();
        //                         if (prop.includes(str)) {
        //                             //console.log('FOUND in lead basic number data', key, prop, str);
        //                             // inBasicData = true;
        //                             //console.log('Matched in basic number data', str, leadObj.id, key, prop);
        //                             return leadObj;
        //                         }
        //                         else {
        //                             //console.log('Not found in leads basic number data>>>', key, prop, str);
        //                         }
        //                     }
        //                 }
        //                 else {
        //                     //console.log('Lead data is empty', key, prop);
        //                 }
        //             }
        //         });
        //         //console.log('running filter', i, str, data);
        //     }
        //     //console.log('filtered table', data);
        //     $scope.convertedLeads = [];
        //     $timeout(function () {
        //         _.each(data, function (lead) {
        //             $scope.convertedLeads.push(lead);
        //         });
        //         //console.log('Final filtered table', $scope.convertedLeads);
        //     })
        //
        // };

        $scope.closeCLSearch = function () {
            $scope.underCLSearch = false;
            var data = [];
            //console.log($('.cL-searchBar').material_chip('data'));
            $('.cL-searchBar').material_chip(data);
            $('.cL-searchBar').material_chip({
                placeholder: 'Further refine your search',
                secondaryPlaceholder: 'Search and press enter',
            });
            fetchConvertedLeads();
        };


        /**
         * Convert Leads section
         * convert Leads to Prospect/Client and get all Converted Leads
         * functions are here
         */
        $scope.openConvertToModal = function (leadData) {
            //console.log('convert this>>>', leadData);
            $scope.tempLeadID = leadData.lead_id;
            $scope.convertLeadObj = {
                'partner': $rootScope.partnerId,
                'fname': leadData.first_name,
                'lname': leadData.last_name,
                'emailId': leadData.email.toLowerCase(),
                'mnumber': leadData.mobile,
                'user_consent': Date()
            };
            $scope.showAvailability = false; //set default as false
            //console.log('got some data>>', $scope.convertLeadObj, $scope.tempLeadID);
            $('#convertToModal').modal('open');
        };

        $scope.checkUsernameValid = function () {
            if ($scope.convertLeadObj.username && $scope.convertLeadObj.username.length >= 6) {
                $scope.showAvailability = true;
                $scope.showErrorMsg = false;
            } else {
                $scope.showAvailability = false;
                $scope.showErrorMsg = true;
            }
            if ($scope.usernameTimeout) {
                clearTimeout($scope.usernameTimeout);
            }
            $scope.usernameTimeout = setTimeout(function () {
                registerService.checkUsername({'username': $scope.convertLeadObj.username}).then(function (result) {

                    if (!result.data.available) {
                        //raise username not unique flag
                        $scope.uniqueUsername = false;
                        $scope.available = false;
                    } else {
                        if ($scope.convertLeadObj.username && $scope.convertLeadObj.username.match($scope.regExpUsername) != null) {
                            $scope.uniqueUsername = true;
                            $scope.available = true;
                        } else {
                            $scope.available = false;
                        }
                    }
                });
            }, 500);
        };

        $scope.matchPassword = function () {
            if ($scope.convertLeadObj.password !== $scope.convertLeadObj.cPass) {
                $scope.passMatchFailed = true;
            }
            else {
                $scope.passMatchFailed = false;
            }
        };

        $scope.disableSubmit = function () {
            // if ($scope.convertLeadForm.$invalid) {
            //     return true;
            // }
            // else if (!$scope.available) {
            //     return true;
            // }
            // else if ($scope.passMatchFailed) {
            //     return true;
            // }
            // else {
            //     return false;
            // }
            if ($scope.convertLeadForm.$valid && $scope.available && !$scope.passMatchFailed) {
                return false;
            }
            else {
                return true;
            }
        };

        $scope.convertLead = function () {
            $scope.convertLeadObj.partner = $rootScope.partnerId;
            $scope.convertLeadObj.leadID = $scope.tempLeadID;
            //console.log('target>>>>', $scope.convertLeadObj);

            var leadConvertedStatus = {};
            leadConvertedStatus.leadID = $scope.tempLeadID;
            //console.log('convert to type', $scope.tempLeadID, $scope.convertLeadObj.customerType);
            leadConvertedStatus.convertTo = $scope.convertLeadObj.customerType;
            // if ($scope.convertLeadObj.customerType === 'prospect') {
            //     leadConvertedStatus.convertTo = 'Prospect';
            // }
            // else if ($scope.convertLeadObj.customerType === 'client') {
            //     leadConvertedStatus.convertTo = 'Client';
            // }
            //console.log('statusLead', leadConvertedStatus);

            $scope.tableLoader = true;
            $scope.inProgress = false;


            if ($scope.convertLeadObj.password == $scope.convertLeadObj.cPass) {

                //console.log('passwords matched', $scope.convertLeadObj.password, $scope.convertLeadObj.cPass);

                var convertLeadStatusPromise = function () {
                    var goToPage = 0;
                    if ($scope.partnerLeads.length <= 1) {
                        goToPage = $scope.currentPage - 1; //go to previous page, if item is last on current page
                    }
                    else {
                        goToPage = $scope.currentPage; //be on same page, if NOT last item on current page
                    }

                    new Promise(function (resolve, reject) {
                        //Register Lead as Client/Prospect//
                        $rootScope.successToast('Registering your ' + leadConvertedStatus.convertTo + ', please wait.');
                        registerService.register($scope.convertLeadObj).then(function (result) {
                            //console.log('registerResponse:', result);
                            if (result.data.status) {
                                //console.log(result.data.status);
                                $rootScope.successToast('Registration was successful');

                                //prepare client/prospect Login object, post successful registration//
                                var loginObj = {
                                    'username': $scope.convertLeadObj.username,
                                    'password': $scope.convertLeadObj.password
                                };
                                $rootScope.successToast('Logging in' + $scope.convertLeadObj.fname + '\'s account');
                                //console.log("login obj ", loginObj);
                                loginService.loginCustomer(JSON.stringify(loginObj)).then(function (result) {
                                    //console.log('chechk', result);
                                    if (result.data.status) {
                                        //console.log('user info post login--', result.data.client_id, result.data.id, result.data.clientname);
                                        var userTokenObj = {
                                            'id': result.data.id,
                                            'user_id': result.data.client_id,
                                            'username': result.data.clientname
                                        }
                                        loginService.setLogs(JSON.stringify(userTokenObj)).then(function (success) {
                                            if (success.data.status) {
                                                $scope.tableLoader = false;
                                                $scope.inProgress = true;
                                                //console.log("client login successful!!!!");
                                                $scope.convertLeadObj = {};
                                                if (window.origin.includes('wealthquotient')) {
                                                    window.open('https://wmplatform.wealthquotient.in', '_blank');
                                                    resolve()
                                                } else {
                                                    window.open("http://localhost:8082", "_blank");
                                                    resolve()
                                                }
                                            }
                                            else {
                                                $scope.tableLoader = false;
                                                $scope.inProgress = true;
                                                //console.log("client login failed!!!!");
                                                $scope.convertLeadObj = {};
                                            }
                                        }).catch(function (error) {
                                            $scope.tableLoader = false;
                                            $scope.inProgress = true;
                                            //console.log('Log in failed due to setLogs!!!!', error);
                                            $rootScope.errorToast('Log in failed');
                                            reject()
                                        });
                                    } else {
                                        $scope.tableLoader = false;
                                        $scope.inProgress = true;
                                        if (window.origin.includes('wealthquotient')) {
                                            document.cookie = "dummyAuth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.wealthquotient.in;path=/;";
                                        } else {
                                            document.cookie = "dummyAuth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/';";
                                        }
                                        // document.cookie = "Authorization1=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/';";
                                        // document.cookie = "Authorization1=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.wealthquotient.in;path=/;";
                                        //console.log("login failed!!!!", document.cookie);
                                        $rootScope.errorToast('Log in failed')

                                    }
                                }).catch(function (error) {
                                    $scope.tableLoader = false;
                                    $scope.inProgress = true;
                                    //console.log('Log in failed', error);
                                    $rootScope.errorToast('Log in failed');
                                    reject()
                                });
                            }
                            else {

                                $scope.tableLoader = false;
                                $scope.inProgress = true;
                                //console.log('Registration failed else', result);
                                if (result.data.msg) {
                                    $rootScope.errorToast(result.data.msg, 3000, 'rounded');
                                } else {
                                    $rootScope.errorToast('Registration unsuccessful', 3000, 'rounded');
                                }
                            }
                        }).catch(function (error) {
                            $scope.tableLoader = false;
                            $scope.inProgress = true;
                            //console.log('Registration failed', error);
                            $rootScope.errorToast('Registration was unsuccessful');
                            reject()
                        });

                    }).then(function (resolved_data) {
                        //console.log("convert leads");
                        return  leadsService.convertLead(leadConvertedStatus).then(function (result) {
                            //console.log('Converted status set for lead>>>', result);
                            if (result.data.status) {
                                $rootScope.successToast('Lead status successfully changed to ' + leadConvertedStatus.convertTo + '');
                                //Update pagination, post lead conversion
                                getAllLeads(goToPage, true).then(function () {
                                    $log.debug('Success getAllLeads post convertLead() with currentPage = ', $scope.currentPage);
                                });
                                //refresh converted leads count, post conversion
                                getConvertedLeadsCount();
                            }
                            else {
                                //console.log('Failed to set lead converted status', result);

                            }
                        }).catch(function (error) {
                            //console.log(error);
                            // reject();
                        });
                    }).catch(function (error) {
                            //console.log("error is ", error);
                            $rootScope.errorToast('Failed to convert lead');
                        });
                }
                convertLeadStatusPromise();
            } else {
                //console.log('convertLead object', $scope.convertLeadObj);
                $rootScope.errorToast('Password and confirm password are not matching');
                $scope.tableLoader = false;
                $scope.inProgress = true;
                //console.log("password and confirm password are not matching");
            }
        };

        $scope.getConvertedLeads = function () {
            $scope.displayConverted = true;
            //setting up Table-Headers for Converted Leads
            $scope.convertedHeads = ['Mobile', 'Email', 'Source', 'Notes', 'Converted To'];
            var temp = $scope.convertedHeads;
            _.each(temp, function (fieldName, index) {
                if (fieldName === 'Email') {
                    $scope.forConvertedTable[index] = 'email';
                }
                else if (fieldName === 'Mobile') {
                    $scope.forConvertedTable[index] = 'mobile';
                }
                else if (fieldName === 'Source') {
                    $scope.forConvertedTable[index] = 'source';
                }
                else if (fieldName === 'Notes') {
                    $scope.forConvertedTable[index] = 'notes';
                }
            });
            $scope.forConvertedTable.push('converted');
            //console.log('getting all converted leads', $scope.displayConverted);
            fetchConvertedLeads(pageZero).then(function () {
                $log.debug('fetchConvertedLeads() success for getConvertedLeads');
            }).catch(function () {
                $log.error('Error in fetchConvertedLeads() for getConvertedLeads');
            })
            // leadsService.getConvertedLeads(pageZero).then(function (result) {
            //     //console.log('result for getAllConvertedLeads api', result);
            //
            //     if (result.data.status) {
            //         $scope.convertedLeads = result.data.response;
            //         //console.log('array with converted leads', $scope.convertedLeads);
            //         // $scope.displayConverted = true;
            //         $scope.leadsCheck = true;
            //     }
            // }).catch(function (error) {
            //     //console.log(error);
            // });
        };

        $scope.goBackToLeads = function () {
            $scope.displayConverted = false;
            $scope.convertedHeads = [];
            $scope.forConvertedTable = [];
            $scope.convertedLeads = [];
            $scope.partnerLeads.length = 0;
            //clear Converted Leads Search Params
            var data = [];
            $('.cL-searchBar').material_chip(data);
            $('.cL-searchBar').material_chip({
                placeholder: 'Further refine your search',
                secondaryPlaceholder: 'Search and press enter',
            });
            $scope.underCLSearch = false;
            //goTo page zero
            getAllLeads(pageZero, false).then(function () {
                $log.debug('Success getAllLeads with goBackToLeads()', $scope.currentPage);
            });
        };


        /**
         * Leads Upload/Download.
         * Download Template, Download allLeads, Validate uploaded file, Import leads.
         */
        $scope.closeImportLeadsModal = function () {
            $scope.disableFileUpload = true;
            $scope.validationSuccess = false;
            $scope.validationFail_headers = false;
            $('#leadsUploadInput').val('');
        };

        $scope.getExcelHeaders = function (forDownload) {
            $scope.templateFields.length = 0;
            excelHeaderFields.length = 0;
            $scope.templateFields.push({'First Name': '', 'Last Name': ''});
            //--- ACTIVE FIELDS(CF+NF) FOR TEMPLATE DOWNLOAD ---//
            var template_fields = _.map($scope.allFields, _.clone);
            template_fields = _.filter(template_fields, function (fieldObj) {
                return (fieldObj.is_deleted === 'N' && fieldObj.field_name !== 'first_name' && fieldObj.field_name !== 'last_name');
            })
            template_fields = _.sortBy(template_fields, 'sort_id');
            _.each(template_fields, function (field) {
                if (field.field_name === 'email') {
                    field.field_name = 'Email';
                }
                else if (field.field_name === 'mobile') {
                    field.field_name = 'Mobile';
                }
                else if (field.field_name === 'source') {
                    field.field_name = 'Source';
                }
                else if (field.field_name === 'status') {
                    field.field_name = 'Status';
                }
                else if (field.field_name === 'assignedto') {
                    field.field_name = 'Assigned To';
                }
                else if (field.field_name === 'action_date') {
                    field.field_name = 'Action Date';
                }
                else if (field.field_name === 'notes') {
                    field.field_name = 'Notes';
                }
                $scope.templateFields[0][field.field_name] = '';
                excelHeaderFields.push(field.field_name);
            });
            excelHeaderFields.unshift('First Name', 'Last Name');//Adding FirstName and LastName
            //console.log('Template Fields -->', template_fields, $scope.templateFields);
            // return $scope.templateFields;

            if (forDownload) {
                /* generate a worksheet */
                var ws = XLSX.utils.json_to_sheet($scope.templateFields);

                /* add to workbook */
                var wb = XLSX.utils.book_new();
                XLSX.utils.book_append_sheet(wb, ws, "leadsTemplate");

                /* write workbook (use type 'binary') */
                var wbout = XLSX.write(wb, {bookType: 'xlsx', type: 'binary'});

                saveAs(new Blob([s2ab(wbout)], {type: "application/octet-stream"}), "leadsTemplate.xlsx");
            }
            else {
                //console.log('Header fields array for Validation generated', excelHeaderFields);
                return excelHeaderFields;
            }

        };

        $scope.validateUploadedFile = function (filesSelected) {
            var fileObj = filesSelected.files[0];
            var validateFieldsArr = $scope.getExcelHeaders(false);

            $scope.validationSuccess = false;
            $scope.validationFail_headers = false;

            //console.log('Compare with', validateFieldsArr, fileObj);

            var is_one = filesSelected.files.length === 1;
            var is_valid_filename = fileObj.name.length <= 64;
            if (is_one && is_valid_filename) {
                var data = new FormData();
                data.append('file', fileObj);

                leadsService.validateLeadsSheet(data, validateFieldsArr).then(function (result) {
                    //console.log('Validation Result', result);
                    if (result.data.status) {
                        $scope.validationSuccess = true;
                        $scope.disableFileUpload = false;
                    }
                    else {
                        // if(result.data.error_code === 'firstNames'){
                        //     $scope.validationFail_firstNames = true;
                        // }
                        if (result.data.error_code === 'headers') {
                            $scope.validationFail_headers = true;
                        }
                        // else if(result.data.error_code === 'invalid'){
                        //     $scope.validationFail_invalidFile = true;
                        // }
                    }
                }).catch(function (error) {
                    //console.log('Error while validating uploaded Leads Sheet', error);
                });
            }
            else if (!is_one) {
                $rootScope.errorToast('Cannot upload more than one file at a time');
            }
            else if (!is_valid_filename) {
                $rootScope.errorToast('Filename cannot exceed 64 Characters');
            }
        };

        $scope.uploadFile = function () {
            var cfArr = _.map($scope.activeCFs, _.clone);

            _.each(cfArr, function (cfObj) {
                cfObj['cf_name'] = cfObj['field_name'];
                delete cfObj['field_name'];

                delete cfObj['id'];
                delete cfObj['is_deleted'];
                delete cfObj['partner_id'];
                delete cfObj['sort_id'];
                delete cfObj['visibility'];
                delete cfObj['field_type'];
            });

            //console.log('CF Array to be passed with upload', cfArr);
            leadsService.uploadValidatedFile(cfArr).then(function (result) {
                //console.log('Upload Result', result);
                if (result.data.status) {
                    //Update pagination and goTo page zero, post upload of new leads via fileUpload
                    getAllLeads(pageZero, true).then(function () {
                        $log.debug('Success getAllLeads post addLead() with currentPage = 0?', $scope.currentPage);
                    });
                    $('#uploadLeadsModal').modal('close');
                    $scope.disableFileUpload = true;
                    $scope.validationSuccess = false;
                    $scope.validationFail_headers = false;
                    $('#leadsUploadInput').val('');
                    $rootScope.successToast('Successfully imported ' + result.data.count + ' Leads.');
                }
                else {
                    $rootScope.errorToast('Error while importing leads');
                }
            }).catch(function (error) {
                //console.log('Error while uploading validated file', error);

            })
        };

        $scope.downloadAllLeads = function () {
            var activeFieldsArr = $scope.getExcelHeaders(false);
            var allLeads = _.map($scope.partnerLeads, _.clone);


            _.each(allLeads, function (leadObj) {
                leadObj['First Name'] = leadObj['first_name'];
                delete leadObj['first_name'];
                leadObj['Last Name'] = leadObj['last_name'];
                delete leadObj['last_name'];
                leadObj['Email'] = leadObj['email'];
                delete leadObj['email'];
                leadObj['Mobile'] = leadObj['mobile'];
                delete leadObj['mobile'];
                leadObj['Mobile'] = leadObj['Mobile'].toString();
                leadObj['Source'] = leadObj['source'];
                delete leadObj['source'];
                leadObj['Assigned To'] = leadObj['assignedto'];
                delete leadObj['assignedto'];
                leadObj['Action Date'] = leadObj['action_date'];
                delete leadObj['action_date'];
                leadObj['Notes'] = leadObj['notes'];
                delete leadObj['notes'];
                leadObj['Status'] = leadObj['status'];
                delete leadObj['status'];

                delete leadObj['id'];
                delete leadObj['is_deleted'];
                delete leadObj['lead_id'];
                delete leadObj['partner_id'];
                delete leadObj['row_created'];
                delete leadObj['row_last_updated'];
                delete leadObj['converted'];
            });

            _.each(allLeads, function (leadObj) {
                _.each(activeFieldsArr, function (fieldName) {
                    if (_.has(leadObj, fieldName)) {
                        //console.log('already Has the key', fieldName);
                    }
                    else {
                        leadObj[fieldName] = '';
                    }
                })
            })
            //console.log('All Leads', allLeads, activeFieldsArr);

            /* generate a worksheet */
            var ws = XLSX.utils.json_to_sheet(allLeads);

            /* add to workbook */
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, "allLeads");

            /* write workbook (use type 'binary') */
            var wbout = XLSX.write(wb, {bookType: 'xlsx', type: 'binary'});

            saveAs(new Blob([s2ab(wbout)], {type: "application/octet-stream"}), "allLeads.xlsx");
        };
    }]);


// //-- ALL FIELDS(NF+CF) --//
// if (Store.allFields.length > 0) {
//     $scope.allFields = Store.allFields;
//     //console.log('ALL fields data taken into ctrl', $scope.allFields);
//
//     //--- SET UP ACTIVE CF ---//
//     $scope.activeCFs = _.map($scope.allFields, _.clone);
//     $scope.activeCFs = _.filter($scope.activeCFs, function (fieldObj) {
//         return (fieldObj.field_type === 'cf' && fieldObj.is_deleted === 'N');
//     })
//     //console.log('Active CFS -->', $scope.activeCFs);
//
//     //--- REMOVE FIRST_NAME AND LAST_NAME FROM REORDER LIST ---//
//     $scope.reorderFieldsList = _.map($scope.allFields, _.clone)
//     $scope.reorderFieldsList = _.filter($scope.reorderFieldsList, function (fieldObj) {
//         return (fieldObj.field_name !== 'first_name' && fieldObj.field_name !== 'last_name');
//     });
//     $scope.reorderFieldsList = _.sortBy($scope.reorderFieldsList, 'sort_id');
//     _.each($scope.reorderFieldsList, function (field) {
//         if (field.field_name === 'email') {
//             field.field_name = 'Email';
//         }
//         else if (field.field_name === 'mobile') {
//             field.field_name = 'Mobile';
//         }
//         else if (field.field_name === 'source') {
//             field.field_name = 'Source';
//         }
//         else if (field.field_name === 'status') {
//             field.field_name = 'Status';
//         }
//         else if (field.field_name === 'assignedto') {
//             field.field_name = 'Assigned To';
//         }
//         else if (field.field_name === 'action_date') {
//             field.field_name = 'Action Date';
//         }
//         else if (field.field_name === 'notes') {
//             field.field_name = 'Notes';
//         }
//     });
//     //console.log('prepared list for reordering', $scope.reorderFieldsList);
//
//     //--- SET TABLE HEADERS(Only active fields) ---//
//     $scope.tableHeaders = _.map($scope.reorderFieldsList, _.clone)
//     $scope.tableHeaders = _.filter($scope.tableHeaders, function (fieldObj) {
//         return fieldObj.is_deleted !== 'Y' && fieldObj.visibility === 'Y';
//     });
//     $scope.tableHeaders = _.sortBy($scope.tableHeaders, 'sort_id');
//     $scope.tableHeaders = _.filter($scope.tableHeaders, function (fieldObj) {
//         return (fieldObj.field_name !== 'first_name' && fieldObj.field_name !== 'last_name');
//     });
//     //console.log('tableHeaders --->', $scope.tableHeaders);
//
//     //--- SET UP LEADS TABLE DATA ---//
//     var tempFields = _.map($scope.tableHeaders, _.clone);
//     tempFields = _.sortBy(tempFields, 'sort_id');
//     _.filter(tempFields, function (fieldObj) {
//         $scope.forTable.push(fieldObj.field_name);
//     })
//     _.each($scope.forTable, function (fieldName, index) {
//         if (fieldName === 'Email') {
//             $scope.forTable[index] = 'email';
//         }
//         else if (fieldName === 'Mobile') {
//             $scope.forTable[index] = 'mobile';
//         }
//         else if (fieldName === 'Source') {
//             $scope.forTable[index] = 'source';
//         }
//         else if (fieldName === 'Status') {
//             $scope.forTable[index] = 'status';
//         }
//         else if (fieldName === 'Assigned To') {
//             $scope.forTable[index] = 'assignedto';
//         }
//         else if (fieldName === 'Action Date') {
//             $scope.forTable[index] = 'action_date';
//         }
//         else if (fieldName === 'Notes') {
//             $scope.forTable[index] = 'notes';
//         }
//     });
//     //console.log('for TABLE', $scope.forTable);
// }
// else {
//     $scope.allFields = [];
// }
// //console.log('ALL customFieldsData', $scope.allCFs);

/**
 * Gets active Custom Fields defined by Partner
 * Call this after Add and Delete Custom Fields
 */
// $scope.getCustomFields = function () {
//     leadsService.getActiveCustomFields().then(function (result) {
//         //console.log('Custom Fields', result);
//         Store.customFieldsCheck = result.data.status;
//         //console.log('cf check in Store variables', Store.customFieldsCheck);
//         if (result.data.status) {
//             $scope.activeCFs = result.data.response;
//             $scope.activeCFs = _.sortBy($scope.activeCFs, 'field_name');
//             //console.log('Custom fields fetched', $scope.activeCFs);
//             $scope.CFforLeadsTable.length = 0; //Re-Initialize CFforLeadsTable array
//             _.each($scope.activeCFs, function (cf) {
//                 //console.log('cf for Leads Table header', cf);
//                 $scope.CFforLeadsTable.push(cf.field_name);
//             })
//             //console.log('header cfs updated from controller', $scope.CFforLeadsTable)
//         }
//         else {
//             $scope.activeCFs.length = 0;
//             $scope.activeCFs.push({});
//         }
//     }).catch(function (error) {
//         //console.log(error);
//     });
// };
