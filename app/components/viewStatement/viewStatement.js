angular.module('wealthQuotientApp')
    .component('viewStatement', {
        templateUrl: '../components/viewStatement/viewStatement.html',
        controller: viewStatementCtrl,
        bindings: {
            dismiss: '='
        }
    });

function viewStatementCtrl($scope,accountService) {
    "ngInject";
    console.log('----------------------- viewStatementCtrl calling');
    $scope.success = false;

    $('#viewStatement').on('shown.bs.modal', function () {
        console.log('---------------shown  22')
        $scope.setModal()
    })

    var self = this;
    $scope.setModal = function () {
        console.log('setModal--------------')
        $scope.success = true;

        getWalletTransactions();
        //$scope.success = false;
    }


    $scope.modalClose = function () {
        self.dismiss = false;
        $('#viewStatement').modal('hide');
    }


    $scope.$on('sub-is-logged', function () {
        // getWalletTransactions();
    });

    /**
     * get the wallet transactions
    */
    function getWalletTransactions() {
        accountService
            .getWalletTransactions()
            .then((result) => {
                $scope.transactions = result.data[0];
                $scope.success = false;
                groupTransactions();
                console.log('-----------------------  $scope.transactions',$scope.transactions)
            })
    }

    /**
     * group by debit or credit
    */
    function groupTransactions() {
        // magic of destructuring with lodash(gandalf the white)
        ({
            'CREDIT': $scope.credit,
            'DEBIT': $scope.debit
        } = _.groupBy($scope.transactions, 'transaction_type'));
    }
}
