angular.module('wealthQuotientApp')
  .component('sideNav', {
    templateUrl: '../components/sideNav/template/sideNav.html',
    controller: sidenavCtrl,
    // controllerAs: vm,
    bindings: {
        getData: '&'
    }

  });

function sidenavCtrl($scope,$rootScope,$cookies, myService) {
    "ngInject";

    $( document ).ready(function() {
        $('ul.nav li').hover(function() {
            $(this).find('.userProfile').stop(true, true).delay(100).fadeIn(500);
        }, function() {
            $(this).find('.userProfile').stop(true, true).delay(100).fadeOut(500);
        });
    });


    $scope.$on('sub-is-logged', function () {
        console.log('listning---------------------------')
        $scope.role = $cookies.get('Role');
        console.log('listning--------$scope.role -------------------',$scope.role)
    });

    $scope.logout = function () {
        myService.logout();
        $rootScope.balance = 0;
        $rootScope.alertDue = false;
        toastr.success('logout successful...');
    };
    $scope.resetPassword = function () {
        $scope.currPassword = '';
        $scope.newPassword = '';
        $scope.confirmnewPassword = '';
    };
    $scope.resetPassword();

    $scope.changePassword = function () {
        $rootScope.spinner = true;
        var oldPass = $scope.currPassword;
        var newPass = $scope.newPassword;
        var confirmPass = $scope.confirmnewPassword;

        console.log(oldPass, newPass, confirmPass);


        if (newPass === confirmPass) {
            var passObj = {
                'oldPassword': oldPass,
                'newPassword': newPass
            };
            console.log(passObj);
            myService.changePassword(passObj).then(function (result) {
                console.log(result);
                if (result.data.status) {
                    console.log(result.data.msg);
                    // $('#close-change-password').click();
                    // alert('sucess!!!');
                    $scope.resetPassword();
                    $rootScope.spinner = false;
                    $('#changePass').modal('hide');
                    toastr.success('Password changed successfully');
                    // myService.logout();

                } else {
                    $rootScope.spinner = false;
                    console.log(result.data.msg);
                    console.log('Current password does not match.');
                    toastr.error('Current password does not match');

                }
            }).catch(function (error) {
                console.log('error while changing the password');
                $rootScope.spinner = false;
                toastr.error('Error while changing the password');
            })
        } else {
            $rootScope.spinner = false;
            toastr.error('New Password and Confirm Password does not match');
            console.log('new password and confirm password do not match');
            $scope.resetPassword();
        }

    };



   // imageDefault();


}
