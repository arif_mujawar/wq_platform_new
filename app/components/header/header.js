angular.module('wealthQuotientApp')
    .component('header', {
        templateUrl: '../components/header/template/header.html',
        controller: headerCtrl,
        // controllerAs: vm,
        bindings: {
            getData: '&'
        }

    });

function headerCtrl( manageUsersService, $scope, $http, $state, loginService, myService, urlService, $rootScope, $cookies, $log, Store, accountService, $window) {
    "ngInject";

    $(document).on('keyup',function(evt) {
        if (evt.keyCode == 27) {
            closeSidenav()
        }
    });

    console.log('----------------------- header calling');
    $scope.viewStatement = false;
    $scope.viewRecharge = false;

    var fullHostName = window.location.hostname;
    var parts = fullHostName.split('.');
    var subDomain = parts[0];
    var domain = parts[1];
    $scope.customerObject = {};

    console.log('divided URL', parts);

    console.log('URL, SUBDOMAIN, DOMAIN', fullHostName, subDomain, domain);

    $scope.partURL = subDomain;

    $scope.loggedIn = $rootScope.loginVar;


    $scope.clientLogging = false;

    $scope.resetPassObj = {};

    $scope.disableResetPass = false;



    $scope.minimizeSideNav = function () {
        console.log('minimizeSideNav calling')
        $("body").toggleClass("mini-navbar");
    };

    $scope.openStatements = function () {

        $scope.viewStatement = !$scope.viewStatement;
        console.log('openStatements-------',$scope.viewStatement)
        if($scope.viewStatement){
            $('#viewStatement').modal('show');
        }else {
            $('#viewStatement').modal('hide');
        }
    };

    function checkSubUsers() {

        manageUsersService.uniqueUsername({ 'checkifnull': true, 'partnername': subDomain })
            .then(function (result) {
                if (result.data) {
                    if (!result.data.available) {

                        $scope.available = false;
                    } else {

                        $scope.available = true;

                    }
                } else {
                    $scope.available = true;
                }

            });
    }

    if (!$cookies.get('Authorization')) {
        checkSubUsers();
    }

    $rootScope.$on('urlCheckEvent', function (event, args) {

        // alert(args);
        console.log('received broadcast', args);
        var partnerName = args.partner;
        var clientName = args.client;
        console.log('partner and client', partnerName, clientName);

        if (clientName == '') {
            // alert('only partner!!!');
            console.log('partner url logo shown???', $rootScope.partnerURLLogo);
            $scope.partnerLogoName = partnerName;
            console.log('partner logo name:', $scope.partnerLogoName);
            $scope.partnerName = partnerName;
            // $rootScope.partnerURLLogo = true;
            // $('#partnerLogin').modal('open');
            if ($rootScope.show_reset_password_modal) {
                $('#resetPassModal').modal();
                $('#resetPassModal').modal('open');
            }

            document.getElementById("partnerNameId").readOnly = true;

        }
        else if (clientName != '') {
            $scope.clientLogging = true;
            console.log('customer url from controller set in service', $rootScope.partnerURLLogo);
            $scope.partnerLogoName = partnerName;
            $scope.customerPartnerName = partnerName;
            console.log('partner logo name:', $scope.partnerLogoName);
            $scope.customerObject.customerUsername = clientName;

            // $scope.customerUsername = clientName;
            //Forgot password check
            $log.debug('$rootScope.show_reset_password_modal value>>>', $rootScope.show_reset_password_modal);
            if ($rootScope.show_reset_password_modal) {
                $('#resetPassModal').modal('open');
            }
            else {
                $('#customerLogin').modal('open');
                document.getElementById("username").readOnly = true;
            }
        }


    });

    $scope.loginPartner = function () {
        $scope.inProgress = false;
        $rootScope.spinner = true;
        $('#partnerLogin').modal('hide');
        var partnerPassword = $scope.partnerPass;
        var partnerNaam = $scope.partnerName;

        var partnerObj = {
            'partnerName': partnerNaam,
            'password': partnerPassword
        };

        loginService.loginPartner(JSON.stringify(partnerObj)).then(function (result) {

            if (result.status === -1) {
                document.cookie = "Authorization=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.wealthquotient.in; path=/;";
                // document.cookie = "Authorization=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                console.log("login failed..in controller!!!!");
                $scope.partnerName = "";
                $scope.partnerPass = "";
                $rootScope.spinner = false;
                $scope.inProgress = true;

                toastr.error('Partner login failed');

            } else {
                if (result.data.status) {

                    console.log('Partner for get details', $rootScope.partnerName);
                    // getPartnerDetails($scope.partnerName);
                    // alert("partner logged in successfully!!!!!");
                    // loginService.getPartnerDetails
                    toastr.success('partner logged in successfully!!!!!');
                    $scope.partnerName = "";
                    $scope.partnerPass = "";
                    $rootScope.spinner = false;
                    console.log('loginVar SET TO true!!!!');
                    $rootScope.loginVar = true;
                    $rootScope.sideBar = true;
                    console.log('calling from controller', $rootScope.partnerName);

                    // $rootScope.getPartnerDetails();
                    $state.go('dashboard');
                    setTimeout(function () {
                        logOutPageChange();
                    }, 500)

                } else {
                    document.cookie = "Authorization=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.wealthquotient.in; path=/;";
                    // document.cookie = "Authorization=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                    console.log("login failed!!!!");
                    toastr.error('Username or password incorrect');
                    // $scope.partnerName = "";
                    $('#partnerLogin').modal('show');
                    $scope.partnerPass = "";
                    $rootScope.spinner = false;
                    $scope.inProgress = true;
                    //$state.go('register');
                    // $scope.showFailureToast();
                }
            }
        });
    };

    $scope.openRecharge = function(){
        $scope.viewRecharge = !$scope.viewRecharge;
        console.log('viewRecharge-------',$scope.viewRecharge)
        if($scope.viewRecharge) {
            $('#payment').modal('show');
        }else {
            $('#payment').modal('hide');
        }
    };

    $( document ).ready(function() {
        $('ul.nav li.dropdown').hover(function() {
            $(this).find('.typeofUsers').stop(true, true).delay(100).fadeIn(500);
        }, function() {
            $(this).find('.typeofUsers').stop(true, true).delay(100).fadeOut(500);
        });
    });

    $scope.logout = function () {
        myService.logout();
        $rootScope.balance = 0;
        $rootScope.alertDue = false;
        toastr.success('logout successful...');
    }

    $scope.sendResetPasswordLinkPartner = function () {
        // $log.debug("for sendResetPasswordLink()", $scope.customerPartnerName, $scope.customerObject.customerUsername);
        $scope.custLoginModalLoader = true;
        var partnerObj = { type: 'partner',partnerName: subDomain };
        loginService.sendResetPasswordLink(partnerObj).then(function (result) {
            // alert('Mail sent');
            $log.debug('sendResetPasswordLink() result>>>', result);
            if (result.data.status) {
                $rootScope.successToast('Reset password link is sent to ' + result.data.userEmail);
            }
            $scope.custLoginModalLoader = false;
        }).catch(function (error) {
            $rootScope.errorToast('Error while sending reset password link');
            $log.error('Error in sendResetPasswordLink()', error);
            $scope.custLoginModalLoader = false;
        });
    };

    $scope.loginSubUser = function () {
        $scope.inProgress = false;
        $rootScope.spinner = true;
        $('#subUserLogin').modal('hide');
        var subUserPassword = $scope.subUserPass;
        var subUserName = $scope.subUserName;

        var subUserObj = {
            'partnername': subDomain,
            'subUserName': subUserName,
            'password': subUserPassword
        };

        console.log(subUserObj);

        loginService.loginSubUser(JSON.stringify(subUserObj)).then(function (result) {

            if (result.status === -1) {
                document.cookie = "Authorization=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.wealthquotient.in; path=/;";
                // document.cookie = "Authorization=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                console.log("login failed..in controller!!!!");
                $scope.subUserName = "";
                $scope.subUserPass = "";
                $rootScope.spinner = false;
                $scope.inProgress = true;
                toastr.error('Sub-User login failed');
            } else {
                if (result.data.status) {

                    console.log('Sub-User for get details', $rootScope.subUserName);
                    console.log("=========================");
                    console.log(result.data);
                    // getPartnerDetails($scope.partnerName);
                    // alert("partner logged in successfully!!!!!");
                    // loginService.getPartnerDetails
                    toastr.success('Sub-User logged in successfully!!!');

                    $scope.subUserName = "";
                    $scope.subUserPass = "";
                    $rootScope.spinner = false;
                    console.log('loginVar SET TO true!!!!');
                    $rootScope.loginVar = true;
                    $rootScope.sideBar = true;
                    console.log('calling from controller', $rootScope.subUserName);

                    // $rootScope.getPartnerDetails();
                    $state.go('dashboard');
                    setTimeout(function () {
                        logOutPageChange();
                    }, 500)


                } else {
                    document.cookie = "Authorization=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.wealthquotient.in; path=/;";
                    // document.cookie = "Authorization=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                    console.log("login failed!!!!");
                    $('#subUserLogin').modal('show');
                    toastr.error('Login failed!! username or password incorrect');
                    // $scope.partnerName = "";
                    $scope.subUserPass = "";
                    $rootScope.spinner = false;
                    $scope.inProgress = true;
                    //$state.go('register');
                    // $scope.showFailureToast();
                }
            }
        });
    };

    $scope.sendResetPasswordLinkSubuser = function () {
        // $log.debug("for sendResetPasswordLink()", $scope.customerPartnerName, $scope.customerObject.customerUsername);
        $scope.custLoginModalLoader = true;
        var partnerObj = { type: 'subUser',subUserName : $scope.subUserName,partnerName: subDomain };
        loginService.sendResetPasswordLink(partnerObj).then(function (result) {
            // alert('Mail sent');
            $log.debug('sendResetPasswordLink() result>>>', result);
            if (result.data.status) {
                $rootScope.successToast('Reset password link is sent to ' + result.data.userEmail);
            }
            $scope.custLoginModalLoader = false;
        }).catch(function (error) {
            $rootScope.errorToast('Error while sending reset password link');
            $log.error('Error in sendResetPasswordLink()', error);
            $scope.custLoginModalLoader = false;
        });
    };

    $scope.loginCustomer = function () {
        $scope.inProgress = false;
        $rootScope.spinner = true;
        var custUsername = $scope.customerObject.customerUsername;
        var custPassword = $scope.customerObject.customerPass;
        // console.log(custUsername);
        // console.log(custPassword);
        var obj = {
            'username': custUsername,
            'password': custPassword
        };

        $log.debug('Customer Object for login>>>', obj, $scope.customerPass);

        loginService.loginCustomer(JSON.stringify(obj)).then(function (result) {
            console.log('chechk', JSON.stringify(result));
            // Materialize.toast('Logging you in', 3000, 'rounded');

            if (result.data.status) {

                console.log('user info post login--', result.data.client_id, result.data.id, result.data.clientname);

                var userTokenObj = {
                    'id': result.data.id,
                    'user_id': result.data.client_id,
                    'username': result.data.clientname
                };

                loginService.setLogs(JSON.stringify(userTokenObj)).then(function (success) {

                    if (success.data.status) {
                        // loginService.setLogs();
                        // console.log('updated user logs!!!!');
                        $scope.customerUsername = "";
                        $scope.customerPass = "";
                        $rootScope.spinner = false;
                        $scope.inProgress = true;
                        // alert(window.location.href);
                        // var xhr = new XMLHttpRequest();
                        // xhr.open('GET', 'http://localhost:8082/clientLogin', true);
                        // xhr.withCredentials = true;
                        // xhr.send(null);
                        $('#customerLogin').modal('hide');
                        console.log('success ------------------------------' + JSON.stringify(success));
                        if (window.origin.includes('wealthquotient')) {
                            window.open('https://wmplatform.wealthquotient.in', '_self');
                        } else {
                            if (Store.isEmptyOrUndefinedOrNull($scope.customerObject.customerUsername)) {
                                //For local testing...
                                window.open("http://localhost:7001", '_blank');
                            }
                            else {
                                window.open("http://" + $scope.customerObject.customerUsername + ".localhost:7001", '_blank');
                            }
                        }
                    }
                    else {
                        if (window.origin.includes('wealthquotient')) {
                            document.cookie = "Authorization1=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.wealthquotient.in;path=/;";
                        } else {
                            document.cookie = "Authorization1=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                        }
                        $cookies.remove('Authorization1');
                        toastr.error('login failed!!!! No such client');
                        $scope.customerUsername = "";
                        $scope.customerPass = "";
                        $rootScope.spinner = false;
                        $scope.inProgress = true;
                    }

                }).catch(function (error) {
                    if (window.origin.includes('wealthquotient')) {
                        document.cookie = "Authorization1=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.wealthquotient.in;path=/;";
                    } else {
                        document.cookie = "Authorization1=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                    }
                    $cookies.remove('Authorization1');
                    $scope.customerUsername = "";
                    $scope.customerPass = "";
                    $rootScope.spinner = false;
                    $scope.inProgress = true;
                    console.log("login failed..in controller!!!!", result);
                    toastr.error('login failed!!!! No such client');

                });


            } else {
                $rootScope.spinner = false;
                $scope.inProgress = true;
                if (window.origin.includes('wealthquotient')) {
                    document.cookie = "dummyAuth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.wealthquotient.in;path=/;";
                } else {
                    document.cookie = "dummyAuth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/';";
                }
                $cookies.remove('dummyAuth');
                console.log("dummyAuth removed");
                // document.cookie = "Authorization1=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/';";
                // document.cookie = "Authorization1=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.wealthquotient.in;path=/;";
                console.log("login failed!!!!");
                let msgHtml = '';
                if(result.data.email) {
                    msgHtml = '<span>' + result.data.msg + '<a href="mailto:' + result.data.email + '" class="btn-flat amber-text" style="text-transform: lowercase;">' + result.data.email + '</a></span>';
                }
                msgHtml = `<span> ${result.data.msg}</span>`
                toastr.error(msgHtml, 'Login Failed');
            }

        }).catch(function (error) {
            $rootScope.spinner = false;
            $scope.inProgress = true;
            console.log('Log in failed', error);
           toastr.error('Log in failed');
        });

    };

    $scope.sendResetPasswordLink = function () {
        $scope.custLoginModalLoader = true;
        var customerObj = { type: 'customer', username: $scope.customerObject.customerUsername, partnerName: $scope.customerPartnerName };
        $log.debug('Customer Obj for reset password>>>', customerObj);
        loginService.sendResetPasswordLink(customerObj).then(function (result) {
            // alert('Mail sent');
            $log.debug('sendResetPasswordLink() result>>>', result);
            if (result.data.status) {
                $rootScope.successToast('Reset password link is sent to ' + result.data.userEmail);
            }
            $scope.custLoginModalLoader = false;
        }).catch(function (error) {
            $rootScope.errorToast('Error while sending reset password link');
            $log.error('Error in sendResetPasswordLink()', error);
            $scope.custLoginModalLoader = false;
        });
    };

}
