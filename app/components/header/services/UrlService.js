'use strict';

/**
 * @ngdoc service
 * @name wealthQuotientApp.loginService
 * @description
 * # loginService
 * Factory in the wealthQuotientApp.
 */
angular.module('wealthQuotientApp')
    .factory('urlService', function ($http,myService, $cookies, $state, $rootScope) {
        /*
         |-------------------------------------------------------------------------------|
         |    Check partner Name from URL                                                       |
         |-------------------------------------------------------------------------------|
         */
        var checkPartnerUrl = function (partnerDetails) {
            console.log('api call for partner name check');
            return $http.post(baseUrl + 'user/checkPartnerName', {urlName: partnerDetails}).then(function (success) {
                // console.log('inside partnerUrlService', success, success.data.status);
                // if (success.data.status) {
                //     $rootScope.partnerURL = true;
                // }
                console.log('success from service : success');
                return success;

            }).catch(function (error) {
                console.log(error);
                return error;
            });
        }

        var checkClientUrl = function (clientDetails) {
            console.log('api call for client name check', clientDetails);
            return $http.post(baseUrl + 'user/checkClientName', {urlName: clientDetails}).then(function (success) {
                // console.log('inside partnerUrlService', success, success.data.status);
                // if (success.data.status) {
                //     $rootScope.partnerURL = true;
                // }
                return success;

            }).catch(function (error) {
                console.log(error);
                return error;
            });
        }



        return {
            checkPartnerUrl: checkPartnerUrl,
            checkClientUrl: checkClientUrl
        };

    });
