'use strict';

/**
 * @ngdoc service
 * @name wealthQuotientApp.loginService
 * @description
 * # loginService
 * Factory in the wealthQuotientApp.
 */
angular.module('wealthQuotientApp')
    .factory('loginService', function ($http, myService, $cookies, $state, $rootScope, $log) {
        /*
         |-------------------------------------------------------------------------------|
         |    Login                                                          |
         |-------------------------------------------------------------------------------|
        */

        var loginPartner = function (partnerDetails) {
            console.log('api call for partner login' + baseUrl);
            return $http.post(baseUrl + 'user/partnerlogin', partnerDetails).then(function (success) {
                console.log('inside service', JSON.stringify(success));
                if (success.status) {

                    console.log(success.data.token)
                    window.sessionStorage.setItem('Authorization', success.data.token);
                    $cookies.put('Authorization', success.data.token);
                    var now = new Date();
                    var time = now.getTime();
                    time += 3600 * 1000;
                    now.setTime(time);
                    $cookies.put('Role', 'partner',{
                        expires : now.toUTCString(),
                        path : '/'
                    });
                    if (window.origin.includes('wealthquotient')) {
                        document.cookie = 'Authorization=' + success.data.token + '; expires=' + now.toUTCString() + ";domain=.wealthquotient.in;path=/;";
                    }
                    else {
                        document.cookie =
                            'Authorization=' + success.data.token +
                            '; expires=' + now.toUTCString() +
                            '; path=/';
                    }

                    $rootScope.getPartnerDetails();
                    $rootScope.cookieValue = success.data.token;
                    return success;

                } else {
                    console.log('no token set as not logged in');
                    return success;
                }
                // console.log('success from service', success);

            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var loginCustomer = function (userDetails) {

            console.log('api call for customer login');
            // var final;
            return $http.post(baseUrl + 'user/login', userDetails).then(function (success) {
                console.log('inside service', JSON.stringify(success));
                if (success.data.status) {
                    console.log('user logged in with partner token - dummyAuth', success.data.status);
                    // window.sessionStorage.setItem('dummyAuth', success.data.token);
                    // $cookies.put('dummyAuth', success.data.token);
                    var now = new Date();
                    var time = now.getTime();
                    time += 3600 * 1000;
                    now.setTime(time);
                    // console.log('the href is ',window.location.href)
                    if (window.origin.includes('wealthquotient')) {
                        document.cookie = 'dummyAuth=' + success.data.token + '; expires=' + now.toUTCString() + ";domain=.wealthquotient.in;path=/;";
                    }
                    else {
                        document.cookie =
                            'dummyAuth=' + success.data.token +
                            '; expires=' + now.toUTCString() +
                            '; path=/';
                    }

                    // loginService.setLogs();
                    // loginService.lastUserPage().then(function (result_state) {
                    //     if (result_state.data.dataArray !== ' ') {
                    //         $state.go(result_state.data.dataArray);
                    //     } else {
                    //         $state.go('gettingStarted');
                    //     }
                    // });

                } else {
                    console.log('no token set as not logged in');
                }

                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };


        var loginSubUser = function (subUserDetails) {
            console.log('api call for sub user login');
            return $http.post(baseUrl + 'manageUsers/subUserLogin', subUserDetails).then(function (success) {
                // console.log('inside service', success, success.status, success.data.token);
                if (success.status) {
                    console.log(success)

                    window.sessionStorage.setItem('Authorization', success.data.token);
                    
                    $cookies.put('Authorization', success.data.token);
                    var now = new Date();
                    var time = now.getTime();
                    time += 3600 * 1000;
                    now.setTime(time);
                    $cookies.put('Role', 'sub_user',{
                        expires : now.toUTCString(),
                        path : '/'
                    });
                    if (window.origin.includes('wealthquotient')) {
                        document.cookie = 'Authorization=' + success.data.token + '; expires=' + now.toUTCString() + ";domain=.wealthquotient.in;path=/;";
                    }
                    else {
                        document.cookie =
                            'Authorization=' + success.data.token +
                            '; expires=' + now.toUTCString() +
                            '; path=/';
                    }

                    $rootScope.getSubUserDetails();

                    $rootScope.cookieValue = success.data.token;
                    console.log('cookie value', $rootScope.cookieValue);
                    console.log('subUser logo name fetched ---', $rootScope.subUserName);
                } else {
                    console.log('no token set as not logged in');
                }
                // console.log('success from service', success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };


        var setUserLogs = function (tokenObj) {

            myService.setDummyHeaders();
            return $http.post(baseUrl + 'user/setLoginTime', tokenObj).then(function (res) {

                console.log('check for client token in service...setLogs()', res);

                if (res.data.status) {
                    console.log('client token status', res.data.status);
                    console.log('user log set success', res.data.status);

                    window.sessionStorage.setItem('Authorization1', res.data.token);
                    $cookies.put('Authorization1', res.data.token);
                    var now = new Date();
                    var time = now.getTime();
                    time += 3600 * 1000;
                    now.setTime(time);
                    // console.log('the href is ',window.location.href)
                    if (window.origin.includes('wealthquotient')) {
                        document.cookie = 'Authorization1=' + res.data.token + '; expires=' + now.toUTCString() + ";domain=.wealthquotient.in;path=/;";
                    }
                    else {
                        document.cookie =
                            'Authorization1=' + res.data.token +
                            '; expires=' + now.toUTCString() +
                            '; path=/';
                    }

                    if (window.origin.includes('wealthquotient')) {
                        document.cookie = "dummyAuth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.wealthquotient.in;path=/;";
                    }
                    else {
                        document.cookie = "dummyAuth=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/';";
                    }

                    $cookies.remove('dummyAuth');
                    // alert('success');
                    console.log('deleted dummyAuth cookie...', document.cookie);
                    // document.cookie = 'Authorization1'+"=" + success.data.token + ";domain=localhost;path=/;"
                    // document.cookie = 'Authorization1=' + success.data.token + '; expires=' + now.toUTCString() + ";path=/;";
                    console.log($cookies.getAll());
                    console.log('Client cookie generated', document.cookie);
                    console.log('returning result to controller', res);


                } else {
                    console.log('error while inserting the user logs');
                    console.log('client token status', res.data.status);

                }
                return res;
            }).catch(function (error) {
                console.log('error while inserting the user logs');
                return error;
            });
        };


        var getLastUserPage = function (client) {
            myService.setHeaders();
            return $http.post(baseUrl + 'user/getUserState', client).then(function (res) {
                console.log('ned to chechk this again', res);
                if (res.data.status) {
                    console.log(res.data.dataArray);
                    return res;
                } else {
                    console.log('error while inserting the user logs');
                    $state.go('login');
                    return res;
                }
            }).catch(function (error) {
                console.log('error while inserting the user logs');
                $state.go('login');
                return error;
            });
        };

        var sendResetPasswordLink = function (customerObj) {
            $log.info('Api call from sendResetPasswordLink() service');
            return $http.post(baseUrl + 'user/sendOTPForgotPassword', customerObj).then(function (success) {
                return success;
            }).catch(function (error) {
                return error;
            });
        };

        var checkMailToken = function(token){
            return $http.post(baseUrl + 'user/verifyMailToken', {token: token}).then(function(success){
                return success;
            }).catch(function(error){
                return error;
            });
        };
        // var getFPStatus = function(client) {
        //     myService.setHeaders();
        //     return $http.post(baseUrl + 'user/getUserState', client).then(function (res) {
        //         console.log('ned to chechk this again', res);
        //         if (res.data.status) {
        //             console.log(res.data.dataArray);
        //             return res;
        //         } else {
        //             console.log('error while inserting the user logs');
        //             $state.go('login');
        //             return res;
        //         }
        //     }).catch(function (error) {
        //         console.log('error while inserting the user logs');
        //         $state.go('login');
        //         return error;
        //     });
        // }

        return {
            loginSubUser: loginSubUser,
            loginPartner: loginPartner,
            loginCustomer: loginCustomer,
            setLogs: setUserLogs,
            lastUserPage: getLastUserPage,
            sendResetPasswordLink: sendResetPasswordLink,
            checkMailToken: checkMailToken
        };
    });
