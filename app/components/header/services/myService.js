'use strict';

/**
 * @ngdoc service
 * @name wealthQuotientApp.myService
 * @description
 * # myService
 * Factory in the wealthQuotientApp.
 */
angular.module('wealthQuotientApp')
    .factory('myService', function ($http, $state, $cookies, $location, $rootScope) {
        var isAuthenticated = false;
        var authToken;
        var userPersonalInfo ={};




        var getUserId = function () {
            loadUserCredentials();
            return $http.post(baseUrl+'user/getUserId').then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getUserSpouseId = function () {
            loadUserCredentials();
            return $http.post(baseUrl+'user/getUserSpouseId').then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };


        var logoutProceedToken = function() {
            console.log('ille nodi pa', $state.current.name);
            $http.post(baseUrl+'user/setLogoutTime',{'state':$state.current.name}).then(function (success) {
                authToken = undefined;
                isAuthenticated = false;
                $http.defaults.headers.common.Authorization = authToken;
                $cookies.remove('Authorization');

                document.cookie = "Authorization=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
                console.log('Deleted Cookie');
            }).catch(function (error) {
                console.log('error while executing the update user logs query');
            });

        };

      /*
       |-------------------------------------------------------------------------------|
       |    set the progress bar                                                       |
       |-------------------------------------------------------------------------------|
       */
        var changePassword = function (data) {
            loadUserCredentials();
            console.log(data);
            return $http.post(baseUrl+'user/changePassword',data).then(function(success) {
                return success;
            }).catch(function (error) {
                return error;
            });
        };

      /*
       |-------------------------------------------------------------------------------|
       |    logout                                                       |
       |-------------------------------------------------------------------------------|
       */
        var logout = function() {
            var logoutStatus = {status: ''} ;
            // $rootScope.spinner = true;
            $rootScope.loginVar = false;
            $rootScope.sideBar = false;
            console.log('disabling side nav and logged in in logout ');
            authToken = undefined;
            isAuthenticated = false;
            $http.defaults.headers.common.Authorization = authToken;
            $cookies.remove('Authorization');
            $cookies.remove('Role');
            
            $rootScope.partnerCookie = undefined;
            $rootScope.subUserCookie = undefined;
            $rootScope.subUserName = undefined;
            $rootScope.subUserId = undefined;
            $rootScope.routeInfo = undefined;

            if (window.origin.includes('wealthquotient')) {
                document.cookie = "Authorization=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.wealthquotient.in;path=/;";
            }
            else {
                document.cookie = "Authorization=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            }
            console.log('Deleted Cookie');
            console.log('still exists???', $rootScope.getCookie('Authorization'));
            if($cookies.get('Authorization') == undefined){
                // $rootScope.spinner = false;
                console.log('logout successful...');
                $rootScope.loginVar = false;
                $rootScope.sideBar = false;
                $location.path("/home");

            }
            else {
                toastr.error('logout failed..!!');
                console.log('logout failed.. cookie present', $rootScope.getCookie('Authorization'));

            }
        };



        var adminLogout = function() {
            var logoutStatus = {status: ''} ;
            $rootScope.loginVar = false;
            $rootScope.sideBar = false;
            console.log('disabling side nav and logged in in logout ');
            authToken = undefined;
            isAuthenticated = false;
            $http.defaults.headers.common.Authorization = authToken;
            $cookies.remove('Authorization');
            $cookies.remove('Role');
            console.log($cookies.get('Role'))
            $rootScope.partnerCookie = undefined;
            $rootScope.subUserCookie = undefined;
            $rootScope.adminCookie = undefined;

            if (window.origin.includes('wealthquotient')) {
                document.cookie = "Authorization=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.wealthquotient.in;path=/;";
            }
            else {
                document.cookie = "Authorization=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            }
            console.log('Deleted Cookie');
            console.log('still exists???', $rootScope.getCookie('Authorization'));
            if($cookies.get('Authorization') == undefined){
                console.log('logout successful...');
                $rootScope.loginVar = false;
                $rootScope.sideBar = false;
                $location.path("/adminLogin");

            }
            else {
                console.log('logout failed.. cookie present', $rootScope.getCookie('Authorization'));

            }
        };

      /*var logout = function () {
       logoutProceedToken();
       };*/


      /*
       |-------------------------------------------------------------------------------|
       |    Check for the username                                                        |
       |-------------------------------------------------------------------------------|
       */

        var checkUserValid = function () {
            loadUserCredentials();
            var url = window.location.href;
            var username = url.substring(url.indexOf('/')+2,url.indexOf('.'));
            return $http.post(baseUrl+'users/checkUserName',{'username':username}).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };



      /*
       |-------------------------------------------------------------------------------|
       |    get user full name                                                        |
       |-------------------------------------------------------------------------------|
       */

        var getFullName = function () {
            loadUserCredentials();
            return $http.post(baseUrl+'register/getUserFullName').then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };



      /*
       |-------------------------------------------------------------------------------|
       |    set the progress bar                                                       |
       |-------------------------------------------------------------------------------|
       */


        var progressBar = function () {
            $(function() {
                var count = 0;
                $('.node, .sub-node').each(function () {
                    var $this = $(this);
                    if ($this.hasClass('active-node')) {
                        count = 1;
                    } else if (count !== 1) {
                        $this.addClass('active-node');
                    }
                });
                var activesubnode = 0;
                var disabledBarWidth = $('.disabled-bar').width();
                var numberOfNodes = $('.disabled-bar .node').length;
                //numberOfNodes = 8;
                var numberOfNodesActive = $('.disabled-bar .node.active-node').length - 1;

                $('.disabled-bar .node.active-node').eq(numberOfNodesActive).addClass('current-active-node');
                for (var i = 2; i <= numberOfNodes; i++) {
                    var leftPosition = disabledBarWidth / (numberOfNodes - 1) * (i - 1) - 10;
                    $('.node-' + i).css({
                        left: leftPosition
                    });
                    var j = i - 1;
                    var subNodeCount = $('.disabled-bar .node-' + j + ' .sub-node').length + 1;
                    count = 0;
                    $('.disabled-bar .node-' + j + ' .sub-node').each(function () {
                        count = count + 1;
                        $(this).css({
                            left: leftPosition / (subNodeCount * j) * count
                        });
                        if ($(this).hasClass('active-node')) {
                            activesubnode = $(this).css('left');
                        }
                    });
                }

                var currentActiveNode = $('.disabled-bar .current-active-node').css('left');
                if ($('.disabled-bar .current-active-node .active-node').length) {
                    activesubnode = parseInt(currentActiveNode) + parseInt(activesubnode) + 10;
                } else {
                    activesubnode = parseInt(currentActiveNode) + 10;
                }

                $('.active-bar').width(activesubnode);
            });
        };

      /*
       |-------------------------------------------------------------------------------|
       |    retrieve data                                                              |
       |-------------------------------------------------------------------------------|
       */

        function loadUserCredentials() {
            if($http.defaults.headers.common.Authorization == null){
                var token = $cookies.get('Authorization');
                if (token != '') {
                    authToken = token;
                    console.log('authToken',authToken);
                    $http.defaults.headers.common.Authorization = authToken;

                }else{
                    console.log('no token in cache please login or register');
                    logout();
                }
            }
        }

        /*
         |-------------------------------------------------------------------------------|
         |    Load partner data from dummyAuth to login Client and set his logs                                                             |
         |-------------------------------------------------------------------------------|
         */

        function loadDummyAuth() {
            console.log('in loadDummyAuth service');
            if($http.defaults.headers.common.Authorization == null){
                var token = $cookies.get('dummyAuth');
                if (token != null) {
                    authToken = token;
                    console.log('authToken',authToken);
                    $http.defaults.headers.common.Authorization = authToken;
                }else{
                    console.log('no token in cache please login or register');
                    // $state.go('register');
                    alert('no dummyAuth found!!!!');
                }
            }
        }

        var storeUserInformation = function (userInfo) {
            userPersonalInfo = extend(userPersonalInfo, userInfo);
        };

        var exportUserInformation = function () {
            return userPersonalInfo;
        };

        function extend(obj, src) {
            for (var key in src) {
                if (src.hasOwnProperty(key)) {
                    obj[key] = src[key];
                }
            }
            return obj;
        };

        var resetPassword = function (resetPassObj) {
            // loadUserCredentials();
            console.log(resetPassObj);
            return $http.post(baseUrl+'user/setForgottenPassword',resetPassObj).then(function(success) {
                return success;
            }).catch(function (error) {
                return error;
            });
        };

        return {
            setProgressBar : progressBar,
            saveInfo : storeUserInformation,
            getInfo : exportUserInformation,
            setHeaders : loadUserCredentials,
            setDummyHeaders : loadDummyAuth,
            getId : getUserId,
            getSpouseId : getUserSpouseId,
            logoutProceedToken:logoutProceedToken,
            getName : getFullName,
            checkUser : checkUserValid,
            logout : logout,
            changePassword : changePassword,
            adminLogout : adminLogout,
            resetPassword: resetPassword
        };
    });
