'use strict';
angular.module('wealthQuotientApp')
    .component('paymentModal', {
        controller: paymentCtrl,
        templateUrl: '../components/payment/payment.html',
        bindings: {
            dismiss: '=',
            amount: '=',
            payAmount: '&',
            isManual: '@'
        }
    });

function paymentCtrl($scope, $timeout, $rootScope, $log) {
    "ngInject";
    var self = this;
    $scope.nextVar = false;

    $scope.customStyleDis2= {};

    this.$onInit = function () {
        $scope.isManual = JSON.parse(self.isManual);
        $scope.checkRechargeAmount = true;
        console.log('-------------------$scope.checkRechargeAmount ',$scope.checkRechargeAmount )
        if($scope.checkRechargeAmount){
            $scope.customStyleDis2.style = {"color": "#fff"};
        }else{
            $scope.customStyleDis2.style = {"color": "#1ab394"};
        }
        $scope.customStyle2 = {};
        $scope.customStyle5 = {};
        $scope.customStyle7 = {};
        $scope.customStyle1= {};
        $scope.payment = '';
        $scope.bonusAmount = 0;
        $scope.bonusPercentage = 0;
        $scope.showBonus = false;
    }


    $scope.rechargeFn = function(){
        var dataMode;
        
        switch ($scope.mode) {
            case 'Cheque':
                dataMode = {
                    chequeNo:$scope.chequeNo,
                    bankName:$scope.bankName
                };
                break;
            case 'DD':
                dataMode = {
                    ddNo:$scope.ddNo,
                    bankName:$scope.bankName1
                };
                break;
            case 'Cash':
                dataMode = {
                    desc:'paid in cash'
                };
                break;
            case 'Online':
                dataMode = {
                    mode:$scope.OnlineMode,
                    bankName:$scope.bankName2,
                    refNo:$scope.refNo1
                };
                break;
            case 'UPI':
                dataMode = {
                    upiId:$scope.upiId,
                    refNo:$scope.referNo
                };
                break;
            case 'Wallet':
                dataMode = {
                    chequeNo:$scope.walletName,
                    refNo:$scope.refer1No
                };
                break;
            default:
                dataMode = {
                    data:null
                }
        }

        self.payAmount({amount:$scope.paymentCustom,bonus:$scope.bonusAmount,data:dataMode});
    }

    $scope.close = function(){
        console.log('close calling')
        $scope.nextVar = false;
        $scope.viewRecharge = true;
        self.dismiss = false;
        $('#payment').modal('hide');
    }

    $scope.setModal = function () {
        if(self.amount){
            $scope.checkRechargeAmount = false;
            $scope.showBonus = true;
            $scope.paymentCustom = self.amount;
            $scope.showBonusCustom();
            console.log('-------------------$scope.checkRechargeAmount ',$scope.checkRechargeAmount )
        }else{
            $scope.checkRechargeAmount = true;
            console.log('-------------------$scope.checkRechargeAmount ',$scope.checkRechargeAmount )
        }

        
    };

    $scope.twofiftycolor = function (data) {
              $scope.errrorMessage = '';
              $scope.checkRechargeAmount = false;
              $scope.paymentCustom = '';
              if (data == '2500') {
                  $scope.payment='2500';
                  $scope.paymentCustom = $scope.payment;
                  $scope.customStyle2.style = {"background-color": "#5f98cd","color":"white"};
                  $scope.customStyle7.style = {"background-color": "white"};
                  $scope.customStyle5.style = {"background-color": "white"};
                  $scope.customStyle1.style = {"background-color": "white "};
                  $scope.customStyleDis2.style = {"color": "#1ab394"};
                  $scope.bonusAmount = 250;
                  $scope.bonusPercentage = 10;
                  $scope.showBonus = true;
              }
              if (data == '5000') {
                  $scope.payment='5000';
                  $scope.paymentCustom = $scope.payment;
                  $log.debug('$scope.payment.twofifty==============',JSON.stringify($scope.payment));
                  $scope.customStyle5.style = {"background-color": "#5f98cd","color":"white"};
                  $scope.customStyle7.style = {"background-color": "white"};
                  $scope.customStyle2.style = {"background-color": "white"};
                  $scope.customStyle1.style = {"background-color": "white "};
                  $scope.customStyleDis2.style = {"color": "#1ab394"};
                  $scope.bonusAmount = 750;
                  $scope.bonusPercentage = 15;
                  $scope.showBonus = true;
              }
              if (data == '7500') {
                  $scope.payment='7500';
                  $scope.paymentCustom = $scope.payment;
                  $log.debug('$scope.payment.twofifty==============',JSON.stringify($scope.payment));
                  $scope.customStyle7.style = {"background-color": "#5f98cd","color":"white"};
                  $scope.customStyle2.style = {"background-color": "white"};
                  $scope.customStyle5.style = {"background-color": "white "};
                  $scope.customStyle1.style = {"background-color": "white "};
                  $scope.customStyleDis2.style = {"color": "#1ab394"};
                  $scope.bonusAmount = 1500;
                  $scope.bonusPercentage = 20;
                  $scope.showBonus = true;
              }
              if (data == '10000') {
                  $scope.payment='10000';
                  $scope.paymentCustom = $scope.payment;
                  $log.debug('$scope.payment.twofifty==============',JSON.stringify($scope.payment));
                  $scope.customStyle1.style = {"background-color": "#5f98cd","color":"white"};
                  $scope.customStyle7.style = {"background-color": "white"};
                  $scope.customStyle2.style = {"background-color": "white"};
                  $scope.customStyle5.style = {"background-color": "white "};
                  $scope.customStyleDis2.style = {"color": "#1ab394"};
                  $scope.bonusAmount = 3000;
                  $scope.bonusPercentage = 30;
                  $scope.showBonus = true;
              }
            };

            $scope.showBonusCustom = function() {
              $scope.showBonus = false;
              $scope.payment = '';


              if ($scope.paymentCustom == undefined) {
                  $scope.showBonus = false;
                  $scope.customStyle1.style = {"background-color": "white "};
                  $scope.customStyle2.style = {"background-color": "white "};
                  $scope.customStyle5.style = {"background-color": "white "};
                  $scope.customStyle7.style = {"background-color": "white "};
                  $scope.customStyleDis2.style = {"color": "#1ab394"};
                  $scope.checkRechargeAmount = true;
              }  else if ($scope.paymentCustom < 500) {
                  $scope.showBonus = false;
                  $scope.customStyle1.style = {"background-color": "white "};
                  $scope.customStyle2.style = {"background-color": "white "};
                  $scope.customStyle5.style = {"background-color": "white "};
                  $scope.customStyle7.style = {"background-color": "white "};
                  $scope.errrorMessage = 'Please enter an amount greater than ₹500';
                  $scope.customStyleDis2.style = {"color": "#1ab394"};
                  $scope.checkRechargeAmount = true;
              }  else if ($scope.paymentCustom < 2500) {
                  $scope.showBonus = false;
                  $scope.customStyle1.style = {"background-color": "white "};
                  $scope.customStyle2.style = {"background-color": "white "};
                  $scope.customStyle5.style = {"background-color": "white "};
                  $scope.customStyle7.style = {"background-color": "white "};
                  $scope.customStyleDis2.style = {"color": "#1ab394"};
                  $scope.errrorMessage = '';
                  $scope.checkRechargeAmount = false;
              }  else if ($scope.paymentCustom < 5000) {
                  $scope.bonusAmount = parseInt($scope.paymentCustom * 0.10);
                  $scope.bonusPercentage = 10;
                  $scope.showBonus = true;
                  $scope.customStyle1.style = {"background-color": "white "};
                  $scope.customStyle2.style = {"background-color": "white "};
                  $scope.customStyle5.style = {"background-color": "white "};
                  $scope.customStyle7.style = {"background-color": "white "};
                  $scope.customStyleDis2.style = {"color": "#1ab394"};
                  $scope.errrorMessage = '';
                  $scope.checkRechargeAmount = false;
              } else if ($scope.paymentCustom < 7500) {
                  $scope.bonusAmount = parseInt($scope.paymentCustom * 0.15);
                  $scope.bonusPercentage = 15;
                  $scope.showBonus = true;
                  $scope.customStyle1.style = {"background-color": "white "};
                  $scope.customStyle2.style = {"background-color": "white "};
                  $scope.customStyle5.style = {"background-color": "white "};
                  $scope.customStyle7.style = {"background-color": "white "};
                  $scope.errrorMessage = '';
                  $scope.checkRechargeAmount = false;
              }  else if ($scope.paymentCustom < 10000) {
                  $scope.bonusAmount = parseInt($scope.paymentCustom * 0.2);
                  $scope.bonusPercentage = 20;
                  $scope.showBonus = true;
                  $scope.customStyle1.style = {"background-color": "white "};
                  $scope.customStyle2.style = {"background-color": "white "};
                  $scope.customStyle5.style = {"background-color": "white "};
                  $scope.customStyle7.style = {"background-color": "white "};
                  $scope.customStyleDis2.style = {"color": "#1ab394"};
                  $scope.errrorMessage = '';
                  $scope.checkRechargeAmount = false;
              } else {
                  $scope.bonusAmount = parseInt($scope.paymentCustom * 0.3);
                  $scope.bonusPercentage = 30;
                  $scope.showBonus = true;
                  $scope.customStyle1.style = {"background-color": "white "};
                  $scope.customStyle2.style = {"background-color": "white "};
                  $scope.customStyle5.style = {"background-color": "white "};
                  $scope.customStyle7.style = {"background-color": "white "};
                  $scope.customStyleDis2.style = {"color": "#1ab394"};
                  $scope.errrorMessage = '';
                  $scope.checkRechargeAmount = false;
              }
            };


            $scope.calculateBonus = function(bonusPercentage){
              $timeout(function() {
                  if(bonusPercentage>=0 || bonusPercentage<=100){
                     $scope.bonusAmount = $scope.paymentCustom * bonusPercentage / 100;
                  }
                  console.log($scope.bonusAmount,bonusPercentage,$scope.bonusPercentage)
              });
              
            }



        

}

angular.module('wealthQuotientApp')
  .directive("preventTypingGreater", function() {
    return {
      link: function(scope, element, attributes) {
        var oldVal = null;
        element.on("keydown keyup", function(e) {
      if (Number(element.val()) > Number(attributes.max) &&
            e.keyCode != 46 // delete
            &&
            e.keyCode != 8 // backspace
          ) {
            e.preventDefault();
            element.val(oldVal);
          } else {
            oldVal = Number(element.val());
          }
        });
      }
    };
});