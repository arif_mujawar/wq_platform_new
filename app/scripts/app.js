/**
 * INSPINIA - Responsive Admin Theme
 *
 */

var baseUrl;

if (window.origin.includes('wealthquotient')) {
    baseUrl = "https://wealthquotient.in/api/";
}
if (window.origin.includes('preprod')) {
    baseUrl = "https://preprod.wealthquotient.in/api/";
}
if (window.origin.includes('localhost')) {
    baseUrl = "http://localhost:8083/api/";
}

function logOutPageChange() {
    if(window.location.href.includes('http://localhost:7001/#/home') || window.location.href.includes('wealthquotient.in/#/home') || window.location.href === 'https://wealthquotient.in' || window.location.href === 'http://localhost:7001'){
        console.log('going inside if 0000000000000000000000000000' + window.location.href);
        $(document).ready(function () {
            $('body').removeClass('ng-scope mini-navbar pace-done fixed-sidebar').addClass('mini-navbar fixed-sidebar');
            // $('body').addClass('mini-navbar fixed-sidebar');
        });
    }else{
        console.log('going inside else ppppppppppppppppppppppppppp' + window.location.href);

        $(document).ready(function () {
            $('body').removeClass('mini-navbar fixed-sidebar').addClass('fixed-sidebar');

        });
    }
}
logOutPageChange();

function closeSidenav() {
    $(document).ready(function () {
        $('body').removeClass('ng-scope mini-navbar pace-done fixed-sidebar').addClass('mini-navbar');
        // $('body').addClass('mini-navbar fixed-sidebar');
    });
}





(function () {
    angular.module('wealthQuotientApp', [
        'ui.router',                    // Routing
        'ui.bootstrap',               // Bootstrap
        'ngCookies',
        'xeditable',
        'treasure-overlay-spinner',
        'xeditable',
        'bootstrap3-typeahead',
        'bw.paging',
        'ngFileUpload'
    ])

        .run(['$window', '$timeout', '$location', '$rootScope', '$cookies', '$state', 'myService', '$http', 'editableOptions', 'manageUsersService', function ($window, $timeout, $location, $rootScope, $cookies, $state, myService, $http, editableOptions, manageUsersService) {

            // $trace.enable('TRANSITION'); //"angular-ui-router": "^1.0.3",

            // $transitions.onStart({ to: '' }, function(trans) {
            //     console.log('statechange detected');
            //     var auth = trans.injector().get('AuthService');
            //     if (!auth.isAuthenticated()) {
            //         // User isn't authenticated. Redirect to a new Target State
            //         return trans.router.stateService.target('login');
            //     }
            // });


            editableOptions.theme = 'default'; // bootstrap3 theme. Can be also 'bs2', 'default'
            $rootScope.Object = Object;
            $rootScope.loginVar = false;

            //>>>>>>SUCCESS AND ERROR MESSAGES<<<<<<<<//
            $rootScope.successToast = function (msg) {
                console.log('msg', msg);
                var $toastContent = $('<span style="color: white;">' + msg + '</span>');
                Materialize.toast($toastContent, 3000, 'green lighten-1 rounded');
            }

            // $rootScope.errorToast = function (msg) {
            //     var $toastContent = $('<span style="color: white;">' + msg + '</span>');
            //     Materialize.toast($toastContent, 3000, 'red lighten-1 rounded');
            // }
            $rootScope.infoToast = function (msg) {
                var $toastContent = $('<span style="color: white;">' + msg + '</span>');
                Materialize.toast($toastContent, 3000, 'orange lighten-1 rounded');
            }
            //---------------------------------------------//

            $rootScope.partnerURLLogo = false;


            $rootScope.online = navigator.onLine;
            $window.addEventListener("offline", function () {
                $rootScope.$apply(function () {
                    $('#checkInternet').modal('open');
                    console.log($('#checkInternet'))
                });
            }, false);

            $window.addEventListener("online", function () {
                $rootScope.$apply(function () {
                    $rootScope.online = true;
                    $('#checkInternet').modal('close');
                });
            }, false);


            // $rootScope.partnerName;

            $rootScope.getCookie = function (cname) {
                var name = cname + "=";
                var decodedCookie = decodeURIComponent(document.cookie);
                var ca = decodedCookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ') {
                        c = c.substring(1);
                    }
                    if (c.indexOf(name) == 0) {
                        return c.substring(name.length, c.length);
                    }
                }
                return "";
            }


            $rootScope.getPartnerDetails = function () {

                console.log('now checking the value', document.cookie);
                myService.setHeaders();

                return $http.post(baseUrl + 'user/getPartnerDetails').then(function (success) {
                    console.log('checking partner token value output', JSON.stringify(success));
                    if (success.data.status && $cookies.get('Authorization')) {
                        console.log('partner token IS VERIFIED!!!!');
                        // $rootScope.loginVar = true;
                        // $rootScope.sideBar = true;
                        // console.log('Logged in..Sidenav enabled !!!!');
                        $rootScope.partnerName = success.data.response[0][0].partnername;
                        $rootScope.logoName = $rootScope.partnerName;
                        $rootScope.partnerId = success.data.response[0][0].partner_id;
                        $rootScope.email = success.data.response[0][0].email;
                        console.log(success.data.response)
                        if (!success.data.response[1].length) {
                            $rootScope.alertDue = false;
                            $rootScope.balance = 0;
                            $cookies.put('balance', 0)
                        }
                        else if (success.data.response[1].length && success.data.response[1][0].balance < 0) {
                            $rootScope.alertDue = true;
                            $rootScope.balance = 0;
                            $cookies.put('balance', 0)
                        } else if (success.data.response[1].length && success.data.response[1][0].balance >= 0) {
                            $rootScope.balance = success.data.response[1][0].balance;
                            $cookies.put('balance', success.data.response[1][0].balance)
                            $rootScope.alertDue = false;
                        }

                        $rootScope.$broadcast('sub-is-logged');
                        $timeout(function () {
                            imageDefault()
                        })
                        console.log('partner name and id from root scope post logging in', $rootScope.partnerName, $rootScope.partnerId);

                    }
                    else {
                        // $rootScope.loginVar = false;
                        // $rootScope.sideBar = false;
                        console.log('logout called...');
                        myService.logout();
                        // $location.path("/register");
                        // $state.go('register');
                    }
                    // return success;
                }).catch(function (error) {
                    console.log(error);
                    // $rootScope.loginVar = false;
                    // $rootScope.sideBar = false;
                    console.log('logout called...');
                    myService.logout();
                    // return error;
                });

            }


            function imageDefault() {
                console.log('$rootScope.logoName---11111------',$rootScope.logoName)
                checkImage( "../../../img/"+$rootScope.logoName+"Logo.jpg", function(result){
                        console.log('---------result',result)
                        $timeout(function () {
                            $rootScope.defaultImageProfile = "../../../img/"+$rootScope.logoName+"Logo.jpg"
                            console.log('$scope.defaultImageProfile------------ccccccc-1111111111-------------------',$rootScope.defaultImageProfile)

                        })

                    },
                    function(error){
                        console.log('---------error',error)
                        $timeout(function () {
                            $rootScope.defaultImageProfile = "../../../img/avatarDefault.jpg"
                            console.log('---------$scope.defaultImageProfile bbbbbbbbb22222222222',$rootScope.defaultImageProfile)
                        })

                    } );
            }

            function checkImage (src, good, bad) {
                var img = new Image();
                img.onload = good;
                img.onerror = bad;
                img.src = src;
            }


            $rootScope.getSubUserDetails = function () {

                console.log('now checking the value');

                myService.setHeaders();

                return $http.post(baseUrl + 'manageUsers/getSubUserDetails').then(function (success) {
                    console.log('checking sub user token value output', success);
                    if (success.data.status && $cookies.get('Authorization')) {
                        console.log('sub user token IS VERIFIED!!!!');
                        // $rootScope.loginVar = true;
                        // $rootScope.sideBar = true;
                        // console.log('Logged in..Sidenav enabled !!!!');
                        $rootScope.subUserName = success.data.response[0][0].username;
                        $rootScope.logoName = $rootScope.subUserName;
                        $rootScope.partnerName = success.data.response[0][0].partnername;
                        $rootScope.subUserId = success.data.response[0][0].id;
                        $rootScope.partnerId = success.data.response[0][0].partner_id;
                        $rootScope.routeInfo = success.data.response[0][0];
                        manageUsersService.store(success.data.response[0][0]);
                        $rootScope.$broadcast('sub-is-logged');
                        console.log('sub user name and id from root scope post logging in', $rootScope.partnerName, $rootScope.subUserId);
                    }
                    else {
                        // $rootScope.loginVar = false
                        // $rootScope.sideBar = false;
                        console.log('logout called...');
                        myService.logout();
                        // $location.path("/register");
                        // $state.go('register');
                    }
                    // return success;
                }).catch(function (error) {
                    console.log(error);
                    // $rootScope.loginVar = false;
                    // $rootScope.sideBar = false;
                    console.log('logout called...');
                    myService.logout();
                    // return error;
                });

            }

            $rootScope.windows = {};

            $rootScope.openWindow = function (url, name, features) {
                $rootScope.windows[name] = window.open(url, name, features);
                console.log('window ---', $rootScope.windows);
                return $rootScope.windows[name];
            }

            $rootScope.findWindow = function (name) {
                console.log('windows name stored', $rootScope.windows);
                return $rootScope.windows[name];
            }

            $rootScope.closeWindow = function (name) {
                var window = $rootScope.windows[name];
                if (window) {
                    window.close();
                    delete $rootScope.windows[name];
                }
            }

            $rootScope.openWM = function (URL) {

                var checkWindow = $rootScope.findWindow('wm_Window');
                console.log('window check', checkWindow);

                if (checkWindow == undefined) {
                    // alert('no existing window found!!!');
                    $rootScope.openWindow(URL, 'wm_Window', '');
                    console.log('windows local array', $rootScope.windows);
                }
                else {
                    // alert('existing window found!!!!!');
                    $rootScope.closeWindow('wm_Window');
                    console.log('window array', $rootScope.windows);
                    $rootScope.openWindow(URL, 'wm_Window', '');
                }
            };

            $rootScope.$safeApply = function ($scope, fn) {
                fn = fn || function () {
                };
                if ($scope.$$phase) {
                    //don't worry, the value gets set and AngularJS picks up on it...
                    console.log('$$phase in digestion, DOM will be updated');
                    fn();
                }
                else {
                    //this will fire to tell angularjs to notice that a change has happened
                    console.log('$apply being called to update DOM');
                    //if it is outside of it's own behaviour...
                    $scope.$apply(fn);
                }
            };


            $rootScope.$on('$stateChangeStart', function (event, toState) {

                console.log('statechange detected 3333333333333333333333');
                if (toState.name != 'contactManagement_interaction') {
                    if ($rootScope.socket && $rootScope.interaction) {
                        var payload = $rootScope.interaction;
                        $rootScope.socket.emit('leave', payload)
                        $rootScope.interaction = ''
                    }
                }

                $rootScope.bg = false;

                var Role = $cookies.get('Role');


                if (Role == 'sub_user') {
                    $rootScope.subUserCookie = $cookies.get('Authorization');
                    console.log('statechange detected 1111111111111111111111111' + $rootScope.partnerCookie);

                    console.log('subUser Cookie set')
                }
                else if (Role == 'partner') {
                    $rootScope.partnerCookie = $cookies.get('Authorization');
                    console.log('statechange detected 2222222222222222222222' + $rootScope.partnerCookie);

                } else if (Role == 'admin') {
                    console.log('statechange detected 333333333333333333333333333333' + $rootScope.partnerCookie);

                    $rootScope.adminCookie = $cookies.get('Authorization');
                }


                if ($rootScope.partnerCookie != undefined) {
                    console.log('authorization cookie has value..', $cookies.get('Authorization'));

                    $rootScope.loginVar = true;
                    $rootScope.sideBar = true;
                    $rootScope.head = true;
                    console.log('Logged in..Sidenav enabled !!!! 1111111111111');
                    $rootScope.getPartnerDetails();
                    // $state.go('dashboard');


                } else if ($rootScope.subUserCookie != undefined) {

                    console.log('authorization cookie has value..', $cookies.get('Authorization'));

                    $rootScope.loginVar = true;
                    $rootScope.sideBar = true;
                    $rootScope.head = true;
                    console.log('Logged in..Sidenav enabled !!!! 2222222222222222222');
                    $rootScope.getSubUserDetails();
                }
                else if ($rootScope.adminCookie != undefined) {

                    if (toState.name == 'admin' || toState.name == 'adminLogin' || toState.name.includes('admin')) {
                        $rootScope.loginVar = false;
                        $rootScope.sideBar = false;
                        $rootScope.head = false;
                        myService.setHeaders();

                    } else {
                        $rootScope.loginVar = false;
                        $rootScope.sideBar = false;
                        $rootScope.head = false;
                        $timeout(function () {
                            $state.go('adminLogin');
                        });
                    }


                }
                else {

                    if (toState.name == 'subuserSignup' || toState.name == 'adminLogin' || toState.name == 'admin' || toState.name.includes('admin')) {
                        $rootScope.loginVar = false;
                        $rootScope.sideBar = false;
                        $rootScope.head = false;
                    } else {
                        console.log('no authorization cookie present...partnerName --->', $rootScope.partnerName);
                        $rootScope.loginVar = false;
                        $rootScope.sideBar = false;
                        $rootScope.head = true;
                        myService.logout();
                        $location.path("/home");
                        // $state.go('register');
                    }


                }

            });

            $rootScope.$on('$stateChangeSuccess', function (event, toState) {
                document.title = 'WealthQuotient - ' + toState.title;
                console.log('here');
            });

            $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {

                console.log(error);
                if (toState.name == 'subuserSignup') {
                    $state.go('dashboard');
                } else if (toState.name == 'admin' || toState.name.includes('admin')) {
                    if (fromState.name !== "") {
                        $state.go(fromState.name)
                    } else {
                        $state.go('dashboard')
                    }
                }
            });

        }])


})();