/**
 * Created by arif on 9/20/17.
 */
'use strict';

angular.module('wealthQuotientApp')
    .factory('misService', function ($http, myService, Upload) {
        "ngInject";
        var misReportByFundHouse = function (data) {
            console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/getAllUsersOfPartner', data).then(function(success) {
               // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var aumDataByUnderFromExel = function () {
           // console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/aumDataByUnderFromExel',{}).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getDataForFundHouseForTable = function (data) {
            // console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/getDataForFundHouseForTable',data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAumReportAssetOutsideTable = function (data) {
            // console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/getAumReportAssetOutsideTable',data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAssetEquityWise = function (data) {
            // console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/getAssetEquityWise',data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAumReportBySchemeoutsideTable = function (data) {
             console.log('9999999999 table=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/getAumReportBySchemeoutsideTable',data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var aumDataBySchemeUnderFromExel = function () {
            // console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/aumDataBySchemeUnderFromExel',{}).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var aumSchemeDatInsideForTabledata = function () {
            // console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/aumSchemeDatInsideForTable',{}).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var aumDataBySchemeUnderFromExelTable = function () {
            // console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/aumDataByFundHouseUnderFromExelTable',{}).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var aumDataByAssetAllocationFromExel = function () {
            // console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/aumDataByAssetAllocationFromExel',{}).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var misReportByAssetAllocation = function (data) {
            //console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/getAumReportByAssetAllocation', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var misReportBySceme = function (data) {
            //console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/getAumReportByScheme', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var getDataForDirectEquity = function (data) {
            //console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/getDataForDirectEquity', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var getDataForDirectEquityUser = function (data) {
            //console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/getDataForDirectEquityUser', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getDataForDirectEquityUserTran = function (data) {
            //console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/getDataForDirectEquityUserTran', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getEquityMFClasfn = function (data) {
            //console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/getEquityMFClasfn', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getEquityMFFundHouse = function (data) {
            //console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/getEquityMFFundHouse', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getEquityMFScheme = function (data) {
            //console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/getEquityMFScheme', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getEquityMFUsers = function (data) {
            //console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/getEquityMFUsers', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getEquityMFTrans = function (data) {
           // console.log('data from controller=======',JSON.stringify(data));
            return $http.post(baseUrl+'mis/getEquityMFTrans', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };


        var uploadValidatedFile = function (data) {
            //console.log('data from uploadValidatedFile service');
            return $http.post(baseUrl+'mis/uploadValidatedFile', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };


        var getAssetCash = function (data) {
            //console.log('data from uploadValidatedFile service');
            return $http.post(baseUrl+'mis/getAssetCash', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCashInHandUsers = function (data) {
            //console.log('data from getCashInHandUsers');
            return $http.post(baseUrl+'mis/getCashInHandUsers', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCashInHandUsersTran = function (data) {
           // console.log('data from getCashInHandUsersTran');
            return $http.post(baseUrl+'mis/getCashInHandUsersTran', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCashAtBank = function (data) {
           // console.log('data from getCashAtBankData');
            return $http.post(baseUrl+'mis/getCashAtBankData', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCashAtBankUsers = function (data) {
           // console.log('data from getCashAtBankAllUsers');
            return $http.post(baseUrl+'mis/getCashAtBankAllUsers', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCashAtBankAllUserTran = function (data) {
            //console.log('data from getCashAtBankAllUsers');
            return $http.post(baseUrl+'mis/getCashAtBankAllUserTran', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAssetGold = function (data) {
            //console.log('data from getAssetGold');
            return $http.post(baseUrl+'mis/getAssetGold', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllGoldUsers = function (data) {
            //console.log('data from getAssetGold');
            return $http.post(baseUrl+'mis/getAllGoldUsers', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getGoldCoinUsersTran = function (data) {
            //console.log('data from getGoldCoinUsersTran');
            return $http.post(baseUrl+'mis/getGoldCoinUsersTran', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllGoldJwelleryUsers = function (data) {
           // console.log('data from getAllGoldJwelleryUsers');
            return $http.post(baseUrl+'mis/getAllGoldJwelleryUsers', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getGoldJwelleryUsersTran = function (data) {
            //console.log('data from getGoldJwelleryUsersTran');
            return $http.post(baseUrl+'mis/getGoldJwelleryUsersTran', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllReal_estateUsers = function (data) {
            //console.log('data from getAllReal_estateUsers');
            return $http.post(baseUrl+'mis/getAllReal_estateUsers', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getrealestateUsersTran = function (data) {
            //console.log('data from getrealestateUsersTran');
            return $http.post(baseUrl+'mis/getrealestateUsersTran', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAssetOthers = function (data) {
           // console.log('data from getAssetOthers');
            return $http.post(baseUrl+'mis/getAssetOthers', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var getAssetOthersUsers = function (data) {
            //console.log('data from getAssetOthersUsers');
            return $http.post(baseUrl+'mis/getAssetOthersUsers', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getOtherTypesUsersTran = function (data) {
           // console.log('data from getOtherTypesUsersTran');
            return $http.post(baseUrl+'mis/getOtherTypesUsersTran', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getDebtLevels = function (data) {
           // console.log('data from getDebtLevels');
            return $http.post(baseUrl+'mis/getDebtLevels', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getBondUsers = function (data) {
            //console.log('data from getBondUsers');
            return $http.post(baseUrl+'mis/getBondUsers', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getBondUsersTran = function (data) {
            //console.log('data from getBondUsersTran');
            return $http.post(baseUrl+'mis/getBondUsersTran', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getRDDataAtBank = function (data) {
            //console.log('data from getRDDataAtBank');
            return $http.post(baseUrl+'mis/getRDDataAtBank', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getRdAtBanksUsers = function (data) {
            //console.log('data from getRdAtBanksUsers');
            return $http.post(baseUrl+'mis/getRdAtBanksUsers', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getRdUsersTran = function (data) {
            //console.log('data from getRdUsersTran');
            return $http.post(baseUrl+'mis/getRdUsersTran', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var getmoneyBankUsers = function (data) {
           // console.log('data from getmoneyBankUsers');
            return $http.post(baseUrl+'mis/getmoneyBankUsers', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getMoneybackUsersTran = function (data) {
            //console.log('data from getMoneybackUsersTran');
            return $http.post(baseUrl+'mis/getMoneybackUsersTran', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getfixeddepositsAtBank = function (data) {
            //console.log('data from getfixeddepositsAtBank');
            return $http.post(baseUrl+'mis/getfixeddepositsAtBank', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getfixeddipositsBanksUsers = function (data) {
           // console.log('data from getfixeddipositsBanksUsers');
            return $http.post(baseUrl+'mis/getfixeddipositsBanksUsers', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var getFdUsersTran = function (data) {
           // console.log('data from getFdUsersTran');
            return $http.post(baseUrl+'mis/getFdUsersTran', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getDebtMFClasfn = function (data) {
          //  console.log('data from getDebtMFClasfn');
            return $http.post(baseUrl+'mis/getDebtMFClasfn', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getDebtMFSchemes = function (data) {
           // console.log('data from getDebtMFSchemes');
            return $http.post(baseUrl+'mis/getDebtMFSchemes', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getDebtMFAllUsers = function (data) {
          //  console.log('data from getDebtMFAllUsers');
            return $http.post(baseUrl+'mis/getDebtMFAllUsers', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getDebMftUsersTran = function (data) {
           // console.log('data from getDebMftUsersTran');
            return $http.post(baseUrl+'mis/getDebMftUsersTran', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var misUploadMBR2 = function (file) {
            return Upload.upload({
                url: `${baseUrl}mis/misUploadFile`,
                data: {file: file}
            });
        };
        var getHybridMfunds = function (data) {
           // console.log('data from getHybridMfunds');
            return $http.post(baseUrl+'mis/getHybridMfunds', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getHybridAllUsers = function (data) {
          //  console.log('data from getHybridAllUsers');
            return $http.post(baseUrl+'mis/getHybridAllUsers', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getHybridMftUsersTran = function (data) {
           // console.log('data from getHybridMftUsersTran');
            return $http.post(baseUrl+'mis/getHybridMftUsersTran', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var misUploadMBR49 = function(file){
                return Upload.upload({
                    url: `${baseUrl}sip/misMBR49Upload`,
                    data: {file : file}
                });
        };

        var getAumReportAssetUnderTable = function (data) {
            // console.log('data from getHybridMftUsersTran');
            return $http.post(baseUrl+'mis/getAumReportAssetUnderTable', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getDataUnderAssetWise = function (data) {
            // console.log('data from getHybridMftUsersTran');
            return $http.post(baseUrl+'mis/getDataUnderAssetWise', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getDataUnderSchemes = function (data) {
            // console.log('data from getHybridMftUsersTran');
            return $http.post(baseUrl+'mis/getDataUnderSchemes', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getDataUnderEquityUsers = function (data) {
            // console.log('data from getHybridMftUsersTran');
            return $http.post(baseUrl+'mis/getDataUnderEquityUsers', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getUnderAllAssetUserTran = function (data) {
            // console.log('data from getHybridMftUsersTran');
            return $http.post(baseUrl+'mis/getUnderAllAssetUserTran', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };






        var getSipRejection = function(data) {
            return $http.get(`${baseUrl}sip/sipRejections`,
            {
                params: data
            });
        };

        var getSipCount = function(){
            return $http.get(`${baseUrl}sip/sipcount`);
        }
        
        var getSipRenewals = function(){
            return $http.get(`${baseUrl}sip/sipRenewals`);
        }

        var getAmcSchemes = function(data){
            return $http.post(`${baseUrl}sip/sipAumSchemeInside`, data);
        }

        var getAmcSchemeOutside = function(data){
            return $http.post(`${baseUrl}sip/sipAumSchemeOutSide`, data);
        }
        
        var getClientInside = function(data){
            return $http.post(`${baseUrl}sip/sipClientInside`, data);
        }

        var getClientOutside = function(data){
            return $http.post(`${baseUrl}sip/sipClientOutside`, data);
        }
        
        var getSipExpire = function(data) {
            return $http.get(`${baseUrl}sip/expire`, { params: data });
        }

        var getRegistration = function(data) {
            return $http.get(`${baseUrl}sip/registrations`, { params: data });
        }

        var getSipClosed = function(data) {
            return $http.get(`${baseUrl}sip/sipClosed`, { params: data });
        }

        return{
            misReportByFundHouse: misReportByFundHouse,
            misReportByAssetAllocation:misReportByAssetAllocation,
            misReportBySceme:misReportBySceme,
            uploadValidatedFile:uploadValidatedFile,
            aumDataByUnderFromExel:aumDataByUnderFromExel,
            aumDataBySchemeUnderFromExel:aumDataBySchemeUnderFromExel,
            aumDataByAssetAllocationFromExel:aumDataByAssetAllocationFromExel,
            getDataForFundHouseForTable:getDataForFundHouseForTable,
            aumDataBySchemeUnderFromExelTable:aumDataBySchemeUnderFromExelTable,
            getAumReportBySchemeoutsideTable:getAumReportBySchemeoutsideTable,
            aumSchemeDatInsideForTabledata:aumSchemeDatInsideForTabledata,
            getAumReportAssetOutsideTable:getAumReportAssetOutsideTable,
            getAssetEquityWise:getAssetEquityWise,
            getDataForDirectEquity:getDataForDirectEquity,
            getDataForDirectEquityUser:getDataForDirectEquityUser,
            getDataForDirectEquityUserTran:getDataForDirectEquityUserTran,
            getEquityMFClasfn:getEquityMFClasfn,
            getEquityMFFundHouse:getEquityMFFundHouse,
            getEquityMFScheme:getEquityMFScheme,
            getEquityMFUsers:getEquityMFUsers,
            getEquityMFTrans:getEquityMFTrans,
            getAssetCash:getAssetCash,
            getCashInHandUsers:getCashInHandUsers,
            getCashInHandUsersTran:getCashInHandUsersTran,
            getCashAtBank:getCashAtBank,
            getCashAtBankUsers:getCashAtBankUsers,
            getCashAtBankAllUserTran:getCashAtBankAllUserTran,
            getAssetGold:getAssetGold,
            getAllGoldUsers:getAllGoldUsers,
            getGoldCoinUsersTran:getGoldCoinUsersTran,
            getAllGoldJwelleryUsers:getAllGoldJwelleryUsers,
            getGoldJwelleryUsersTran:getGoldJwelleryUsersTran,
            getAllReal_estateUsers:getAllReal_estateUsers,
            getrealestateUsersTran:getrealestateUsersTran,
            getAssetOthers:getAssetOthers,
            getAssetOthersUsers:getAssetOthersUsers,
            getOtherTypesUsersTran:getOtherTypesUsersTran,
            getDebtLevels:getDebtLevels,
            getBondUsers:getBondUsers,
            getBondUsersTran:getBondUsersTran,
            getRDDataAtBank:getRDDataAtBank,
            getRdAtBanksUsers:getRdAtBanksUsers,
            getRdUsersTran:getRdUsersTran,
            getmoneyBankUsers:getmoneyBankUsers,
            getMoneybackUsersTran:getMoneybackUsersTran,
            getfixeddepositsAtBank:getfixeddepositsAtBank,
            getfixeddipositsBanksUsers:getfixeddipositsBanksUsers,
            getFdUsersTran:getFdUsersTran,
            getDebtMFClasfn:getDebtMFClasfn,
            getDebtMFSchemes:getDebtMFSchemes,
            getDebtMFAllUsers:getDebtMFAllUsers,
            getDebMftUsersTran:getDebMftUsersTran,
            misUploadMBR2: misUploadMBR2,
            misUploadMBR49: misUploadMBR49,
            getHybridMfunds:getHybridMfunds,
            getHybridAllUsers:getHybridAllUsers,
            getHybridMftUsersTran:getHybridMftUsersTran,
            getAumReportAssetUnderTable:getAumReportAssetUnderTable,
            getDataUnderAssetWise:getDataUnderAssetWise,
            getDataUnderSchemes:getDataUnderSchemes,
            getDataUnderEquityUsers:getDataUnderEquityUsers,
            getSipRejection: getSipRejection,
            getSipCount: getSipCount,
            getSipRenewals: getSipRenewals,
            getUnderAllAssetUserTran:getUnderAllAssetUserTran,
            getAmcSchemes: getAmcSchemes,
            getAmcSchemeOutside: getAmcSchemeOutside,
            getClientInside: getClientInside,
            getClientOutside: getClientOutside,
            getSipExpire: getSipExpire,
            getRegistration: getRegistration,
            getSipClosed: getSipClosed
        };
    });