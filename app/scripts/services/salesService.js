'use strict';

/**
 * @ngdoc service
 * @name wealthQuotientApp.mySalesService
 * @description
 * # mySalesService
 * Factory in the wealthQuotientApp.
 */
angular.module('wealthQuotientApp')
    .factory('mySalesService', ['$http', 'myService', '$cookies', function ($http, myService, $cookies) {

        var createProductCustomFields = function (data) {
            // console.log('data from addCustomFields service 777777777777777777777777777', JSON.stringify(data));
            return $http.post(baseUrl+'sales/addProductCustomFields', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var checkProductCFName = function (data) {
            // console.log('data from checkCFName service', data);
            return $http.post(baseUrl+'sales/checkProductUniqueCFName', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllProductCustomFields = function (customQry) {
            console.log('getCustomFields service'+  JSON.stringify(customQry));
            return $http.post(baseUrl+'sales/getAllProductCFs', customQry).then(function(success) {
                console.log('custom fields', success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var createProductDropDownCF = function (data) {
            console.log('data from createDropDownCF service 9999999999999999999999', JSON.stringify(data));
            return $http.post(baseUrl + 'sales/addProductDropDownCF', data).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var addProductDropDownCFItem = function (data) {
            // console.log('data from addDropDownCFItem service', data);
            return $http.post(baseUrl + 'sales/addProductDropDownCFItem', data).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getProductCFDropDownList = function (cfObj) {
            // console.log('data from getCFDropDownList service', cfObj);
            return $http.post(baseUrl + 'sales/cfProductDropDownList', cfObj).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var showORhideField = function (data) {
            // console.log('data from showORhideField service', data);
            return $http.post(baseUrl+'sales/updateFieldVisibility', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var createSalesClient = function (data) {
            // console.log('data from showORhideField service', JSON.stringify(data));
            return $http.post(baseUrl+'sales/createSalesClient', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var createAddhocSalesClient = function (data) {
            // console.log('data from showORhideField service', JSON.stringify(data));
            return $http.post(baseUrl+'sales/createAddhocSalesClient', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var deleteCf = function (data) {
            // console.log('data from showORhideField service', JSON.stringify(data));
            return $http.post(baseUrl+'sales/updateProductCfData', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllSalesClients = function (data) {
            // console.log('data from showORhideField service', JSON.stringify(data));
            return $http.post(baseUrl+'sales/getAllSaleClient', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var getPartnerUsers = function (data) {
            // console.log('data from showORhideField service', JSON.stringify(data));
            return $http.post(baseUrl+'sales/getPartnerUserNames', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getSubUsersData = function (data) {
            // console.log('data from showORhideField service', JSON.stringify(data));
            return $http.post(baseUrl+'sales/getClientFamilyMembersNames', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var updateSaleClient = function (data) {
            // console.log('data from showORhideField service', JSON.stringify(data));
            return $http.post(baseUrl+'sales/updateSaleClient', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var getPSData = function (data) {
            // console.log('data from showORhideField service', JSON.stringify(data));
            return $http.post(baseUrl+'sales/getPSName', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var getOppDataOfClient = function (data) {
            // console.log('data from showORhideField service', JSON.stringify(data));
            return $http.post(baseUrl+'sales/getPSDataAndUserCFData', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var salePSNames = function (data) {
            // console.log('data from showORhideField service', JSON.stringify(data));
            return $http.post(baseUrl+'sales/getSalePSName', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var saleSubUser = function (data) {
            // console.log('data from showORhideField service', JSON.stringify(data));
            return $http.post(baseUrl+'sales/getSaleSubName', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getTotalValues = function () {
            // console.log('data from showORhideField service', JSON.stringify(data));
            return $http.post(baseUrl+'sales/totalSaleData').then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllSubUsersOfPartner = function () {
            // console.log('data from showORhideField service', JSON.stringify(data));
            return $http.post(baseUrl+'sales/getAllSubUserOfPartner').then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        return {
            createProductCustomFields: createProductCustomFields,
            checkProductCFName : checkProductCFName,
            getAllProductCustomFields: getAllProductCustomFields,
            createProductDropDownCF : createProductDropDownCF,
            addProductDropDownCFItem :addProductDropDownCFItem,
            getProductCFDropDownList : getProductCFDropDownList,
            showORhideField:showORhideField,
            createSalesClient:createSalesClient,
            deleteCf:deleteCf,
            getAllSalesClients : getAllSalesClients,
            getPartnerUsers:getPartnerUsers,
            updateSaleClient : updateSaleClient,
            getPSData : getPSData,
            getOppDataOfClient : getOppDataOfClient,
            createAddhocSalesClient: createAddhocSalesClient,
            salePSNames : salePSNames,
            saleSubUser : saleSubUser,
            getTotalValues : getTotalValues,
            getSubUsersData : getSubUsersData,
            getAllSubUsersOfPartner:getAllSubUsersOfPartner
        };
    }]);