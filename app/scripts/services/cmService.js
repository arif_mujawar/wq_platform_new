angular.module('wealthQuotientApp')
    .service('cmService', function($http){

    	this.getAllUsers = function (data){
        	return $http.post(baseUrl+'contactManagement/allUsers',data)
        }

        this.getGmail = function (){
        	return $http.get(baseUrl+'contactManagement/getGmail')
        }
        this.allMeeting = function () {
            return $http.get(baseUrl + 'contactManagement/allMeeting')
        }

        this.getSearch = function (data){
        	return $http.post(baseUrl+'contactManagement/getSearch',data)
        }
        this.getSearchInteraction = function (data) {
            return $http.post(baseUrl + 'contactManagement/getSearchInteraction', data)
        }
        this.deleteTemplate = function(data){
            return $http.post(baseUrl + 'contactManagement/deleteTemplate', data)
        }

        this.deleteCFforCategory = function(data){
            return $http.post(baseUrl +'contactManagement/deleteCFforCategory', data)
        }

        this.getAllContactsGoogle = function (data) {
            return $http.post(baseUrl + 'contactManagement/getAllContactsGoogle', data)
        }

        this.getAllInteractionHistoryAnalytics = function(data){
            return $http.post(baseUrl+'contactManagement/getAllInteractionHistoryAnalytics',data)
        }

        this.checkemailexists = function (data) {
            return $http.post(baseUrl + 'contactManagement/checkemailexists', data)
        }

        this.getAllInteractionGmailCached = function () {
            return $http.get(baseUrl + 'contactManagement/getAllInteractionGmailCached')
        }

        this.getAllInteractionCached = function () {
            return $http.get(baseUrl + 'contactManagement/getAllInteractionCached')
        }

        this.getAllInteractionGmailAnalytics = function(){
            return $http.get(baseUrl + 'contactManagement/getAllInteractionGmailAnalytics')
        }

        this.deleteCFforContacts = function (data) {
            return $http.post(baseUrl + 'contactManagement/deleteCFforContacts', data)
        }

        this.addCfforContacts = function(data){
            return $http.post(baseUrl + 'contactManagement/addCfforContacts',data)
        }

        this.getUsers = function (){
        	return $http.get(baseUrl+'contactManagement/getUsers')
        }

        this.checkLogin = function (){
        	return $http.get(baseUrl+'contactManagement/checkLogin')
        }

        this.addCFCategory = function(data){
            return $http.post(baseUrl +'contactManagement/addCFCategory',data)
        }

        this.allCategories = function (){
        	return $http.get(baseUrl+'contactManagement/allCategories')
        }

        this.checkCFnameValid = function (data){
        	return $http.post(baseUrl+'contactManagement/checkCFnameValid',data)
        }
        this.deleteCategory = function (data) {
            return $http.post(baseUrl + 'contactManagement/deleteCategory', data)
        }

        this.searchDate = function (data) {
            return $http.post(baseUrl + 'contactManagement/searchByDate', data)
        }

        this.searchMode = function (data) {
            return $http.post(baseUrl + 'contactManagement/searchByMode', data)
        }

        this.addTemplate = function (data){
        	return $http.post(baseUrl+'contactManagement/addTemplate',data)
        }

        this.addSignature = function (data) {
            return $http.post(baseUrl + 'contactManagement/addSignature', data)
        }
        this.editTemplate = function (data) {
            return $http.post(baseUrl + 'contactManagement/editTemplate', data)
        }
        this.getSignature = function (data) {
            return $http.post(baseUrl + 'contactManagement/getSignature', data)
        }
        this.getTemplates = function (){
        	return $http.get(baseUrl+'contactManagement/getTemplates')
        }
        this.addCategories = function (data){
        	return $http.post(baseUrl+'contactManagement/addCategories',data)
        }

        this.updateContactFrequency = function (data){
        	return $http.post(baseUrl+'contactManagement/updateContactFrequency',data)
        }

        this.updateLimits = function (data){
        	return $http.post(baseUrl+'contactManagement/updateLimits',data)
        }

        this.addContact = function (data){
        	return $http.post(baseUrl+'contactManagement/addNewContact',data)
        }

        this.getLastDateByMail = function (data){
        	return $http.post(baseUrl+'contactManagement/getLastDateByMail',data)
        }

        this.saveHistory = function(data){
            return $http.post(baseUrl+'contactManagement/addInteraction',data);
        }

        this.getInteractions = function(data){
            return $http.post(baseUrl+'contactManagement/getInteractions',data);
        }

        this.getAllInteractions = function(data){
            return $http.post(baseUrl+'contactManagement/getAllInteractions',data);
        }

        this.getContactFrequencyAnalytics = function(data){
            return $http.post(baseUrl+'contactManagement/getContactFrequencyAnalytics',data);
        }

        this.getAllHistory = function(data){
            return $http.post(baseUrl+"contactManagement/getAllHistory",data);
        }

    })