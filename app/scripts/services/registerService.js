'use strict';

/**
 * @ngdoc service
 * @name wealthQuotientApp.registerService
 * @description
 * # registerService
 * Factory in the wealthQuotientApp.
 */
angular.module('wealthQuotientApp')
  .factory('registerService', function ($http) {
      var userData = '';
    /*
     |-------------------------------------------------------------------------------|
     |    Registration                                                               |
     |-------------------------------------------------------------------------------|
     */


    var registerUser = function (registrationData) {
      return $http.post(baseUrl+'user/register', registrationData).then(function(success) {
        console.log(success);
          if(success.status === 200 && success.data.status){
              console.log('Mail has to be sent to user');
              var data = {
                  'first_name' : userData.fname,
                  'last_name' : userData.lname,
                  'username' : userData.username,
                  'mobile' : userData.mnumber,
                  'account_type' : 'Starter',
                  'email' : userData.emailId
              };
              // $http.post('https://mail.wealthquotient.in/register',data);
          }
        return success;
      }).catch(function (error) {
        console.log(error);
        return error;
      });

      // console.log(registrationData);
    };

    // var loginAfterRegister = function (userData) {
    //   console.log(userData);
    //   return $http.post(baseUrl+'/user/login', userData).then(function(success) {
    //     console.log(success);
    //     return success;
    //   }).catch(function (error) {
    //     console.log(error);
    //     return error;
    //   });
    // };
    //
    //
    /*
     |-------------------------------------------------------------------------------|
     |    Uniqueness Checks in registration page                                     |
     |-------------------------------------------------------------------------------|
     */

    var uniqueUsername = function (username) {
      return $http.post(baseUrl+'validate/validateUsername', username).then(function(success) {
        console.log(success);
        return success;
      }).catch(function (error) {
        console.log(error);
        return error;
      });
    };
    
    //
    // var uniqueEmail = function (emailId) {
    //   return $http.post(baseUrl+'validate/validateEmail', emailId).then(function(success) {
    //     console.log(success);
    //     return success;
    //   }).catch(function (error) {
    //     console.log(error);
    //     return error;
    //   });
    // };
    //
    // var uniqueMnumber = function (mnumber) {
    //   return $http.post(baseUrl+'validate/validateMnumber', mnumber).then(function(success) {
    //     console.log(success);
    //     return success;
    //   }).catch(function (error) {
    //     console.log(error);
    //     return error;
    //   });
    // };
    //
    // var verifyCaptcha = function (token) {
    //   return $http.post(baseUrl+'user/checkgCaptcha',{'token' : token} ).then(function(success) {
    //     console.log(success);
    //     return success;
    //   }).catch(function (error) {
    //     console.log(error);
    //     return error;
    //   });
    // };

      /*
       |-------------------------------------------------------------------------------|
       |    Send OTP service                                     |
       |-------------------------------------------------------------------------------|
       */
      var sendOTPtoUser = function (query) {
          return $http.post(baseUrl+'user/sendOTP',query).then(function(success) {
              return success;
          }).catch(function (error) {
              return error;
          });
      };

      /*
       |-------------------------------------------------------------------------------|
       |    Verify OTP service                                     |
       |-------------------------------------------------------------------------------|
       */
      var verifyOTPofUser = function (otpData) {
          return $http.post(baseUrl+'user/verifyOTP',otpData).then(function(success) {
              return success;
          }).catch(function (error) {
              return error;
          });
      };

    return{
      register: registerUser,
      // registerLogin: loginAfterRegister,
      checkUsername: uniqueUsername,
      sendOTPtoUser : sendOTPtoUser,
      verifyOTPofUser : verifyOTPofUser
    };
  });
