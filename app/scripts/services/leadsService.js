'use strict';

/**
 * @ngdoc service
 * @name wealthQuotientApp.leadsService
 * @description
 * # leadsService
 * leadsService Factory in the wealthQuotientApp.
 */
angular.module('wealthQuotientApp')
    .factory('leadsService', ['$http', function ($http) {

        var createLeadsFlow = function (data) {
            console.log('data from controller', data);
            return $http.post(baseUrl + 'leads/addLeadsFlow', data).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var deleteLeadsFlow = function (data) {
            console.log('data from controller', data);
            return $http.post(baseUrl + 'leads/deleteLFStatus', data).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        }

        var getLeadsFlow = function () {
            console.log('getLeadsFlow service');
            return $http.post(baseUrl + 'leads/getAllLeadsStatus').then(function (success) {
                console.log('leads flow', success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var addPartnerLead = function (data) {
            console.log('data from controller');
            return $http.post(baseUrl + 'leads/createLead', data).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllLeads = function (pageNumber) {
            console.log('data from controller');
            return $http.post(baseUrl + 'leads/getAllLeads?pageNumber='+pageNumber,{}).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var deleteLeadData = function (data) {
            console.log('data from controller');
            return $http.post(baseUrl + 'leads/deleteLead', data).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var deleteAllLeadsCFData = function (data) {
            console.log('data from controller');
            return $http.post(baseUrl + 'leads/deleteLeadCFData', data).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var updateLead = function (leadData) {
            console.log('updateLead service');
            return $http.post(baseUrl + 'leads/updateLead', leadData).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        }

        var convertLead = function (convertLeadData) {
            console.log('convertLead service');
            return $http.post(baseUrl + 'leads/setConvertLeadStatus', convertLeadData).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var convertedLeadsCount = function () {
            console.log('convertedLeadsCount service');
            return $http.post(baseUrl + 'leads/getConvertedLeadsCount',{}).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getConvertedLeads = function (pgNo) {
            console.log('getConvertedLeads service');
            return $http.post(baseUrl + 'leads/getAllConvertedLeads?pageNumber='+pgNo,{}).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getActiveCustomFields = function () {
            console.log('getCustomFields service');
            return $http.post(baseUrl + 'leads/getActiveCustomFields').then(function (success) {
                console.log('custom fields', success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllCustomFields = function () {
            console.log('getCustomFields service');
            return $http.post(baseUrl + 'leads/getAllCustomFields').then(function (success) {
                console.log('custom fields', success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var createCustomFields = function (data) {
            console.log('data from addCustomFields service', data);
            return $http.post(baseUrl + 'leads/addCustomFields', data).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var deleteCustomFields = function (data) {
            console.log('data from deleteCustomFields service', data);
            return $http.post(baseUrl + 'leads/deleteCustomFields', data).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var changeCFStatus = function (data) {
            console.log('data from changeCFStatus service', data);
            return $http.post(baseUrl + 'leads/updateCFStatus', data).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var searchLead = function (searchData) {

            console.log('data from searchLead service', searchData);
            return $http.post(baseUrl + 'leads/searchLead', searchData).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getSearchedLeadsData = function (searchResult, pageNumber) {

            console.log('data from getSearchedLeadsData service', searchResult, pageNumber);
            return $http.post(baseUrl + 'leads/getSearchedLeadsData?pageNumber='+pageNumber, searchResult).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var searchConvertedLead = function (searchData, pgNo) {

            console.log('data from searchConvertedLead service', searchData, pgNo);
            return $http.post(baseUrl + 'leads/searchConvertedLead?pageNumber='+pgNo, searchData).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var createNormalFields = function () {
            console.log('data from createNormalFields service');
            return $http.post(baseUrl + 'leads/createNF').then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllFields = function () {
            console.log('data from getAllFields service');
            return $http.post(baseUrl + 'leads/getAllFields').then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var reorderFields = function (allFields) {
            console.log('data from reorderFields service');
            return $http.post(baseUrl + 'leads/sortFields', allFields).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var showORhideField = function (data) {
            console.log('data from showORhideField service', data);
            return $http.post(baseUrl + 'leads/updateFieldVisibility', data).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var createDropDownCF = function (data) {
            console.log('data from createDropDownCF service', data);
            return $http.post(baseUrl + 'leads/addDropDownCF', data).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var addDropDownCFItem = function (data) {
            console.log('data from addDropDownCFItem service', data);
            return $http.post(baseUrl + 'leads/addDropDownCFItem', data).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCFDropDownList = function (cfObj) {
            console.log('data from getCFDropDownList service', cfObj);
            return $http.post(baseUrl + 'leads/cfDropDownList', cfObj).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var delCFDropDownItem = function (itemObj) {
            console.log('data from delCFDropDownItem service', itemObj);
            return $http.post(baseUrl + 'leads/deleteCFDropDownItem', itemObj).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var updateCFDropDownItem = function (itemObj) {
            console.log('data from updateCFDropDownItem service', itemObj);
            return $http.post(baseUrl + 'leads/updateCFDropDownItem', itemObj).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var validateLeadsSheet = function (fileObj, activeFieldsArr) {
            console.log('data from validateLeadsSheet service', fileObj, activeFieldsArr);

            var url = baseUrl+'leads/validateLeadsSheet?compare='+activeFieldsArr;

            console.log('URL created for validation', url);

            return $http({
                method: 'POST',
                url: url,
                headers: {'Content-Type': undefined},
                data: fileObj
            }).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var uploadValidatedFile = function (cfArr) {
            console.log('data from uploadValidatedFile service');
            return $http.post(baseUrl + 'leads/uploadLeadsSheet', cfArr).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var checkCFName = function (data) {
            console.log('data from checkCFName service', data);
            return $http.post(baseUrl+'leads/checkUniqueCFName', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getVisibleFields = function () { //(`is_deleted` === N && `visibility` === Y)
            console.log('getVisibleFields service');
            return $http.post(baseUrl + 'leads/fetchVisibleFields').then(function (success) {
                console.log('custom fields', success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        return {
            getLeadsFlow: getLeadsFlow,
            createLeadsFlow: createLeadsFlow,
            deleteLeadsFlow: deleteLeadsFlow,
            addPartnerLead: addPartnerLead,
            getAllLeads: getAllLeads,
            deleteLeadData: deleteLeadData,
            deleteAllLeadsCFData: deleteAllLeadsCFData,
            updateLead: updateLead,
            convertLead: convertLead,
            getConvertedLeads: getConvertedLeads,
            getActiveCustomFields: getActiveCustomFields,
            getAllCustomFields: getAllCustomFields,
            createCustomFields: createCustomFields,
            deleteCustomFields: deleteCustomFields,
            changeCFStatus: changeCFStatus,
            searchLead: searchLead,
            getSearchedLeadsData: getSearchedLeadsData,
            searchConvertedLead: searchConvertedLead,
            createNormalFields: createNormalFields,
            getAllFields: getAllFields,
            reorderFields: reorderFields,
            showORhideField: showORhideField,
            createDropDownCF: createDropDownCF,
            addDropDownCFItem: addDropDownCFItem,
            getCFDropDownList: getCFDropDownList,
            delCFDropDownItem: delCFDropDownItem,
            updateCFDropDownItem: updateCFDropDownItem,
            validateLeadsSheet: validateLeadsSheet,
            uploadValidatedFile: uploadValidatedFile,
            checkCFName: checkCFName,
            convertedLeadsCount: convertedLeadsCount,
            getVisibleFields: getVisibleFields
        };
    }]);
