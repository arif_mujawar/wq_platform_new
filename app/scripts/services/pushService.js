class PushService {
  /*@ngInject*/
  constructor(calendarService) {
    this.calendarService = calendarService;
    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyDY7z5lRbJE-Yy4j39UbL9wEsCiDYUgkFM",
      authDomain: "wealthquotient-204206.firebaseapp.com",
      databaseURL: "https://wealthquotient-204206.firebaseio.com",
      projectId: "wealthquotient-204206",
      storageBucket: "",
      messagingSenderId: "742756827782"
    };
    firebase.initializeApp(config);
    // Retrieve Firebase Messaging object.
    this.messaging = firebase.messaging();
    this.messaging.usePublicVapidKey(
      "BIiE5lP_N-6hqK5zyy7emKGs6Q2wevqiupXEO4zSBwWIVUidFJEVXSWJNRtmGIVJL95QGVJDzp33lBeGqQRfQdk"
    );
  }

  requestPermission() {
    return this.messaging
      .requestPermission()
      .then(result => {
        console.log("Notification permission granted.", result);
        this.getToken();
      })
      .catch(function(err) {
        console.log("Unable to get permission to notify.", err);
      });
  }

  getToken() {
    // Get Instance ID token. Initially this makes a network call, once retrieved
    // subsequent calls to getToken will return from cache.
    return this.messaging
      .getToken()
      .then(currentToken => {
        if (currentToken) {
          return this.savePushToken(currentToken);
        }
      })
      .catch(function(err) {
        console.log("An error occurred while retrieving token. ", err);
      });
  }

  refreshToken() {
    // Callback fired if Instance ID token is updated.
    this.messaging.onTokenRefresh(() => {
      this.getToken();
    });
  }

  savePushToken(token) {
    return this.calendarService.savePushToken(token);
  }
}

angular.module("wealthQuotientApp").service("pushService", PushService);
