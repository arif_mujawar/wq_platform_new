angular.module('wealthQuotientApp')
    .factory('Store', function () {
        var leadsFlowCheck = null;
        var leadsFlowData = null;
        var partnerLeads = null;
        var customFieldsCheck = null;
        var customFieldsData = null;
        var checkNF = null;
        var checkMyClientsNF = null;
        var allFields = null;
        var customersCheck = null;
        var customerview = null;
        var assingeeview = null;
        var dateview = null;
        var isEmptyOrUndefinedOrNull = function (input) {
            if(typeof input === 'string' || input instanceof String){
                console.log('String');
                return(input === '' || input === null || input === undefined || input === 'undefined');
            }
            else if(Array.isArray(input)){
                console.log('Array');
                return (input.length === 0 || input === null || input === undefined);
            }
            else if(typeof input === 'object' && input.constructor === Object){
                console.log('Object', input);
                return (Object.keys(input).length === 0 || input === null || input === undefined);
            }
            else{
                console.log('Others');
                return (input === null || input === undefined);
            }
        };
        var clientVMData = {
            clientDataForToken: {}
        };
        // this.inDetailGoalsData = null;
        // this.MasterGoalsData = null;
        // this.fullNameData = null;
        // this.ratePMT = null;
        // this.allGoalsUser = null;
        // this.splash3 = null;
        // this.walletBalance = null;
        // this.walletStmt = null;
        // this.minigoalsArray = null;
        // this.spouseId = null;
        // this.tutorialTakenByUser = null;
        // this.subscriptionStatus = null;
        // this.rechargeData = null;
        // this.subscriptionStatus = null;
        // this.CompleteGoalsData = null;

        return{
            leadsFlowCheck : leadsFlowCheck,
            leadsFlowData : leadsFlowData,
            partnerLeads : partnerLeads,
            customFieldsCheck : customFieldsCheck,
            customFieldsData : customFieldsData,
            checkNF : checkNF,
            allFields : allFields,
            customersCheck : customersCheck,
            checkMyClientsNF : checkMyClientsNF,
            customerview : customerview,
            assingeeview : assingeeview,
            dateview : dateview,
            isEmptyOrUndefinedOrNull: isEmptyOrUndefinedOrNull,
            clientVMData: clientVMData
        };
    });

