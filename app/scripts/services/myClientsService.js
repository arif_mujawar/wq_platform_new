'use strict';

/**
 * @ngdoc service
 * @name wealthQuotientApp.myClientsService
 * @description
 * # myClientsService
 * Factory in the wealthQuotientApp.
 */
angular.module('wealthQuotientApp')
    .factory('myClientsService', ['$http', 'myService', '$cookies', function ($http, myService, $cookies) {

        var getAllCustomers = function (pageNumber) {
            // var pN = 0;
            return $http.post(baseUrl + 'mc/getAllCustomers?pageNumber='+pageNumber).then(function (success) {
                console.log("getAllCustomers result from backend---", success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var createNormalFields = function () {
            console.log('data from createNormalFields for myClients service');
            return $http.post(baseUrl+'mc/createNF').then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllFields = function () {
            console.log('data from getAllFields service');
            return $http.post(baseUrl+'mc/getAllFields').then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var reorderFields = function (allFields) {
            console.log('data from reorderFields service');
            return $http.post(baseUrl+'mc/sortFields', allFields).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var createCustomFields = function (data) {
            console.log('data from addCustomFields service', data);
            return $http.post(baseUrl+'mc/addCustomFields', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllCustomFields = function () {
            console.log('getCustomFields service');
            return $http.post(baseUrl+'mc/getAllCFs').then(function(success) {
                console.log('custom fields', success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var changeCFStatus = function (data) {
            console.log('data from changeCFStatus service', data);
            return $http.post(baseUrl+'mc/updateCFStatus', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var showORhideField = function (data) {
            console.log('data from showORhideField service', data);
            return $http.post(baseUrl+'mc/updateFieldVisibility', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var updateCustCFData = function (data) {
            console.log('--------------------data from updateCustCFData service', JSON.stringify(data));
            return $http.post(baseUrl+'mc/updateCFData', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCustomerCFData = function (userID) {
            console.log('data from getCustomerCFData service', userID);
            return $http.post(baseUrl+'mc/customerCFData', userID).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var deleteCustomer = function (userID) {
            console.log('data from deleteCustomer service', userID);
            return $http.post(baseUrl+'mc/deleteCustomer', userID).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCustomers = function (searchObj) {
            console.log('data from getCustomers service', searchObj);
            return $http.post(baseUrl+'mc/getCustomers', searchObj).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var createDropDownCF = function (data) {
            console.log('data from createDropDownCF service', data);
            return $http.post(baseUrl + 'mc/addDropDownCF', data).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var addDropDownCFItem = function (data) {
            console.log('data from addDropDownCFItem service', data);
            return $http.post(baseUrl + 'mc/addDropDownCFItem', data).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCFDropDownList = function (cfObj) {
            console.log('data from getCFDropDownList service', cfObj);
            return $http.post(baseUrl + 'mc/cfDropDownList', cfObj).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var delCFDropDownItem = function (itemObj) {
            console.log('data from delCFDropDownItem service', itemObj);
            return $http.post(baseUrl + 'mc/deleteCFDropDownItem', itemObj).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var updateCFDropDownItem = function (itemObj) {
            console.log('data from updateCFDropDownItem service', itemObj);
            return $http.post(baseUrl + 'mc/updateCFDropDownItem', itemObj).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var updateCustCategory = function (data) {
            console.log('data from updateCustCategory service', data);
            return $http.post(baseUrl+'mc/rateCustomer', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var createRatedUserCategory = function (data) {
            return $http.post(baseUrl+'mc/createRatedUserCategory', data).then(function(success) {
                return success;
            }).catch(function (error) {
                return error;
            });
        };
        var getRatedUsersDetails = function (data) {
            return $http.post(baseUrl+'mc/getRatedUseresData', data).then(function(result) {
                return result.data;
            }).catch(function (error) {
                return error;
            });
        };

        var checkCFName = function (data) {
            console.log('data from checkCFName service', data);
            return $http.post(baseUrl+'mc/checkUniqueCFName', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var updateCFName = function (data) {
            return $http.post(baseUrl+'mc/updateCfName', data).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };



        return {
            getAllCustomers: getAllCustomers,
            createNormalFields: createNormalFields,
            getAllFields: getAllFields,
            reorderFields: reorderFields,
            createCustomFields: createCustomFields,
            getAllCustomFields: getAllCustomFields,
            changeCFStatus: changeCFStatus,
            showORhideField: showORhideField,
            updateCustCFData: updateCustCFData,
            getCustomerCFData: getCustomerCFData,
            deleteCustomer: deleteCustomer,
            getCustomers: getCustomers,
            createDropDownCF: createDropDownCF,
            addDropDownCFItem: addDropDownCFItem,
            getCFDropDownList: getCFDropDownList,
            delCFDropDownItem: delCFDropDownItem,
            updateCFDropDownItem: updateCFDropDownItem,
            updateCustCategory: updateCustCategory,
            createRatedUserCategory: createRatedUserCategory,
            getRatedUsersDetails: getRatedUsersDetails,
            checkCFName: checkCFName,
            updateCFName:updateCFName
        };
    }]);