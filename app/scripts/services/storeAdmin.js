/**
 * Created by vikram on 15/6/17.
 */
class StoreAdmin{
    constructor(){
        this.retirementData = null;
        this.graphdata = null;
        this.incomeData = null;
        this.personalInfo = null;
        this.tutorialTakenByUser = null;
        this.assumptionData = null;
        this.planLevel = null;
        this.spouseIncome = null;
        this.inDetailGoalsData = null;
        this.MasterGoalsData = null;
        this.fullNameData = null;
        this.ratePMT = null;
        this.allGoalsUser = null;
        this.splash3 = null;
        this.walletBalance = null;
        this.walletStmt = null;
        this.minigoalsArray = null;
        this.spouseId = null;
        this.tutorialTakenByUser = null;
        this.subscriptionStatus = null;
        this.rechargeData = null;
        this.subscriptionStatus = null;
        this.CompleteGoalsData = null;
        this.statements_loader = false;
        this.show_cams_captcha = false;
        this.show_karvy_captcha = false;
        this.show_summary = true;
        this.loader_text = null;
        this.imported = false;
        this.pending = false;
        this.scan_loader_cams = []
        this.scan_loader_karvy = []
        this.import_failed_cams = []
        this.import_failed_karvy = []
        this.import_completed_cams = []
        this.import_completed_karvy = []
        this.hide_captcha_timer_cams = false
        this.hide_captcha_timer_karvy = false
        this.pageAccess = null;
    }
}
StoreAdmin.$inject = [];
angular.module('wealthQuotientApp').service('StoreAdmin', StoreAdmin);
