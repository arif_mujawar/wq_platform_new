function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}
class Sync {
  constructor($q, $localStorage, $timeout, $rootScope, $http, $log) {
    this.$q = $q
    this.$timeout = $timeout
    this.$rootScope = $rootScope
    this.$http = $http
    this.$log = $log
    this.$localStorage = $localStorage.$default()
    this.makeSocketSession()
  }
  makeSocketSession(){
    if(this.$localStorage.socket_session){
      this.socket_session = this.$localStorage.socket_session
    }else{
      this.socket_session = randomString(10, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
      this.$localStorage.socket_session = this.socket_session
    }
  }
  
  getInteractions(){
    this.$log.debug("NBTEST :: in getStatements ");
    let deferred = this.$q.defer()
    let data = {
      'channel' : `c:interaction:${this.socket_session}`,
      'data' : {
        
      }
    }
    let channel = this.$rootScope.socket.emit('join', data);
    this.$rootScope.interaction = data;
    // channel.on(`c:interaction`, (payload)=>{
    //   this.$log.debug("NBTEST :: response socket ",payload);
    //   deferred.resolve(payload)
    // })
    deferred.resolve(channel)
    return deferred.promise
  }

}
Sync.$inject = ['$q', '$localStorage', '$timeout', '$rootScope', '$http', '$log']
angular.module('wealthQuotientApp').service('Sync', Sync)
