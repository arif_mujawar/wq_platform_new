'use strict';

/**
 * @ngdoc service
 * @name wealthQuotientApp.searchService
 * @description
 * # searchService
 * Factory in the wealthQuotientApp.
 */
angular.module('wealthQuotientApp')
    .factory('searchService', function ($http, myService) {
            /*
             |-------------------------------------------------------------------------------|
             |   Search                                                              |
             |-------------------------------------------------------------------------------|
             */

            // var spouseObj = {};
            var spouseCheck = false;
            var otherIncomeFlag = false;
            var oneTimeIncomeFlag = false;
            var clientAllData = {
                personalDetails: [],
                income: [],
                spouseIncome: [],
                expense: [],
                loans: [],
                goals: [],
                riskProfile: []
            };

            var searchClient = function (searchData) {

                // alert("in searchService!!!");
                console.log(searchData);
                // console.log(searchBy);
                // spouseObj = {};
                // spouseCheck = false;

                myService.setHeaders();

                return $http.post(baseUrl + 'user/searchClient', searchData).then(function (success) {
                    console.log("from backend---", success);
                    return success;
                }).catch(function (error) {
                    // console.log(error);
                    return error;
                });
            };

            var getSearchedCustData = function (searchResult, pageNumber) {

                console.log('data from getSearchedCustData service', searchResult, pageNumber);
                return $http.post(baseUrl + 'user/getSearchedClientData?pageNumber='+pageNumber, searchResult).then(function (success) {
                    console.log(success);
                    return success;
                }).catch(function (error) {
                    console.log(error);
                    return error;
                });
            };

            var searchPlannedClient = function (searchData) {

                // alert("in searchService!!!");
                console.log(searchData);
                // console.log(searchBy);
                // spouseObj = {};
                // spouseCheck = false;

                myService.setHeaders();

                return $http.post(baseUrl + 'user/searchPlannedClient', searchData).then(function (success) {
                    console.log("from backend---", success);
                    return success;
                }).catch(function (error) {
                    // console.log(error);
                    return error;
                });

                // console.log(registrationData);
            }

            var getClientDemographics = function (clientData) {
                myService.setHeaders();
                return $http.post(baseUrl + 'user/getClientDemographics', clientData).then(function (success1) {
                    console.log("DEMOGRAPHICS---", success1.data.response);
                    clientAllData.personalDetails = success1.data.response;
                    return success1.data;
                }).catch(function (error) {
                    // console.log(error);
                    return error;
                });
            }

            var getClientIncome = function (clientData) {
                myService.setHeaders();

                return $http.post(baseUrl + 'user/getClientIncome', clientData).then(function (success2) {
                    console.log("INCOME---", success2);
                    console.log('success from all income... service', success2);
                    clientAllData.income = success2.data.response;
                    return success2.data;
                    // console.log('user id', success2.data.response[0].user_id);
                    // console.log('spouse id', success2.data.response[0].spouse_id);
                    //
                    // return clientAllData;
                }).catch(function (error) {
                    // console.log(error);
                    return error;
                });

            }


            var getClientSpouseIncome = function (spouseObj) {
                // if (spouseCheck) {
                console.log('spouse obj to check income:', spouseObj);
                myService.setHeaders();
                return $http.post(baseUrl + 'user/getClientSpouseIncome', spouseObj).then(function (success2a) {
                    console.log("SPOUSE INCOME---", success2a);
                    // return success;
                    clientAllData.spouseIncome = success2a.data.response;
                    return success2a.data;
                    // console.log('client data with spouse income', clientAllData);
                    // return clientAllData;
                }).catch(function (error) {
                    // console.log(error);
                    return error;
                })
                // }
                // else{
                //     clientAllData.spouseIncome = [];
                //     return [];
                // }
            }

            var getClientExpenses = function (clientData) {
                myService.setHeaders();

                return $http.post(baseUrl + 'user/getClientExpenses', clientData).then(function (success3) {
                    console.log("EXPENSES---", success3);
                    clientAllData.expense = success3.data.response;
                    return success3.data;
                }).catch(function (error) {
                    // console.log(error);
                    return error;
                });
            }


            var getClientLoans = function (clientData) {
                myService.setHeaders();

                return $http.post(baseUrl + 'user/getClientLoans', clientData).then(function (success4) {
                    console.log("LOANS---", success4);
                    // return success;
                    clientAllData.loans = success4.data.response;
                    return success4.data;
                    // return clientAllData;
                }).catch(function (error) {
                    // console.log(error);
                    return error;
                });
            }


            var getClientGoals = function (clientData) {
                myService.setHeaders();

                return $http.post(baseUrl + 'user/getClientGoals', clientData).then(function (success5) {
                    console.log("GOALS---", success5);
                    // return success;
                    clientAllData.goals = success5.data.response;
                    return success5.data;
                }).catch(function (error) {
                    // console.log(error);
                    return error;
                });

            }


            var getClientRiskProfile = function (clientData) {
                myService.setHeaders();

                return $http.post(baseUrl + 'user/getClientRiskScore', clientData).then(function (success6) {
                    console.log("RISK SCORE---", success6);
                    // return success;
                    clientAllData.riskProfile = success6.data.response;
                    return success6.data;
                }).catch(function (error) {
                    // console.log(error);
                    return error;
                });
            }

            // console.log('client data with all data', clientAllData);


            var getClientBasicData = function (custIDObj) {

                console.log('data from getClientBasicData service', custIDObj);
                return $http.post(baseUrl + 'user/getClientBasicData', custIDObj).then(function (success) {
                    console.log(success);
                    return success;
                }).catch(function (error) {
                    console.log(error);
                    return error;
                });
            };

        var getAllFamilyDetails = function (custIDObj) {
            //console.log('data from getAllFamilyDetails', custIDObj);
            return $http.post(baseUrl + 'user/getAllFamilyDetails', custIDObj).then(function (success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };


            return {
                search: searchClient,
                searchPlanned: searchPlannedClient,
                getDemographics: getClientDemographics,
                getIncome: getClientIncome,
                getSpouseIncome: getClientSpouseIncome,
                getExpenses: getClientExpenses,
                getLoans: getClientLoans,
                getGoals: getClientGoals,
                getRiskScore: getClientRiskProfile,
                getSearchedCustData: getSearchedCustData,
                getClientBasicData: getClientBasicData,
                getAllFamilyDetails:getAllFamilyDetails
            };

        }
    );