/**
 * Created by somnath on 1/8/17.
 */

angular.module('wealthQuotientApp')
    .factory('srService', function ($http, myService,$state,Upload) {

        var createSRTemplate = function (data) {
            // //console.log('data from serviceRequests controller',JSON.stringify(data));
            return $http.post(baseUrl+'serviceRequests/createTempForRequest', data).then(function(success) {
                // //console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var getAllSRTemplates = function (query) {
            myService.setHeaders();
            return $http.post(baseUrl+'serviceRequests/getAllRequestTemplates',query).then(function(success) {
                // //console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        }

        var serviceDeleteTemplate = function (query) {
            return $http.post(baseUrl+'serviceRequests/serviceDeleteTemplate',query).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var addTempForRequest = function (query) {
            return $http.post(baseUrl+'serviceRequests/addTempForRequest',query).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var requestUpdateTemplate = function (query) {
            return $http.post(baseUrl+'serviceRequests/requestUpdateTemplate',query).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var crmRemoveTempSubstaskReq = function (query) {
            return $http.post(baseUrl+'serviceRequests/crmRemoveTempSubstaskReq',query).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var getSRTemplatesByName = function (data) {
            // console.log('data from serviceRequests controller',data);
            return $http.post(baseUrl+'serviceRequests/getServiceTempByName', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var getAllServiceByName = function (data) {
            return $http.post(baseUrl+'serviceRequests/getAllServiceRequestByName', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var craeteCrmService = function (query) {
            return $http.post(baseUrl+'serviceRequests/createCrmServiceReq', query).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var crmNoteForSerRequest = function (query) {
            return $http.post(baseUrl+'serviceRequests/crmNoteForSerRequest', query).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var crmRemoveTempSerReq = function (query) {
            return $http.post(baseUrl+'serviceRequests/crmRemoveTempSerReq', query).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };
        var crmUpdateRemoveFreshSer = function (query) {
            return $http.post(baseUrl+'serviceRequests/crmUpdateRemoveFreshSer', query).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var AddMoreTemplateSerRequest = function (query) {
            return $http.post(baseUrl+'serviceRequests/AddMoreTemplateSerRequest', query).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var serviceReqUpdateTemplate = function (query) {
            return $http.post(baseUrl+'serviceRequests/serviceReqUpdateTemplate', query).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var getAllCrmServiceRequests = function (query) {
            return $http.post(baseUrl+'serviceRequests/getCrmAllServiceRequest',query).then(function(success) {
                // //console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var getCrmSerTempForDownload = function () {
            return $http.post(baseUrl+'serviceRequests/getCrmSerTempForDownload',{}).then(function(success) {
                // //console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };




        var getSubtaksforSRTemplate = function (data) {
            // console.log('data from serviceRequests controller',data);
            return $http.post(baseUrl+'serviceRequests/getSRTemplateSubtasks', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var crmSnoozeServiceRequest = function (data) {
            return $http.post(baseUrl+'serviceRequests/crmSnoozeServiceRequest', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var AddFreshServiceRequest = function (data) {
            return $http.post(baseUrl+'serviceRequests/AddFreshServiceRequest', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var crmUpdateFreshServiceReq = function (data) {
            // console.log('data from serviceRequests controller',data);
            return $http.post(baseUrl+'serviceRequests/crmUpdateFreshServiceReq', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };
        var createIndividualSR = function (data) {
            // console.log('data from serviceRequests controller', data);
            myService.setHeaders();
            return $http.post(baseUrl+'serviceRequests/addIndividualSR', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var createTemplateSR = function (data) {
            // console.log('data from serviceRequests controller', data);
            myService.setHeaders();
            return $http.post(baseUrl+'serviceRequests/addTemplateSR', data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var crmUpdateCommentsService = function (data) {
            return $http.post(baseUrl+'serviceRequests/crmUpdateCommentForService',data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var crmUpdateTemplateservice = function (data) {
            return $http.post(baseUrl+'serviceRequests/crmUpdateTemplateService',data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var crmUpdateTemplateServiceForAllIds = function (data) {
            return $http.post(baseUrl+'serviceRequests/crmUpdateTemplateServiceStatus',data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var getCrmAllServiceRequestBySearch = function (data) {
            return $http.post(baseUrl+'serviceRequests/getCrmAllServiceRequestBySearch',data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };


        var getCrmAllsnoozedSerReq = function () {
            return $http.post(baseUrl+'serviceRequests/getCrmAllsnoozedSerReq',{}).then(function(success) {
                // //console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };
        var getCrmCompetedTempServices = function () {
            return $http.post(baseUrl+'serviceRequests/getCrmAllCompletedTempSerRequest',{}).then(function(success) {
                // //console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var crmUpdateServiceContent = function (data) {
            return $http.post(baseUrl+'serviceRequests/crmUpdateServiceSingle',data).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var craeteCrmTaskIndividualServices = function (query) {
            return $http.post(baseUrl+'serviceRequests/createCrmIndivudualService', query).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var checkServiceRequestsByName = function (query) {
            return $http.post(baseUrl+'serviceRequests/checkServiceRequestsByName', query).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };
        var crmRemoveServiceReq = function (query) {
            return $http.post(baseUrl+'serviceRequests/crmRemoveServiceReq', query).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var getSerReqAssigneeNames = function (query) {
            return $http.post(baseUrl+'serviceRequests/getSerReqAssigneeNames', query).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var getCrmAllSerReqByAssingee = function (query) {
            return $http.post(baseUrl+'serviceRequests/getCrmAllSerReqByAssingee', query).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var getCrmAllSerReqByDate = function (query) {
            return $http.post(baseUrl+'serviceRequests/getCrmAllSerReqByDate', query).then(function(success) {
                // console.log(success);
                return success;
            }).catch(function (error) {
                // console.log(error);
                return error;
            });
        };

        var templateUploadFile = function (file) {
            console.log('templateUploadFile service----------------',templateUploadFile);
            return Upload.upload({
                url: `${baseUrl}serviceRequests/templatesUploadFile`,
                data: {file: file}
            });
        };

        var serReqUpComingUpload = function (file) {
            console.log('serReqUpComingUpload service----------------',templateUploadFile);
            return Upload.upload({
                url: `${baseUrl}serviceRequests/serReqUpComingUpload`,
                data: {file: file}
            });
        };



        return{
            createSRTemplate: createSRTemplate,
            getAllSRTemplates: getAllSRTemplates,
            getSRTemplatesByName: getSRTemplatesByName,
            serviceDeleteTemplate: serviceDeleteTemplate,
            getSubtaksforSRTemplate: getSubtaksforSRTemplate,
            craeteCrmService:craeteCrmService,
            getAllCrmServiceRequests:getAllCrmServiceRequests,
            getAllServiceByName:getAllServiceByName,
            createIndividualSR: createIndividualSR,
            createTemplateSR: createTemplateSR,
            crmUpdateCommentsService:crmUpdateCommentsService,
            crmUpdateTemplateservice:crmUpdateTemplateservice,
            crmUpdateTemplateServiceForAllIds:crmUpdateTemplateServiceForAllIds,
            getCrmCompetedTempServices:getCrmCompetedTempServices,
            crmUpdateServiceContent:crmUpdateServiceContent,
            craeteCrmTaskIndividualServices:craeteCrmTaskIndividualServices,
            checkServiceRequestsByName:checkServiceRequestsByName,
            requestUpdateTemplate:requestUpdateTemplate,
            addTempForRequest:addTempForRequest,
            serviceReqUpdateTemplate:serviceReqUpdateTemplate,
            crmRemoveTempSerReq:crmRemoveTempSerReq,
            AddMoreTemplateSerRequest:AddMoreTemplateSerRequest,
            crmNoteForSerRequest:crmNoteForSerRequest,
            crmUpdateFreshServiceReq:crmUpdateFreshServiceReq,
            AddFreshServiceRequest:AddFreshServiceRequest,
            crmUpdateRemoveFreshSer:crmUpdateRemoveFreshSer,
            crmSnoozeServiceRequest:crmSnoozeServiceRequest,
            getCrmAllsnoozedSerReq:getCrmAllsnoozedSerReq,
            getCrmAllServiceRequestBySearch:getCrmAllServiceRequestBySearch,
            getSerReqAssigneeNames:getSerReqAssigneeNames,
            getCrmAllSerReqByAssingee:getCrmAllSerReqByAssingee,
            getCrmAllSerReqByDate:getCrmAllSerReqByDate,
            crmRemoveServiceReq:crmRemoveServiceReq,
            crmRemoveTempSubstaskReq:crmRemoveTempSubstaskReq,
            getCrmSerTempForDownload:getCrmSerTempForDownload,
            templateUploadFile:templateUploadFile,
            serReqUpComingUpload:serReqUpComingUpload
        };



    })
    .factory('serachSerType', function($rootScope,$state,Store) {
        // console.log('serachSerType calling');
        var searchtype = {};
        searchtype.searchmessage  = '';
        searchtype.prepForPublish = function(msg) {
            // console.log('calling serachSerType search factory',msg);
            this.searchmessage  = msg;
            // console.log('this.searchmessage 11111111111',JSON.stringify(this.searchmessage));
            if(this.searchmessage.type === 'customer'){
                Store.customerview = true;
                Store.assingeeview = false;
                Store.dateview = false;
            }else if(this.searchmessage.type === 'assignee'){
                Store.assingeeview = true;
                Store.customerview = false;
                Store.dateview = false;
            }else{
                Store.dateview = true;
                Store.assingeeview = false;
                Store.customerview = false;
            }

            this.publishItemSearch();
        };

        searchtype.publishItemSearch = function() {
            //$rootScope.$broadcast('searchType');
            $state.go('searchServiceReq');

        };
        return searchtype;
    });