class AdminStatements {
  /*@ngInject*/
  constructor($http) {
    this.$http = $http
    
    this.asset_allocation = []
    this.asset_allocation_drilldown = []
    this.resetStatementsData()
    this.resetUploadStatementsData()
    this.resetFundMetrics()
    this.resetFundAnalytics()
    this.resetFundMappings()
    this.searchFunds = null
  }
  resetFundMetrics(){
    this.FundMetrics = []
  }
  resetFundMappings(){
    this.FundMappings = []
  }
  resetFundAnalytics(){
    this.FundAnalytics = []
  }
  resetUploadStatementsData(){
    this.UploadStatementsData = {
      'all' : []
    }
  }
  resetStatementsData(){
    this.StatementsData = {
      'pending' : [],
      'failed' : [],
      'exceeded' : [],
      'not_received' : [],
      'not_imported' : [],
      'due' : [],
      'all' : []
    }
  }
  successToast(msg) {

      var $toastContent = $('<span style="color: white;">' + msg + '</span>');
      Materialize.toast($toastContent, 3000, 'green lighten-1 rounded');

  }

  errorToast(msg) {
      var $toastContent = $('<span style="color: white;">' + msg + '</span>');
      Materialize.toast($toastContent, 3000, 'red lighten-1 rounded');
  }
  RequestAgain(statements_data, index, registrar, is_due_request, adminUserName){
    statements_data[index][registrar].last_request = moment().unix()
    statements_data[index][registrar].status.push("requested")
    statements_data[index][registrar].timestamps.push(moment().unix())
    statements_data[index][registrar].is_due_request = is_due_request
    this.$http({
      method: 'POST',
      url: baseUrl+'admin/requestagain',
      data: {
        statements_data : statements_data[index],
        registrar: registrar
      }
    }).then((response)=>{
      console.log(response)
      if(response.data.status){
        this.successToast(`${registrar.toUpperCase()} Statements Request Updated & Notified`)
      }else{
        this.errorToast(`${registrar.toUpperCase()} Statements Request Updation failed`)
      }
      this.makeAdminStatements(adminUserName)
    })
  }
  ImportAgain(statements_data, index, registrar, adminUserName){
    this.$http({
      method: 'POST',
      url: '/api/admin/importagain',
      data: {
        statements_data : statements_data[index],
        registrar: registrar
      }
    }).then((response)=>{
      if(response.data.status){
        this.successToast(`${registrar.toUpperCase()} Statements Request Imported & Notified`)
      }else{
        this.errorToast(`${registrar.toUpperCase()} Statements Request Import failed`)
      }
      this.makeAdminStatements(adminUserName)
    })
  }
  UploadStatementImport(statements_data, index){
    this.$http({
      method: 'POST',
      url: '/api/admin/upload_import_update',
      data: {
        user_id: statements_data[index].user_id,
        file_name: statements_data[index].file_name,
      }
    }).then((response)=>{
      if(response.data.status){
        this.successToast(`Statements Upload Import Updated & Notified`)
      }else{
        this.errorToast(`Statements Upload Import Update failed`)
      }
      this.makeAdminUploadStatements()
    })
  }
  updateRiskFactor(){
    this.$http({
      method: 'POST',
      url: baseUrl+'admin/fund_metrics',
      data: this.FundMetrics
    }).then((response)=>{
      if(response.data.status){
        this.successToast(`Fund Metrics Updated`)
      }else{
        this.errorToast(`Failed to update Fund Metrics`)
      }
      this.makeMutualFundMetrics()
    })
  }
  makeMutualFundMetrics(){
    this.resetFundMetrics()
    this.$http({
      method: 'GET',
      url: baseUrl+'admin/fund_metrics'
    }).then((response)=>{
      if(response.data.status){
        this.FundMetrics = response.data.data
      }
    })
  }
  makeMutualFundMappings(){
    this.resetFundMappings()
    this.$http({
      method: 'GET',
      url: baseUrl+'admin/fund_mappings'
    }).then((response)=>{
      if(response.data.status){
        this.tempFundMappings = response.data.data
        this.FundMappings = response.data.data
      }
    })
  }
  makeMutualFundAnalytics(){
    this.resetFundAnalytics()
    this.$http({
      method: 'GET',
      url: baseUrl+'admin/fund_analytics'
    }).then((response)=>{
      if(response.data.status){
        this.tempFundAnalytics = response.data.data
        this.FundAnalytics = response.data.data
        this.FundAnalytics = _.sortBy(this.FundAnalytics, 'score').reverse()
      }
    })
  }
  filterFunds(){
    this.FundAnalytics = _.sortBy(_.filter(this.tempFundAnalytics, (funds)=>{
      return (funds.amfi_code.includes(this.searchFunds) || funds.scheme_name.toLowerCase().includes(this.searchFunds.toLowerCase()) || funds.classification.toLowerCase().includes(this.searchFunds.toLowerCase()))
    }), 'score').reverse()
    this.FundMappings = _.filter(this.tempFundMappings, (funds)=>{
      return (funds.amfi_code.includes(this.searchFunds) || funds.scheme_name.toLowerCase().includes(this.searchFunds.toLowerCase()))
    })
  }
  makeAdminUploadStatements(){
    this.resetUploadStatementsData()
    this.$http({
      method: 'GET',
      url: baseUrl+'admin/upload_statements'
    }).then((response)=>{
      if(response.data.status){
        _.each(response.data.data, (data)=>{
          _.each(data.upload_files, (files)=>{
            this.UploadStatementsData.all.push({
              user_id: data.user_id,
              option: files.option,
              status: files.status,
              file_name: files.file_name,
              url: `https://s3-us-west-1.amazonaws.com/cq-upload-statement/${files.option}/${files.file_name}`,
              timestamp: files.timestamp
            })
          })
        })
        console.log(this.UploadStatementsData.all)

        this.UploadStatementsData.all = _.sortBy(this.UploadStatementsData.all, 'timestamp').reverse()
      }
    })
  }
  makeAdminStatements(adminUserName){
    this.resetStatementsData()
    this.$http({
      method: 'GET',
      url: baseUrl+'admin/statements'
    }).then((response)=>{
      console.log(response)
      let statementsResponse = response.data.data;
      if(response.data.data.length > 0){
        if(true){
          let reqData = {
              cac_source : adminUserName
          };
          this.$http({
              method: 'POST',
              url: baseUrl+'admin/getFilteredUser',
              data: reqData
          }).then((response1)=>{
            console.log(response1)
            if(response1.data.status){
              let admin_users = response1.data.value[0]
              let finalStatementsResult = [];
              _.each(admin_users,(user)=>{
                  _.each(statementsResponse,(userData)=>{
                      if(user.user_id === userData.user_id){
                        finalStatementsResult.push(userData);
                      }
                  })
              });
              this.StatementsData.all = finalStatementsResult;
              _.each(this.StatementsData.all, (statements_data)=>{
                    if(statements_data.cams.last_imported){
                        statements_data.cams.start_date = moment.unix(statements_data.cams.last_imported).subtract(7, 'days').format("DD/MM/YYYY")
                    }else{
                        statements_data.cams.start_date = "01/01/2000"
                    }
                    if(statements_data.karvy.last_imported){
                        statements_data.karvy.start_date = moment.unix(statements_data.karvy.last_imported).subtract(7, 'days').format("DD/MM/YYYY")
                    }else{
                        statements_data.karvy.start_date = "01/01/2000"
                    }
                    let last_cams_status = statements_data.cams.status[statements_data.cams.status.length - 1]
                    let last_cams_request = statements_data.cams.last_request
                    let last_karvy_status = statements_data.karvy.status[statements_data.karvy.status.length - 1]
                    let last_karvy_request = statements_data.karvy.last_request
                    console.log(last_cams_status, last_karvy_status)
                    if(last_cams_status == "pending" || last_karvy_status == "pending"){
                        if(last_cams_status == "pending"){
                            statements_data.cams.request_again = true
                        }else{
                            statements_data.cams.request_again = false
                        }
                        if(last_karvy_status == "pending"){
                            statements_data.karvy.request_again = true
                        }else{
                            statements_data.karvy.request_again = false
                        }
                        this.StatementsData.pending.push(statements_data)
                    }
                    if(last_cams_status == "failed" || last_karvy_status =="failed"){
                        statements_data.cams.request_again = false
                        statements_data.karvy.request_again = false
                        if(last_cams_status == 'failed'){
                            statements_data.cams.request_again = true
                        }
                        if(last_karvy_status == 'failed'){
                            statements_data.karvy.request_again = true
                        }
                        this.StatementsData.failed.push(statements_data)
                    }
                    if(last_cams_status == "exceeded" || last_karvy_status == "exceeded"){
                        statements_data.cams.request_again = false
                        statements_data.karvy.request_again = false
                        if(last_cams_status == 'exceeded'){
                            statements_data.cams.request_again = true
                        }
                        if(last_karvy_status == 'exceeded'){
                            statements_data.karvy.request_again = true
                        }
                        this.StatementsData.exceeded.push(statements_data)
                    }
                    if(last_cams_status == 'failed_import' || last_karvy_status == 'failed_import'){
                        console.log("failed_import came")
                        statements_data.cams.import_again = false
                        statements_data.karvy.import_again = false
                        if(last_cams_status == 'failed_import'){
                            statements_data.cams.import_again = true
                        }
                        if(last_karvy_status == 'failed_import'){
                            statements_data.karvy.import_again = true
                        }
                        console.log(statements_data)
                        this.StatementsData.not_imported.push(statements_data)
                    }
                    let current_time = moment().unix()
                    let not_received_cams = moment.unix(last_cams_request).add(1, 'days').startOf('day').unix()
                    let not_received_karvy = moment.unix(last_karvy_request).add(1, 'days').startOf('day').unix()
                    if(current_time > not_received_karvy || current_time > not_received_cams){
                        statements_data.cams.request_again = false
                        statements_data.karvy.request_again = false
                        if(current_time > not_received_karvy){
                            statements_data.karvy.request_again = true
                        }
                        if(current_time > not_received_cams){
                            statements_data.cams.request_again = true
                        }
                        this.StatementsData.not_received.push(statements_data)
                    }

                    let next_cams_request = moment.unix(last_cams_request).add(30, 'days').startOf('day').unix()
                    let next_karvy_request = moment.unix(last_karvy_request).add(30, 'days').startOf('day').unix()
                    if(current_time > next_cams_request || current_time > next_karvy_request){
                        this.StatementsData.due.push(statements_data)
                    }
                })
            }
          })
        }else{
            this.StatementsData.all = response.data.data
            _.each(this.StatementsData.all, (statements_data)=>{
                if(statements_data.cams.last_imported){
                    statements_data.cams.start_date = moment.unix(statements_data.cams.last_imported).subtract(7, 'days').format("DD/MM/YYYY")
                }else{
                    statements_data.cams.start_date = "01/01/2000"
                }
                if(statements_data.karvy.last_imported){
                    statements_data.karvy.start_date = moment.unix(statements_data.karvy.last_imported).subtract(7, 'days').format("DD/MM/YYYY")
                }else{
                    statements_data.karvy.start_date = "01/01/2000"
                }
                let last_cams_status = statements_data.cams.status[statements_data.cams.status.length - 1]
                let last_cams_request = statements_data.cams.last_request
                let last_karvy_status = statements_data.karvy.status[statements_data.karvy.status.length - 1]
                let last_karvy_request = statements_data.karvy.last_request
                console.log(last_cams_status, last_karvy_status)
                if(last_cams_status == "pending" || last_karvy_status == "pending"){
                    if(last_cams_status == "pending"){
                        statements_data.cams.request_again = true
                    }else{
                        statements_data.cams.request_again = false
                    }
                    if(last_karvy_status == "pending"){
                        statements_data.karvy.request_again = true
                    }else{
                        statements_data.karvy.request_again = false
                    }
                    this.StatementsData.pending.push(statements_data)
                }
                if(last_cams_status == "failed" || last_karvy_status =="failed"){
                    statements_data.cams.request_again = false
                    statements_data.karvy.request_again = false
                    if(last_cams_status == 'failed'){
                        statements_data.cams.request_again = true
                    }
                    if(last_karvy_status == 'failed'){
                        statements_data.karvy.request_again = true
                    }
                    this.StatementsData.failed.push(statements_data)
                }
                if(last_cams_status == "exceeded" || last_karvy_status == "exceeded"){
                    statements_data.cams.request_again = false
                    statements_data.karvy.request_again = false
                    if(last_cams_status == 'exceeded'){
                        statements_data.cams.request_again = true
                    }
                    if(last_karvy_status == 'exceeded'){
                        statements_data.karvy.request_again = true
                    }
                    this.StatementsData.exceeded.push(statements_data)
                }
                if(last_cams_status == 'failed_import' || last_karvy_status == 'failed_import'){
                    console.log("failed_import came")
                    statements_data.cams.import_again = false
                    statements_data.karvy.import_again = false
                    if(last_cams_status == 'failed_import'){
                        statements_data.cams.import_again = true
                    }
                    if(last_karvy_status == 'failed_import'){
                        statements_data.karvy.import_again = true
                    }
                    console.log(statements_data)
                    this.StatementsData.not_imported.push(statements_data)
                }
                let current_time = moment().unix()
                let not_received_cams = moment.unix(last_cams_request).add(1, 'days').startOf('day').unix()
                let not_received_karvy = moment.unix(last_karvy_request).add(1, 'days').startOf('day').unix()
                if(current_time > not_received_karvy || current_time > not_received_cams){
                    statements_data.cams.request_again = false
                    statements_data.karvy.request_again = false
                    if(current_time > not_received_karvy){
                        statements_data.karvy.request_again = true
                    }
                    if(current_time > not_received_cams){
                        statements_data.cams.request_again = true
                    }
                    this.StatementsData.not_received.push(statements_data)
                }

                let next_cams_request = moment.unix(last_cams_request).add(30, 'days').startOf('day').unix()
                let next_karvy_request = moment.unix(last_karvy_request).add(30, 'days').startOf('day').unix()
                if(current_time > next_cams_request || current_time > next_karvy_request){
                    this.StatementsData.due.push(statements_data)
                }
            })
        }
        }
    })
  }
  getAssetAllocation(){
    return this.$http({
      method: 'GET',
      url : baseUrl+'admin/asset_allocation'
    }).then((response)=>{
      if(response.data.status){
        this.asset_allocation = response.data.data.asset_allocation
        this.asset_allocation_drilldown = response.data.data.asset_allocation_drilldown
      }
    })
  }
}


angular.module("wealthQuotientApp").service('AdminStatements', AdminStatements)
