/**
 * Created by somnath on 31/10/17.
 */

'use strict';

/**
 * @ngdoc service
 * @name wealthQuotientApp.opportunitiesService
 * @description
 * # opportunitiesService
 * Factory in the wealthQuotientApp.
 */
angular.module('wealthQuotientApp')
    .factory('opportunitiesService', function ($http, myService, $cookies, $log, $q) {

        var assetClasses = [
            {'id': 1, 'displayName': 'Equity', 'assetName': 'equity'},
            {'id': 2, 'displayName': 'Debt', 'assetName': 'debt'},
            {'id': 3, 'displayName': 'Gold', 'assetName': 'gold'},
            {'id': 4, 'displayName': 'Cash', 'assetName': 'cash'},
            {'id': 5, 'displayName': 'Hybrid', 'assetName': 'hybrid'},
            {'id': 6, 'displayName': 'Real Estate', 'assetName': 'real_estate'},
            {'id': 7, 'displayName': 'Others', 'assetName': 'others'}
        ];

        var equityTypes = [
            {'id': 1, 'displayName': 'Direct Equity', 'invType': 'direct_equity'},
            {'id': 2, 'displayName': 'Mutual Fund', 'invType': 'mutual_funds'}
        ];

        var debtTypes = [
            {'id': 1, 'displayName': 'Bonds', 'invType': 'bonds'},
            {'id': 2, 'displayName': 'Fixed Deposits', 'invType': 'fixed_deposits'},
            {'id': 3, 'displayName': 'Recurring Deposits', 'invType': 'recurring_deposits'},
            {'id': 4, 'displayName': 'Endowment', 'invType': 'endowment'},
            {'id': 5, 'displayName': 'Money Back', 'invType': 'moneyback'},
            {'id': 6, 'displayName': 'Retiral Funds', 'invType': 'retirals'},
            {'id': 7, 'displayName': 'Mutual Fund', 'invType': 'mutual_funds'}
        ];

        var hybridTypes = [
            {'id': 1, 'displayName': 'Mutual Fund', 'invType': 'mutual_funds'}
        ];

        var othersTypes = [
            {'id': 1, 'displayName': 'PMS', 'invType': 'pms'},
            {'id': 2, 'displayName': 'Structured', 'invType': 'structured'},
            {'id': 3, 'displayName': 'Alternate', 'invType': 'alternate'},
            {'id': 4, 'displayName': 'ULIPS', 'invType': 'ulips'},
            {'id': 5, 'displayName': 'Offshore', 'invType': 'offshore'},
            {'id': 6, 'displayName': 'Mutual Fund', 'invType': 'mutual_funds'},
            {'id': 7, 'displayName': 'Others', 'invType': 'others'}
        ];

        var newPS = function (psObj) {
            return $http.post(baseUrl + 'opp/addPS', psObj).then(function (success) {
                console.log("Add new product result from backend---", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllPS = function () {
            return $http.post(baseUrl + 'opp/getAllPS').then(function (success) {
                console.log("Get all PS result", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var checkName = function (obj) {
            return $http.post(baseUrl + 'opp/checkProductOrServiceName', obj).then(function (success) {
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var updatePS = function (psObj) {
            return $http.post(baseUrl + 'opp/updatePS', psObj).then(function (success) {
                console.log("Update PS result from backend---", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var deletePS = function (psObj) {
            return $http.post(baseUrl + 'opp/deletePS', psObj).then(function (success) {
                console.log("Delete PS result from backend---", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var createOpportunity = function (opportunityObject) {
            return $http.post(baseUrl + 'opp/addOpportunity', opportunityObject).then(function (success) {
                console.log("Create opportunity result from backend---", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var addCondition = function (opportunityObject) {
            return $http.post(baseUrl + 'opp/addOppCondition', opportunityObject).then(function (success) {
                console.log("Add condition result from backend---", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getOpportunityDetails = function (opportunityObject) {
            return $http.post(baseUrl + 'opp/getOpportunityWithID', opportunityObject).then(function (success) {
                console.log("Get opportunity details result from backend---", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllOpportunities = function () {
            return $http.post(baseUrl + 'opp/getAllOpps').then(function (success) {
                console.log("Get all Opportunities result", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var deleteOpportunity = function (oppObj) {
            return $http.post(baseUrl + 'opp/deleteOpportunity', oppObj).then(function (success) {
                console.log("Delete Opportunity result", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var deleteCondition = function (condnObj) {
            return $http.post(baseUrl + 'opp/deleteCondition', condnObj).then(function (success) {
                console.log("Delete Condition result", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var updateCondition = function (condnObj) {
            return $http.post(baseUrl + 'opp/updateCondition', condnObj).then(function (success) {
                console.log("Update Condition result", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getMFClassifications = function (dataObj) {
            return $http.post(baseUrl + 'opp/getMFClassifications', dataObj).then(function (success) {
                console.log("Get MF Classifications result", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getInvestmentNames = function (dataObj) {
            return $http.post(baseUrl + 'opp/getInvNames', dataObj).then(function (success) {
                console.log("Get Inv Names result", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getInvNamesForNonPF = function (dataObj) {
            return $http.post(baseUrl + 'opp/getNonPFInvNames', dataObj).then(function (success) {
                console.log("Get Inv Names for NonPF result", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllPSLevelData = function (categoryQry) {
            return $http.post(baseUrl + 'opp/allPSLevelData', categoryQry).then(function (success) {
                console.log("Get all PS result", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var updateOppValueCalLogic = function (oppObj) {
            return $http.post(baseUrl + 'opp/updateOppValueCalLogic', oppObj).then(function (success) {
                console.log("updateOppValueCalLogic result", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var groupConditionsIntoOpportunities = function (conditionsArr) {
            var finalAllOpps = [];
            var count = 0;
            var groupedOpp = _.groupBy(conditionsArr, function (item) {
                return item.opportunity_id;
            });
            $log.debug('Grouped Opp>>>', groupedOpp);
            count = Object.keys(groupedOpp).length;
            return new Promise(function (resolve, reject) {
                _.each(groupedOpp, function (conditionsArr, oppID) {
                    if(conditionsArr[0].link_type === 'withIH'){
                        console.log('Opp Object', conditionsArr, oppID);
                        var IHOppObject = {};
                        IHOppObject.oppID = oppID;
                        IHOppObject.link_type = conditionsArr[0].link_type;
                        IHOppObject.psID = conditionsArr[0].for_ps;
                        IHOppObject.psName = conditionsArr[0].for_psName;
                        IHOppObject.psType = conditionsArr[0].for_psType;
                        IHOppObject.businessLogicType = conditionsArr[0].businessLogicType;

                        var assetDisplayName = _.findWhere(assetClasses, {assetName: conditionsArr[0].linked_asset});
                        console.log('Asset', assetDisplayName);
                        IHOppObject.assetName = assetDisplayName.displayName;
                        if (IHOppObject.assetName === 'Equity') {
                            var invTypeDisplayName1 = _.findWhere(equityTypes, {invType: conditionsArr[0].linked_investmentType});
                            console.log('Investment Type', invTypeDisplayName1);
                            IHOppObject.invType = invTypeDisplayName1.displayName;
                        }
                        else if (IHOppObject.assetName === 'Debt') {
                            var invTypeDisplayName2 = _.findWhere(debtTypes, {invType: conditionsArr[0].linked_investmentType});
                            console.log('Investment Type', invTypeDisplayName2);
                            IHOppObject.invType = invTypeDisplayName2.displayName;
                        }
                        // else if (IHOppObject.assetName === 'Gold') {
                        //     // var invTypeDisplayName3 =  _.findWhere(goldTypes, {invType: conditionsArr[0].linked_investmentType});
                        //     // console.log('Investment Type', invTypeDisplayName3);
                        //     // IHOppObject.invType = invTypeDisplayName3.displayName;
                        // }
                        // else if (IHOppObject.assetName === 'Cash') {
                        //     // var invTypeDisplayName4 =  _.findWhere(cashTypes, {invType: conditionsArr[0].linked_investmentType});
                        //     // console.log('Investment Type', invTypeDisplayName4);
                        //     // IHOppObject.invType = invTypeDisplayName4.displayName;
                        // }
                        // else if (IHOppObject.assetName === 'Real Estate') {
                        //     // var invTypeDisplayName4 =  _.findWhere(cashTypes, {invType: conditionsArr[0].linked_investmentType});
                        //     // console.log('Investment Type', invTypeDisplayName4);
                        //     // IHOppObject.invType = invTypeDisplayName4.displayName;
                        // }
                        else if (IHOppObject.assetName === 'Hybrid') {
                            var invTypeDisplayName5 = _.findWhere(hybridTypes, {invType: conditionsArr[0].linked_investmentType});
                            console.log('Investment Type', invTypeDisplayName5);
                            IHOppObject.invType = invTypeDisplayName5.displayName;
                        }
                        else if (IHOppObject.assetName === 'Others') {
                            var invTypeDisplayName6 = _.findWhere(othersTypes, {invType: conditionsArr[0].linked_investmentType});
                            console.log('Investment Type', invTypeDisplayName6);
                            IHOppObject.invType = invTypeDisplayName6.displayName;
                        }
                        if ((IHOppObject.invType) && (IHOppObject.invType === 'Mutual Fund' || IHOppObject.invType === 'Direct Equity' || IHOppObject.invType === 'Others' || IHOppObject.invType === 'Retiral Funds')) {
                            IHOppObject.invName = conditionsArr[0].linked_investmentName;
                        }
                        if ((IHOppObject.assetName === 'Equity' || IHOppObject.assetName === 'Debt' || IHOppObject.assetName === 'Hybrid') && IHOppObject.invType === 'Mutual Fund') {
                            var classificationType = conditionsArr[0].linked_investmentClassification.split("-", 2);
                            IHOppObject.invClassification = classificationType[classificationType.length - 1].trim();
                        }
                        IHOppObject.partnerID = conditionsArr[0].partner_id;
                        IHOppObject.createdOn = moment(conditionsArr[0].created_on, 'DD/MM/YYYY h:mm a').format('DD/MM/YYYY hh:mm');
                        IHOppObject.createdOnUnix = moment(IHOppObject.createdOn, "DD/MM/YYYY hh:mm").unix()
                        IHOppObject.conditions = _.map(conditionsArr, function (condObj) {
                            return _.omit(condObj, 'for_ps', 'for_psName', 'for_psType', 'linked_cf', 'linked_cfType', 'partner_id', 'opportunity_id', 'businessLogicType');
                        });
                        finalAllOpps.push(IHOppObject);
                    }
                    else{
                        console.log('Opp Object', conditionsArr, oppID);
                        var CFOppObject = {};
                        CFOppObject.oppID = oppID;
                        CFOppObject.link_type = conditionsArr[0].link_type;
                        CFOppObject.oppCount = count;
                        CFOppObject.psID = conditionsArr[0].for_ps;
                        CFOppObject.psName = conditionsArr[0].for_psName;
                        CFOppObject.psType = conditionsArr[0].for_psType;
                        CFOppObject.cfName = conditionsArr[0].linked_cf;
                        CFOppObject.cfType = conditionsArr[0].linked_cfType;
                        CFOppObject.partnerID = conditionsArr[0].partner_id;
                        CFOppObject.createdOn = moment(conditionsArr[0].created_on, 'DD/MM/YYYY h:mm a').format('DD/MM/YYYY hh:mm');
                        CFOppObject.createdOnUnix = moment(CFOppObject.createdOn, "DD/MM/YYYY hh:mm").unix()
                        CFOppObject.conditions = _.map(conditionsArr, function (condObj) {
                            return _.omit(condObj, 'for_ps', 'for_psName', 'for_psType', 'linked_cf', 'linked_cfType', 'partner_id', 'opportunity_id');
                        });

                        finalAllOpps.push(CFOppObject);
                    }
                    count -= 1;
                    $log.debug('Count>>>', count);
                });

                if(count === 0){
                    resolve(finalAllOpps);
                }
            });
        };

        var getAllLinkedClients = function () {
            return $http.post(baseUrl + 'opp/allLinkedClients').then(function (success) {
                console.log("Get all linked clients result", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getOpportunitiesForOppID = function (oppIDArr) {
            return $http.post(baseUrl + 'opp/opportunitiesFromIDArr', oppIDArr).then(function (success) {
                console.log("Get opportunitiesFromOppID result", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllLinkedCF = function () {
            return $http.post(baseUrl + 'opp/linkedCFWithBlankUsers').then(function (success) {
                console.log("Get getAllLinkedCF result", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllLinkedIH = function () {
            return $http.post(baseUrl + 'opp/linkedIHWithBlankUsers').then(function (success) {
                console.log("Get getAllLinkedIH result", success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getDropDownList = function (cfData) {
            return $http.post(baseUrl + 'opp/getDropDownList', cfData).then(function (success) {
                var listData = success.data;
                return listData;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var renameIHCondition = function (arr) {
            var arrLen = arr.length - 1;
            var deferred = $q.defer();
            _.each(arr, function (iHObj, index) {
                console.log('IH Object', iHObj, iHObj.condition);
                var assetDisplayName = _.findWhere(assetClasses, {assetName: iHObj.condition.linked_asset});
                console.log('Asset', assetDisplayName);
                iHObj.assetName = assetDisplayName.displayName;
                if (iHObj.assetName === 'Equity') {
                    var invTypeDisplayName1 = _.findWhere(equityTypes, {invType: iHObj.condition.linked_investmentType});
                    console.log('Investment Type', invTypeDisplayName1);
                    iHObj.invType = invTypeDisplayName1.displayName;
                }
                else if (iHObj.assetName === 'Debt') {
                    var invTypeDisplayName2 = _.findWhere(debtTypes, {invType: iHObj.condition.linked_investmentType});
                    console.log('Investment Type', invTypeDisplayName2);
                    iHObj.invType = invTypeDisplayName2.displayName;
                }
                else if (iHObj.assetName === 'Hybrid') {
                    var invTypeDisplayName5 = _.findWhere(hybridTypes, {invType: iHObj.condition.linked_investmentType});
                    console.log('Investment Type', invTypeDisplayName5);
                    iHObj.invType = invTypeDisplayName5.displayName;
                }
                else if (iHObj.assetName === 'Others') {
                    var invTypeDisplayName6 = _.findWhere(othersTypes, {invType: iHObj.condition.linked_investmentType});
                    console.log('Investment Type', invTypeDisplayName6);
                    iHObj.invType = invTypeDisplayName6.displayName;
                }
                if ((iHObj.invType) && (iHObj.invType === 'Mutual Fund' || iHObj.invType === 'Direct Equity' || iHObj.invType === 'Others' || iHObj.invType === 'Retiral Funds')) {
                    iHObj.invName = iHObj.condition.linked_investmentName;
                }
                if ((iHObj.assetName === 'Equity' || iHObj.assetName === 'Debt' || iHObj.assetName === 'Hybrid') && iHObj.invType === 'Mutual Fund') {
                    var classificationType = iHObj.condition.linked_investmentClassification.split("-", 2);
                    iHObj.invClassification = classificationType[classificationType.length - 1].trim();
                }


                if(arrLen === index){
                    deferred.resolve(arr);
                }
            });

            return deferred.promise;
        };

        var updateExcludeClient = function (excludeClData) {
            return $http.post(baseUrl + 'opp/updateExcludeClient', excludeClData).then(function (success) {
                var listData = success.data;
                return listData;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var getExceptionList = function (query) {
            return $http.post(baseUrl + 'opp/getOppOfClient', query).then(function (success) {
                var listData = success.data;
                return listData;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var removeUserFromExcludedList = function (removeClientQry) {
            return $http.post(baseUrl + 'opp/removeUserFromExcludedList', removeClientQry).then(function (success) {
                // var listData = success.data;
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCategoryList = function (proObj) {
            return $http.post(baseUrl + 'opp/getCategorylist', proObj).then(function (success) {
                // var listData = success.data;
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllProductCustomFields = function (query) {
            console.log('data from getAllFields service');
            return $http.post(baseUrl+'sales/getAllProductFields', query).then(function(success) {
                console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };







        return {
            newPS: newPS,
            getAllPS: getAllPS,
            checkName: checkName,
            updatePS: updatePS,
            deletePS: deletePS,
            createOpportunity: createOpportunity,
            getOpportunityDetails: getOpportunityDetails,
            addCondition: addCondition,
            getAllOpportunities: getAllOpportunities,
            deleteOpportunity: deleteOpportunity,
            deleteCondition: deleteCondition,
            updateCondition: updateCondition,
            getMFClassifications: getMFClassifications,
            getInvestmentNames: getInvestmentNames,
            getInvNamesForNonPF: getInvNamesForNonPF,
            getAllPSLevelData: getAllPSLevelData,
            updateOppValueCalLogic: updateOppValueCalLogic,
            groupConditionsIntoOpportunities: groupConditionsIntoOpportunities,
            getAllLinkedClients: getAllLinkedClients,
            getOpportunitiesForOppID: getOpportunitiesForOppID,
            getAllLinkedCF: getAllLinkedCF,
            getAllLinkedIH: getAllLinkedIH,
            renameIHCondition: renameIHCondition,
            getDropDownList : getDropDownList,
            updateExcludeClient : updateExcludeClient,
            getExceptionList: getExceptionList,
            removeUserFromExcludedList : removeUserFromExcludedList,
            getCategoryList :getCategoryList,
            getAllProductCustomFields :getAllProductCustomFields
        };
    });
