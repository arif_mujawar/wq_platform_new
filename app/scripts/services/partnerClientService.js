'use strict';

/**
 * @ngdoc service
 * @name wealthQuotientApp.partnerClientService
 * @description
 * # partnerClientService
 * Factory in the wealthQuotientApp.
 */
angular.module('wealthQuotientApp')
    .factory('partnerClientService', function ($http, myService, $cookies) {


            var getClientAccess = function (id, uid, uname) {

                var userSelected = {
                    'id': id,
                    'user_id': uid,
                    'username': uname
                }

                console.log("in partnerClientService!!!", userSelected);

                myService.setHeaders();

                return $http.post(baseUrl + 'user/partnerClientAccess', userSelected).then(function (success) {
                    console.log("partnerClientToken", success);
                    if(success.data.status){
                        // alert('cookie in making...');
                        window.sessionStorage.setItem('Authorization1', success.data.token);
                        $cookies.put('Authorization1', success.data.token);
                        var now = new Date();
                        var time = now.getTime();
                        time += 3600 * 1000;
                        now.setTime(time);
                        // console.log('the href is ',window.location.href)
                        if (window.origin.includes('wealthquotient')) {
                            document.cookie = 'Authorization1=' + success.data.token + '; expires=' + now.toUTCString() + ";domain=.wealthquotient.in;path=/;";
                        }
                        else {
                            document.cookie =
                                'Authorization1=' + success.data.token +
                                '; expires=' + now.toUTCString() +
                                '; path=/';
                        }
                        console.log('client token generated in partner client service', document.cookie);
                    }
                    return success;
                }).catch(function (error) {
                    console.log(error);
                    return error;
                });

            };

            var getAllClients = function () {
                myService.setHeaders();

                return $http.post(baseUrl + 'user/getAllClients').then(function (success) {
                    console.log("from backend---", success);
                    return success;
                }).catch(function (error) {
                    // console.log(error);
                    return error;
                });
            };

            var updateCustomerType = function (clientObj) {
                myService.setHeaders();

                return $http.post(baseUrl + 'user/updateCustomerType', clientObj).then(function (success) {
                    console.log("from backend---", success);
                    return success;
                }).catch(function (error) {
                    // console.log(error);
                    return error;
                });
            };

            var getClientWithUID = function (clientObj) {
                myService.setHeaders();

                return $http.post(baseUrl + 'user/getClientBasicData', clientObj).then(function (success) {
                    console.log("from backend---", success);
                    return success;
                }).catch(function (error) {
                    // console.log(error);
                    return error;
                });
            }



            return {
                getAccess: getClientAccess,
                getAllClients: getAllClients,
                updateCustomerType: updateCustomerType,
                getClientWithUID: getClientWithUID
            };

        }
    );