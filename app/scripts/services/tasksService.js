/**
 * Created by arif on 7/26/17.
 */

/**
 * Created by arif on 7/24/17.
 */


'use strict';

/**
 * @ngdoc service
 * @name wealthQuotientApp.registerService
 * @description
 * # registerService
 * Factory in the wealthQuotientApp.
 */
angular.module('wealthQuotientApp')
    .factory('tasksService', function ($http,$state,$cookies,Upload) {
        var userData = '';

        var createTasksTemplate = function (data) {
            return $http.post(baseUrl+'tasks/createTemplate', data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var crmUpdateTemplate = function (data) {
            return $http.post(baseUrl+'tasks/crmUpdateTemplate', data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var crmAddTempTaskUpdate = function (data) {
            return $http.post(baseUrl+'tasks/AddMoreFreshTasks', data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var AddMoreTemplateTasks = function (data) {
            return $http.post(baseUrl+'tasks/AddMoreTemplateTasks', data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var getsubuserName = function (data) {
            return $http.post(baseUrl+'tasks/getsubuserName', data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };



        var getTasksTemplate = function (query) {
            return $http.post(baseUrl+'tasks/getCrmTasksTemplates', query).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getTemplatesByName = function (data) {
            return $http.post(baseUrl+'tasks/getTemplatesByName', data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllTemplatesTasksByName = function (data) {
            return $http.post(baseUrl+'tasks/getTemplatesAllTasksByName', data).then(function(success) {
               // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllClientsDetails = function (data) {
            return $http.post(baseUrl+'tasks/getAllClientsDetail', data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllClientsDetailById = function (data) {
            return $http.post(baseUrl+'tasks/getAllClientsDetailById', data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAssigneeNames = function (data) {
            return $http.post(baseUrl+'tasks/getAssigneeNames', data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var craeteCrmTask = function (query) {
            return $http.post(baseUrl+'tasks/createCrmTasks', query).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var craeteCrmTaskIndividualTasks = function (query) {
            return $http.post(baseUrl+'tasks/createCrmIndivudualTasks', query).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var crmUpdateFreshTasks = function (query) {
            return $http.post(baseUrl+'tasks/crmUpdateFreshTasks', query).then(function(success) {
               // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var crmUpdateTasksTemplate = function (query) {
            return $http.post(baseUrl+'tasks/crmUpdateTasksTemplate', query).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getAllCrmTasks = function (query) {
            return $http.post(baseUrl+'tasks/getCrmAllTasksCreated',query).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var crmUpdateRemoveTempTask = function (query) {
            return $http.post(baseUrl+'tasks/crmUpdateRemoveTempTask',query).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var crmremoveTempSubtask = function (query) {
            return $http.post(baseUrl+'tasks/crmremoveTempSubtask',query).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCrmAllTasksBySearch = function (query) {
            return $http.post(baseUrl+'tasks/getCrmAllTasksBySearch',query).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCrmAllTasksByAssignee = function (query) {
            return $http.post(baseUrl+'tasks/getCrmAllTasksByAssignee',query).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCrmAllTasksByDate = function (query) {
            return $http.post(baseUrl+'tasks/getCrmAllTasksByDate',query).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCrmAllOtherTasks = function () {
            return $http.post(baseUrl+'tasks/getCrmAllOtherTasks',{}).then(function(success) {
               // console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCrmCopmlatedOtherTasks = function () {
            return $http.post(baseUrl+'tasks/getCrmCompletedOtherTasks',{}).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var crmUpdateOtherTasks = function (data) {
            return $http.post(baseUrl+'tasks/crmUpdateOtherTasks',data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var crmUpdateTemplateTasks = function (data) {
            return $http.post(baseUrl+'tasks/crmUpdateTemplateTasks',data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var crmUpdateComments = function (data) {
            return $http.post(baseUrl+'tasks/crmUpdateCommentForTasks',data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCrmCompetedTempTasks = function () {
            return $http.post(baseUrl+'tasks/getCrmAllCompletedTempTasks',{}).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCrmAllsnoozedTasks = function () {
            return $http.post(baseUrl+'tasks/getCrmAllsnoozedTasks',{}).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var getCrmTemplatesForDownload = function () {
            return $http.post(baseUrl+'tasks/getCrmTemplatesForDownload',{}).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var crmUpdateNoteForTemp = function (query) {
            return $http.post(baseUrl+'tasks/crmUpdateNoteForTemp',query).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var crmDeleteTemplate = function (query) {
            return $http.post(baseUrl+'tasks/crmDeleteTemplate',query).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var crmUpdateTemplateTasksForAllIds = function (data) {
            return $http.post(baseUrl+'tasks/crmUpdateTemplateTasksStatus',data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var crmSnoozeTask = function (data) {
            return $http.post(baseUrl+'tasks/crmSnoozeTask',data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var crmRemoveTask = function (data) {
            return $http.post(baseUrl+'tasks/crmRemoveTask',data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var crmUpdateTaskContent = function (data) {
            return $http.post(baseUrl+'tasks/crmUpdateSubTaskSingle',data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var AddTempTaskUpdate = function (data) {
            return $http.post(baseUrl+'tasks/AddTempTaskUpdate',data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };
        var checkCrmTasksByName = function (data) {
            return $http.post(baseUrl+'tasks/checkCrmTasksByName',data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };

        var checkCrmTasksByNameSuggest = function (data) {
            return $http.post(baseUrl+'tasks/checkCrmTasksByNameSuggest',data).then(function(success) {
                //console.log(success);
                return success;
            }).catch(function (error) {
                console.log(error);
                return error;
            });
        };



        var templateUploadFile = function (file) {
            console.log('templateUploadFile service----------------',file);
            return Upload.upload({

                url: `${baseUrl}tasks/templatesUploadFile`,
                data: {file: file}
            });
        };

        var TaskUploadFile = function (file) {
            console.log('TaskUploadFile service----------------');
            return Upload.upload({
                url: `${baseUrl}tasks/taskUploadFile`,
                data: {
                    file: file
                }
            })
        };


        return{
            createTasksTemplate: createTasksTemplate,
            getTasksTemplate: getTasksTemplate,
            getTemplatesByName: getTemplatesByName,
            getAllTemplatesTasksByName: getAllTemplatesTasksByName,
            getAllClientsDetails: getAllClientsDetails,
            craeteCrmTask: craeteCrmTask,
            getAllCrmTasks: getAllCrmTasks,
            getCrmAllOtherTasks: getCrmAllOtherTasks,
            getCrmCopmlatedOtherTasks: getCrmCopmlatedOtherTasks,
            crmUpdateOtherTasks: crmUpdateOtherTasks,
            crmUpdateTemplateTasks: crmUpdateTemplateTasks,
            getCrmCompetedTempTasks: getCrmCompetedTempTasks,
            craeteCrmTaskIndividualTasks: craeteCrmTaskIndividualTasks,
            crmDeleteTemplate: crmDeleteTemplate,
            crmUpdateTemplateTasksForAllIds: crmUpdateTemplateTasksForAllIds,
            crmUpdateTaskContent: crmUpdateTaskContent,
            crmUpdateComments: crmUpdateComments,
            checkCrmTasksByName:checkCrmTasksByName,
            crmUpdateTemplate:crmUpdateTemplate,
            crmAddTempTaskUpdate:crmAddTempTaskUpdate,
            crmUpdateFreshTasks:crmUpdateFreshTasks,
            crmUpdateTasksTemplate:crmUpdateTasksTemplate,
            AddMoreTemplateTasks:AddMoreTemplateTasks,
            crmUpdateRemoveTempTask:crmUpdateRemoveTempTask,
            crmUpdateNoteForTemp:crmUpdateNoteForTemp,
            crmSnoozeTask:crmSnoozeTask,
            getCrmAllsnoozedTasks:getCrmAllsnoozedTasks,
            AddTempTaskUpdate:AddTempTaskUpdate,
            getCrmAllTasksBySearch:getCrmAllTasksBySearch,
            getAllClientsDetailById:getAllClientsDetailById,
            getAssigneeNames:getAssigneeNames,
            getCrmAllTasksByAssignee:getCrmAllTasksByAssignee,
            getCrmAllTasksByDate:getCrmAllTasksByDate,
            crmRemoveTask:crmRemoveTask,
            crmremoveTempSubtask:crmremoveTempSubtask,
            templateUploadFile:templateUploadFile,
            getCrmTemplatesForDownload:getCrmTemplatesForDownload,
            getsubuserName:getsubuserName,
            checkCrmTasksByNameSuggest:checkCrmTasksByNameSuggest,
           // TaskUploadFile:TaskUploadFile

        };
    })
    .factory('mySharedService', function($rootScope) {
        var sharedService = {};
        sharedService.sharedmessage  = '';
        sharedService.prepForPublish = function(msg) {
            this.sharedmessage  = msg;
            console.log('this.sharedmessage',JSON.stringify(this.sharedmessage));
            this.publishItem();
        };

        sharedService.publishItem = function() {
            $rootScope.$broadcast('handlePublish');
        };
        return sharedService;
    })

    .factory('colorClick', function($rootScope) {
        var sharedService1 = {};
        sharedService1.sharedmessage  = '';
        sharedService1.prepForPublish = function(msg) {
            this.sharedmessage  = msg;
            console.log('this.sharedmessage',JSON.stringify(this.sharedmessage));
            this.publishItem();
        };

        sharedService1.publishItem = function() {
            $rootScope.$broadcast('handlePublish');
        };
        return sharedService1;
    })

    .factory('colorClicksecond', function($rootScope) {
        var sharedService1 = {};
        sharedService1.sharedmessage  = '';
        sharedService1.prepForPublish = function(msg) {
            this.sharedmessage  = msg;
            console.log('this.sharedmessage',JSON.stringify(this.sharedmessage));
            this.publishItem();
        };

        sharedService1.publishItem = function() {
            $rootScope.$broadcast('handlePublishsecond');
        };
        return sharedService1;
    })

    .factory('colorClickThird', function($rootScope) {
        var sharedService1 = {};
        sharedService1.sharedmessage  = '';
        sharedService1.prepForPublish = function(msg) {
            this.sharedmessage  = msg;
            console.log('this.sharedmessage',JSON.stringify(this.sharedmessage));
            this.publishItem();
        };

        sharedService1.publishItem = function() {
            $rootScope.$broadcast('handlePublishThird');
        };
        return sharedService1;
    })

    .factory('colorClickFourth', function($rootScope) {
        var sharedService1 = {};
        sharedService1.sharedmessage  = '';
        sharedService1.prepForPublish = function(msg) {
            this.sharedmessage  = msg;
            console.log('this.sharedmessage',JSON.stringify(this.sharedmessage));
            this.publishItem();
        };

        sharedService1.publishItem = function() {
            $rootScope.$broadcast('handlePublishFourth');
        };
        return sharedService1;
    })

    .factory('colorClickFive', function($rootScope) {
        var sharedService1 = {};
        sharedService1.sharedmessage  = '';
        sharedService1.prepForPublish = function(msg) {
            this.sharedmessage  = msg;
            console.log('this.sharedmessage',JSON.stringify(this.sharedmessage));
            this.publishItem();
        };

        sharedService1.publishItem = function() {
            $rootScope.$broadcast('handlePublishFive');
        };
        return sharedService1;
    })



    .factory('serachType', function($rootScope,$state,Store) {
        console.log('serachType calling');
        var searchtype = {};
        searchtype.searchmessage  = '';
        searchtype.prepForPublish = function(msg) {
            console.log('calling search factory',msg);
            this.searchmessage  = msg;
            console.log('this.searchmessage 11111111111',JSON.stringify(this.searchmessage));
            if(this.searchmessage.type === 'customer'){
                Store.customerview = true;
                Store.assingeeview = false;
                Store.dateview = false;
            }else if(this.searchmessage.type === 'assignee'){
                Store.assingeeview = true;
                Store.customerview = false;
                Store.dateview = false;
            }else{
                Store.dateview = true;
                Store.assingeeview = false;
                Store.customerview = false;
            }

            this.publishItemSearch();
        };

        searchtype.publishItemSearch = function() {
            //$rootScope.$broadcast('searchType');
            $state.go('searchTasks');

        };
        return searchtype;
    });


