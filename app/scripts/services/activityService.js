angular.module('wealthQuotientApp')
	.service('activityService', function($http){

		

		this.getUserActivity = function(data){

			return $http.post(baseUrl+'activityLog/getUserActivity',data);

		} // gets all queries specific to loggged in user

        this.searchDate = function (data){
        	return $http.post(baseUrl+'activityLog/searchActivityByDate',data)
        }

        this.searchDesc = function (data){
        	return $http.post(baseUrl+'activityLog/searchActivityByDesc',data)
        }

        this.searchBoth = function (data){
        	return $http.post(baseUrl+'activityLog/searchActivityByBoth',data)
        }

		

	});