/**
 * calendar event services
 */

angular.module('wealthQuotientApp')
    .factory('calendarService', function ($http, $q) {
        var getMonthlyBirthday = function (monthDate) {
            var q = $q.defer();
            return $http.get(baseUrl + 'calendar/monthlyEvents', {
                params: {
                    month: monthDate
                }
            })
                .then(function (result) {
                    console.log("%c", 'background: #222; color: #bada55', 'resultBirthday', result);
                    return result.data[0];
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        }

        var getTaskServiceRequest = function (monthDate) {
            var q = $q.defer();
            return $http.get(baseUrl + 'calendar/monthlyDueTasks', {
                params: {
                    month: monthDate
                }
            })
                .then(function (result) {
                    console.log("%c", 'background: #222; color: #bada55', 'resultServiceRequest', result);
                    return result.data[0];
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        }

        var getAllUsers = function () {
            var q = $q.defer();
            return $http.get(baseUrl + 'calendar/getUsers')
                .then(function (result) {
                    return result.data[0];
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        }

        var getUsedEventNames = function () {
            var q = $q.defer();
            return $http.get(baseUrl + 'calendar/usedEvents')
                .then(function (result) {
                    return result.data[0];
                })
                .catch(function (error) {
                    return q.reject(error);
                })
        }

        var createAdditionalEvents = function (data) {
            var q = $q.defer();
            return $http.post(baseUrl + 'calendar/newEvents', data)
                .then(function (result) {
                    return result.data[0];
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        }

        var getAdditionalEvents = function (month) {
            var q = $q.defer();
            return $http.get(baseUrl + 'calendar/newEvents', {
                params: {
                    month: month
                }
            })
                .then(function (result) {
                    return result.data[0];
                })
                .catch(function (error) {
                    return q.reject(error);
                })

        }

        var addReminder = function (data) {
            var q = $q.defer();
            return $http.post(baseUrl + 'calendar/addReminder', data)
                .then(function (result) {
                    return result.data;
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        }

        var getReminder = function (date) {
            var q = $q.defer();
            return $http.get(baseUrl + 'calendar/reminders', {
                params: {
                    date: date
                }
            })
                .then(function (result) {
                    return result.data[0];
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        }

        var rescheduleReminder = function (data) {
            var q = $q.defer();
            return $http.post(baseUrl + 'calendar/reschedule', data)
                .then(function (result) {
                    console.log(result)
                    return result.data[0];
                })
                .catch(function (error) {
                    return q.reject(error);
                })
        }

        var getGoalsMaturity = function (year) {
            var q = $q.defer();
            return $http.get(baseUrl + 'calendar/goalsReminder', {
                params: {
                    year: year
                }
            })
                .then(function (result) {
                    return result.data[0]
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        }

        var createAlertPreference = function (data) {
            var q = $q.defer();
            return $http.post(baseUrl + 'calendar/alertPreference', data)
                .then(function (result) {
                    return result.data[0];
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        }

        var getAlertPreference = function (data) {
            var q = $q.defer();
            return $http.get(baseUrl + 'calendar/alertPreference', {
                params: data
            })
                .then(function (result) {
                    return result.data[0];
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        }

        var updateAlertPreference = function (data) {
            var q = $q.defer();
            return $http.post(baseUrl + 'calendar/alertPreference/' + data.id, data)
                .then(function (result) {
                    return result.data[0];
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        }

        var updateAdditionalEvents = function (data) {
            var q = $q.defer();
            return $http.post(baseUrl + 'calendar/newEvents/' + data.id, data)
                .then(function (result) {
                    return result.data[0];
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        }

        var getInvestmentData = function (date) {
            var q = $q.defer();
            return $http.get(baseUrl + 'calendar/investmentData', {
                params: {
                    date: date
                }
            })
                .then(function (result) {
                    console.log('dude iam here', result)
                    return result.data;
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        }

        var deleteAdditionalEvent = function (id, parent_id, delete_all) {
            var q = $q.defer();
            return $http.delete(baseUrl + 'calendar/newEvents/' + id, {
                params: {
                    parent_id: parent_id,
                    delete_all: delete_all
                }
            })
                .then(function (result) {
                    return result.data;
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        };

        var deleteReminder = function (id, parent_id, delAll) {
            var q = $q.defer();
            return $http.delete(baseUrl + 'calendar/reminders/' + id, {
                params: {
                    parent_id: parent_id,
                    recurring: delAll
                }
            })
                .then(function (result) {
                    return result.data;
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        };

        var patchAddittionalEvents = function (id, date) {
            var q = $q.defer();
            return $http.patch(baseUrl + 'calendar/newEvents/' + id, {
                eventDate: date
            })
                .then(function (result) {
                    return result.data;
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        };

        var patchReminderEvents = function (id, date, time) {
            var q = $q.defer();
            return $http.patch(baseUrl + 'calendar/reminders/' + id, {
                reminderDate: date,
                reminderTime: time
            })
                .then(function (result) {
                    return result.data;
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        };

        var createMeeting = function (data) {
            var q = $q.defer();
            return $http.post(baseUrl + 'calendar/meeting', data)
                .then(function (result) {
                    return result.data;
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        };

        var getMeetings = function (date) {
            var q = $q.defer();
            return $http.get(baseUrl + 'calendar/meeting', {
                params: {
                    date: date
                }
            })
                .then(function (result) {
                    return result.data;
                })
                .catch(function (error) {
                    return q.reject(error);
                });
        };

        var deleteMeeting = function (id) {
            return $http.delete(baseUrl + 'calendar/meeting/' + id);
        };

        var updateReminderTime = function (reminder) {
            return $http.post(baseUrl + 'calendar/rescheduleTime', reminder);
        };

        var getCalendarSettings = function () {
            return $http.get(baseUrl + 'calendar/calendarSettings');
        };

        var updateSettings = function (name, flag) {
            return $http.put(baseUrl + 'calendar/calendarSettings', {
                eventName: name,
                eventType: flag
            });
        };

        var createSettings = function (name, flag) {
            return $http.post(baseUrl + 'calendar/calendarSettings', {
                eventName: name,
                eventType: flag
            });
        };

        var savePushToken = function (token) {
            return $http.post(baseUrl + 'calendar/savePushToken', { token })
        }

        var googleCaledarEvents = function(date) {
            return $http.get(baseUrl + 'calendar/getGoogleCaledarEvents', { params: {date: date} });
        }
        
        var gmailAuth = function(){
            return $http.get(baseUrl + 'contactManagement/getGmail');
        }

        return {
            getMonthlyBirthday: getMonthlyBirthday,
            getTaskServiceRequest: getTaskServiceRequest,
            getAllUsers: getAllUsers,
            getUsedEventNames: getUsedEventNames,
            createAdditionalEvents: createAdditionalEvents,
            getAdditionalEvents: getAdditionalEvents,
            addReminder: addReminder,
            getReminder: getReminder,
            rescheduleReminder: rescheduleReminder,
            getGoalsMaturity: getGoalsMaturity,
            createAlertPreference: createAlertPreference,
            getAlertPreference: getAlertPreference,
            updateAlertPreference: updateAlertPreference,
            updateAdditionalEvents: updateAdditionalEvents,
            getInvestmentData: getInvestmentData,
            deleteAdditionalEvent: deleteAdditionalEvent,
            deleteReminder: deleteReminder,
            patchAddittionalEvents: patchAddittionalEvents,
            patchReminderEvents: patchReminderEvents,
            createMeeting: createMeeting,
            getMeetings: getMeetings,
            deleteMeeting: deleteMeeting,
            updateReminderTime: updateReminderTime,
            getCalendarSettings: getCalendarSettings,
            updateSettings: updateSettings,
            createSettings: createSettings,
            savePushToken: savePushToken,
            googleCaledarEvents: googleCaledarEvents,
            gmailAuth: gmailAuth
        };

    });