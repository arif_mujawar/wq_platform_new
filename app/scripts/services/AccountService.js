

angular.module('wealthQuotientApp')
    .factory('accountService', function ($http, $q) {
        var getuserIds = function () {
            return $http.post(baseUrl + 'mc/getAllCustomers').then(function (resp) {
                return resp;
            }).catch(function (error) {
                return error;
            });
        }

        var updateSubscription = function (data) {
            console.log('data', data)
            return $http.post(baseUrl + 'user/toggleSubscription', data).then(function (resp) {
                Materialize.toast('Updated Successfully', 2000, 'rounded');
                return resp;
            }).catch(function (error) {
                return error;
            });
        }

        var getPaymentDetails = function () {
            return $http.get(baseUrl + 'billing/paymentDetails').then(function (result) {
                return Object.assign({}, ...result.data[0], ...result.data[1]);
            }).catch(function (error) {
                return error;
            });
        }

        var getPaymentDetailsById = function (payment_id, payment_request_id) {
            return $http.get(baseUrl + 'billing/paymentDetails/' + payment_id + '/' + payment_request_id)
                .then(function (result) {
                    return result.data[0][0];
                }).catch(function (error) {
                    return error;
                });
        }

        var requestPayment = function (data) {
            return $http.post(baseUrl + 'payment/instaPaymentRequest', data).then(function (result) {
                return result.data;
            }, function (error) {
                return $q.reject(error);
            });
        }

        var getWalletTransactions = function () {
            return $http.get(baseUrl + 'billing/wallet');
        }

        var getBalance = function () {
            return $http.get(baseUrl + 'billing/walletBalance');
        }

        return {
            getuserIds: getuserIds,
            updateSubscription: updateSubscription,
            getPaymentDetails: getPaymentDetails,
            requestPayment: requestPayment,
            getPaymentDetailsById: getPaymentDetailsById,
            getWalletTransactions: getWalletTransactions,
            getBalance: getBalance
        };

    })