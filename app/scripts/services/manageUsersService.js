angular.module('wealthQuotientApp')
	.service('manageUsersService', function($http){

		var SubUserData = {};

		this.store = function(data){

			SubUserData = data;
		}

		this.getStore = function(){

			return SubUserData;
		}
	

		this.addSubUsers = function(data){

			return $http.post(baseUrl+'manageUsers/registerSubUsers',data)

		} // gets all queries specific to loggged in user

		this.updatePassword = function(token,data){

			return $http.post(baseUrl+'manageUsers/updatePass/'+token,data)

		} // gets all queries specific to loggged in user

		this.uniqueUsername = function (username) {
	      return $http.post(baseUrl+'manageUsers/validateSubUsername', username).then(function(success) {
	        console.log(success);
	        return success;
	      }).catch(function (error) {
	        console.log(error);
	        return error;
	      });
	    };

	    this.getAllSubUsers= function(){

			return $http.post(baseUrl+'manageUsers/getSubUsers');

		} // gets all queries specific to loggged in user

		this.getUserActivity = function(data){

			return $http.post(baseUrl+'manageUsers/getUserActivity',data);

		} // gets all queries specific to loggged in user

		this.deleteSubUsers= function(data){

			return $http.post(baseUrl+'manageUsers/deleteSubUser',data);

		} // gets all queries specific to loggged in user

		this.editSubUsers= function(id, data){

			return $http.post(baseUrl+'manageUsers/editSubUser/'+id, data);

		} // gets all queries specific to loggged in user

		this.checkToken = function (token) {
            return $http.get(baseUrl+'manageUsers/check/'+token)
                
        }

        this.searchDate = function (data){
        	return $http.post(baseUrl+'manageUsers/searchActivityByDate',data)
        }

        this.searchDesc = function (data){
        	return $http.post(baseUrl+'manageUsers/searchActivityByDesc',data)
        }

        this.searchBoth = function (data){
        	return $http.post(baseUrl+'manageUsers/searchActivityByBoth',data)
        }

		

	});