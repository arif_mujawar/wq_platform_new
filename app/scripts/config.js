/**
 * INSPINIA - Responsive Admin Theme
 *
 * Inspinia theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written stat for all view in theme.
 *
 */
function config($stateProvider, $urlRouterProvider, $httpProvider,$qProvider) {
    //$qProvider.errorOnUnhandledRejections(false);
    $stateProvider
        .state('dashboard', {
            name: 'dashboard',
            url: "/dashboard",
            templateUrl: "views/dashboard/dashboard.html",
            controller: 'dashboardCtrl',
            title: 'Dashboard'
        })
        .state('homepage', {
            url: "/home",
            templateUrl: "views/homeScreen/homeScreen.html",
            controller: 'homeCtrl',
            title: 'Home'
        })

        .state('myclients', {
            url: "/myclients",
            templateUrl: "views/myClients/myClients.html",
            controller: 'myClientsCtrl',
            title: 'clients'
        })


        .state('tasks', {
            name: 'tasks',
            url: "/tasks",
            views: {
                '': {
                    controller: 'taskCtrl',
                    templateUrl: 'views/crm/templates/mainTask.html'
                },
                'taskcrm@tasks': {
                    templateUrl: 'views/crm/templates/task.html'
                },
                'duecrmtask@tasks': {
                    templateUrl: 'views/crm/templates/task.due.html'
                },
                'alltemplate@tasks': {
                    templateUrl: 'views/crm/templates/task.temp.html'
                },
                'completedtasks@tasks': {
                    controller: 'completedTaskCtrl',
                    templateUrl: 'views/crm/templates/tasks.completad.html'
                },
                'snoozedtasks@tasks': {
                    controller: 'snoozedTaskCtrl',
                    templateUrl: 'views/crm/templates/snoozedtask.html'
                }
            },
            title: 'Task'
        })


        .state('searchTasks', {
            name: 'searchTasks',
            url: '/searchTasks',
            templateUrl: 'views/crm/templates/searchtask.html',
            controller: 'tasksSearchCtrl',
            params: {
                myClientsObj: null
            },
            title: 'searchTasks',
            // resolve: {
            //     app: ['$q','$rootScope','$cookies',function ($q,$rootScope,$cookies) {
            //         var defer = $q.defer();
            //
            //         if($cookies.get('Role')=='partner'){
            //             defer.resolve();
            //         }
            //         else if($cookies.get('Role')=='sub_user'){
            //
            //             $rootScope.getSubUserDetails()
            //                 .then(function(){
            //                     if($rootScope.routeInfo && $rootScope.routeInfo.crm != 'notVisible'){
            //                         defer.resolve();
            //                     }
            //                     else{
            //                         $rootScope.errorToast('You are not authorized to view this page');
            //                         defer.reject();
            //                     }
            //                 })
            //         }
            //         return defer.promise;
            //     }]
            // }
        })
    $urlRouterProvider.otherwise("/home");


}

angular
    .module('wealthQuotientApp')
    .config(config)
    .run(function ($rootScope, $state) {
        $rootScope.$state = $state;
        // console.log('$rootScope.$state -----------------',$rootScope.$state )
    });